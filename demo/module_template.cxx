/******************************************************************
 * Template Module
 * Author: David Gallacher
 * November 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include <iostream>
#include <fstream>
#include <cstdlib>


#include "json.hpp"
using json = nlohmann::json;

class TemplateModuleFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};

class TemplateModule : public TARunObject
{
public:
   TemplateModuleFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors

   //Add public data members here

   int fIntegerVariable; //We read this from the .json file

private:

public:
   TemplateModule(TARunInfo* runinfo, TemplateModuleFlags* f): TARunObject(runinfo),
							     fFlags(f),
							     fCounter(0), fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"TemplateModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="TemplateModule";
#endif

#ifdef HAVE_CONFIG
     fFlags->fConfigName=std::getenv("MERCI_CONFIG")+fFlags->fConfigName; // Read the path from the environment variable
     std::cout << "Have Config on, using config file = " << fFlags->fConfigName.c_str() << std::endl;
#endif

    //Parse the JSON File
    std::ifstream fin(fFlags->fConfigName.c_str());
    json settings;
    fin>>settings;
    if(fFlags->fVerbose)
       std::cout<<"TemplateModule Json parsing success!"<<std::endl;
    fin.close();
    //Read the data from f
    fIntegerVariable = settings["Template"]["IntegerVariable"].get<int>();

   }

   //Required functions
   /// Begin run method
   void BeginRun(TARunInfo* runinfo){}

   /// End run method
   void EndRun(TARunInfo* runinfo){}

   /// Analyze flow event method
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow){ return flow; }


};


class TemplateModuleFactory : public TAFactory
{

public:
   TemplateModuleFlags fFlags;

public:

   void Init(const std::vector<std::string> &args)
   {
      printf("TemplateModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++){
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
             if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
	   }

   }

   void Finish()
   {
      printf("TemplateModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("TemplateModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new TemplateModule(runinfo, &fFlags);
   }

};

static TARegister tar(new TemplateModuleFactory);
