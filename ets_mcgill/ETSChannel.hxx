/// Raw SiPM waveform class
/// Holds ROOT output of midas2root in event branch DS
/// Author: David Gallacher

#ifndef ETS_CHANNEL
#define ETS_CHANNEL

#include "TObject.h"
#include "TString.h"
#include "TTree.h"
#include "TBranch.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>


class ETSChannel : public TObject {
private:
  // Private data members
  Int_t               type;            /// Type of Channel (DT5730 ==0 ,VX2740==1, etc)
  Int_t               sipmID;          /// What is the ID of the sipm that made this ETSChannel
  std::vector<Double_t>    aV;         /// Vector of adc in waveform

  Double_t            t0_chan;         /// Time of first pulse in this channel
  Int_t               nPulses;         /// Number of pulses found in this channel
  Double_t            baseline;        /// Average baseline
  Double_t            baseline_rms;    /// RMS of the baseline
  Double_t            nPE;             /// Number of charge based PE in this channel
  Double_t            aPE;             /// Number of height based PE in this channel

  Double_t            waveformIntegral;/// Naiive summation of full waveform after baseline correction

public:
   ETSChannel();
   ~ETSChannel();
   ETSChannel(const ETSChannel &rhs) : TObject() { Init(); CopyObj(rhs); }
   ETSChannel &operator=(const ETSChannel &rhs) { CopyObj(rhs); return *this; }

   void       Init();
   void       Clear(Option_t *option = "");
   void       CopyObj(const ETSChannel &rhs);

   // Public member functions

   ///Convert Midas data into a ETSChannel object
   void       ConvertDT5730(const std::vector<Double_t> *vADC,int id);
   void       ConvertVX2740(const std::vector<Double_t> *vADC,int id);

   ///Return the vector of voltages in this ETSChannel
   std::vector<Double_t>     *GetADCs(){return &aV;}
   ///Get the Type of this ETSChannel (type 0==DT5730, etc)
   Int_t      GetType(){return type;}
   ///Set the unique ID of this SiPM
   void       SetID(Int_t _sipmID){ sipmID= _sipmID;}
   ///Get the unique ID of this SiPM
   Int_t      GetID(){return sipmID;}
   ///Set t0 of this channel, the leading edge time of the first pulse in the waveform
   void       SetT0_Channel(Double_t t0){t0_chan=t0;}
   /// Get the t0 of this channel, the leading edge time of the first pulse in the waveform
   Double_t   GetT0_Channel(){return t0_chan;}
   /// Get the total charge-based PE estimate for this channel 
   Double_t   GetNPE(){return nPE;}
   /// Get the total amplitude-based PE estimate for this channel 
   Double_t   GetAPE(){return aPE;}
   /// Get the baseline value before subtraction for this channel 
   Double_t   GetBaseline(){return baseline;}
   /// Get the baseline RMS for this channel 
   Double_t   GetBaselineRMS(){return baseline_rms;}
   /// Get the naiive integral of the waveform
   Double_t   GetWaveformIntegral(){return waveformIntegral;}
 

   /// Set the total charge-based PE estimate for this channel 
   void       SetNPE(Double_t _nPE){nPE =_nPE;}
   /// Set the total amplitude-based PE estimate for this channel 
   void       SetAPE(Double_t _aPE){aPE =_aPE;}
   /// Set the baseline value before subtraction for this channel 
   void       SetBaseline(Double_t _baseline){baseline = _baseline;}
   /// Set the baseline RMS value for this channel 
   void       SetBaselineRMS(Double_t _baseline_rms){baseline_rms = _baseline_rms;}
   /// Set the naiive waveform integral
   void       SetWaveformIntegral(Double_t _waveformIntegral){waveformIntegral = _waveformIntegral;}

   /// Remove the vADC array (Used before writing root files in TreeWriter)
   void       ClearWaveform(Option_t *option = "");


   struct PulseFinderResult{
     std::vector<Double_t> vPulseLeadingTime;   /// Leading edge time (t0) of pulses (ns)
     std::vector<Double_t> vPulsePeakTime;      /// Peak time of pulses (ns)
     std::vector<Double_t> vPulseWidth;         /// Width of pulses (ns)
     std::vector<Double_t> vPulseHeight;        /// Amplitude of pulses (ADC)
     std::vector<Double_t> vPulseCharge;        /// Charge of Pulses (ADC*ns)
   }pulses;                                   /// Container for pulse finder results;

   void       SetPulseFinderResult(PulseFinderResult p){pulses = p;}
   PulseFinderResult       GetPulseFinderResult(){ return pulses; }
   //Add more access methods
   void       SetNPulses(int np){nPulses=np;}

   Int_t          GetNPulses(){return nPulses;}
   Double_t       GetPulseCharge(int i){
     if(i >= nPulses) return -99.0;
     return pulses.vPulseCharge.at(i);
   }
   Double_t       GetPulseLeadingTime(int i){
     if(i >= nPulses) return -99.0;
     return pulses.vPulseLeadingTime.at(i);
   }
   Double_t       GetPulseHeight(int i){
     if(i >= nPulses) return -99.0;
     return pulses.vPulseHeight.at(i);
   }
   Double_t       GetPulsePeakTime(int i){
     if(i >= nPulses) return -99.0;
     return pulses.vPulsePeakTime.at(i);
   }
   Double_t       GetPulseWidth(int i){
     if(i >= nPulses) return -99.0;
     return pulses.vPulseWidth.at(i);
   }

   ClassDef(ETSChannel,4)  //Event structure
};

#endif
