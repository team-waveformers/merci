#ifndef ETS_DS
#define ETS_DS

//Custom headers
#include "ETSChannel.hxx"

//ROOT headers
#include "TObject.h"
#include "TTimeStamp.h"
#include <iostream>
using namespace std;

class ETSDS : public TObject {
private:

   //MIDAS Header information
   ///Run number        (midas run)
   Int_t         run;
   ///Sub-Run number    (midas sub-run)
   Int_t         subRun;
   ///sequence number   (midas subrun)
   Int_t         seqNum;
   ///Trigger type      (midas trigger mask)
   Int_t         trigID;
   ///Event ID          (midas subrun event index)
   Int_t         evID;
   ///Event time        (midas subrun event time from 125MHz clock) in unix time (s)
   Double_t      evTime;
   ///Global trigger ID (midas serial number)
   Int_t         gtID;
   ///trigger timestamp (midas time stamp)
   TTimeStamp    timeStamp;
   ///Start of the run time
   TTimeStamp    fRunStartTime;

   ///Trigger time stamp from board(8 ns resolution thus a range of 625 h, i.e., 8 ns*(2^48-1).)
   double      triggerTime;

   ///Time from start of run to start of waveform in s
   double      runTime; 

   ///Cut ID - event-level, can be used in the future to identify bad events from good
   UInt_t        eventCutID;
   ///Number of hit SiPMs in this event
   Int_t         nHit;
   ///Total number of channels in this event
   Int_t         nTot;

   Double_t      nPE;/// Total charge in event

   Double_t      aPE;/// Total amplitude based PE in event

   //WF data from each channel
   std::vector<ETSChannel*> vChannel;

public:
   ETSDS();
   ~ETSDS();
   ETSDS(const ETSDS &rhs) : TObject() { Init(); CopyObj(rhs); }
   ETSDS &operator=(const ETSDS &rhs) { CopyObj(rhs); return *this; }

   void     Init();
   void     Clear(Option_t *option = "");
   void     CopyObj(const ETSDS &rhs);

    // MIDAS header and event summary related information
   /// Set the runID for this event
   void     SetRun(Int_t i) { run= i; }
   /// Set the subrunID for this event
   void     SetSubRun(Int_t i) { subRun = i; }
   /// Set the sequence number for this event
   void     SetSeqNum(Int_t i) { seqNum = i; }
   /// Set the trigger type for this event
   void     SetTrigID(Int_t i) { trigID = i;  }
   /// Set the event ID for this event (midas subrun event index)
   void     SetEVID(Int_t i) { evID = i; }
   /// Set the event time (midas subrun event time)
   void     SetEVTime(Double_t t) { evTime  = t; }
   /// set the event time (time-stamp from board in s)
   void     SetTriggerTime(double ts){triggerTime = ts;}
   /// Set the time of the start of the waveform in seconds (pre-trigger and trigger delay corrected)
   void     SetDurationTime(double _runTime){ runTime = _runTime;}
   ///Set total charge based pe in this event
   void     SetNPE(Double_t pe){nPE = pe;}
   ///Set total height based pe in this event
   void     SetAPE(Double_t pe){aPE = pe;}


   /// Get the run ID for this event
   Int_t    GetRun(){ return run; }
   /// Get the sub-run ID for this event
   Int_t    GetSubRun(){ return subRun; }
   /// Get the sequence number for this event
   Int_t    GetSeqNum() { return seqNum; }
   /// Get the global trigger ID for this event
   Int_t    GetTrigID() { return trigID; }
   /// Get the event ID for this event (midas subrun event index)
   Int_t    GetEVID(){ return evID; }
   /// Get the event time (Unix time in seconds)(midas subrun event time)
   Double_t GetEVTime(){ return evTime; }
   /// Get the triggered event time (time-stamp from board in s, with 8ns precision)
   Double_t GetTriggerTime(){ return triggerTime; }

   /// Get the time from runstart to the start of the waveform (Pre-trigger corrected trigger time)
   Double_t GetDurationTime(){return runTime;}

   /// Get the total charge in this event
   Double_t GetNPE(){return nPE;}
   /// Get the total amplitude in this event
   Double_t GetAPE(){return aPE;}


   /// Set the triggered MIDAS time-stamp of this event
   Int_t        SetTimeStamp(TTimeStamp *aTimeStamp);
   /// Get the triggered MIDAS time-stamp of this event
   TTimeStamp   *GetTimeStamp(){return &timeStamp; }

   /// Set the triggered time-stamp of this event
   Int_t        SetRunStartTime(TTimeStamp *_RunStartTime);
   /// Get the triggered time-stamp of this event
   TTimeStamp   *GetRunStartTime(){return &fRunStartTime;}

   /// Set the EventCutID for this event (low-level bad-cut idenification)
   void         SetEventCutID(UInt_t i) {eventCutID = i;}
   /// Set the EventCutID for this event (low-level bad-cut idenification)
   UInt_t       GetEventCutID() {return eventCutID;}

   /// Get the i-th raw ETSChannel object for this event
   ETSChannel     *GetChannel(UInt_t i);
   /// Check to see if the ETSChannel objects exist for this event (Pruned in higher level analysis)
   Bool_t    ExistChannel(){ return vChannel.size() != 0; }
   /// Add a raw ETSChannel to the list of raw ETSChannel's
   void      AddChannel(ETSChannel *aWF);
   /// Add a new raw ETSChannel to the list of raw ETSChannel's
   ETSChannel*       AddNewChannel();
   /// Remove this branch
   void      PruneChannels();
   /// Remove waveforms
   void      ClearWaveforms(Option_t *option = "");
   /// Get the number of hit SiPMs in this event
   Int_t     GetNHit();
   /// Get the total number of channels in this event
   Int_t     GetNTot(){return nTot;}

   ///Get total number of channels
   Int_t     GetNChannels(){return nTot;}


   ClassDef(ETSDS,4)  //Event structure
};

#endif
