#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;

#pragma link C++ struct ETSChannel::PulseFinderResult+;
#pragma link C++ class ETSChannel+;
#pragma link C++ class ETSDS+;

#pragma link C++ class std::vector<ETSChannel*>;
#pragma link C++ class std::vector<ETSChannel*>::iterator;
#pragma link C++ class std::vector<ETSChannel*>::const_iterator;


#endif
