#include "ETSDS.hxx"

ClassImp(ETSDS)

ETSDS::ETSDS()
{
  Init();
}

ETSDS::~ETSDS()
{
  Clear();
}

void ETSDS::CopyObj(const ETSDS &rhs)
{
  run = rhs.run;
  subRun = rhs.subRun;
  seqNum = rhs.seqNum;
  trigID = rhs.trigID;
  evID = rhs.evID;
  evTime = rhs.evTime;
  timeStamp = rhs.timeStamp;
  triggerTime = rhs.triggerTime;
  runTime = rhs.runTime;
  eventCutID = rhs.eventCutID;
  nPE = rhs.nPE;
  aPE = rhs.aPE;

  nHit = rhs.nHit;
  nTot = rhs.nTot;

  std::vector<ETSChannel*>::iterator i;
  for (i=vChannel.begin(); i != vChannel.end(); i++)
  {
    delete (*i);
    vChannel.resize(0);
  }

  vChannel.resize((rhs.vChannel).size());
  for (unsigned i=0; i < (rhs.vChannel).size(); i++)
  {
    if ((rhs.vChannel)[i]) vChannel[i] = dynamic_cast<ETSChannel*>((rhs.vChannel[i])->Clone()); // Use Clone() for proper polymorphism
    else vChannel[i] = 0;
  }


}

///Set timestamp for the event
Int_t ETSDS::SetTimeStamp(TTimeStamp *aTimeStamp)
{
  //Set this event's timestamp
  if ( aTimeStamp ){
    timeStamp= *aTimeStamp;
    return 0;
  }
  else return -1;
}

///Set timestamp for the event
Int_t ETSDS::SetRunStartTime(TTimeStamp *_RunStartTime)
{
  //Set this event's timestamp
  if ( _RunStartTime ){
    fRunStartTime= *_RunStartTime;
    return 0;
  }
  else return -1;
}

/// Return i-th waveform from the list of ETSChannel's, Valid up to n-hit channels
ETSChannel * ETSDS::GetChannel(UInt_t i)
{
  if ( vChannel.empty() ) return 0;
  ETSChannel *aWF = NULL;
  if ( i < vChannel.size() ) aWF = vChannel[i];
  return aWF;
}

int ETSDS::GetNHit()
{
  int n = 0;
  for(int iC =0;iC<nTot;iC++) GetChannel(iC)->GetID() > 0 ? n++ : n;
  nHit = n; //Assign internal value
  return n;
}

/// Add a SiPM ETSChannel to this event.
void ETSDS::AddChannel(ETSChannel *aWF)
{
  vChannel.push_back(aWF);
  nTot++;
}

/// Add a SiPM ETSChannel to this event and return the pointer
ETSChannel* ETSDS::AddNewChannel()
{
  ETSChannel *aWF = new ETSChannel();
  vChannel.push_back(aWF);
  nTot++;
  return aWF;
}

void ETSDS::PruneChannels()
{
  for(UInt_t iWF =0;iWF<vChannel.size();iWF++){
    delete (vChannel[iWF]);
  }
  vChannel.clear();
}

void ETSDS::ClearWaveforms(Option_t *option)
{
  for(UInt_t iWF =0; iWF < vChannel.size();iWF++){
    vChannel[iWF]->ClearWaveform(option);
  }
}

/// Reset event
void ETSDS::Clear(Option_t *option)
{
  Init();

  for(UInt_t iWF =0;iWF<vChannel.size();iWF++){
    delete (vChannel[iWF]);
  }
  vChannel.clear();
}

///Initialize parameters
void ETSDS::Init()
{
  run         = 0;
  subRun      = 0;
  seqNum      = 0;
  trigID      = 0;
  evID        = 0;
  evTime      = 0;
  triggerTime = 0;
  runTime     = 0;
  eventCutID  = 0;

  nHit        = 0;
  nTot        = 0;
  vChannel.clear();
}
