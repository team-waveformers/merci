# CMake for ets Analysis

cmake_minimum_required(VERSION 3.12 FATAL_ERROR)
project(etsana)

option(DT5730 "Enable DT730 unpacker" OFF)
option(VX2740 "Enable VX2740 unpacker" ON)
option(TTREE "Enable ETSTree Writing" ON)
option(GENPULSE "Enable generic pulse finding" ON)
option(ANALYSIS "Enable analysis module (LoLX and ETS)" ON)
option(WAVEFORMS_ON "Enable Online Waveform display" ON)
option(WRITE_NTP "Write NTP output as well as full DS" OFF)
option(WRITE_WAVEFORMS "Write waveforms to output ttree" OFF)


message(STATUS "${PROJECT_NAME} installation path: ${CMAKE_INSTALL_PREFIX} ")

##########################################
# set binary rpath
set(CMAKE_INSTALL_RPATH ${CMAKE_INSTALL_LIBDIR})
set(CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

##########################################
# Generate dictionary for ETS event description
ROOT_GENERATE_DICTIONARY(G__ets ETSChannel.hxx ETSDS.hxx LINKDEF ETSLinkDef.hxx)
# Create a shared library with generated dictionary
add_library(ets SHARED ETSChannel.cxx ETSDS.cxx G__ets)
target_include_directories(ets PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(ets ${ROOT_LIBRARIES})

##########################################
# Configure current project
message(STATUS "Configuring ${PROJECT_NAME}")

if( NOT VX2740 AND NOT DT5730 )
  message(FATAL_ERROR
    "This target cannot be built without any ADC module
  Please use -DVX2740=ON or -DDT5730=ON" )
endif()


#Offline modules

list(APPEND Modules ${CMAKE_SOURCE_DIR}/src/baseline_module.cxx)

if(DT5730)
  list(INSERT Modules 0 ${CMAKE_SOURCE_DIR}/src/unpackDT5730_module.cxx)
endif(DT5730)

if(VX2740)
  list(INSERT Modules 0 ${CMAKE_SOURCE_DIR}/src/unpackVX2740_module.cxx)
endif(VX2740)

if(GENPULSE)
  list(APPEND Modules ${CMAKE_SOURCE_DIR}/src/generic_pulsefinder_module.cxx)
endif(GENPULSE)

if(ANALYSIS)
  list(APPEND Modules ${CMAKE_CURRENT_SOURCE_DIR}/ETS_analysis_module.cxx)
endif(ANALYSIS)

if(TTREE)
  list(APPEND Modules ${CMAKE_CURRENT_SOURCE_DIR}/ETS_treewriter_module.cxx)
endif(TTREE)

#Online modules

list(APPEND Modules_on ${CMAKE_SOURCE_DIR}/src/baseline_module.cxx)

if(DT5730)
  list(INSERT Modules_on 0 ${CMAKE_SOURCE_DIR}/src/unpackDT5730_module.cxx)
endif(DT5730)

if(VX2740)
  list(INSERT Modules_on 0 ${CMAKE_SOURCE_DIR}/src/unpackVX2740_module.cxx)
endif(VX2740)

if(GENPULSE)
  list(APPEND Modules_on ${CMAKE_SOURCE_DIR}/src/generic_pulsefinder_module.cxx)
endif(GENPULSE)

if(ANALYSIS)
  list(APPEND Modules_on ${CMAKE_CURRENT_SOURCE_DIR}/ETS_analysis_module.cxx)
endif(ANALYSIS)

if(WAVEFORMS_ON)
  message(STATUS "Enabling waveform display")
  list(APPEND Modules_on ${CMAKE_SOURCE_DIR}/src/generic_waveform_module.cxx)
endif(WAVEFORMS_ON)


##########################################
# Configure targets

set(bin "etsana_offline.exe")
set(Sources  ${CMAKE_SOURCE_DIR}/src/TDT5730RawData.cxx ${CMAKE_SOURCE_DIR}/src/TVX2740RawData.cxx ${CMAKE_SOURCE_DIR}/src/adcflow.hxx ${CMAKE_SOURCE_DIR}/src/pulseflow.hxx ${CMAKE_SOURCE_DIR}/src/baselineflow.hxx  ${CMAKE_SOURCE_DIR}/src/utils.hxx ${Modules})
add_executable(${bin} ${Sources})

set(bin_on "etsana_online.exe")
set(Sources_on  ${CMAKE_SOURCE_DIR}/src/TDT5730RawData.cxx ${CMAKE_SOURCE_DIR}/src/TVX2740RawData.cxx ${CMAKE_SOURCE_DIR}/src/adcflow.hxx ${CMAKE_SOURCE_DIR}/src/pulseflow.hxx ${CMAKE_SOURCE_DIR}/src/baselineflow.hxx  ${CMAKE_SOURCE_DIR}/src/utils.hxx ${Modules_on})
add_executable(${bin_on} ${Sources_on})

#Define flag for config json file path
set(CONFIG_PATH ${CMAKE_CURRENT_SOURCE_DIR}/config/)
target_compile_definitions(${bin} PUBLIC CONFIG_PATH=${CONFIG_PATH})
target_compile_definitions(${bin_on} PUBLIC CONFIG_PATH=${CONFIG_PATH})


if(WARNINGS_ALL)
  target_compile_options(${bin} PUBLIC -Wall -Wextra -pedantic)
  target_compile_options(${bin_on} PUBLIC -Wall -Wextra -pedantic)
  message(STATUS "Enabling strict compilation rules")
endif(WARNINGS_ALL)

if(WRITE_NTP)
  target_compile_definitions(${bin} PUBLIC WRITE_NTP)
  target_compile_definitions(${bin_on} PUBLIC WRITE_NTP)
  message(STATUS "Enabling Flat NTP output")
endif(WRITE_NTP)

if(WRITE_WAVEFORMS)
  message(STATUS "WRITE_WAVEFORMS ON, to turn off use -WRITE_WAVEFORMS=OFF")
  target_compile_definitions(${bin} PUBLIC WRITE_WAVEFORMS)
  target_compile_definitions(${bin_on} PUBLIC WRITE_WAVEFORMS)
endif(WRITE_WAVEFORMS)


target_include_directories(${bin} PUBLIC ${CMAKE_SOURCE_DIR}/src)
target_compile_definitions(${bin} PUBLIC ${ROOT_CCX_FLAGS} HAVE_ROOT HAVE_ROOT_HTTP HAVE_THTTP_SERVER HAVE_CONFIG)
target_include_directories(${bin} PUBLIC ${MANA_PATH_DIR}/manalyzer ${MANA_PATH_DIR}/midasio ${MANA_PATH_DIR}/mvodb ${MANA_PATH_DIR}/mjson ${MANA_PATH_DIR}/mxml)
target_include_directories(${bin} PUBLIC ${ROOT_INCLUDE_DIRS})

target_include_directories(${bin_on} PUBLIC ${CMAKE_SOURCE_DIR}/src)
target_compile_definitions(${bin_on} PUBLIC ${ROOT_CCX_FLAGS} HAVE_ROOT HAVE_ROOT_HTTP HAVE_THTTP_SERVER HAVE_CONFIG)
target_include_directories(${bin_on} PUBLIC ${MANA_PATH_DIR}/manalyzer ${MANA_PATH_DIR}/midasio ${MANA_PATH_DIR}/mvodb ${MANA_PATH_DIR}/mjson ${MANA_PATH_DIR}/mxml)
target_include_directories(${bin_on} PUBLIC ${ROOT_INCLUDE_DIRS})


if( MIDAS_FOUND )
  target_link_directories(${bin} PUBLIC ${MIDAS_LIBRARY_DIRS})
  target_include_directories(${bin} PUBLIC ${MIDAS_INCLUDE_DIRS})
  target_link_directories(${bin_on} PUBLIC ${MIDAS_LIBRARY_DIRS})
  target_include_directories(${bin_on} PUBLIC ${MIDAS_INCLUDE_DIRS})
endif( MIDAS_FOUND )

target_link_libraries(${bin} ets manalyzer manalyzer_main ${MIDAS_LIBRARIES} ${ROOT_LIBRARIES} )
target_link_libraries(${bin_on} ets manalyzer manalyzer_main ${MIDAS_LIBRARIES} ${ROOT_LIBRARIES} )

message(STATUS "${PROJECT_NAME} ${CMAKE_BUILD_TYPE} build")

##########################################
install(TARGETS ets DESTINATION ${CMAKE_INSTALL_LIBDIR})
install(FILES ${PROJECT_BINARY_DIR}/libets_rdict.pcm DESTINATION ${CMAKE_INSTALL_LIBDIR})
install(FILES ${PROJECT_BINARY_DIR}/libets.rootmap DESTINATION ${CMAKE_INSTALL_LIBDIR})
message(STATUS "[${PROJECT_NAME}] ets library will be installed in ${CMAKE_INSTALL_LIBDIR}")

install(TARGETS ${bin} DESTINATION ${CMAKE_INSTALL_PREFIX})
install(TARGETS ${bin_on} DESTINATION ${CMAKE_INSTALL_PREFIX})
message(STATUS "[${PROJECT_NAME}] ${bin} will be installed in ${CMAKE_INSTALL_PREFIX}")
message(STATUS "[${PROJECT_NAME}] ${bin_on} will be installed in ${CMAKE_INSTALL_PREFIX}")
