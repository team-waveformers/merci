/******************************************************************
 * ETSAnalysis Module
 * Author: David Gallacher
 * February 2022
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"
#include "baselineflow.hxx"
#include "pulseflow.hxx"
#include "adcflow.hxx"
#include "ETS_flow.hxx"
#include "ETSDS.hxx"
#include "ETSChannel.hxx"

#include <iostream>
#include <fstream>
#include <cstdlib>

//Read preprocessor flag value as string literal
#define STRING(s) #s
#define STRSTRING(s) STRING(s)

#include "json.hpp"
using json = nlohmann::json;

class ETSAnalysisModuleFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};

class ETSAnalysisModule : public TARunObject
{
public:
   ETSAnalysisModuleFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors

   //Add public data members here

   std::string fBoardType; //We read this from the .json file

private:

public:
   ETSAnalysisModule(TARunInfo* runinfo, ETSAnalysisModuleFlags* f): TARunObject(runinfo),
							     fFlags(f),
							     fCounter(0), fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"ETSAnalysisModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="ETSAnalysisModule";
#endif

      std::string config_path;
#ifdef CONFIG_PATH
     config_path = STRSTRING(CONFIG_PATH) + fFlags->fConfigName;//Read config file path from CMAKE source dir (lolx)
     std::cout << "Reading config file from path = "<< config_path << "\n";
#endif

    std::ifstream fin(config_path);
    json settings;
    fin>>settings;
    if(fFlags->fVerbose)
       std::cout<<"ETSAnalysisModule Json parsing success!"<<std::endl;
    fin.close();
    //Read the data from f
    fBoardType = settings["Analysis"]["BoardType"].get<std::string>();

   }

   //Required functions
   /// Begin run method
   void BeginRun(TARunInfo* runinfo){

   }

   /// End run method
   void EndRun(TARunInfo* runinfo){

   }

   /// Analyze flow event method
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow){

     BaselineEventFlow* corr_flow = flow->Find<BaselineEventFlow>();
     if( !corr_flow )
        {
           ++fError;
#ifdef HAVE_MANALYZER_PROFILER
           *flags|=TAFlag_SKIP_PROFILE;
#endif
           return flow;
        }

     AdcEventFlow* adc_flow = flow->Find<AdcEventFlow>();
     if( !adc_flow )
         {
           ++fError;
      #ifdef HAVE_MANALYZER_PROFILER
           *flags|=TAFlag_SKIP_PROFILE;
      #endif
           return flow;
         }


     PulseFlow* pulse_flow = flow->Find<PulseFlow>();
        if( !pulse_flow )
         {
              ++fError;
     #ifdef HAVE_MANALYZER_PROFILER
              *flags|=TAFlag_SKIP_PROFILE;
     #endif
              return flow;
         }


       if( fFlags->fVerbose  )
          std::cout<<"ETS_analysis_module::AnalyzeFlowEvent # of ch: "<<adc_flow->getNchannels()<<std::endl;

      //Write pulse flow and waveforms to ETS DS
      ETSProcessorFlow* the_flow  = new ETSProcessorFlow(flow);
      the_flow->SetConfig(fBoardType);//Set the board type

      //Get the ETS DS
      ETSDS *ds = the_flow->event;
      int num_channels = pulse_flow->nChannels;

      //Create channels and copy data from pulsefinder output
      for(int iChan =0;iChan < num_channels ;iChan++){
          ETSChannel *chan = ds->AddNewChannel();
          //Copy data from pulsefinder module to channel
          ETSChannel::PulseFinderResult *pResults = &chan->pulses;
          pResults->vPulseLeadingTime.insert(pResults->vPulseLeadingTime.begin(),
          pulse_flow->results.at(iChan)->pulseResults.fPulseStart.begin(),
          pulse_flow->results.at(iChan)->pulseResults.fPulseStart.end());

          pResults->vPulsePeakTime.insert(pResults->vPulsePeakTime.begin(),
          pulse_flow->results.at(iChan)->pulseResults.fPeakTime.begin(),
          pulse_flow->results.at(iChan)->pulseResults.fPeakTime.end());

          pResults->vPulseWidth.insert(pResults->vPulseWidth.begin(),
          pulse_flow->results.at(iChan)->pulseResults.fPulseWidth.begin(),
          pulse_flow->results.at(iChan)->pulseResults.fPulseWidth.end());

          pResults->vPulseHeight.insert(pResults->vPulseHeight.begin(),
          pulse_flow->results.at(iChan)->pulseResults.fAmplitude.begin(),
          pulse_flow->results.at(iChan)->pulseResults.fAmplitude.end());

          pResults->vPulseCharge.insert(pResults->vPulseCharge.begin(),
          pulse_flow->results.at(iChan)->pulseResults.fCharge.begin(),
          pulse_flow->results.at(iChan)->pulseResults.fCharge.end());

          chan->SetNPulses(pResults->vPulseLeadingTime.size());

      }

      //Write waveform info
      for(int iChan =0;iChan < num_channels ;iChan++){
          ETSChannel *chan = ds->GetChannel(iChan);
          int daq_chan = -1;
          if(chan==NULL) continue;
          std::map<int,unsigned int>::iterator it;
          it = corr_flow->fChannelMap.find(iChan);
          if(it != corr_flow->fChannelMap.end()) daq_chan = it->second;
          if(fBoardType=="DT5730") chan->ConvertDT5730(corr_flow->getChannelVector(iChan),daq_chan);
          else if (fBoardType=="VX2740") chan->ConvertVX2740(corr_flow->getChannelVector(iChan),daq_chan);
          chan->SetBaseline(corr_flow->getBaseline(iChan));
          chan->SetBaselineRMS(corr_flow->getBaselineRMS(iChan));
      }

      CalculatePE(ds);//Sum PE

      IntegrateWaveform(ds);//Naiive integral of baseline corrected waveform

      if(fFlags->fVerbose) std::cout << "Leaving ETS_analysis_module AnalyzeFlowEvent" <<std::endl;
      flow = the_flow;
      fCounter++;
      return flow;
    }

    //Method to sum PE over channels and event
    void CalculatePE(ETSDS *event){

      ETSChannel *chan = NULL;
      double eventPE = 0.0;
      double eventAPE = 0.0;
      for(int iChan=0;iChan < event->GetNTot();iChan++){
        double t0 = 1e5;
        double chanPE = 0.0;
        double chanAPE = 0.0;
        chan = event->GetChannel(iChan);
        if(!chan) continue;
        for(int iP=0 ; iP < chan->GetNPulses();iP++){
          double pulse_time = chan->GetPulseLeadingTime(iP);
          if(pulse_time < t0) t0 = pulse_time;
          chanPE += chan->GetPulseCharge(iP);//Could add SPE normalization here
          chanAPE += chan->GetPulseHeight(iP);//Could add SPE normalization here
        }
        chan->SetNPE(chanPE);
        chan->SetAPE(chanAPE);
        chan->SetT0_Channel(t0);
        eventPE += chanPE;
        eventAPE += chanAPE;
      }
      event->SetNPE(eventPE);
      event->SetAPE(eventAPE);
    }

   //Integrate the waveform for Lucas, in units of ADC*8ns
    void IntegrateWaveform(ETSDS *event){

      ETSChannel *chan = NULL;
      for(int iChan=0;iChan < event->GetNTot();iChan++){
         //Get the channel
         chan = event->GetChannel(iChan);
         vector<double> *vADC = chan->GetADCs();
         //Accumulate the waveform
         double wave_intrgl = std::accumulate(vADC->begin(),vADC->end(),0);  
         chan->SetWaveformIntegral(wave_intrgl);
      }
   }

};


class ETSAnalysisModuleFactory : public TAFactory
{
public:
   ETSAnalysisModuleFlags fFlags;

public:

   void Init(const std::vector<std::string> &args)
   {
      printf("ETSAnalysisModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++){
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
             if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
	   }

   }

   void Finish()
   {
      printf("ETSAnalysisModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("ETSAnalysisModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new ETSAnalysisModule(runinfo, &fFlags);
   }

};

static TARegister tar(new ETSAnalysisModuleFactory);
