/******************************************************************
 *  ETS Specific TTree Writer *
 *
 * D. Gallacher
 * February 2022
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include <iostream>
#include <fstream>
#include <cstdlib>

#include "TTree.h"
#include "TObjString.h"
#include "TError.h"

//ETS Specific headers
#include "ETSDS.hxx"
#include "ETSChannel.hxx"
#include "ETS_flow.hxx"
#include "pulseflow.hxx"
#include "adcflow.hxx"

//Read preprocessor flag value as string literal
#define STRING(s) #s
#define STRSTRING(s) STRING(s)

#include "json.hpp"
using json = nlohmann::json;

class ETSTreeWriterFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};

class ETSTreeWriterModule : public TARunObject
{
public:
   ETSTreeWriterFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors

private:
  int fNchan;
  TTree *Tree; //TTree to contain event information
  ETSDS *lDS; //Custom DS for ETS
  TTree *flatTree = NULL; //Flat ttree output
  TFile *fileFlat = NULL; //File for flat ttree

public:
   ETSTreeWriterModule(TARunInfo* runinfo, ETSTreeWriterFlags* f): TARunObject(runinfo),
							     fFlags(f),
							     fCounter(0), fError(0),
							     Tree(0)
   {
      if(fFlags->fVerbose) std::cout<<"ETSTreeWriterModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="ETSTreeWriter";
#endif
      std::string config_path;
#ifdef CONFIG_PATH
     config_path = STRSTRING(CONFIG_PATH) + fFlags->fConfigName;//Read config file path from CMAKE source dir (lolx)
     std::cout << "Reading config file from path = "<< config_path << "\n";
#endif


   }

   //Required functions
   /// Begin run method
   void BeginRun(TARunInfo* runinfo){

     lDS = new ETSDS(); //New event structure
     Tree = new TTree("T","ETS Event DS");
     Tree->Branch("event",&lDS,32000,0);

#ifdef WRITE_NTP
     CreateFlatTree(runinfo);//Make flat ttree
#endif
     if(runinfo->fFileName.empty() )
        std::cout<<"ETSTTreeWriterModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
     else
        std::cout<<"ETSTTreeWriterModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;

   }
   /// End run method
   void EndRun(TARunInfo* runinfo){
     std::cout<<"ETSTreeWriterModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;

    if (fileFlat != NULL) {
       fileFlat->Write();
       fileFlat->Close();
       fileFlat = NULL;
    }
   }
   /// Analyze Flow event method
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow){

       AdcEventFlow* raw_flow = flow->Find<AdcEventFlow>();
          if( !raw_flow )
           {
                ++fError;
       #ifdef HAVE_MANALYZER_PROFILER
                *flags|=TAFlag_SKIP_PROFILE;
       #endif
                return flow;
           }

       PulseFlow* pulse_flow = flow->Find<PulseFlow>();
          if( !pulse_flow )
           {
                ++fError;
       #ifdef HAVE_MANALYZER_PROFILER
                *flags|=TAFlag_SKIP_PROFILE;
       #endif
                return flow;
           }

      ETSProcessorFlow* ets_flow = flow->Find<ETSProcessorFlow>();
          if( !ets_flow )
           {
                ++fError;
       #ifdef HAVE_MANALYZER_PROFILER
                *flags|=TAFlag_SKIP_PROFILE;
       #endif
                return flow;
           }

         //Get MIDAS Header info
         int run_num = runinfo->fRunNo;
         ULong64_t tmask = raw_flow->getTriggerMask();
         int event_num = raw_flow->getEventNumber();
         double tstamp = raw_flow->fmidas_ts;
         double triggerTime = raw_flow->fTriggerTime[0];//Get the trigger time from the board
         int runType = 0;
         UInt_t startTime = 0;
         int subrun_num = 0;
         std::string filename = runinfo->fFileName;
         //subrun_num = GetSubRun(filename);
         //std::cout << "Sub run number = "<< subrun_num <<std::endl;
         runinfo->fOdb->RU32("/Runinfo/Start time binary/",&startTime,0,0);
         runinfo->fOdb->RI("/Runinfo/runID/",&runType,0,0);

         time_t ev_time_stamp = (time_t) tstamp;//Cast to time_t
         time_t start_time = (time_t) startTime;//Cast to time_t
         int nsec = (int)((tstamp - (int)tstamp) * 1e9 + 0.5);
         TTimeStamp ev_ts(ev_time_stamp,nsec); //ROOT TTimeStamp event time
         TTimeStamp start_ts(start_time,0); //ROOT TTimeStamp start run time

         //Get the channel data from the pulse-finder
         lDS = ets_flow->event;
         int nhit = lDS->GetNHit();


         //Using the midas info, find the time from start of run to start of waveform (Trigger time - (pretrigger and delay) )
         MVOdbError *error = new MVOdbError();
         double corrTriggerTime = triggerTime;
         double nsPerSample = ets_flow->conf->fNanosecsPerSample;// Get nanoseconds per sample
         UInt_t preTrigger = 0;
         runinfo->fOdb->RU32("/Equipment/VX2740_Config_000/Settings/Pre-trigger (samples)/",&preTrigger,0,error); 
         UInt_t triggerDelay = 0;
         runinfo->fOdb->RU32("/Equipment/VX2740_Config_000/Settings/Trigger delay (samples)/",&triggerDelay,0,error); 
         //Leave in for debugging later
         //std::cout << "Erorr status = " << error->fErrorString <<std::endl;
         corrTriggerTime = corrTriggerTime - (preTrigger + triggerDelay) * nsPerSample*1e-9; 
         lDS->SetDurationTime(corrTriggerTime);
         delete error;


         if(fFlags->fVerbose) std::cout << "Filling event"<<std::endl;
         //Fill MIDAS Header info
         lDS->SetRun(run_num);
         lDS->SetSubRun(subrun_num);
         lDS->SetSeqNum(event_num);
         lDS->SetTrigID(tmask);
         lDS->SetEVID(event_num);
         lDS->SetEVTime(tstamp);
         lDS->SetTimeStamp(&ev_ts);
         lDS->SetTriggerTime(triggerTime);
         lDS->SetRunStartTime(&start_ts);

         #ifndef WRITE_WAVEFORMS_TO_FILE
            lDS->ClearWaveforms(); //if we're not requesting waveforms, clear before writing to file
         #endif 

         Tree->Fill();

        //Fill Flat ntuple Tree, non-null when cmake flag is set
         if(flatTree != NULL){
           double npe = lDS->GetNPE();
           double ape = lDS->GetAPE();
           flatTree->SetBranchAddress("run",&run_num);
           flatTree->SetBranchAddress("subRun",&subrun_num);
           flatTree->SetBranchAddress("seqNum",&event_num);
           flatTree->SetBranchAddress("trigID",&tmask);
           flatTree->SetBranchAddress("evID",&event_num);
           flatTree->SetBranchAddress("evTime",&tstamp);
           flatTree->SetBranchAddress("triggerTime",&triggerTime);
           flatTree->SetBranchAddress("nHit",&nhit);
           flatTree->SetBranchAddress("nPE",&npe);
           flatTree->SetBranchAddress("aPE",&ape);
           flatTree->Fill();
         }

         if(fFlags->fVerbose) std::cout << "Leaving ETS ETSTreeWriter Module AnalyzeFlowEvent" <<std::endl;
         ++fCounter;
         return flow;

   }
   //Create and fill the ntuple output
   void CreateFlatTree(TARunInfo *runinfo)
   {
    //For flat ttree
    ///Run number        (midas run)
    Int_t         run;
    ///Sub-Run number    (midas sub-run)
    Int_t         subRun;
    ///sequence number   (midas subrun)
    Int_t         seqNum;
    ///Trigger type      (midas trigger mask)
    ULong64_t     trigID;
    ///Event ID          (midas subrun event index)
    Int_t         evID;
    ///Event time        (midas subrun event time from 125MHz clock) in unix time (s)
    Double_t      evTime;
    ///Trigger timestamp
    Double_t      triggerTime;
    /// Global trigger ID (midas serial number)
    Int_t         gtID;
    /// Cut ID - event-level, can be used in the future to identify bad events from good
    UInt_t        eventCutID;
    /// Number of hit SiPMs in this event
    Int_t         nHit;
    /// Total charge in event
    Double_t      nPE;
    /// Total amplitude in event
    Double_t      aPE;

    std::string outfile = runinfo->fRoot->fOutputFile->GetName();
    outfile = outfile.substr(0,outfile.find(".root"));//Cut off .root at end of file
    outfile+="_ntp.root";//add ntp
    std::cout << "Output file name = "<<outfile <<std::endl;
    fileFlat = new TFile(outfile.c_str(),"RECREATE");
    if (!fileFlat->IsOpen()) {
      fprintf(stderr, "ETSTreeWriter::BeginRun: Error: cannot open output ROOT file \"%s\"\n", outfile.c_str());
      fileFlat = new TFile("/dev/null", "UPDATE");
      assert(fileFlat);
      assert(fileFlat->IsOpen());
    }

    if (fileFlat != NULL) {
      fileFlat->cd();
    }

    //Make the tree
    flatTree = new TTree("ntp","Flat ETSDS Tree");
    //Create branches
    flatTree->Branch("run",&run);
    flatTree->Branch("subRun",&subRun);
    flatTree->Branch("seqNum",&seqNum);
    flatTree->Branch("trigID",&trigID,"trigID/l");
    flatTree->Branch("evID",&evID);
    flatTree->Branch("evTime",&evTime);
    flatTree->Branch("triggerTime",&triggerTime);
    flatTree->Branch("gtID",&gtID);
    flatTree->Branch("eventCutID",&eventCutID);
    flatTree->Branch("nHit",&nHit);
    flatTree->Branch("nPE",&nPE);
    flatTree->Branch("aPE",&aPE);

  }//End CreateFlatTree

};


class ETSTreeWriterModuleFactory : public TAFactory
{
public:
   ETSTreeWriterFlags fFlags;

public:
   void Usage()
   {
      printf("ETSTreeWriterModuleFactory flags:\n");
      printf("--verbose\tprint status information\n");
   }

   void Init(const std::vector<std::string> &args)
   {
      printf("ETSTreeWriterModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++){
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
             if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
	   }

   }

   void Finish()
   {
      printf("ETSTreeWriterModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("ETSTreeWriterModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new ETSTreeWriterModule(runinfo, &fFlags);
   }

};

static TARegister tar(new ETSTreeWriterModuleFactory);
