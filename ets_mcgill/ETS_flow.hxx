/******************************************************************
 * Flow Object(s) for ETS analysis *
 *
 * D. Gallacher
 * Feb 2022
 *
******************************************************************/

#ifndef __ETSFLOW__
#define __ETSFLOW__

#include "manalyzer.h"

#include <map>
#include <vector>
#include <string>

//ETS Headers
#include "ETSDS.hxx"

class ETSConfig {

  public:
    std::string fBoardType; //V1740, WAVE, VX2740
    int fNumberChannelPerModule;
    double fNanosecsPerSample;
    int fNSamples;
    int fResolution;

    ETSConfig(){ init(); }
    ETSConfig(std::string s)
    {
      fBoardType = s;
      fNSamples = 0;

       if(fBoardType=="VX2740")
          {
             fNumberChannelPerModule=64;
             fNanosecsPerSample=8.0; // 125 MS/s = 8 ns
             fResolution=0xffff; // 16 bits
          }
       else if(fBoardType=="DT5730")
          {
             fNumberChannelPerModule=8;
             fNanosecsPerSample=2.0; //500 MS/s = 16 ns
             fResolution=0xffff; // 14 bits
          }
        else
          std::cerr<<"ETSConfig:: Unknown ADC type "<<s<<std::endl;
    }

    void print() const
    {
       std::cout<<fBoardType<<" # of channels: "<<fNumberChannelPerModule
                <<" Sampling period: "<<fNanosecsPerSample<<" ns"
                <<" Number of Samples: "<<fNSamples<<std::endl;
    }

    void init(){
      fBoardType="null";
      fNumberChannelPerModule = 0;
      fNanosecsPerSample = 0.0;
      fNSamples = 0;
      fResolution = 0;
    }

};


class ETSProcessorFlow: public TAFlowEvent
{
public:
  ETSDS *event;
  ETSConfig *conf;

public:
  ETSProcessorFlow(TAFlowEvent* flow):TAFlowEvent(flow)
  {
    event = new ETSDS();
  }

  ~ETSProcessorFlow()
  {
    event->Clear();
    delete event;
    delete conf;
  }

  inline int GetNumberOfChannels() const { return (int) event->GetNHit(); }
  inline void SetConfig(std::string name) {  conf = new ETSConfig(name);}

};

#endif
