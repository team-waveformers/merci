#include "ETSChannel.hxx"

ClassImp(ETSChannel)

ETSChannel::ETSChannel()
{
  Init();
}

ETSChannel::~ETSChannel()
{
  Clear();
}

void ETSChannel::CopyObj(const ETSChannel &rhs)
{

  type = rhs.type;
  aV = rhs.aV;
  t0_chan = rhs.t0_chan;
  nPulses = rhs.nPulses;
  baseline = rhs.baseline;
  baseline_rms = rhs.baseline_rms;
  nPE = rhs.nPE;
  aPE = rhs.aPE;
  waveformIntegral = rhs.waveformIntegral;

  pulses = rhs.pulses;

}

void ETSChannel::ConvertDT5730(const std::vector<double> *vADC,int id)
{
  type = 0; // Declare this ETSChannel as a DT5730 waveform
  const double kNanoSecsPerSample = 2.0; // 500MS/s
  int n = vADC->size();
  aV.insert(aV.end(),vADC->begin(),vADC->end());
  sipmID = id;
}

void ETSChannel::ConvertVX2740(const std::vector<double> *vADC,int id)
{
  type = 1; // Declare this ETSChannel as a VX2740 waveform
  const double kNanoSecsPerSample = 8.0; // 500MS/s
  int n = vADC->size();
  aV.insert(aV.end(),vADC->begin(),vADC->end());
  sipmID = id;
}


/// Empty the waveform adc array
void ETSChannel::ClearWaveform(Option_t *option)
{
  aV.clear();
}

/// Reset class
void ETSChannel::Clear(Option_t *option)
{
  Init();
}

///Initialize parameters
void ETSChannel::Init()
{
  type = 0;
  sipmID =-1;

  nPE = -1;
  aPE = -1;
  baseline = -1;
  baseline_rms = -1;
  nPulses = 0;
  t0_chan = -999.0;
  waveformIntegral = -1;

  aV.clear();

  pulses = {};

}
