# to run: python get_ODB_params.py /path/to/json/file

import json
import sys

def get_ODB_params(file_path):
    with open(file_path, 'r') as file:
            data = json.load(file)
            
            equipment = data.get("Equipment", {})
            dt5730B_control = equipment.get("DT5730B_control", {})
            settings = dt5730B_control.get("Settings",{})
            buffer_organization = int(settings.get("Buffer organization"))
            post_trigger = int(settings.get("Post Trigger"), 16)
            
            print("Buffer organization: ", buffer_organization)
            print("Post Trigger: ", post_trigger)
            
            # convert buffer organization to the total number of samples recorded
            total_num_samples = 640*2**(10-buffer_organization) - 10
            
            # in the README for the ODB it says that post_trigger_samples = 4*post_trigger + constantLatency, where constantLatency is a few clock cycles. 
            # for the purpose of this script I'm assuming constantLatency is negligible in the post trigger samples calculation
            post_trigger_samples = post_trigger*4 
            pre_trigger_samples = total_num_samples - post_trigger_samples
            
            print("Total Number of Samples: ", total_num_samples)
            print("Number of Post Trigger Samples: ", post_trigger_samples)
            print("Number of Pre Trigger Samples: ", pre_trigger_samples)
 
if __name__ == "__main__":
        file_path = sys.argv[1]
        get_ODB_params(file_path)
