#!/bin/bash
# Usage: ./compile_root Macro_Name
# Don't include the (*.cxx) extension in the above, just the name
# of the file without the extension will suffice
path="/Users/davidgallacher/packages/merci"
set -x
cflags="-std=c++11"
g++ -Wall -W -Woverloaded-virtual -fPIC -Iinclude -pthread $cflags -I$path/src/ -I$path/lolx/ -I$ROOTSYS/include -o $1.o -c $1.cxx
#Need to compile our own version of Fitter.o since it's not in the shared library
#g++ -Wall -W -Woverloaded-virtual -fPIC -Iinclude -pthread $cflags -I$path/src/ -I$path/lolx/ -I$ROOTSYS/include -o Fitter.o -c $path/src/Fitter.cxx
g++ $1.o -L$path/bin/lib -L$ROOTSYS/lib -llolx -lCore  -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lTree -lRint -lPostscript -lMatrix -lMinuit -lPhysics -lMathCore -lThread -pthread -lm -ldl -rdynamic -o $1.exe
