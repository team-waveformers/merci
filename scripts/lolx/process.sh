#!/usr/bin/sh
# bash process.sh runNum subRunNum numEvents

source ~/.bash_profile
cd /home/tdaq/merci/bin/

run="$1"
events="$2"

input=$(printf "/data/logger/run%05d.mid.lz4" $run)
output=$(printf "/data/merci_output/lolx_%05d.root" $run)
echo "Reading $input ..."

if [ -z "$events" ]
then
      echo "\$events is empty"
      events="0"
      echo "Processing all events, writing to $output"
else
      echo "Processing " $events " events, writing to $output"
fi

./lolxana_offline.exe $input -e$events -O$output 
