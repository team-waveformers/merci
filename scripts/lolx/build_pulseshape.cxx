/// Macro to build a liquid xenon pulsehape
/// Author: David Gallacher
//Compiled macro for plotting
//Compile by calling "bash compile_root_macro.sh build_pulseshape"
//Eg for use: ./build_pulseshape nEvents fileName

#include "TFile.h"
#include "TROOT.h"
#include "TH1D.h"
#include "TH1.h"
#include "TH2D.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TLegend.h"
#include "TPaveStats.h"
#include "TPad.h"
#include "TObject.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "TApplication.h"
#include "TTree.h"
#include "TVector3.h"
//TROOT and TStyle give access to gPad and gStyle global pointers inside compiled macro.
#include <TROOT.h>
#include <TStyle.h>

#include <iostream>
#include <vector>
#include <string>

//Lolx headers
#include "utils.hxx"
#include "LoLXDS.hxx"
#include "LoLXChannel.hxx"


using namespace std;

vector< string > ParseCommandLineArguments( int argc, char** argv ){

	vector< string > vecStr;
	stringstream tmpStream;
	string tmpStr = "";
	for ( Int_t iArg = 0; iArg < argc; iArg++ ){
		tmpStr = ""; tmpStream.clear(); tmpStream.str("");
		tmpStream << argv[ iArg ];
		tmpStream >> tmpStr;
		vecStr.push_back( tmpStr );
	}
	return vecStr;

}



int main(int argc, char **argv)
{

  // Parse command line arguments
  vector< string > argsVec = ParseCommandLineArguments( argc, argv );
  // Argument 0 - script name [null]
  // Argument 1 - Event Number
  // Arguement 2 - FIle name
  Int_t numEvent = 1;
  if(argsVec.size()>1){
    numEvent = std::atoi(argsVec[ 1 ].c_str());
  }
  // Argument 2 - filename
  string fname;
  if(argsVec.size()>2){
    fname = argsVec[ 2 ].c_str();
  }

  if(argsVec.size()<2){
    cout << "./build_pulseshape.exe num_events filename "<<endl;
    cout << "Not enough arguments provided, dieing now"<<endl;
    exit(1);
  }

	cout << "Reading "<<numEvent<< " events from file: "<< fname <<endl;

  //TApplication will hold the Canvas so it can be interacted with using a compiled macro.
  TApplication theApp("Application", &argc, argv);;

  LoLXDS *event = new LoLXDS();

  TFile *filein = new TFile(fname.c_str(),"READ");
  TTree *T = (TTree*)filein->Get("T");
  T->SetBranchAddress("event",&event);

  if(numEvent>T->GetEntries()){
		cout <<"Requested more events than in file, file has "<<T->GetEntries()<<" events."<<endl;
		exit(1);
	}

  Utils *utils = new Utils();

	//Pulseshape parameters
	double max_time = 2000.0;
	double min_time = -304;
	int nbins = int((max_time-min_time)/16.0);

	double spe_a = 25.0;// Aproximate SPE for all channels
	double spe_q = 150.0;// Approximate charge SPE for all channels

	TH1D *hPS_A = new TH1D("hPS_A",Form("Leading edge Pulseshape using Amplitude PE; T0 subtracted Time [ns]; Intensity[PE/%2.1f ns]",(max_time-min_time)/nbins), nbins, min_time,max_time);
	TH1D *hPS_Q = new TH1D("hPS_Q",Form("Leading edge Pulseshape using Charge PE; T0 subtracted Time [ns]; Intensity[PE/%2.1f ns]",(max_time-min_time)/nbins), nbins, min_time,max_time);

	TH1D *hPS_A_BP = new TH1D("hPS_LP",Form("Bandpass leading edge Pulseshape using Amplitude PE; T0 subtracted Time [ns]; Intensity[PE/%2.1f ns]",(max_time-min_time)/nbins), nbins, min_time,max_time);
	TH1D *hPS_A_LP = new TH1D("hPS_BP",Form("Longpass leading edge Pulseshape using Amplitude PE; T0 subtracted Time [ns]; Intensity[PE/%2.1f ns]",(max_time-min_time)/nbins), nbins, min_time,max_time);
	TH1D *hPS_A_bare = new TH1D("hPS_Bare",Form("Bare leading edge Pulseshape using Amplitude PE; T0 subtracted Time [ns]; Intensity[PE/%2.1f ns]",(max_time-min_time)/nbins), nbins, min_time,max_time);

	//T0
	double lowt0 = 800;//From raw waveform inspection
	double hight0 = 1050;
	TH1D *hT0 = new TH1D("hT0","T0 of event;Time[ns];Count",20,lowt0*0.9,hight0*1.1);

	TH2D *hTime_v_Pulse = new TH2D("hTime","",nbins,min_time,max_time,100,0,50);
	hTime_v_Pulse->SetTitle("Leading Edge Time vs Pulse Height;T0 subtracted Time [ns];Charge [aprox. PE]");

	TH2D *hTimeLastPulse = new TH2D("hTimeLastPulse","Time since last pulse vs charge",100,0,3000,100,0,10);

	LoLXChannel* chan = NULL;
  for(int iE=0;iE< numEvent;iE++)
  {
    //Loop over channels
    T->GetEvent(iE);
		double t0 = 9999.9;
		//Get t0 of the event
		for(int ic =0; ic < event->GetNHit();ic++){
			chan = event->GetChannel(ic);
			double t0_chan = chan->GetT0_Chan();
			if( t0_chan < lowt0 || t0_chan > hight0 ) continue;//Far away from trigger window
			if( t0_chan < t0 ) t0 = t0_chan;
		}
		hT0->Fill(t0);
		//cout << "T0 event = " << t0 <<endl;
    for(int iWF=0;iWF<event->GetNHit();iWF++)
    {
      chan = event->GetChannel(iWF);
      if(chan==NULL) continue;//Skip events without this channel
      int daq_chan = chan->GetChannelID();
			int type = chan->GetType(); //SiPM Type, 0==bare,1==bp,2==lp
			double baselineRMS = chan->GetBaselineRMS();
			if(baselineRMS > 20) continue;//Bad baseline
      // Fill histogram with pulses, pulse width based on time and pulse height based on amplitude
			double timelastpulse = 0.0;//time since the last pulse
      for(int iP =0; iP < chan->GetNPulses();iP++)
      {
        double pulseStart = chan->GetPulseLeadingTime(iP);//Leading edge
        double pulseHeight = chan->GetPulseHeight(iP);
        double pulseCharge = chan->GetPulseCharge(iP);
        int pulsetype = chan->GetPulseType(iP); //0 == primary pulse, 1 == sub-pulse

        if( chan->GetPulseWidth(iP) < 150.0 ) continue; // Short pulses, mostly noise
				if( pulseHeight > 3950 || pulseCharge > 1e7) continue; // clipping
				if( pulseHeight < 8 || pulseCharge < 80 ) continue; // Skip little pulses (most likely noise)
				if( pulsetype!=0 ) continue; // skip subpeaks for now (not a good idea generally, but they aren't tuned yet)

				//Rescale pulses by approximate spe
				pulseHeight/=spe_a;
				pulseCharge/=spe_q;

				if( pulseStart < min_time ) continue;//Skip early pulses
				if( pulseCharge < 0.1 || TMath::IsNaN(pulseCharge)) continue;//nan is bad
				if( TMath::IsNaN(pulseHeight)) continue;
				if( TMath::IsNaN(pulseStart)) continue;

				pulseStart-=t0;//Subtract off event t0

				hPS_A->Fill(pulseStart,pulseHeight);//Fill time, weighted by PE
				hPS_Q->Fill(pulseStart,pulseCharge);
				if(type==0){
					hPS_A_bare->Fill(pulseStart,pulseHeight);
				}else if(type==1){
					hPS_A_BP->Fill(pulseStart,pulseHeight);
				}else {
					hPS_A_LP->Fill(pulseStart,pulseHeight);
				}

				hTime_v_Pulse->Fill(pulseStart,pulseCharge);
				if(iP>0){
					hTimeLastPulse->Fill(pulseStart-timelastpulse,pulseHeight);
				}

				timelastpulse = pulseStart;
      }//End loop over pulses
    }//End loop over channels
  }//End loop over events

	TCanvas *c1 = new TCanvas("c1","Pulsehape",1200,800);
	c1->Divide(1,2);
	c1->cd(1);
	gPad->SetGrid();
	gPad->SetLogy();
	hPS_A->Draw("hist");

	c1->cd(2);
	gPad->SetLogy();
	gPad->SetGrid();
	hPS_Q->Draw("hist");

	TCanvas *c2 = new TCanvas("c2","2d plot",1200,800);
	c2->Divide(2,1);
	c2->cd(1);
	gPad->SetGrid();
	gPad->SetLogz();
	hTime_v_Pulse->Draw("colz");

	c2->cd(2);
	gPad->SetGrid();
	gPad->SetLogz();
	hTimeLastPulse->Draw("colz");

	TCanvas *c3 = new TCanvas("c3","Pulsehape",1200,800);
	c3->cd();
	gPad->SetLogy();
	gPad->SetGrid();

	hPS_A_BP->SetLineColor(kMagenta);
	hPS_A_BP->Scale(1.0/hPS_A_BP->GetMaximum());
	hPS_A_bare->SetLineColor(kBlue);
	hPS_A_bare->Scale(1.0/hPS_A_bare->GetMaximum());
	hPS_A_LP->SetLineColor(kRed);
	hPS_A_LP->Scale(1.0/hPS_A_LP->GetMaximum());

	hPS_A_bare->Draw("hist");
	hPS_A_BP->Draw("histsame");
	hPS_A_LP->Draw("histsame");

	TLegend *leg = new TLegend(0.6,0.6,0.9,0.9);
	leg->AddEntry(hPS_A_bare,"Bare SiPM PS","lpf");
	leg->AddEntry(hPS_A_BP,"BP SiPM PS","lpf");
	leg->AddEntry(hPS_A_LP,"LP SiPM PS","lpf");
	leg->SetFillStyle(3001);
	leg->Draw("same");

	gStyle->SetOptStat(0);

	TCanvas *c4 = new TCanvas("c4","",1200,800);
	gPad->SetGrid();
	hT0->Draw();



	theApp.Run();


}//End main
