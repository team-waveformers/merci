import uproot
import argparse
#from sys import argv
import json

parser = argparse.ArgumentParser()
parser.add_argument('fin',            type=str, help="path to root file")
parser.add_argument('-s','--save', action='store_true', help="save configuration")

args = parser.parse_args()

f = uproot.open(args.fin)
#f = uproot.open(argv[1])
conf = json.loads(f['config'])
print(json.dumps(conf, indent=4, sort_keys=True))

if args.save:
    print("Saving configuration to conf.json")
    with open('conf.json', 'w', encoding='utf-8') as fout:
        json.dump(conf, fout, ensure_ascii=False, indent=4)
