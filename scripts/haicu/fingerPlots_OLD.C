#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TVectorD.h>
#include <iostream>
#include <string>
#include <vector>
#include <math.h>

// to run: root -l -b -q 'fingerPlots.C("~/packages/merci/bin/root_output_files/4V_allChan_15550.root")'
// creates a file output.root, with the histograms 

void fingerPlots(const char* filename) {
    // Open the ROOT file
    TFile *file = TFile::Open(filename);
    if (!file || file->IsZombie()) {
        std::cerr << "Error opening file: " << filename << std::endl;
        return;
    }

    // Get the tree from the file
    TTree *tree = dynamic_cast<TTree*>(file->Get("TChan"));
    if (!tree) {
        std::cerr << "Error: Tree TPulse not found in file: " << filename << std::endl;
        file->Close();
        return;
    }
    
    
    std::vector<double> *roiq = nullptr;
    tree->SetBranchAddress("roiq", &roiq);

    TH1F *h_q = new TH1F("charge_hist", "Charge Finger Plot;Height;Count", 300, -10000, 100000);
    Long64_t q_entries = tree->GetEntries();
    for (Long64_t i = 0; i < q_entries; ++i) {
        tree->GetEntry(i);
        for (double val : *roiq) {
            h_q->Fill(val);
        }
    }

    std::vector<double> *roih = nullptr;
    tree->SetBranchAddress("roih", &roih);

    TH1F *h_A = new TH1F("amplitude_hist", "Amplitude Finger Plot;Height;Count", 50, 0, 200);
    TH1F *h_A_0PE = new TH1F("amplitude_hist_0PE", "0PE Finger;Height;Count", 50, -50, 100);
    Long64_t A_entries = tree->GetEntries();
    for (Long64_t i = 0; i < A_entries; ++i) {
        tree->GetEntry(i);
        for (double val : *roih) {
            h_A->Fill(val);
            h_A_0PE->Fill(val);
        }
    }
    
    double total_RMS = 0.0;
    int counter = 0;
    std::vector<double> *noise = nullptr;
    tree->SetBranchAddress("noise", &noise);

    TH1F *h_baselineRMS = new TH1F("baseline_RMS_hist", "Baseline RMS ;Baseline RMS;Count", 100, 0, 200);
    Long64_t RMS_entries = tree->GetEntries();
    for (Long64_t i = 0; i < RMS_entries; ++i) {
        tree->GetEntry(i);
        for (double val : *noise) {
            h_baselineRMS->Fill(val);
            total_RMS += val;
            counter += 1;
        }
    }
    
    TF1 *f0 = new TF1("f0", "gaus", -2000, 2000);
    TFitResultPtr r0 = h_q->Fit("f0", "SR");
    double q_err0 = r0->ParError(2);
    double q_mean0 = r0->Parameter(1);
    double q_sigma_0PE = r0->Parameter(2);
    double FWHM_0PE = q_sigma_0PE * 2 * sqrt(2*log(2));
    
    /*TF1 *f1 = new TF1("f1", "gaus", 7500, 17500);
    TFitResultPtr r1 = h_q->Fit("f1", "SR+");
    double q_err1 = r1->ParError(2);
    double q_mean1 = r1->Parameter(1);*/
    std::cout<<"0PE peak: "<<q_mean0<<" +/- "<<q_err0<<endl;
    std::cout<<"FWHM 0PE: "<<FWHM_0PE<<endl;
    //std::cout<<"1PE peak: "<<q_mean1<<" +/- "<<q_err1<<endl;
    
    /*TF1 *f1 = new TF1("f1", "gaus", -10, 10);
    TFitResultPtr r1 = h_A_0PE->Fit("f1", "SR");
    double A_err_0PE = r1->ParError(2);
    double A_mean_0PE = r1->Parameter(1);
    double A_sigma_0PE = r1->Parameter(2);
    double FWHM_0PE = A_sigma_0PE * 2 * sqrt(2*log(2));*/
    
    TF1 *f2 = new TF1("f2", "gaus", 10, 75);
    TFitResultPtr r2 = h_A->Fit("f2", "SR");
    double A_err = r2->ParError(2);
    double A_mean= r2->Parameter(1);
    double A_sigma_0PE = r2->Parameter(2);
    double FWHM_0PE_A = A_sigma_0PE * 2 * sqrt(2*log(2));
    
    std::cout<<"1PE peak: "<<A_mean<<" +/- "<<A_err<<endl;
    std::cout<<"FWHM 1PE: "<<FWHM_0PE_A<<endl;
    TCanvas *c1 = new TCanvas("c1", "0PE Amplitude Plot", 800, 600);
    c1->cd();
    h_A_0PE->Draw();
    
    //std::cout<<"0PE peak: "<<A_mean_0PE<<" +/- "<<A_err_0PE<<endl;
    //std::cout<<"FWHM 0PE: "<<FWHM_0PE<<endl;
    TCanvas *c2 = new TCanvas("c2", "Amplitude Plot", 800, 600);
    c2->cd();
    h_A->Draw();
    
    std::cout<<"Avg baseline RMS: "<<total_RMS/counter<<endl;
    TCanvas *c3 = new TCanvas("c3", "Baseline RMS Plot", 800, 600);
    c3->cd();
    h_baselineRMS->Draw();
    
    TFile *fileout = new TFile(Form("output.root"),"RECREATE");
    h_q->Write();
    h_A_0PE->Write();
    h_A->Write();
    h_baselineRMS->Write();
    fileout->Close(); 
    
}

int main(int argc, char** argv) {
    if (argc < 2) {
        std::cerr << "Usage: root -l -b -q 'fingerPlot.C(\"filename.root\")'" << std::endl;
        return 1;
    }
    gROOT->ProcessLine(Form(".x %s(\"%s\")", __FILE__, argv[1]));
    return 0;
}

