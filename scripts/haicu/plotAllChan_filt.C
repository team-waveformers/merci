//////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////// Written by Zach Charlesworth -- May 29, 2024 //////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// This script will make charge and amplitude finger plots for each channel 1-4, they will be plotted and   //
// saved to a file called finger_plt.root. It also calculates the signal to noise ratio using two methods:  //
// 1) By dividing the position of the 1PE peak by the FWHM of the 0PE peak                                  //
//    (both for charge and amplitude plots)                                                                 //
// 2) By dividing the position of the 1PE peak by the peak of baseline RMS (for amplitude only)             //
//    NOTE: the peak of the baseline RMS was used instead of the mean since the baseline RMS is noisy and   //
//          the larger values seem to be from actual pulses not noise.                                      //
// TO RUN: root -l -q 'plotAllChan_filt.C("~/packages/merci/bin/root_output_files/4V_allChan_15550.root")'  //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <TFile.h>
#include <TTree.h>
#include <TH1F.h>
#include <TCanvas.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TVectorD.h>
#include <iostream>
#include <string>
#include <vector>
#include <math.h>

void plotAllChan_filt(const char* filename) {

    TFile *file = TFile::Open(filename);
    if (!file || file->IsZombie()) {
        std::cerr << "Error opening file: " << filename << std::endl;
        return;
    }

    TTree *tpulse = dynamic_cast<TTree*>(file->Get("TPulse"));
    if (!tpulse) {
        std::cerr << "Error: Tree TChan not found in file: " << filename << std::endl;
        file->Close();
        return;
    }
    
    TH1F *h_q[4];
    h_q[0] = new TH1F("h_q_1", "Chan 1;Charge;Count", 100, 0, 2000);
    h_q[1] = new TH1F("h_q_2", "Chan 2;Charge;Count", 100, 0, 2000);
    h_q[2] = new TH1F("h_q_3", "Chan 3;Charge;Count", 100, 0, 2000);
    h_q[3] = new TH1F("h_q_4", "Chan4;Charge;Count", 100, 0, 2000);
    
    std::vector<double> *charge = nullptr;
    tpulse->SetBranchAddress("charge", &charge);

    Long64_t q_entries = tpulse->GetEntries();
    for (Long64_t i = 0; i < q_entries; ++i) {
        tpulse->GetEntry(i);
        for (double val : *charge) {
       	    if (i%4 == 0) {h_q[0]->Fill(val);}
       	    if (i%4 == 1) {h_q[1]->Fill(val);}
       	    if (i%4 == 2) {h_q[2]->Fill(val);}
       	    if (i%4 == 3) {h_q[3]->Fill(val);}
        }
    }

    TH1F *h_A[4];
    h_A[0] = new TH1F("h_A_1", "Chan 1;Amplitude;Count", 100, 0, 350);
    h_A[1] = new TH1F("h_A_2", "Chan 2;Amplitude;Count", 100, 0, 350);
    h_A[2] = new TH1F("h_A_3", "Chan 3;Amplitude;Count", 100, 0, 350);
    h_A[3] = new TH1F("h_A_4", "Chan4;Amplitude;Count", 100, 0, 350);
    
    std::vector<double> *height = nullptr;
    tpulse->SetBranchAddress("height", &height);

    Long64_t A_entries = tpulse->GetEntries();
    for (Long64_t i = 0; i < A_entries; ++i) {
        tpulse->GetEntry(i);
        for (double val : *height) {
       	    if (i%4 == 0) {h_A[0]->Fill(val);}
       	    if (i%4 == 1) {h_A[1]->Fill(val);}
       	    if (i%4 == 2) {h_A[2]->Fill(val);}
       	    if (i%4 == 3) {h_A[3]->Fill(val);}
        }
    }
     
    // Fitting 0PE peaks for charge histograms
    int lowq_0PE = 150;
    int highq_0PE = 300;
     
    int lowq_1PE = 550;
    int highq_1PE = 750;
    
    int lowh_1PE = 70;
    int highh_1PE = 135;
    
    int lowh_0PE = 7;
    int highh_0PE = 18;
     
    //int lowh_1PE = 50;
    //int highh_1PE = 80;
     
    TF1 *fq1 = new TF1("fq1", "gaus", lowq_0PE, highq_0PE);
    TFitResultPtr rq1 = h_q[0]->Fit("fq1", "SR");
    double q_err1 = rq1->ParError(1);
    double q_mean1 = rq1->Parameter(1);
    double q_sigma1 = rq1->Parameter(2);
    double q_sigma1_err = rq1->ParError(2);
    double FWHM1_0PE = q_sigma1 * 2 * sqrt(2*log(2));
    double FWHM1_0PE_err = q_sigma1_err * 2 * sqrt(2*log(2));

    TF1 *fq2 = new TF1("fq2", "gaus", lowq_0PE, highq_0PE);
    TFitResultPtr rq2 = h_q[1]->Fit("fq2", "SR");
    double q_err2 = rq2->ParError(1);
    double q_mean2 = rq2->Parameter(1);
    double q_sigma2 = rq2->Parameter(2);
    double q_sigma2_err = rq2->ParError(2);
    double FWHM2_0PE = q_sigma2 * 2 * sqrt(2*log(2));
    double FWHM2_0PE_err = q_sigma2_err * 2 * sqrt(2*log(2));
    
    TF1 *fq3 = new TF1("fq3", "gaus", lowq_0PE, highq_0PE);
    TFitResultPtr rq3 = h_q[2]->Fit("fq3", "SR");
    double q_err3 = rq3->ParError(1);
    double q_mean3 = rq3->Parameter(1);
    double q_sigma3 = rq3->Parameter(2);
    double q_sigma3_err = rq3->ParError(2);
    double FWHM3_0PE = q_sigma3 * 2 * sqrt(2*log(2));
    double FWHM3_0PE_err = q_sigma3_err * 2 * sqrt(2*log(2));
    
    TF1 *fq4 = new TF1("fq4", "gaus", lowq_0PE, highq_0PE);
    TFitResultPtr rq4 = h_q[3]->Fit("fq4", "SR");
    double q_err4 = rq4->ParError(1);
    double q_mean4 = rq4->Parameter(1);
    double q_sigma4 = rq4->Parameter(2);
    double q_sigma4_err = rq4->ParError(2);
    double FWHM4_0PE = q_sigma4 * 2 * sqrt(2*log(2));
    double FWHM4_0PE_err = q_sigma4_err * 2 * sqrt(2*log(2));
    
    // Fitting 0PE peaks for height histograms
    TF1 *fA1 = new TF1("fA1", "gaus", lowh_0PE, highh_0PE);
    TFitResultPtr rA1 = h_A[0]->Fit("fA1", "SR");
    double A_err1 = rA1->ParError(1);
    double A_mean1 = rA1->Parameter(1);
    double A_sigma1 = rA1->Parameter(2);
    double A_sigma1_err = rA1->ParError(2);
    double FWHM1_0PE_A = A_sigma1 * 2 * sqrt(2*log(2));
    double FWHM1_0PE_A_err = A_sigma1_err * 2 * sqrt(2*log(2));
    
    TF1 *fA2 = new TF1("fA2", "gaus", lowh_0PE, highh_0PE);
    TFitResultPtr rA2 = h_A[1]->Fit("fA2", "SR");
    double A_err2 = rA2->ParError(1);
    double A_mean2 = rA2->Parameter(1);
    double A_sigma2 = rA2->Parameter(2);
    double A_sigma2_err = rA2->ParError(2);
    double FWHM2_0PE_A = A_sigma2 * 2 * sqrt(2*log(2));
    double FWHM2_0PE_A_err = A_sigma2_err * 2 * sqrt(2*log(2));
    
    TF1 *fA3 = new TF1("fA3", "gaus", lowh_0PE, highh_0PE);
    TFitResultPtr rA3 = h_A[2]->Fit("fA3", "SR");
    double A_err3 = rA3->ParError(1);
    double A_mean3 = rA3->Parameter(1);
    double A_sigma3 = rA3->Parameter(2);
    double A_sigma3_err = rA3->ParError(2);
    double FWHM3_0PE_A = A_sigma3 * 2 * sqrt(2*log(2));
    double FWHM3_0PE_A_err = A_sigma3_err * 2 * sqrt(2*log(2));
    
    TF1 *fA4 = new TF1("fA4", "gaus", lowh_0PE, highh_0PE);
    TFitResultPtr rA4 = h_A[3]->Fit("fA4", "SR");
    double A_err4 = rA4->ParError(1);
    double A_mean4 = rA4->Parameter(1);
    double A_sigma4 = rA4->Parameter(2);
    double A_sigma4_err = rA4->ParError(2);
    double FWHM4_0PE_A = A_sigma4 * 2 * sqrt(2*log(2));
    double FWHM4_0PE_A_err = A_sigma4_err * 2 * sqrt(2*log(2));
    
    // Fitting 1PE peaks for charge histograms
    TF1 *fq1_1PE = new TF1("fq1_1PE", "gaus", lowq_1PE, highq_1PE);
    TFitResultPtr rq1_1PE = h_q[0]->Fit("fq1_1PE", "SR+");
    double q_err1_1PE = rq1_1PE->ParError(1);
    double q_mean1_1PE = rq1_1PE->Parameter(1);
    double q_sigma1_1PE = rq1_1PE->Parameter(2);
    double q_sigma1_1PE_err = rq1_1PE->ParError(2);
    double FWHM1_1PE = q_sigma1_1PE * 2 * sqrt(2*log(2));
    double FWHM1_1PE_err = q_sigma1_1PE_err * 2 * sqrt(2*log(2));
    
    TF1 *fq2_1PE = new TF1("fq2_1PE", "gaus", lowq_1PE, highq_1PE);
    TFitResultPtr rq2_1PE = h_q[1]->Fit("fq2_1PE", "SR+");
    double q_err2_1PE = rq2_1PE->ParError(1);
    double q_mean2_1PE = rq2_1PE->Parameter(1);
    double q_sigma2_1PE = rq2_1PE->Parameter(2);
    double q_sigma2_1PE_err = rq2_1PE->ParError(2);
    double FWHM2_1PE = q_sigma2_1PE * 2 * sqrt(2*log(2));
    double FWHM2_1PE_err = q_sigma2_1PE_err * 2 * sqrt(2*log(2));
    
    TF1 *fq3_1PE = new TF1("fq3_1PE", "gaus", lowq_1PE, highq_1PE);
    TFitResultPtr rq3_1PE = h_q[2]->Fit("fq3_1PE", "SR+");
    double q_err3_1PE = rq3_1PE->ParError(1);
    double q_mean3_1PE = rq3_1PE->Parameter(1);
    double q_sigma3_1PE = rq3_1PE->Parameter(2);
    double q_sigma3_1PE_err = rq3_1PE->ParError(2);
    double FWHM3_1PE = q_sigma3_1PE * 2 * sqrt(2*log(2));
    double FWHM3_1PE_err = q_sigma3_1PE_err * 2 * sqrt(2*log(2));
    
    TF1 *fq4_1PE = new TF1("fq4_1PE", "gaus", lowq_1PE, highq_1PE);
    TFitResultPtr rq4_1PE = h_q[3]->Fit("fq4_1PE", "SR+");
    double q_err4_1PE = rq4_1PE->ParError(1);
    double q_mean4_1PE = rq4_1PE->Parameter(1);
    double q_sigma4_1PE = rq4_1PE->Parameter(2);
    double q_sigma4_1PE_err = rq4_1PE->ParError(2);
    double FWHM4_1PE = q_sigma4_1PE * 2 * sqrt(2*log(2));
    double FWHM4_1PE_err = q_sigma4_1PE_err * 2 * sqrt(2*log(2));
    
    // Fitting 1PE peaks for height histograms
    TF1 *fA1_1PE = new TF1("fA1_1PE", "gaus", lowh_1PE, highh_1PE);
    TFitResultPtr rA1_1PE = h_A[0]->Fit("fA1_1PE", "SR+");
    double A_err1_1PE = rA1_1PE->ParError(1);
    double A_mean1_1PE = rA1_1PE->Parameter(1);
    double A_sigma1_1PE = rA1_1PE->Parameter(2);
    double A_sigma1_1PE_err = rA1_1PE->ParError(2);
    double FWHM1_1PE_A = A_sigma1_1PE * 2 * sqrt(2*log(2));
    double FWHM1_1PE_A_err = A_sigma1_1PE_err * 2 * sqrt(2*log(2));
    
    TF1 *fA2_1PE = new TF1("fA2_1PE", "gaus", lowh_1PE, highh_1PE);
    TFitResultPtr rA2_1PE = h_A[1]->Fit("fA2_1PE", "SR+");
    double A_err2_1PE = rA2_1PE->ParError(1);
    double A_mean2_1PE = rA2_1PE->Parameter(1);
    double A_sigma2_1PE = rA2_1PE->Parameter(2);
    double A_sigma2_1PE_err = rA2_1PE->ParError(2);
    double FWHM2_1PE_A = A_sigma2_1PE * 2 * sqrt(2*log(2));
    double FWHM2_1PE_A_err = A_sigma2_1PE_err * 2 * sqrt(2*log(2));
    
    TF1 *fA3_1PE = new TF1("fA3_1PE", "gaus", lowh_1PE, highh_1PE);
    TFitResultPtr rA3_1PE = h_A[2]->Fit("fA3_1PE", "SR+");
    double A_err3_1PE = rA3_1PE->ParError(1);
    double A_mean3_1PE = rA3_1PE->Parameter(1);
    double A_sigma3_1PE = rA3_1PE->Parameter(2);
    double A_sigma3_1PE_err = rA3_1PE->ParError(2);
    double FWHM3_1PE_A= A_sigma3_1PE * 2 * sqrt(2*log(2));
    double FWHM3_1PE_A_err = A_sigma3_1PE_err * 2 * sqrt(2*log(2));
    
    TF1 *fA4_1PE = new TF1("fA4_1PE", "gaus", lowh_1PE, highh_1PE);
    TFitResultPtr rA4_1PE = h_A[3]->Fit("fA4_1PE", "SR+");
    double A_err4_1PE = rA4_1PE->ParError(1);
    double A_mean4_1PE = rA4_1PE->Parameter(1);
    double A_sigma4_1PE = rA4_1PE->Parameter(2);
    double A_sigma4_1PE_err = rA4_1PE->ParError(2);
    double FWHM4_1PE_A = A_sigma4_1PE * 2 * sqrt(2*log(2));
    double FWHM4_1PE_A_err = A_sigma4_1PE_err * 2 * sqrt(2*log(2));

    // printing fit results to the screen
    std::cout<<"CHARGE PLOTS:"<<endl;
    std::cout<<"CHAN 1		0PE peak: "<<q_mean1<<" +/- "<<q_err1<<",	1PE peak: "<<q_mean1_1PE<<" +/- "<<q_err1_1PE<<",	Signal to noise ratio [FWHM]: "<<q_mean1_1PE/FWHM1_0PE<<" +/- "<<(q_mean1_1PE/FWHM1_0PE)*sqrt((q_err1_1PE/q_mean1_1PE)*(q_err1_1PE/q_mean1_1PE) + (FWHM1_0PE_err/FWHM1_0PE)*(FWHM1_0PE_err/FWHM1_0PE))<<endl;
    std::cout<<"CHAN 2		0PE peak: "<<q_mean2<<" +/- "<<q_err2<<",	1PE peak: "<<q_mean2_1PE<<" +/- "<<q_err2_1PE<<",	Signal to noise ratio [FWHM]: "<<q_mean2_1PE/FWHM2_0PE<<" +/- "<<(q_mean2_1PE/FWHM2_0PE)*sqrt((q_err2_1PE/q_mean2_1PE)*(q_err2_1PE/q_mean2_1PE) + (FWHM2_0PE_err/FWHM2_0PE)*(FWHM2_0PE_err/FWHM2_0PE))<<endl;
    std::cout<<"CHAN 3		0PE peak: "<<q_mean3<<" +/- "<<q_err3<<",	1PE peak: "<<q_mean3_1PE<<" +/- "<<q_err3_1PE<<",	Signal to noise ratio [FWHM]: "<<q_mean3_1PE/FWHM3_0PE<<" +/- "<<(q_mean3_1PE/FWHM3_0PE)*sqrt((q_err3_1PE/q_mean3_1PE)*(q_err3_1PE/q_mean3_1PE) + (FWHM3_0PE_err/FWHM3_0PE)*(FWHM3_0PE_err/FWHM3_0PE))<<endl;
    std::cout<<"CHAN 4		0PE peak: "<<q_mean4<<" +/- "<<q_err4<<",	1PE peak: "<<q_mean4_1PE<<" +/- "<<q_err4_1PE<<",	Signal to noise ratio [FWHM]: "<<q_mean4_1PE/FWHM4_0PE<<" +/- "<<(q_mean4_1PE/FWHM4_0PE)*sqrt((q_err4_1PE/q_mean4_1PE)*(q_err4_1PE/q_mean4_1PE) + (FWHM4_0PE_err/FWHM4_0PE)*(FWHM4_0PE_err/FWHM4_0PE))<<endl;

    std::cout<<"AMPLITUDE PLOTS:"<<endl;
    std::cout<<"CHAN 1		0PE peak: "<<A_mean1<<" +/- "<<A_err1<<",	1PE peak: "<<A_mean1_1PE<<" +/- "<<A_err1_1PE<<",	Signal to noise ratio [FWHM]: "<<A_mean1_1PE/FWHM1_0PE_A<<" +/- "<<(A_mean1_1PE/FWHM1_0PE_A)*sqrt((A_err1_1PE/A_mean1_1PE)*(A_err1_1PE/A_mean1_1PE) + (FWHM1_0PE_A_err/FWHM1_0PE_A)*(FWHM1_0PE_A_err/FWHM1_0PE_A))<<endl;
    std::cout<<"CHAN 2		0PE peak: "<<A_mean2<<" +/- "<<A_err2<<",	1PE peak: "<<A_mean2_1PE<<" +/- "<<A_err2_1PE<<",	Signal to noise ratio [FWHM]: "<<A_mean2_1PE/FWHM2_0PE_A<<" +/- "<<(A_mean2_1PE/FWHM2_0PE_A)*sqrt((A_err2_1PE/A_mean2_1PE)*(A_err2_1PE/A_mean2_1PE) + (FWHM2_0PE_A_err/FWHM2_0PE_A)*(FWHM2_0PE_A_err/FWHM2_0PE_A))<<endl;
    std::cout<<"CHAN 3		0PE peak: "<<A_mean3<<" +/- "<<A_err3<<",	1PE peak: "<<A_mean3_1PE<<" +/- "<<A_err3_1PE<<",	Signal to noise ratio [FWHM]: "<<A_mean3_1PE/FWHM3_0PE_A<<" +/- "<<(A_mean3_1PE/FWHM3_0PE_A)*sqrt((A_err3_1PE/A_mean3_1PE)*(A_err3_1PE/A_mean3_1PE) + (FWHM3_0PE_A_err/FWHM3_0PE_A)*(FWHM3_0PE_A_err/FWHM3_0PE_A))<<endl;
    std::cout<<"CHAN 4		0PE peak: "<<A_mean4<<" +/- "<<A_err4<<",	1PE peak: "<<A_mean4_1PE<<" +/- "<<A_err4_1PE<<",	Signal to noise ratio [FWHM]: "<<A_mean4_1PE/FWHM4_0PE_A<<" +/- "<<(A_mean4_1PE/FWHM4_0PE_A)*sqrt((A_err4_1PE/A_mean4_1PE)*(A_err4_1PE/A_mean4_1PE) + (FWHM4_0PE_A_err/FWHM4_0PE_A)*(FWHM4_0PE_A_err/FWHM4_0PE_A))<<endl;

    
    TFile *fileout = new TFile(Form("finger_plt.root"),"RECREATE");
    for (int i = 0; i<4; i++){
    h_q[i]->Write();
    h_A[i]->Write();
    }
    fileout->Close(); 
    
}
