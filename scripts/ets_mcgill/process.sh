#!/usr/bin/sh
# EG to use bash process.sh runNum subRunNum configName numEvents

source ~/.bash_profile
cd /home/ets/packages/merci/bin/

run="$1"
subrun="$2"
config="$3"
events="$4"


input=$(printf "/data/midasFiles/CF/run%05d_%03d.mid.lz4" $run $subrun)
output=$(printf "/data/merciOutput/CF/ets_%05d_%03d.root" $run $subrun)
echo "Reading $input ..."

if [ -z "$events" ]
then
      echo "\$events is empty"
      events="0"
      echo "Processing all events, writing to $output"
else
      echo "Processing " $events " events, writing to $output"
fi

./etsana_offline.exe $input -e$events -O$output -- --conf $config
