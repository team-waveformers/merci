##############
# Example script for plotting PE from MERCI Output
# Author: David Gallacher
##############


import uproot as up
import numpy as np
import pandas as pd
import boost_histogram as bh
import os
import matplotlib.pyplot as plt
import pickle

#Name of file, change to use another file
filename = "~/packages/merci/bin/Test_ntp.root"
with up.open(filename) as file:
	print(file.classnames())


#Now let's print the leaves as well.
#Get the tree
treename = filename+":ntp"
tree  = up.open(treename)
print("TTree 'ntp' contains the following keys:")
tree.show()


#Lets put one of our branches ('pe') into a numpy array to work with

pe = tree['nPE'].array(library="np") #Get the branch and cast it to a np array
# Now let's make a histogram of pe
nbins = 100
lowbin = 171500
highbin = 173000

#Make the histogram with our custom binning and ranges
hist_npe = bh.Histogram(bh.axis.Regular(nbins,lowbin,highbin))
#Fill the histogram from our np array
hist_npe.fill(pe)


#Repeat for amplitude estimate
ape = tree['aPE'].array(library="np") #Get the branch and cast it to a np array

nbins = 80
lowbin_amp = 14200
highbin_amp = 14280

#Make the histogram with our custom binning and ranges
hist_ape = bh.Histogram(bh.axis.Regular(nbins,lowbin_amp,highbin_amp))
#Fill the histogram from our np array
hist_ape.fill(ape)


#Display the plot, boost_histogram is powerful for making and storing histograms but doesn't include display
#so we use matplotlib to display as a bar chart

fig, axes = plt.subplots(1,2,figsize=(12,5))
axes[0].bar(hist_npe.axes[0].centers,hist_npe.view(),width=hist_npe.axes[0].widths)
axes[0].set(ylabel="Counts",xlabel="QPE")
axes[0].grid()
axes[0].set_title("Charge based PE MERCI Output")



axes[1].bar(hist_ape.axes[0].centers,hist_ape.view(),width=hist_ape.axes[0].widths)
axes[1].set(ylabel="Counts",xlabel="APE")
axes[1].grid()
axes[1].set_title("Amplitude based PE MERCI Output")

plt.savefig('plotPE.png')

#Display plots last
plt.show()


#Now let's save the plots as pickled files so we can get the histograms later without parsing the root file again
pdictionary = {'hist_npe':hist_npe,'hist_ape':hist_npe} #Save them as a dictionary so we can access them by name
with open("samplePlots.pkl", "wb") as f:
    pickle.dump(pdictionary, f)
