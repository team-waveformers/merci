#!/bin/bash


WORKDIR="$HOME/dsana_merci"
cd $WORKDIR/bin

#./dsanana.exe --mt $DATADIR/dsna/setup2/run01010/run01010sub*.mid -O$DATADIR/dsnaroot/setup2/dsrun1010_MA64_3sigma_50ToT.root -- --conf ../dsadc/config/pduplus.json |& tee ../RunLogs/dsRun1010.log

#./dsanana.exe --mt $DATADIR/dsna/setup2/run01010/run01010sub*.mid -O$DATADIR/dsnaroot/setup2/dsrun1010_MA64_3sigma_10ToT.root -- --conf ../dsadc/config/pduplus.json |& tee ../RunLogs/dsRun1010.log

#./dsanana.exe --mt $DATADIR/dsna/setup2/run01010/run01010sub*.mid -O$DATADIR/dsnaroot/setup2/dsrun1010_MA64_100fix_50ToT.root -- --conf ../dsadc/config/pduplus_fixed.json |& tee ../../RunLogs/dsRun1010_MA64_100fix_50ToT.log

#./dsanana.exe --mt $DATADIR/dsna/setup2/run01010/run01010sub*.mid -O$DATADIR/dsnaroot/setup2/dsrun1010_MA64_100fix_100ToT.root -- --conf ../dsadc/config/pduplus_fixed.json &> ../RunLogs/dsRun1010_MA64_100fix_100ToT.log &

#./dsanana.exe --mt $DATADIR/dsna/setup2/run01010/run01010sub*.mid -O$DATADIR/dsnaroot/setup2/dsrun1010_MA64_50fix_50ToT.root -- --conf ../dsadc/config/pduplus_fixed.json &> ../RunLogs/dsRun1010_MA64_500fix_50ToT.log &

#./dsanana.exe --mt $DATADIR/dsna/setup2/run01010/run01010sub*.mid -O$DATADIR/dsnaroot/setup2/dsrun1010_MA64_50fix_50ToT_new.root -- --conf ../dsadc/config/pduplus_fixed.json &> ../RunLogs/dsRun1010_MA64_500fix_50ToT_new.log &

#dsanana.exe --mt $DATADIR/dsna/setup2/run01010/run01010sub*.mid -O$DATADIR/dsnaroot/setup2/dsrun1010_AR_3sigma_MA300_Q20.root -- --conf $WORKDIR/dsadc/config/ar_hitf.json &> $WORKDIR/RunLogs/dsRun1010_AR_3sigma_MA300_Q20.log &
dsanana.exe --mt $DATADIR/dsna/setup2/run01010/run01010sub*.mid -O$DATADIR/dsnaroot/setup2/dsrun1010_AR_3sigma_MA300_Q20_fit3.root -- --conf $WORKDIR/dsadc/config/ar_hitf.json |& tee $WORKDIR/RunLogs/dsRun1010_AR_3sigma_MA300_Q20_fit.log
#dsanana.exe -e100 $DATADIR/dsna/setup2/run01010/run01010sub*.mid -O$DATADIR/dsnaroot/setup2/dsrun1010plot.root -- --conf $WORKDIR/dsadc/config/ar_hitf.json --verbose &> $WORKDIR/RunLogs/dsRun1010plot.log

cd -
