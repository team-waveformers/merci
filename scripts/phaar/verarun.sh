#!/bin/bash

cd $HOME/dsana_merci/bin


./phaarana.exe --mt -D$DATADIR/vera /daq/daqstore/vera/data2/run01105.mid.gz -- --conf $HOME/dsana_merci/phaar/config/vera.json &> $HOME/dsana_merci/RunLogs/veraRun1105.log &
./phaarana.exe --mt -D$DATADIR/vera /daq/daqstore/vera/data2/run01106.mid.gz -- --conf $HOME/dsana_merci/phaar/config/vera.json &> $HOME/dsana_merci/RunLogs/veraRun1106.log &
./phaarana.exe --mt -D$DATADIR/vera /daq/daqstore/vera/data2/run01107.mid.gz -- --conf $HOME/dsana_merci/phaar/config/vera.json &> $HOME/dsana_merci/RunLogs/veraRun1107.log &
./phaarana.exe --mt -D$DATADIR/vera /daq/daqstore/vera/data2/run01108.mid.gz -- --conf $HOME/dsana_merci/phaar/config/vera.json &> $HOME/dsana_merci/RunLogs/veraRun1108.log &


./phaarana.exe --mt -O$DATADIR/vera/veraR1105.root /daq/daqstore/vera/data2/run01105.mid.gz -- --conf $HOME/dsana_merci/phaar/config/verafix.json &> $HOME/dsana_merci/RunLogs/veraRun1105_fix.log &
./phaarana.exe --mt -O$DATADIR/vera/veraR1106.root /daq/daqstore/vera/data2/run01106.mid.gz -- --conf $HOME/dsana_merci/phaar/config/verafix.json &> $HOME/dsana_merci/RunLogs/veraRun1106_fix.log &
./phaarana.exe --mt -O$DATADIR/vera/veraR1107.root /daq/daqstore/vera/data2/run01107.mid.gz -- --conf $HOME/dsana_merci/phaar/config/verafix.json &> $HOME/dsana_merci/RunLogs/veraRun1107_fix.log &
./phaarana.exe --mt -O$DATADIR/vera/veraR1108.root /daq/daqstore/vera/data2/run01108.mid.gz -- --conf $HOME/dsana_merci/phaar/config/verafix.json &> $HOME/dsana_merci/RunLogs/veraRun1108_fix.log &





