#!/usr/bin/python3

import json
import argparse


parser = argparse.ArgumentParser()
parser.add_argument('fin',            type=argparse.FileType('r'), default='master.json')
parser.add_argument('--keys',   '-k', nargs='+',                   default=['Filter','Par0'])
parser.add_argument('--newval', '-w', type=int,                    default=100)
parser.add_argument('--fout',   '-o', type=argparse.FileType('w'), default='matest.json')
args = parser.parse_args()

# returns JSON object as a dictionary
data = json.load(args.fin)

# update the entry
data[args.keys[0]][args.keys[1]]=args.newval

# write to disk
json.dump(data, args.fout, indent=4)
