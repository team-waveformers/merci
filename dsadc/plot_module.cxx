/******************************************************************
 * Plot Waveforms *
 * 
 * A. Capra
 * August 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "adcflow.hxx"
#include "dsflow.hxx"
#include "dsdata.hxx"
#include "avgspe.hxx"

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <fstream>

#include "TCanvas.h"
#include "TH1D.h"
#include "TLine.h"
#include "TPolyMarker.h"

#ifdef EXPORT_ASCII
#include <sys/stat.h>
#endif

#include "json.hpp"
using json = nlohmann::json;

class PlotFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};
  

class PlotModule: public TARunObject
{
public:
   PlotFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors
   
   bool fEnabled; // enable canvases creation
   int fChannel;  // special channel to plot separately

   bool fSave; // save PDFs of canvases
   int fSaveEvents; // limit of events to save
   
   bool fPersistency; // enable summation of waveforms
   int fPersistencyChannel; // visualize a special channel
   int fPersistencyEvents; // terminate the summation at this event
 
   TCanvas* fC; // the canvas for the special channel
   TCanvas* fCA; // the canvas for the first 16 channels
   std::map<int,TH1D*> fhwf; // raw waveforms
   double fRawWFmin;
   double fRawWFmax;
   std::map<int,TH1D*> fhwfb; // baseline subtracted waveforms
   std::map<int,TH1D*> fhwff; // baseline subtracted filtered waveforms
   std::map<int,TH1D*> fhpulse; // found pulse location and size
   std::map<int,TH1D*> ffpulse; // fit pulse

   std::map<int,TH1D*> fhROIheight; // the pulse height in the Region-Of-Interest
   std::map<int,TH1D*> fhROIcharge; // the charge in the Region-Of-Interest

   bool fFoundPulse;
   bool fGoodFit;

   TDirectory* fPercyDir;
   TCanvas* fCP; // the canvas for the persistency plot for the selected channel
   std::map<int,TH1D*> fhPersistency; // the persistency histograms

   int fROImin;
   int fROImaxq;
   int fROImaxp;
   TLine* fROIlow; // line to identify the lower bound of the ROI in the plots
   TLine* fROIupq; // line to identify the upper bound of the ROI in the plots
   TLine* fROIupp; // line to identify the upper bound of the ROI in the plots

   int ffROImin;
   int ffROImaxq;
   int ffROImaxp;
   TLine* ffROIlow; // line to identify the lower bound of the ROI in the plots
   TLine* ffROIupq; // line to identify the upper bound of the ROI in the plots
   TLine* ffROIupp; // line to identify the upper bound of the ROI in the plots

#ifdef _HIT_FINDER_
   std::map<int,TPolyMarker*> fpm;
#endif

#ifdef EXPORT_ASCII
   std::ofstream fout;   // this feature can be only be enabled from cmake
   std::string fdirname; // save ASCII WF to this directory
#endif

   std::map<int,TCanvas*> fCraw; // the canvases for the raw waveforms
   std::map<int,TCanvas*> fCROI; // the canvases for the ROI plots

public:
   PlotModule(TARunInfo* runinfo, PlotFlags* f): TARunObject(runinfo),fFlags(f),
                                                 fCounter(0),fError(0),
                                                 fEnabled(false),fSave(false),
                                                 fPersistency(false),
                                                 fC(0),fCA(0),
                                                 fPercyDir(0),fCP(0),
                                                 fROImin(-1),
                                                 fROImaxq(-1),fROImaxp(-1),
                                                 ffROImin(-1),
                                                 ffROImaxq(-1),ffROImaxp(-1)
   {
      if(fFlags->fVerbose) std::cout<<"PlotModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Plot";
#endif

      fhwf.clear();
      fhwfb.clear();
      fhwff.clear();
      fhpulse.clear();
      ffpulse.clear();
      fhROIheight.clear();
      fhROIcharge.clear();
      fhPersistency.clear();
      fCraw.clear();
      fCROI.clear();
 
      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"PlotModule Json parsing success!"<<std::endl;
      fin.close();

      fEnabled=settings["Plot"]["Canvas"].get<bool>();
      fChannel=settings["Plot"]["Channel"].get<int>();
      fSave=settings["Plot"]["Save PDF"].get<bool>();
      fSaveEvents=settings["Plot"]["Save Events Limit"].get<int>();
      
      try {
      fROImin=settings["ROI"]["min"].get<int>();
      fROImaxq=settings["ROI"]["max charge"].get<int>();
      fROImaxp=settings["ROI"]["max peak"].get<int>();
      } catch(json::out_of_range& e) {
         std::cerr<<"PlotModule::PlotModule(...) ROI bounds: "<<e.what()<<std::endl;
      }

      try {
         ffROImin=settings["ROI"]["filter min"].get<int>();
         ffROImaxq=settings["ROI"]["filter max charge"].get<int>();
         ffROImaxp=settings["ROI"]["filter max peak"].get<int>();
      } catch(json::out_of_range& e) {
         std::cerr<<"PlotModule::PlotModule(...) Filtered ROI bounds: "<<e.what()<<std::endl;
      }
      

      fPersistency=settings["Plot"]["Persistency"].get<bool>();
      fPersistencyChannel=settings["Plot"]["Persistency Channel"].get<int>();
      fPersistencyEvents=settings["Plot"]["Persistency Events Limit"].get<int>();

      fRawWFmin=settings["Plot"]["Raw WF Minimum"].get<double>();
      fRawWFmax=settings["Plot"]["Raw WF Maximum"].get<double>();

      // decoration for ROI in raw waveform
      fROIlow = new TLine(fROImin,-1.,fROImin,1.);  // lower limit: black solid line
      fROIlow->SetLineColor(1);  fROIlow->SetLineStyle(1);
      fROIupp = new TLine(fROImaxp,-1.,fROImaxp,1.);// upper limit ph: black fine-dash line
      fROIupp->SetLineColor(1);  fROIupp->SetLineStyle(7);
      fROIupq = new TLine(fROImaxq,-1.,fROImaxq,1.);// upper limit charge: black long-dash line
      fROIupq->SetLineColor(1);  fROIupq->SetLineStyle(9);

      // decoration for ROI in filtered waveform
      ffROIlow = new TLine(ffROImin,-1.,ffROImin,1.);  // lower limit: blue solid line
      ffROIlow->SetLineColor(9);  ffROIlow->SetLineStyle(1); 
      ffROIupp = new TLine(ffROImaxp,-1.,ffROImaxp,1.);// upper limit ph: blue fine-dash line
      ffROIupp->SetLineColor(9);  ffROIupp->SetLineStyle(7);
      ffROIupq = new TLine(ffROImaxq,-1.,ffROImaxq,1.);// upper limit charge: blue long-dash line
      ffROIupq->SetLineColor(9);  ffROIupq->SetLineStyle(9);

#ifdef _HIT_FINDER_
   fpm.clear();
#endif
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty())
         std::cout<<"PlotModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"PlotModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
      
      if( fEnabled )
         {
            int runn=runinfo->fRunNo;
            
            std::string cname="Plot_ADC_channel"+std::to_string(fChannel)
               +"_Run"+std::to_string(runn);
            std::string ctitle="Plot ADC channel "+std::to_string(fChannel)
               +" Run "+std::to_string(runn);
            fC=new TCanvas(cname.c_str(),ctitle.c_str(),1000,750);
            fC->ToggleEventStatus();

            cname="Plot_ADC_Run"+std::to_string(runn)+"_First16Channels";
            ctitle="Plot ADC Run "+std::to_string(runn);
            fCA = new TCanvas(cname.c_str(),ctitle.c_str(),2000,1500);
            fCA->ToggleEventStatus();
            fCA->Divide(4,4);
         }

      if(fPersistency)
         {
            std::string cname="cPersistencyR"+std::to_string(runinfo->fRunNo)+"CH"+std::to_string(fPersistencyChannel);
            fCP = new TCanvas(cname.c_str(),cname.c_str(),900,700);
            if( runinfo->fFileName.empty() )
               {
                  gDirectory->cd("RootApp:/");
                  fPercyDir = new TDirectory("Persistency", "Persistency Histograms");
               }
            else
               {
                  runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
                  fPercyDir=runinfo->fRoot->fOutputFile->mkdir("Persistency");
               }
         }
#ifdef EXPORT_ASCII
      fdirname="./WFASCIIR"+std::to_string(runinfo->fRunNo);
      int status = mkdir(fdirname.c_str(),0777);
      if(status==-1)
         std::cout<<"PlotModule::BeginRun Cannot create directory "<<fdirname<<std::endl;
      else
         std::cout<<"PlotModule::BeginRun "<<fdirname<<" succesfully created"<<std::endl;
#endif
   }

   void EndRun(TARunInfo* runinfo)
   {
      for(auto it=fhwf.begin();it!=fhwf.end();++it)
         if(it->second) delete it->second;
      fhwf.clear();
      for(auto it=fhwfb.begin();it!=fhwfb.end();++it)
         if(it->second) delete it->second;
      fhwfb.clear();
      for(auto it=fhwff.begin();it!=fhwff.end();++it)
         if(it->second) delete it->second;
      fhwff.clear();
      for(auto it=fhpulse.begin();it!=fhpulse.end();++it)
         if(it->second) delete it->second;
      fhpulse.clear();
      for(auto it=ffpulse.begin();it!=ffpulse.end();++it)
         if(it->second) delete it->second;
      ffpulse.clear();
   
      if( fEnabled )
         {
            if(fROIlow) delete fROIlow;
            if(fROIupp) delete fROIupp;
            if(fROIupq) delete fROIupq;
            fROIlow=fROIupp=fROIupq=0;

            if(ffROIlow) delete ffROIlow;
            if(ffROIupp) delete ffROIupp;
            if(ffROIupq) delete ffROIupq;
            ffROIlow=ffROIupp=ffROIupq=0;
            
            if(fC) delete fC;
            fC=0;
            if(fCA) delete fCA;
            fCA=0;
            
            for(auto it=fCraw.begin(); it!=fCraw.end(); ++it)
               if(it->second) delete it->second;
            fCraw.clear();
            
            for(auto it=fCROI.begin(); it!=fCROI.end(); ++it)
               if(it->second) delete it->second;
            fCROI.clear();
         }
      
   
      if(fPersistency)
         {
            fCP->SaveAs(".pdf");
            delete fCP;
            if( runinfo->fFileName.empty() )
               {
                  gDirectory->cd("RootApp:/");
                  if( fPercyDir ) delete fPercyDir;
               }
         }
      std::cout<<"PlotModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      ResetHisto();

      AdcEventFlow* adc_flow = flow->Find<AdcEventFlow>();
      if( !adc_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow ) 
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      
      int NumberOfChannels = adc_flow->getNchannels();
      fFoundPulse=fGoodFit=false;
      TDirectory* dir = runinfo->fRoot->fgDir;    
      for(int ich=0; ich<NumberOfChannels; ++ich)
         {
            const TRawChannel* adc_raw = adc_flow->getChannel(ich);
            if( adc_raw->IsEmpty() ) continue;
            const TDSChannel* dschan = wf_flow->GetDSchan(ich);
            dir->cd();// RootApp:/manalyzer  
            PlotChannel(adc_raw,dschan);
            if( fPersistency && (fCounter < fPersistencyEvents) )
               {
                  if( runinfo->fgFileList.size() )
                     runinfo->fRoot->fOutputFile->cd(); // <filename>:/
                  else
                     gDirectory->cd("RootApp:/");
                  fPercyDir->cd();
                  PersistencyPlot(dschan);
               }
         }

      if( fFlags->fVerbose )
         std::cout<<"PlotModule::AnalyzeFlowEvent Plotting "<<wf_flow->GetNumberOfPulses()
                  <<" pulse(s) in "<<NumberOfChannels<<" ("<<wf_flow->GetNumberOfChannels()<<") channel(s)"
                  <<" for "<<fhpulse.size()<<" plots"<<std::endl;
      double max_height=200., plot_height=200.;
      for( int ip=0; ip<wf_flow->GetNumberOfPulses(); ++ip)
         {
            const TDSPulse* dsp = wf_flow->GetDSpulse(ip);
            if( fFlags->fVerbose && 0 ) dsp->Print();
            PlotPulse(dsp);
            max_height=max_height>dsp->height?max_height:dsp->height;
            if( dsp->index == fChannel ) plot_height = plot_height>dsp->height?plot_height:dsp->height;
            fFoundPulse=true;
            if( wf_flow->GetNumberOfPeaks() > 0 )
               {
                  double timestep=wf_flow->conf[dsp->board].fNanosecsPerSample;
                  const TDSPeak* pp = wf_flow->GetDSpeak(ip);
                  if( fFlags->fVerbose && 0 ) pp->Print();
                  if( pp->chi2 >= 0. )
                     {
                        PlotFit(pp,timestep);
                        fGoodFit=true;
                     }
               }
         }
      max_height*=1.2;
      plot_height*=1.2;

      // if this module is enabled, run until here
      // i.e., make waveforms available in JSROOT
      if( !fEnabled ) 
         {
            ++fCounter;
            return flow;
         }
      // if graphics is available on the host
      // run with -g and select a valid channel

      if( fFlags->fVerbose )
         std::cout<<"PlotModule::AnalyzeFlowEvent Plotting Channel: "<<fChannel<<std::endl;

      if( !fhwf[fChannel] && wf_flow->fEventNumber<3 )
         {
            std::cerr<<"PlotModule::Analyze() channel "<<fChannel
                     <<" not present in waveform container"<<std::endl;
         }

      std::string runstring(std::to_string(runinfo->fRunNo));
      std::string evtstring(std::to_string(adc_flow->getEventNumber()));
      ShowPlots(NumberOfChannels, max_height, plot_height, runstring, evtstring);
      ShowRaw(NumberOfChannels, runstring, evtstring);

      // read and plot here histos saved
      // in subdirectories
      if( runinfo->fgFileList.size() == 0 )
      //    runinfo->fRoot->fOutputFile->cd(); // <filename>:/
      // else
         {
            gDirectory->cd("RootApp:/");
            ShowROI(runstring,evtstring);
         }

      if( fSave && (fCounter < fSaveEvents) ) SaveCanvases(evtstring);

      ++fCounter;
      return flow;
   }


   void PlotChannel(const TRawChannel* wf, const TDSChannel* dschan)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock); // PlotChannel
#endif
      int Nbins = wf->GetNSamples();
      if( fFlags->fVerbose )
         std::cout<<"PlotModule::PlotChannels   # of bins: "<<Nbins<<" in channel "<<dschan->index<<std::endl;
      std::string hname;
      std::string htitle;

      if( !fhwf[dschan->index] )
         {
            hname="hwf_ADC";
            hname+=std::to_string(wf->GetBoardNumber());
            hname+="_CH"; hname+=std::to_string(wf->GetChannelNumber());
            if( fFlags->fVerbose && 0 ) std::cout<<hname<<std::endl;
            htitle="Waveform ";
            htitle+=dschan->board+" ";
            htitle+="ADC: "; htitle+=std::to_string(wf->GetBoardNumber());
            htitle+=" CH: "; htitle+=std::to_string(wf->GetChannelNumber());
            htitle+=";Sample;ADC";
            fhwf[dschan->index] = new TH1D(hname.c_str(),htitle.c_str(),Nbins,0.,double(Nbins));
            fhwf[dschan->index]->SetStats(0);
            fhwf[dschan->index]->SetLineColor(kBlack);
            fhwf[dschan->index]->SetMinimum(fRawWFmin);
            fhwf[dschan->index]->SetMaximum(fRawWFmax);
         }
      for(int i=0; i<Nbins; ++i)
         {
            fhwf[dschan->index]->SetBinContent(i+1,(double)wf->GetADCSample(i));
         }

      if( !fhwfb[dschan->index] )
         {
            hname="hDSchan_ADC";
            hname+=std::to_string(wf->GetBoardNumber());
            hname+="_CH"; hname+=std::to_string(wf->GetChannelNumber());
            if( fFlags->fVerbose && 0 ) std::cout<<hname<<std::endl;
            htitle="Wavefrom Pedestal Subtracted ";
            htitle+="ADC: "; htitle+=std::to_string(wf->GetBoardNumber());
            htitle+=" CH: "; htitle+=std::to_string(wf->GetChannelNumber());
            htitle+=";Sample;ADC";
            fhwfb[dschan->index] = new TH1D(hname.c_str(),htitle.c_str(),Nbins,0.,double(Nbins));
            fhwfb[dschan->index]->SetStats(0);
            fhwfb[dschan->index]->SetLineColor(kBlue);
            fhwfb[dschan->index]->SetMinimum(-16384.);
            fhwfb[dschan->index]->SetMaximum(16384.);
         }
#ifdef EXPORT_ASCII
      if( fChannel == dschan->index || fChannel == -1 )
         {
            std::string foutname=fdirname+"/WFadc"+std::to_string(wf->GetBoardNumber())+"ch"+std::to_string(wf->GetChannelNumber())+"_"+std::to_string(fCounter)+".dat";
            fout.open(foutname,std::ofstream::out);
            std::cout<<"Saving... "<<foutname<<std::endl;
         }
#endif
      for(int i=0; i<Nbins; ++i)
         {
            //std::cout<<dschan->index<<" "<<i<<" "<<dschan->unfiltered_wf.at(i)<<std::endl;
            fhwfb[dschan->index]->SetBinContent(i+1,-1.*dschan->unfiltered_wf.at(i));
#ifdef EXPORT_ASCII
           if( fChannel == dschan->index || fChannel == -1 ) fout<<i<<","<<dschan->unfiltered_wf.at(i)<<"\n";
#endif
         }
#ifdef EXPORT_ASCII
      if( fChannel == dschan->index || fChannel == -1 ) fout.close();
#endif

      if( !fhwff[dschan->index] )
         {
            hname="hDSchan_Filtered";
            hname+=std::to_string(wf->GetBoardNumber());
            hname+="_CH"; hname+=std::to_string(wf->GetChannelNumber());
            if( fFlags->fVerbose ) std::cout<<"Creating: "<<hname<<std::endl;
            htitle="Waveform Filtered ";
            htitle+="ADC: "; htitle+=std::to_string(wf->GetBoardNumber());
            htitle+=" CH: "; htitle+=std::to_string(wf->GetChannelNumber());
            htitle+=";Sample;ADC";
            fhwff[dschan->index] = new TH1D(hname.c_str(),htitle.c_str(),Nbins,0.,double(Nbins));
            fhwff[dschan->index]->SetStats(0);
            fhwff[dschan->index]->SetLineColor(kRed);
            fhwff[dschan->index]->SetLineWidth(2);
            fhwff[dschan->index]->SetMinimum(-16384.);
            fhwff[dschan->index]->SetMaximum(16384.);
         }
      for(int i=0; i<Nbins; ++i)
         {
            fhwff[dschan->index]->SetBinContent(i+1,(double)dschan->filtered_wf.at(i));
         }
      if( fFlags->fVerbose && 0 ) // extreme debug of the filter
         std::cout<<"PlotModule::PlotChannel(...) "
                  <<fhwff[dschan->index]->GetName()<<" "
                  <<fhwff[dschan->index]->GetEntries()<<" "
                  <<fhwff[dschan->index]->GetMaximumBin()<<" "
                  <<fhwff[dschan->index]->GetBinContent( fhwff[dschan->index]->GetMaximumBin() )
                  <<std::endl;

      if( !fhpulse[dschan->index] )
         {
            hname="hDSpulse_";
            hname+=std::to_string(wf->GetBoardNumber());
            hname+="_CH"; hname+=std::to_string(wf->GetChannelNumber());
            if( fFlags->fVerbose ) std::cout<<"Creating: "<<hname<<std::endl;
            htitle="Pulses";
            htitle+="ADC: "; htitle+=std::to_string(wf->GetBoardNumber());
            htitle+=" CH: "; htitle+=std::to_string(wf->GetChannelNumber());
            htitle+=";Sample;ADC";
            fhpulse[dschan->index] = new TH1D(hname.c_str(),htitle.c_str(),Nbins,0.,double(Nbins));
            fhpulse[dschan->index]->SetStats(0);
            fhpulse[dschan->index]->SetLineColor(3);
            fhpulse[dschan->index]->SetLineWidth(2);
         }
      for(int i=0;i<Nbins;++i)
         {
#ifdef _HIT_FINDER_
            fhpulse[dschan->index]->SetBinContent(i+1,(double)dschan->peak.at(i));
#else
            fhpulse[dschan->index]->SetBinContent(i+1,0.0);
#endif
         }

#ifdef _HIT_FINDER_
      if( !fpm[dschan->index] )
         {
            fpm[dschan->index] = new TPolyMarker();
            fpm[dschan->index]->SetMarkerStyle(23);
            fpm[dschan->index]->SetMarkerColor(kRed);
         }
#endif
      if( !ffpulse[dschan->index] )
         {
            hname="hpulsefit_";
            hname+=std::to_string(wf->GetBoardNumber());
            hname+="_CH"; hname+=std::to_string(wf->GetChannelNumber());
            if( fFlags->fVerbose && 0 ) std::cout<<hname<<std::endl;
            htitle="Pulses";
            htitle+="ADC: "; htitle+=std::to_string(wf->GetBoardNumber());
            htitle+=" CH: "; htitle+=std::to_string(wf->GetChannelNumber());
            htitle+=";Sample;ADC";
            ffpulse[dschan->index] = new TH1D(hname.c_str(),htitle.c_str(),Nbins,0.,double(Nbins));
            ffpulse[dschan->index]->SetStats(0);
            ffpulse[dschan->index]->SetLineColor(1);
            ffpulse[dschan->index]->SetLineWidth(2);
         }
      for(int i=0;i<Nbins;++i) ffpulse[dschan->index]->SetBinContent(i+1,0.0);
   }

   void PlotPulse(const TDSPulse* p)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock); // PlotPulse
#endif
      if( fFlags->fVerbose && 0 )
         std::cout<<"PlotModule::PlotPulse "<<p->channel
                  <<" at "<<p->index
                  <<" in "<<fhpulse[p->index]->GetName()<<std::endl;
      int start=((int)p->time)+1;
#ifndef _HIT_FINDER_
      double size=p->charge/p->height;
      int len = start+int(size);
      for( int b=start; b<len; ++b )
         fhpulse[p->index]->SetBinContent( b, p->height );
#else
      fpm[p->index]->SetNextPoint(start,p->height);
#endif
   }

   void PlotFit(const TDSPeak* p, double& bin_size)
   {
      if( fFlags->fVerbose && 0 )
         std::cout<<"WfviewModule::PlotFit "<<p->Counter
                  <<" at "<<p->ChannelIndex
                  <<" in "<<ffpulse[p->ChannelIndex]->GetName()<<std::endl;
      AvgSPE func(p->Charge, p->PulseTime, p->FallTimeShort, p->FallTimeLong);
      for( int b=1; b<=ffpulse[p->ChannelIndex]->GetNbinsX(); ++b )
         {
            double t = fhpulse[p->ChannelIndex]->GetBinCenter(b)*bin_size;
            if( t < p->MinValue ) continue;
            if( t > p->MaxValue ) break;
            double y = func(t);
            //std::cout<<"("<<b<<","<<t<<","<<y<<")";
            ffpulse[p->ChannelIndex]->SetBinContent( b, -y );
            //ffpulse[p->ChannelIndex]->Fill( t, y );
         }
   }

   void ShowPlots(int& NumberOfChannels, double& pulse_max, double& plot_max,
                  std::string& runn, std::string& evtn)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock); // ShowPlots
#endif

      TLine* fROIlow_clone = (TLine*) fROIlow->Clone();
      TLine* fROIupp_clone = (TLine*) fROIupp->Clone();
      TLine* fROIupq_clone = (TLine*) fROIupq->Clone();
      TLine* ffROIlow_clone = (TLine*) ffROIlow->Clone();
      TLine* ffROIupp_clone = (TLine*) ffROIupp->Clone();
      TLine* ffROIupq_clone = (TLine*) ffROIupq->Clone();

      std::string ctitle;
      if( fhwf[fChannel] )
         {
            double plot_min = -2.7*plot_max;
            fROIlow_clone->SetY1(plot_min); fROIlow_clone->SetY2(plot_max); 
            fROIupp_clone->SetY1(plot_min); fROIupp_clone->SetY2(plot_max);
            fROIupq_clone->SetY1(plot_min); fROIupq_clone->SetY2(plot_max);
            ffROIlow_clone->SetY1(plot_min); ffROIlow_clone->SetY2(plot_max); 
            ffROIupp_clone->SetY1(plot_min); ffROIupp_clone->SetY2(plot_max);
            ffROIupq_clone->SetY1(plot_min); ffROIupq_clone->SetY2(plot_max);

            ctitle="Plot ADC channel "+std::to_string(fChannel)
               +" Run "+runn+" Event "+evtn;
            if( fFlags->fVerbose ) std::cout<<"PlotModule::ShowPlots "<<ctitle
                                            <<" Range: ["<<plot_min<<","<<plot_max<<"]"<<std::endl;
            fC->SetTitle(ctitle.c_str());
            fC->cd();
            fhwfb[fChannel]->Draw("hist");
            fhwf[fChannel]->Draw("histsame");
#ifdef _HIT_FINDER_
            fpm[fChannel]->Draw("same");
#else
            fhwff[fChannel]->Draw("histsame");
#endif
            fhpulse[fChannel]->Draw("histsame");
            if(fGoodFit) ffpulse[fChannel]->Draw("histsame");

            fROIlow_clone->Draw("same");
            fROIupp_clone->Draw("same");
            fROIupq_clone->Draw("same");
            ffROIlow_clone->Draw("same");
            ffROIupp_clone->Draw("same");
            ffROIupq_clone->Draw("same");
            fhwfb[fChannel]->GetYaxis()->SetRangeUser(plot_min,plot_max);
            fC->Modified();
            fC->Draw();
            fC->SetGridy(1);
            fC->Update();
         }

      double pulse_min=-2.2*pulse_max;
      fROIlow->SetY1(pulse_min); fROIlow->SetY2(pulse_max); 
      fROIupp->SetY1(pulse_min); fROIupp->SetY2(pulse_max);
      fROIupq->SetY1(pulse_min); fROIupq->SetY2(pulse_max);
      ffROIlow->SetY1(pulse_min); ffROIlow->SetY2(pulse_max); 
      ffROIupp->SetY1(pulse_min); ffROIupp->SetY2(pulse_max);
      ffROIupq->SetY1(pulse_min); ffROIupq->SetY2(pulse_max);

      ctitle="Plot ADC Run "+runn+" Event "+evtn;
      if( fFlags->fVerbose ) std::cout<<"PlotModule::ShowPlots "<<ctitle
                                      <<" Range: ["<<pulse_min<<","<<pulse_max<<"]"<<std::endl;
      fCA->SetTitle(ctitle.c_str());
      fCA->cd();
      int ipad=1;
      int iplot=0;
      int ichan=0;
      while(ipad<17)
         {
            if(fhwfb[iplot])
               {
                  fCA->cd(ipad);
                  if( iplot == fChannel )
                  {
                     TH1D* htemp = (TH1D*) fhwfb[iplot]->Clone();
                     htemp->GetYaxis()->SetRangeUser(pulse_min,pulse_max);
                     htemp->Draw("hist");
                  }
                  else
                  {
                     fhwfb[iplot]->Draw("hist");
                     fhwfb[iplot]->GetYaxis()->SetRangeUser(pulse_min,pulse_max);
                  }
#ifdef _HIT_FINDER_
                  fpm[iplot]->Draw("same");
#else
                  fhwff[iplot]->Draw("histsame");
#endif
                  fhpulse[iplot]->Draw("histsame");
                  if(fGoodFit) ffpulse[iplot]->Draw("histsame");
                  fROIlow->Draw("same");
                  fROIupp->Draw("same");
                  fROIupq->Draw("same");
                  ffROIlow->Draw("same");
                  ffROIupp->Draw("same");
                  ffROIupq->Draw("same");
                  ++ipad;
                  ++ichan;
               }
            ++iplot;
            if( ichan == NumberOfChannels ) break;
         }
      fCA->Modified();
      fCA->Draw();
      fCA->Update();
   }

   void ShowRaw(int& nchan, std::string& runn, std::string& evtn)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock); // ShowRaw
#endif
      int ncanvases = std::ceil(nchan/16.0);
      if(fFlags->fVerbose)
         std::cout<<"PlotModule::ShowRaw("<<nchan<<",...) Number of canvanses: "
                  <<ncanvases<<std::endl;
      int ichan=0;
      for(int icanv=0; icanv<ncanvases; ++icanv )
         {
            std::string ctitle="Plot Raw ADC Run "+runn
               +" Event "+evtn+" Section"
               +std::to_string(icanv+1);
            if( fFlags->fVerbose ) std::cout<<"PlotModule::ShowRaw"<<ctitle<<std::endl;
            bool isPresent=fCraw.count(icanv);
            if( !isPresent )
               {
                  std::string cname="Plot_RawADC_Run"+runn+"_section"+std::to_string(icanv+1);
                  if(fFlags->fVerbose&&0) std::cout<<"\tnew canvas: "<<cname<<std::endl;
                  fCraw[icanv] = new TCanvas(cname.c_str(),ctitle.c_str(),2000,1500);
                  fCraw[icanv]->ToggleEventStatus();
                  fCraw[icanv]->Divide(4,4);
                  fCraw[icanv]->Iconify();
               }
            else
               fCraw[icanv]->SetTitle(ctitle.c_str());
            int ipad=1;
            int iplot=icanv*16;
            while(ipad<17)
               {
                  if(fhwf[iplot])
                     {
                        fCraw[icanv]->cd(ipad);
                        fhwf[iplot]->Draw("hist");
                        fhwf[iplot]->GetYaxis()->SetRangeUser(fRawWFmin,fRawWFmax);
                        ++ipad;
                        ++ichan;
                     }
                  ++iplot;
                  if( ichan == nchan ) break;
               }
            fCraw[icanv]->Modified();
            fCraw[icanv]->Draw();
            fCraw[icanv]->Update();
         }
   }

   void PersistencyPlot( const TDSChannel* dschan )
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock); // PersistencyPlot
#endif
      int idx=dschan->index;
      if(fFlags->fVerbose)
         {
            std::cout<<"PlotModule::PersistencyPlot "<<idx<<" \n";
            //gDirectory->pwd();
         }
      int nsamples=(int)dschan->unfiltered_wf.size();
      if( !fhPersistency.count(idx) )
         {
            std::string hname="hPersistencyCH"+std::to_string(idx);
            //std::cout<<"PlotModule::PersistencyPlot count "<<hname<<std::endl;
            std::string htitle="Persistency Plot for channel "+std::to_string(idx)+";Samples;ADC";
            fhPersistency[idx] = new TH1D(hname.c_str(),htitle.c_str(),nsamples,0.,(double)nsamples);
            fhPersistency[idx]->SetStats(0);
         }
      else if( fhPersistency[idx]->GetNbinsX() != nsamples )
         {
            std::string hname="hPersistencyCH"+std::to_string(idx);
            //std::cout<<"PlotModule::PersistencyPlot samples "<<hname<<std::endl;
            std::string htitle="Persistency Plot for channel "+std::to_string(idx)+";Samples;ADC";
            fhPersistency[idx] = new TH1D(hname.c_str(),htitle.c_str(),nsamples,0.,(double)nsamples);
            fhPersistency[idx]->SetStats(0);
         }
      for(int i=0; i<nsamples; ++i)
         {
            fhPersistency[idx]->AddBinContent(i+1,-1.*dschan->unfiltered_wf.at(i));
         }
      if( idx==fPersistencyChannel )
         {
            fCP->cd();
            fhPersistency[fPersistencyChannel]->Draw();
            fCP->Modified();
            fCP->Draw();
            fCP->Update();
         }
      if(fCounter==(fPersistencyEvents-1) && fFlags->fVerbose) std::cout<<"PlotModule::PersistencyPlot for channel:"<<idx<<" Last WF."<<std::endl;
   }

   void ShowROI(std::string& runn, std::string& evtn)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock); // ShowROI
#endif
      gDirectory->cd("/Histograms/ROIxChan_Q");
      if(fFlags->fVerbose&&0)
         {
            std::cout<<"PlotModule::ShowROI("<<runn<<") ";
            //gDirectory->pwd();
         }
      int ic=0; int ipad=1;
      for(auto k : *gDirectory->GetList())
         {
            TH1D *htemp = static_cast<TH1D*>(k);
            if(!htemp) continue;
            
            std::string ctitle="Plot Charge in ROI "+runn
               +" Up to Event "+evtn
               +" Section "+std::to_string(ic+1);
            bool isPresent=fCROI.count(ic);
            if( !isPresent || !fCROI[ic] )
               {
                  std::string cname="Plot_Q_ROI_Run"+runn+"_section"+std::to_string(ic+1);
                  fCROI[ic]=new TCanvas(cname.c_str(),ctitle.c_str(),2000,1500);
                  fCROI[ic]->ToggleEventStatus();
                  fCROI[ic]->Divide(4,4);
               }
            else fCROI[ic]->SetTitle(ctitle.c_str());
            fCROI[ic]->cd(ipad);
            htemp->Draw("hist");
            ++ipad;
            if( ipad==17 )
               {
                  ++ic;
                  ipad=1;
               }
         }

      gDirectory->cd("/Histograms/ROIxChan_PH");
      if(fFlags->fVerbose)
         {
            std::cout<<"PlotModule::ShowROI("<<runn<<") ";
            gDirectory->pwd();
         }
      ++ic;
      ipad=1;
      int sec=1;
      for(auto k : *gDirectory->GetList())
         {
            TH1D *htemp = static_cast<TH1D*>(k);
            if(!htemp) continue;
            
            std::string ctitle="Plot Pulse Height in ROI "+runn
               +" Up to Event "+evtn
               +" Section "+std::to_string(sec);
            bool isPresent=fCROI.count(ic);
            if( !isPresent || !fCROI[ic] )
               {
                  std::string cname="Plot_PH_ROI_Run"+runn+"_section"+std::to_string(sec);
                  fCROI[ic]=new TCanvas(cname.c_str(),ctitle.c_str(),2000,1500);
                  fCROI[ic]->ToggleEventStatus();
                  fCROI[ic]->Divide(4,4);
                  ++sec;
               }
            else fCROI[ic]->SetTitle(ctitle.c_str());
            fCROI[ic]->cd(ipad);
            htemp->Draw("hist");
            ++ipad;
            if( ipad==17 )
               {
                  ++ic;
                  ipad=1;
               }
         }
   }

   void SaveCanvases(std::string& evtn)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock); // SaveCanvases
#endif
      TString sname(fC->GetName());
      //sname+="_Event"+evtn+".pdf";
      sname+="_Event"+evtn+".png";
      fC->SaveAs(sname); fC->SaveAs(sname);
      sname=fCA->GetName();
      //sname+="_Event"+evtn+".pdf";
      sname+="_Event"+evtn+".png";
      fCA->SaveAs(sname); fCA->SaveAs(sname);
   }
   
   void ResetHisto()
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock); // ResetHisto
#endif
      for(auto it=fhwf.begin();it!=fhwf.end();++it)
         {
            if(it->second) 
               {
                  it->second->Reset();
                  it->second->SetMinimum(fRawWFmin);
                  it->second->SetMaximum(fRawWFmin);
               }
         }
      for(auto it=fhwfb.begin();it!=fhwfb.end();++it)
         {
            if(it->second) 
               {
                  it->second->Reset();
                  it->second->SetMinimum(-16384.);
                  it->second->SetMaximum(16384.);
               }
         }
      for(auto it=fhwff.begin();it!=fhwff.end();++it)
         {
            if(it->second) 
               {
                  it->second->Reset();
                  it->second->SetMinimum(-16384.);
                  it->second->SetMaximum(16384.);
               }
         }
      for(auto it=fhpulse.begin();it!=fhpulse.end();++it)
         if(it->second) it->second->Reset();
#ifdef _HIT_FINDER_
      for( auto it = fpm.begin();it!=fpm.end();++it )
         it->second->SetPolyMarker(-1);
#endif
      for(auto it=ffpulse.begin();it!=ffpulse.end();++it)
         if(it->second) it->second->Reset();
   }
};


class PlotModuleFactory: public TAFactory
{
public:
   PlotFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("PlotModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
   }
   
   void Finish()
   {
      printf("PlotModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("PlotModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new PlotModule(runinfo, &fFlags);
   }

};

static TARegister tar(new PlotModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
