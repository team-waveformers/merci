/******************************************************************
 * Flow Object(s) for DS analysis *
 *
 * A. Capra
 * August 2021
 *
******************************************************************/

#ifndef __DSFLOW__
#define __DSFLOW__

#include "manalyzer.h"

#include <map>
#include <vector>
#include <string>

#include "dsdata.hxx"

class DSProcessorFlow: public TAFlowEvent
{
public:
  std::vector<TDSPulse>* pulses;
  std::vector<TDSChannel>* channels;
  std::vector<TDSPeak>* peaks;
  std::map<std::string,ADCconf> conf;
  int fEventNumber;

private:
  std::map<int,int> channel_index;
  std::map<int,int> pulse_index;

public:
  DSProcessorFlow(TAFlowEvent* flow):TAFlowEvent(flow)
  {
    pulses = new std::vector<TDSPulse>();
    channels = new std::vector<TDSChannel>();
    peaks = new std::vector<TDSPeak>();
    conf["V1725"]=ADCconf("V1725");
    conf["V1730"]=ADCconf("V1730");
    conf["VX2740"]=ADCconf("VX2740");
    conf["VX2745"]=ADCconf("VX2745");
  }
  DSProcessorFlow(TAFlowEvent* flow,int size):TAFlowEvent(flow)
  {
    pulses = new std::vector<TDSPulse>(size);
    channels = new std::vector<TDSChannel>(size);
    peaks = new std::vector<TDSPeak>();
  }

  ~DSProcessorFlow()
  {
    pulses->clear();
    if(pulses) delete pulses;
    channels->clear();
    if(channels) delete channels;
    peaks->clear();
    if(peaks) delete peaks;
  }

  inline void AddChannel(TDSChannel* ch)
  {
     channels->push_back(*ch);
     channel_index[ch->index]=channels->size()-1;
  }

  inline int GetNumberOfChannels() const { return (int) channels->size(); }
  inline TDSChannel* GetDSchan(int i) { return &channels->at(i); }
  inline int GetNumberOfPulses() const   { return (int) pulses->size();   }
  inline const TDSPulse* GetDSpulse(int i) const { return &pulses->at(i); }
  inline int GetNumberOfPeaks() const   { return (int) peaks->size();   }
  inline const TDSPeak* GetDSpeak(int i) const { return &peaks->at(i); }

  inline TDSChannel* GetChannel(int i) { return &channels->at( channel_index[i] ); }

  inline ADCconf* GetConf() { return &conf[ GetChannel(0)->board ]; }
};

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
