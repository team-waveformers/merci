/******************************************************************
 *  Basic Filerting *
 * 
 * A. Capra
 * August 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "adcflow.hxx"
#include "baselineflow.hxx"
#include "dsflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <cassert>

#include "filter.hxx"

#include "json.hpp"
using json = nlohmann::json;


class FilterFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};
  

class FilterModule: public TARunObject
{
public:
   FilterFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors

   int fWindow=64;
   double fSmoothing=0.88;
   bool fMAF=false;
   bool fEXF=false;

   // experimental feature
   int fMemory=3;
   bool fEnF=false;

   // ARMA filter, see filter.hxx:ARMA
   bool fARMA=false;
   double fARMAtau=80.;
   double fARMAsigma=2.;
   double fARMAexp=1.;

   // AR filter
   bool fARF=false;

   // 'Generic' FIR
   bool fFIR=false;
   std::vector<double> fCoeff;

   // Cross-Correlation filter
   bool fCCF=false;
   std::string fTemplateFile;


   // Filter classes
   MAF avgfilter;
   EXF expfilter;
   EnF testfilter;
   ARMA armafilter;
   ARF arfilter;
   FIR firfilter;
   FIR xcorrfilter;

   // skip these channels 
   std::vector<int> fFilteredChannels;

public:
   FilterModule(TARunInfo* runinfo, FilterFlags* f): TARunObject(runinfo),
                                                     fFlags(f), fCounter(0), fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"FilterModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Filter";
#endif
      
      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"FilterModule Json parsing success!"<<std::endl;
      fin.close();

      std::string filter_name = settings["Filter"]["Name"].get<std::string>();
      if( filter_name == "Moving Average" )
         {
            fMAF=true;
            fWindow=settings["Filter"]["Par0"].get<int>();
            std::cout<<filter_name<<" window: "<<fWindow<<std::endl;
            avgfilter = MAF( fWindow );
         }
      else if( filter_name == "Exponential" )
         {
            fEXF=true;
            fSmoothing=settings["Filter"]["Par0"].get<double>();
            std::cout<<filter_name<<" smoothing: "<<fSmoothing<<std::endl;
            expfilter = EXF( fSmoothing );
         }
      else if( filter_name == "Exponential with Memory" )
         {
            fEnF=true;
            fSmoothing=settings["Filter"]["Par0"].get<double>();
            fMemory=settings["Filter"]["Par1"].get<int>();
            std::cout<<filter_name<<" smoothing: "<<fSmoothing<<" memory: "<<fMemory<<std::endl;
            testfilter = EnF( fSmoothing, fMemory );
         }
      else if( filter_name == "ARMA" )
         {
            fARMA=true;
            fARMAtau=settings["Filter"]["Par0"].get<double>();
            fARMAsigma=settings["Filter"]["Par1"].get<double>();
            fARMAexp=settings["Filter"]["Par2"].get<double>();
            std::cout<<filter_name<<" tau: "<<fARMAtau
                     <<" sigma_fp: "<<fARMAsigma<<" Afp_Aexp_ratio: "<<fARMAexp<<std::endl;
            armafilter = ARMA( fARMAtau, fARMAsigma, fARMAexp );
         }
      else if( filter_name == "AR" )
         {
            fARF=true;
            fARMAtau=settings["Filter"]["Par0"].get<double>();
            std::cout<<filter_name<<" filter tau: "<<fARMAtau<<std::endl;
            arfilter = ARF( fARMAtau );
         }
      else if( filter_name == "FIR" )
         {
            fFIR=true;
            firfilter = FIR();
            std::cout<<filter_name<<" filter with coefficients from ODB"<<std::endl;
         }
      else if( filter_name == "Cross-Correlation" )
         {
            fCCF=true; 
            
            try {
               fTemplateFile=settings["Filter"]["Par0"].get<std::string>();
               xcorrfilter=FIR(fTemplateFile);
            }
            catch(json::type_error& e) {
               std::cerr<<e.what()<<" couldn't find path to template file, trying with sequence"<<std::endl;            
            
               try {
                  fCoeff=settings["Filter"]["Par0"].get<std::vector<double>>();
               }
               catch(json::type_error& e) {
                  std::cerr<<e.what()<<" coeffiecient sequence not available"<<std::endl;
               }

               if( fCoeff.empty() )
                  fCoeff.assign(100,1);
               else if( fCoeff.size() == 1 )
                  fCoeff.assign(fCoeff[0],1);
               xcorrfilter=FIR(fCoeff);
            }
            std::cout<<filter_name<<" filter with "<<xcorrfilter.GetNumberOfCoefficients()<<" coefficients"<<std::endl;
         }
      else
         {
            std::cerr<<"Unkwown filter "<<filter_name<<std::endl;
         }

      try
         {
            fFilteredChannels=settings["Filter"]["Skip"].get<std::vector<int>>();
            std::cout<<"FilterModule::ctor Already filtered channels:";
            for(auto& ch: fFilteredChannels) std::cout<<" "<<ch;
            std::cout<<"\n";
         }
      catch(json::type_error& e) 
         {
            fFilteredChannels.clear();
            std::cerr<<"FilterModule::ctor reading skip channels: "<<e.what()<<std::endl;
         }
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"FilterModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"FilterModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
      if( fFIR )
         {
            std::vector<int> a;
            runinfo->fOdb->RIA("/VX2740 defaults/User registers/FIR filter coefficients",&a);
            firfilter.SetODBcoefficients( a );
            firfilter.Print();
         }
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"FilterModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      if( fFlags->fVerbose )
         std::cout<<"FilterModule::AnalyzeFlowEvent : "<<wf_flow->GetNumberOfChannels()<<std::endl;

      for(int i=0; i<wf_flow->GetNumberOfChannels(); ++i)
         {
            TDSChannel* dsch = wf_flow->GetDSchan(i);
            // dsch.unfiltered_wf

            if( fFlags->fVerbose )
               std::cout<<"FilterModule::AnalyzeFlowEvent ch: "<<dsch->index
                        <<" Number of Samples: "<<dsch->unfiltered_wf.size()
                        <<" ("<<wf_flow->conf[dsch->board].fNSamples<<")"<<std::endl;

            if( IsFiltered( dsch ) ) 
               {
                  if( fFlags->fVerbose )
                     std::cout<<"FilterModule::AnalyzeFlowEvent Skipping selected channel "
                              <<dsch->index<<" at "<<i<<std::endl;
                  dsch->filtered_wf = std::vector<double>(dsch->unfiltered_wf);
               }
            else Filter( dsch );
         }
      
      flow=wf_flow;
      ++fCounter;
      return flow;
   }

   int Filter(TDSChannel* dsch)
   {
      int shift=0;
      if( fMAF )
         {
            dsch->filtered_wf = std::vector<double>(*avgfilter( &dsch->unfiltered_wf ));
            shift=fWindow;
         }
      else if( fEXF )
         {
            dsch->filtered_wf = std::vector<double>(*expfilter( &dsch->unfiltered_wf ));
            shift=1;
         }
      else if( fEnF )
         {
            dsch->filtered_wf = std::vector<double>(*testfilter( &dsch->unfiltered_wf ));
            shift=fMemory;
         }
      else if( fARMA )
         {
            dsch->filtered_wf = std::vector<double>(*armafilter( &dsch->unfiltered_wf ));
         }
      else if( fARF )
         {
            dsch->filtered_wf = std::vector<double>(*arfilter( &dsch->unfiltered_wf ));
         }
      else if( fFIR )
         {
            dsch->filtered_wf = std::vector<double>(*firfilter( &dsch->unfiltered_wf ));
            shift=firfilter.GetNumberOfCoefficients();
         }
      else if( fCCF )
         {
            dsch->filtered_wf = std::vector<double>(*xcorrfilter( &dsch->unfiltered_wf ));
            shift=xcorrfilter.GetNumberOfCoefficients();
         }
      else
         {
            if( fFlags->fVerbose )
               std::cout<<"FilterModule::AnalyzeFlowEvent WARNING: the filtered waveform will be unfiltered"<<std::endl;
            dsch->filtered_wf = std::vector<double>(dsch->unfiltered_wf);
         }
      return shift;
   }

   bool IsFiltered(TDSChannel* dsch)
   {
      for( auto& ich: fFilteredChannels )
         {
            if( ich == dsch->index ) return true;
         }
      return false;
   }

};


class FilterModuleFactory: public TAFactory
{
public:
   FilterFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("FilterModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
      
   }
   
   void Finish()
   {
      printf("FilterModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("FilterModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new FilterModule(runinfo, &fFlags);
   }

};

static TARegister tar(new FilterModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
