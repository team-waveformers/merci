/******************************************************************
 *  Waveform Statistics *
 * 
 * A. Capra
 * December 2022
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "adcflow.hxx"
#include "baselineflow.hxx"
#include "dsflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <cassert>


#include "json.hpp"
using json = nlohmann::json;


class StatFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};
  

class StatModule: public TARunObject
{
public:
   StatFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors

   // Waveform baseline
   int fPedestal;

public:
   StatModule(TARunInfo* runinfo, StatFlags* f): TARunObject(runinfo),
                                                     fFlags(f), fCounter(0), fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"StatModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Stat";
#endif
      
      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"StatModule Json parsing success!"<<std::endl;
      fin.close();

      try
         {
            fPedestal=settings["Baseline"]["Pedestal"].get<int>();
         }
      catch(json::out_of_range& e) 
         {
            fPedestal = 100;
            std::cerr<<e.what()<<std::endl;
         }
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"StatModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"StatModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"StatModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      AdcEventFlow* adc_flow = flow->Find<AdcEventFlow>();
      if( !adc_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      
      BaselineEventFlow* base_flow = flow->Find<BaselineEventFlow>();
      if( !base_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      if( fFlags->fVerbose )
         std::cout<<"StatModule::AnalyzeFlowEvent : "<<base_flow->getNchannels()<<std::endl;
      
      DSProcessorFlow* wf_flow = new DSProcessorFlow(flow);
      wf_flow->conf[base_flow->fTypes[0]].fNSamples=base_flow->getNsamples(0);
      wf_flow->fEventNumber=adc_flow->getEventNumber();
      
      for(int i=0; i<base_flow->getNchannels(); ++i)
         {
            TDSChannel dsch( base_flow->getADCmodule(i), base_flow->getADCchannel(i),
                             base_flow->fTypes.at(i).c_str() );
            dsch.unfiltered_wf = *(base_flow->getChannelVector(i));

            if( fFlags->fVerbose )
               std::cout<<"StatModule::AnalyzeFlowEvent ch: "<<dsch.index
                        <<" Number of Samples: "<<dsch.unfiltered_wf.size()
                        <<" ("<<wf_flow->conf[dsch.board].fNSamples<<")"<<std::endl;

            dsch.baseline = base_flow->getBaseline(i);
            dsch.baseline_rms = base_flow->getBaselineRMS(i);

            dsch.maxph = fabs(*std::min_element(dsch.unfiltered_wf.begin(),
                                                dsch.unfiltered_wf.end()));

            // calculate the standard deviation of the whole waveform
            // using the mean baseline of the fist nbase_samples
            double number_of_samples = (double) (dsch.unfiltered_wf.size());
            double wf_rms=std::inner_product(dsch.unfiltered_wf.begin(),
                                             dsch.unfiltered_wf.end(),
                                             dsch.unfiltered_wf.begin(),double(0));
            wf_rms=sqrt( wf_rms / number_of_samples );
            dsch.wf_rms = wf_rms;

            if( fFlags->fVerbose )
               std::cout<<"StatModule::AnalyzeFlowEvent ch: "<<dsch.index
                        <<" wf rms: "<<wf_rms
                        <<"\t baseline: "<<dsch.baseline
                        <<" RMS: "<<dsch.baseline_rms
                        <<std::endl;

            
            wf_flow->channels->push_back(dsch);
         }
      
      flow=wf_flow;
      ++fCounter;
      return flow;
   }
};


class StatModuleFactory: public TAFactory
{
public:
   StatFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("StatModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
      
   }
   
   void Finish()
   {
      printf("StatModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("StatModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new StatModule(runinfo, &fFlags);
   }

};

static TARegister tar(new StatModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
