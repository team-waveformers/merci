/******************************************************************
 *  Histogramming *
 * 
 * A. Capra
 * August 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "adcflow.hxx"
#include "dsflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <numeric>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <ctime>

#include "TH1D.h"
#include "TH2D.h"

#include "json.hpp"
using json = nlohmann::json;


class HistoFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};


double vabs(double v) { return v>0.?v:-1.*v; }
double vopp(double v) { return -1.*v; }

class HistoModule: public TARunObject
{
public:
   HistoFlags* fFlags;
   
private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors
   // 'Baseline_Summary'
   TH2D* fBaselineSummary;
   // 'Baseline_RMS_Summary'
   TH2D* fBaselineRMS;
   // 'Maximum_PH_Summary Raw Waveform baseline subtracted'
   TH2D* fMaxHeight;
   // 'Maximum_PH_Summary Raw Waveform '
   TH2D* fRawHeight;
   // 'WF_AreaOverHeight'
   TH1D* fAreaOverHeight;
   //'PulseHeightSummary'
   TH2D* fPulseHeight;
   // 'PulseChargeSummary'
   TH2D* fPulseCharge;
   // 'Pulses_Vs_Time'
   TH2D* fPulseTime;
   //'PulseCountVsPosition'
   //TH2D*fPulsePos;
   // Pulse Charge
   TH1D* fCharge;
   // Pulse Time
   TH1D* fTime;
   // number of pulses
   TH1D* hNpulses;
   // Pulse height
   TH1D* fHeight;
   // PH vs. T
   TH2D* hTPH;

   // Rate histo
   TH1D* fhRate;
   TH2D* fhTSpulse;
   bool isFirstEvent;
   double TimeOfFirstEvent;

   std::map<int,TH1D*> fhROIheight;
   std::map<int,TH1D*> fhROIcharge;
   std::map<int,TH1D*> fhROIcharge_filt;

   TDirectory* fHistoDir;
   TDirectory* fROIHDir;
   TDirectory* fROIQDir;

   TDirectory* fROIQFiltDir;

   Proto0MB2map pmap;

   int NumberOfChannels;

   int NbinsBaseline;
   double MinBaseline;
   double MaxBaseline;
   int NbinsBaselineRMS;
   double MinBaselineRMS;
   double MaxBaselineRMS;
   
   int NbinsHeight;
   double MaxHeight;
   int NbinsCharge;
   double MaxCharge;
   int NbinsTime;
   double MaxTime;

   int NbinsROIHeight;
   double MaxROIHeight;
   int NbinsROICharge;
   double MaxROICharge;

   int NbinsRate;
   double MaxRunTime;

   double fRawWFmin;
   double fRawWFmax;

public:
   HistoModule(TARunInfo* runinfo, HistoFlags* f): TARunObject(runinfo),
                                                   fFlags(f),
                                                   fCounter(0), fError(0),
                                                   isFirstEvent(true),
                                                   TimeOfFirstEvent(0.0)
							     
   {
      if(fFlags->fVerbose) std::cout<<"HistoModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Histo";
#endif

      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"HistoModule Json parsing success!"<<std::endl;
      fin.close();

      NumberOfChannels=settings["Histo"]["Number of Channels"].get<int>();
      
      NbinsBaseline=settings["Histo"]["Bins baseline"].get<int>();
      MinBaseline=settings["Histo"]["Lower bound baseline"].get<double>();
      MaxBaseline=settings["Histo"]["Upper bound baseline"].get<double>();
      NbinsBaselineRMS=settings["Histo"]["Bins baseline rms"].get<int>();
      MinBaselineRMS=settings["Histo"]["Lower bound baseline rms"].get<double>();
      MaxBaselineRMS=settings["Histo"]["Upper bound baseline rms"].get<double>();

      NbinsHeight=settings["Histo"]["Bins pulse height"].get<int>();
      MaxHeight=settings["Histo"]["Upper bound pulse height"].get<double>();
      NbinsCharge=settings["Histo"]["Bins charge"].get<int>();
      MaxCharge=settings["Histo"]["Upper bound charge"].get<double>();
      NbinsTime=settings["Histo"]["Bins time"].get<int>();
      MaxTime=settings["Histo"]["Upper bound time"].get<double>();

      NbinsROIHeight=settings["Histo"]["Bins ROI PH"].get<int>();
      MaxROIHeight=settings["Histo"]["Upper bound ROI PH"].get<double>();
      NbinsROICharge=settings["Histo"]["Bins ROI Q"].get<int>();
      MaxROICharge=settings["Histo"]["Upper bound ROI Q"].get<double>();

      NbinsRate=settings["Histo"]["Bins Rate"].get<int>();
      MaxRunTime=settings["Histo"]["Upper bound run time"].get<double>();
      if( MaxRunTime < NbinsRate )
         {
            NbinsRate=(int)MaxRunTime;
            std::cout<<"HistoModule WARNING time resolution of rate plot too high (<1s), changing the number of bins to "<<NbinsRate<<std::endl;
         }

      fRawWFmin=settings["Plot"]["Raw WF Minimum"].get<double>();
      fRawWFmax=settings["Plot"]["Raw WF Maximum"].get<double>();

   }

   void BeginRun(TARunInfo* runinfo)
   {
      if( runinfo->fFileName.empty() )
         {
            std::cout<<"HistoModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
            runinfo->fRoot->fgDir->cd();
            // select the correct directory
            gDirectory->cd("..");
            // and create new ones appropriately
            fHistoDir = new TDirectory("Histograms", "Online Histograms");
            fROIHDir=fHistoDir->mkdir("ROIxChan_PH");
            fROIQDir=fHistoDir->mkdir("ROIxChan_Q");
            fHistoDir->cd();
         }
      else
         {
            std::cout<<"HistoModule::BeginRun() run: "<<runinfo->fRunNo
                     <<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
            runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
            fHistoDir=runinfo->fRoot->fOutputFile->mkdir("Histograms");
            fROIHDir=fHistoDir->mkdir("ROIxChan_PH");
            fROIQDir=fHistoDir->mkdir("ROIxChan_Q");
            //fROIQFiltDir=fHistoDir->mkdir("ROIxChan_Q_Filt");
            fHistoDir->cd();
         }
      
      fHistoDir->cd();
      // 'Baseline_Summary'
      fBaselineSummary = new TH2D("Baseline_vs_Channel", "Baseline vs Channel;Baseline (ADC);Channel",
                                  NbinsBaseline, MinBaseline, MaxBaseline,
                                  NumberOfChannels, 0., double(NumberOfChannels));
      // 'Baseline_RMS_Summary'
      fBaselineRMS = new TH2D("BaselineRMS_vs_Channel", "Baseline RMS vs Channel;Baseline RMS (ADC);Channel",
                              NbinsBaselineRMS, MinBaselineRMS, MaxBaselineRMS,
                              NumberOfChannels, 0., double(NumberOfChannels));
      // 'Maximum_PH_Summary Raw Waveform baseline subtracted'
      fMaxHeight = new TH2D("RawMaxPH_vs_Channel", "Max Pulse Height Raw WF BL-sub;Pulse Height (ADC);Channel",
                            NbinsHeight, 0., MaxHeight,
                            NumberOfChannels, 0., double(NumberOfChannels));

      fRawHeight = new TH2D("RawPH_vs_Channel", "Pulse Height Raw WF;Pulse Height (ADC);Channel",
                            NbinsHeight, fRawWFmin, fRawWFmax,
                            NumberOfChannels, 0., double(NumberOfChannels));

      // 'WF_AreaOverHeight'
      fAreaOverHeight = new TH1D("hAreaOverHeight", "Total Area/Height;Total filtered area / height", 1000, 0., 10000.);

      //'PulseHeightSummary'
      fPulseHeight = new TH2D("PulseHeight_vs_Channel", "Pulse Height vs Channel;Pulse Height (ADC value);Channel",
                              NbinsHeight, 0., MaxHeight,
                              NumberOfChannels, 0., double(NumberOfChannels));
      // 'PulseChargeSummary'
      fPulseCharge = new TH2D("PulseCharge_vs_Channel", "Pulse Charge vs Channel;Pulse Charge (ADC*sample value);Channel",
                              NbinsCharge, -10000., MaxCharge,
                              NumberOfChannels, 0., double(NumberOfChannels));
      // 'Pulses_Vs_Time
      fPulseTime = new TH2D("PulseTime_vs_Channel", "Pulse Time vs Channel;Pulse Time (ns);Channel",
                            NbinsTime,0.,MaxTime,
                            NumberOfChannels,0.,double(NumberOfChannels));
      fPulseTime->SetCanExtend(TH2::kAllAxes);
      // 'PulseCountVsPosition'
      // fPulsePos = new TH2D("PulseCountVsPosition", "Pulse Count Vs Position;PDM X;PDM Y",5, 0, 5, 5, 0, 5);
      // Pulse charge
      fCharge = new TH1D("Charge Spectrum", "Charge Spectrum;Charge (ADC*sample value);Pulse Count",
                         NbinsCharge, -10000., MaxCharge);
      // Pulse time
      fTime = new TH1D("Time Spectrum", "Arrival Times of all found pulses;Time (ns);Pulse Count",
                       NbinsTime,0.,MaxTime);
      fTime->SetCanExtend(TH1::kAllAxes);
      // Number of Pulses
      hNpulses = new TH1D("NPulsesPerEvent","Number of Pulses per Event;Pulse Count per Event;Events",
                          100,0.,100.);
      // Pulse peak height
      fHeight = new TH1D("Height Spectrum","Height Spectrum;Pulse Height (ADC value);Pulse Count",
                         NbinsHeight, 0., MaxHeight);
      hTPH = new TH2D("hTimePulseHeight","Pulse Height vs Time;Pulse Leading Edge Time;Pulse Height",
                      NbinsTime,0.,MaxTime,
                      NbinsHeight, 0., MaxHeight);
      hTPH->SetCanExtend(TH2::kAllAxes);

      std::string htitle("Pulse Rate;Time from first event [s];Pulses/Event/");
      double temp=MaxRunTime/double(NbinsRate);
      std::stringstream stemp; stemp<<std::setprecision(3)<<temp;
      htitle+=stemp.str()+"s";
      fhRate = new TH1D("hRate",htitle.c_str(),NbinsRate,0,MaxRunTime);
      fhRate->SetCanExtend(TH1::kAllAxes);
      fhTSpulse = new TH2D("hTSpulse","Pulses time vs Event time;Time from first event [s];Pulse time [ns];Number of pulses",
                           NbinsRate,0,MaxRunTime,NbinsTime,0.,MaxTime);
      fhTSpulse->SetCanExtend(TH2::kAllAxes);
      
      uint32_t midas_start_time;
      runinfo->fOdb->RU32("Runinfo/Start time binary",(uint32_t*) &midas_start_time);
      time_t ts1 = midas_start_time;
      std::cout<<"HistoModule::BeginRun() Start of Run time: "
               <<std::string(asctime(localtime(&ts1)));
   }

   void EndRun(TARunInfo* runinfo)
   {
      if( runinfo->fFileName.empty() )
         {
            runinfo->fRoot->fgDir->cd();
            gDirectory->cd("../Histograms");
            if(fROIHDir) delete fROIHDir;
            if(fROIQDir) delete fROIQDir;
            if(fHistoDir) delete fHistoDir;
         }
      uint32_t midas_stop_time;
      runinfo->fOdb->RU32("Runinfo/Stop time binary",(uint32_t*) &midas_stop_time);
      time_t ts1 = midas_stop_time;
      std::cout<<"HistoModule::EndRun() End of Run time: "
               <<std::string(asctime(localtime(&ts1)));     
      std::cout<<"HistoModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow ) 
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            ++fError;
            return flow;
         }

      double EventTimeStamp=-1.;
      AdcEventFlow* adc_flow = flow->Find<AdcEventFlow>();
      if( adc_flow ) EventTimeStamp=adc_flow->fmidas_ts;

      if(fFlags->fVerbose && 0)
         std::cout<<"HistoModule::AnalyzeFlowEvent # of ch: "
                  <<wf_flow->GetNumberOfChannels()<<std::endl;

      for(int n=0; n<adc_flow->getNchannels(); ++n)
         {
            int ch_idx=adc_flow->getChannelIndex(n);
            if( fFlags->fVerbose )
               std::cout<<"HistoModule:: "<<std::setw(4)<<n<<") "<<adc_flow->ftype[n]
                        <<" idx: "<<ch_idx<<std::endl;
            // obtain the waveform, we will not modify it
            const std::vector<double>* wf = adc_flow->getVector(n);
            
            double ph = *std::min_element(wf->begin(),wf->end());
            fRawHeight->Fill(ph,ch_idx);
         }

      if( isFirstEvent )
         {
            TimeOfFirstEvent=EventTimeStamp;
            isFirstEvent=false;
            time_t ts1 = TimeOfFirstEvent;
            if(fFlags->fVerbose)
               std::cout<<"HistoModule::AnalyzeFlowEvent Time of First Event: "
                        <<std::string(asctime(localtime(&ts1)))<<std::endl;
         }
      double TimeDelta=EventTimeStamp-TimeOfFirstEvent;

      int nsamp = wf_flow->GetDSchan(0)->filtered_wf.size();
      std::vector<double> total_wf(nsamp,0.0);
      
      for(int i = 0; i < wf_flow->GetNumberOfChannels(); i++)
         {
            TDSChannel* chan = wf_flow->GetDSchan(i);
            fBaselineSummary->Fill(chan->baseline,chan->index);
            fBaselineRMS->Fill(chan->baseline_rms,chan->index);
            fMaxHeight->Fill(chan->maxph,chan->index);
            if( (chan->filtered_wf.size()-nsamp) ) 
               {
                  std::cerr<<"HistoModule::AnalyzeFlowEvent CH:"<<chan->index
                           <<" inconsistent # of samples: "<<chan->filtered_wf.size()<<std::endl;
               }
            std::transform(chan->filtered_wf.begin(),chan->filtered_wf.end(),
                           total_wf.begin(),total_wf.begin(),std::plus<double>());
            if( runinfo->fFileName.empty() )
               {
                  runinfo->fRoot->fgDir->cd();
                  gDirectory->cd("..");
               }
            else runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
            ROIplot(chan);
         }

      // std::transform(total_wf.begin(),total_wf.end(),total_wf.begin(),vabs);
      std::transform(total_wf.begin(),total_wf.end(),total_wf.begin(),vopp);
      double height = *std::max_element(total_wf.begin(),total_wf.end());
      double charge = std::accumulate(total_wf.begin(),total_wf.end(),0.0);
      // Fill the histogram
      if( charge > 0. && height > 0. )
         fAreaOverHeight->Fill(charge/height);

      if(fFlags->fVerbose)
         std::cout<<"HistoModule::AnalyzeFlowEvent Total Waveform amplitude: "<<height
                  <<" charge: "<<charge<<std::endl;
      total_wf.clear();
         
      double ptime;
      for(int i = 0; i < wf_flow->GetNumberOfPulses(); i++)
         {
            const TDSPulse* pulse = wf_flow->GetDSpulse(i);
            fPulseHeight->Fill(pulse->height, pulse->index);
            fPulseCharge->Fill(pulse->charge, pulse->index);
            ptime=pulse->time*wf_flow->conf[pulse->board].fNanosecsPerSample;
            fPulseTime->Fill(ptime,pulse->index);
            // if(pmap.mb2_map.find(pulse->index) != pmap.mb2_map.end())
            //    {
            //       int x = pmap.mb2_map[pulse->index].first;
            //       int y = pmap.mb2_map[pulse->index].second;
            //       //fPulsePos->Fill(x,y);
            //    }
            fTime->Fill(ptime);
            fHeight->Fill(pulse->height);
            fCharge->Fill(pulse->charge);
            hTPH->Fill(ptime,pulse->height);
            fhTSpulse->Fill(TimeDelta,ptime);
         }
      hNpulses->Fill(wf_flow->GetNumberOfPulses());
      fhRate->Fill(TimeDelta,wf_flow->GetNumberOfPulses());

      if(fFlags->fVerbose)
         std::cout<<"HistoModule::AnalyzeFlowEvent ~RunTime: "<<TimeDelta
                  <<" # of pulses: "<<wf_flow->GetNumberOfPulses()<<std::endl;

      ++fCounter;
      return flow;
   }

   void ROIplot(const TDSChannel* chan)
   {
      // gDirectory->cd("/");
      // fHistoDir->cd();
      // make histos on-the-fly
      if( !fhROIheight.count(chan->index) )
         {
            std::string hname="hDS_ROIheight_"+std::to_string(chan->module)+
               "_CH"+std::to_string(chan->channel);
            std:: string htitle="Laser ROI BL-sub Max Amplitude ADC: "+std::to_string(chan->module)
               +" CH: "; htitle+std::to_string(chan->channel);
            htitle+=";ROI Max Amplitude;ADC";// axis labels
            //fHistoDir->cd();
            //fROIHDir->cd();
            std::string dirpath(fROIHDir->GetPath());
            gDirectory->cd(dirpath.c_str());
            fhROIheight[chan->index] = new TH1D(hname.c_str(),htitle.c_str(),
                                                NbinsROIHeight,0.,MaxROIHeight);
            fhROIheight[chan->index]->SetLineColor(kBlue);
         }
      fhROIheight[chan->index]->Fill(chan->roipeak);

      // gDirectory->cd("/");
      // fHistoDir->cd();
      // make histos on-the-fly
      if( !fhROIcharge.count(chan->index) )
         {
            std::string hname="hDS_ROIcharge_"+std::to_string(chan->module)
               +"_CH"+std::to_string(chan->channel);
            std::string htitle="Laser ROI BL-sub Charge ADC: "+std::to_string(chan->module)
               +" CH: "+std::to_string(chan->channel);
            htitle+=";ROI Charge;ADC";// axis labels
            
            std::string hname_filt="hDS_ROIcharge_filtered_"+std::to_string(chan->module)
               +"_CH"+std::to_string(chan->channel);
            std::string htitle_filt="Laser ROI BL-sub Filtered Charge ADC: "+std::to_string(chan->module)
               +" CH: "+std::to_string(chan->channel);
            htitle_filt+=";ROI Filtered Charge;ADC";
            
            //gDirectory->cd("..");
            //fHistoDir->cd();
            //fROIQDir->cd();
            std::string dirpath(fROIQDir->GetPath());
            gDirectory->cd(dirpath.c_str());
            fhROIcharge[chan->index] = new TH1D(hname.c_str(),htitle.c_str(),
                                                NbinsROICharge,-10000.,MaxROICharge);
            fhROIcharge[chan->index]->SetLineColor(kBlue);

            //fROIQFiltDir->cd();
            fhROIcharge_filt[chan->index] = new TH1D(hname_filt.c_str(),htitle_filt.c_str(),NbinsROICharge,-10000.,MaxROICharge);
            fhROIcharge_filt[chan->index]->SetLineColor(kBlue);
        }
      fhROIcharge[chan->index]->Fill(chan->roicharge);
      fhROIcharge_filt[chan->index]->Fill(chan->filter_roicharge);
   }
};

class HistoModuleFactory: public TAFactory
{
public:
   HistoFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("HistoModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
	 }
   }
   
   void Finish()
   {
      printf("HistoModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("HistoModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new HistoModule(runinfo, &fFlags);
   }

};

static TARegister tar(new HistoModuleFactory);


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
