/******************************************************************
 *  Hit finding with K-sigma trigger *
 * 
 * A. Capra
 * August 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "adcflow.hxx"
#include "dsflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric>

#include <cassert>

#include "TCanvas.h"
#include "TH1D.h"

class KsTriggerFilter
{
private:
   std::vector<double> filter;
   std::vector<double> baseline;
   std::vector<double> sigma;
   std::vector<double> deviation;
   std::vector<double> result;

   const int tf;
   const int tb;
   const int ts;
   double ff;
   double fb;
   double fs;

public:
   KsTriggerFilter(int& tau_f, int& tau_b, int& tau_s):tf(tau_f),tb(tau_b),ts(tau_s)
   {
      assert((tf > 0) && (tb > 0) && (ts > 0));
      ff=1./double(tf);
      fb=1./double(tb);
      fs=1./double(ts);
   }

   void apply(const std::vector<double>* adc_samples,double threshold=5.)
   {
      int n=adc_samples->size();
      filter.resize(n);
      baseline.resize(n);
      sigma.resize(n);
      deviation.resize(n);
      result.resize(n);
      sigma[0]=deviation[0]=0.0;
      result[0]=0.0;
      baseline[0]=filter[0]=adc_samples->at(0);
      baseline[0]=adc_samples->at(0);
      for( int i=1; i<n; ++i )
         {
            filter[i]    = baseline[i-1] + ff * (adc_samples->at(i)-baseline[i-1]);
            baseline[i]  = baseline[i-1] + fb * (adc_samples->at(i)-baseline[i-1]);
            deviation[i] = fabs(adc_samples->at(i)-baseline[i]);
            //deviation[i] = fabs(filter[i]-baseline[i]);
            sigma[i]     = sigma[i-1] + fs * (deviation[i]-sigma[i-1]);
            if( sigma[i] > threshold )
               {
                  result[i]    = sigma[i]?fabs(deviation[i]/sigma[i]):0.0;
                  std::cout<<i<<"\t"<<baseline[i]<<"\t"<<deviation[i]<<"\t"<<sigma[i]<<"\t"<<result[i]<<std::endl;
                  // sigma[0]=deviation[0]=0.0;
                  // result[0]=0.0;
                  // baseline[0]=filter[0]=adc_samples->at(0);
               }
         }    
   }

   int operator()(double threshold=5.)
   {
      std::vector<double>::iterator it =
         std::find_if( sigma.begin(), sigma.end(),
                       [threshold](double const& r)
                       { return (fabs(r)>threshold); } );
      if( it!=sigma.end() )
         return ((int) std::distance(sigma.begin(),it));
      return 0;
   }

   int operator()(int start,double threshold=5.)
   {
      if( !(start>0) ) return 0;
      std::vector<double>::iterator it =
         std::find_if( sigma.begin()+start,
                       sigma.end(),
                       [threshold](double const& r)
                       { return (fabs(r)<threshold); } );
      if( it!=sigma.end() )
         return ((int) std::distance(sigma.begin(),it));
      return 0;
   }

   inline const std::vector<double>* GetFiltered() const { return &filter; }
   inline const std::vector<double>* GetResult()    const { return &result; }
   inline const std::vector<double>* GetDeviation() const { return &deviation; }
   inline const std::vector<double>* GetSigma()     const { return &sigma; }
   inline const std::vector<double>* GetBaseline()  const { return &baseline; }

   void reset()
   {
      filter.clear();
      baseline.clear();
      sigma.clear();
      deviation.clear();
      result.clear();
   }
};

   
class KsTriggerFlags
{
public:
   bool fVerbose = false;
   int fTau_b = 100;
   int fTau_f = fTau_b*0.2;
   //int fTau_s = fTau_b*200.;
   int fTau_s = 10;
   int fNsigma = 300;
};
  

class KsTriggerModule: public TARunObject
{
public:
   KsTriggerFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors
   
   bool fPlot;
   int fChannel;
   TCanvas* fCanvas; // the canvas for the special channel
   TH1D* hwf; // raw wf
   TH1D* hf;  // filtered wf
   TH1D* hb;  // baseline
   TH1D* hd;  // deviation
   TH1D* hs;  // sigma
   TH1D* hr;  // result
   TH1D* hp;  // pulse

public:
   KsTriggerModule(TARunInfo* runinfo, KsTriggerFlags* f): TARunObject(runinfo),
                                                           fFlags(f), fCounter(0), fError(0),
                                                           fPlot(true), fChannel(0),
                                                           hwf(0), hf(0), hb(0), hd(0), hs(0), hr(0), hp(0)
   {
      if(fFlags->fVerbose) std::cout<<"KsTriggerModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="KsTrigger";
#endif

   }

   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"KsTriggerModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"KsTriggerModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;

      int runn = runinfo->fRunNo;
      
      std::string cname="Plot_Ks_trigger"+std::to_string(fChannel)
         +"_Run"+std::to_string(runn);
      std::string ctitle="Plot ADC channel "+std::to_string(fChannel)
         +" Run "+std::to_string(runn);
      fCanvas = new TCanvas(cname.c_str(),ctitle.c_str(),2000,1500);
      fCanvas->Divide(1,2);
      fCanvas->ToggleEventStatus();
      fCanvas->SetGridy(1);
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"KsTriggerModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      AdcEventFlow* adc_flow = flow->Find<AdcEventFlow>();
      if( !adc_flow )
         {
            ++fError;
     #ifdef HAVE_MANALYZER_PROFILER
           *flags|=TAFlag_SKIP_PROFILE;
     #endif
           return flow;
         }

      if(fFlags->fVerbose)
         std::cout<<"KsTriggerModule::AnalyzeFlowEvent # of Channels: "
                  <<adc_flow->getNchannels()<<std::endl;

      DSProcessorFlow* wf_flow = new DSProcessorFlow(flow);
      wf_flow->fEventNumber=adc_flow->getEventNumber();

      KsTriggerFilter trig( fFlags->fTau_f, fFlags->fTau_b, fFlags->fTau_s );
      for(int i=0; i<adc_flow->getNchannels(); ++i)
         {
            adc_flow->getChannel(i)->Print();
          
            const std::vector<double>* wf = adc_flow->getVector(i);
            trig.apply(wf,fFlags->fNsigma);

            TDSChannel dsch( adc_flow->getBoard(i), adc_flow->getChannelNumber(i),
                             adc_flow->ftype[i].c_str() );
            dsch.unfiltered_wf.assign(wf->begin(),wf->end());
            std::cout<<"KsTriggerModule::AnalyzeFlowEvent "<<i<<" # of Samples: "<<wf->size()<<std::endl;
            //dsch.filtered_wf = *trig.GetFiltered();
            dsch.filtered_wf.assign(trig.GetSigma()->begin(),trig.GetSigma()->end());

            int start_sample = trig(fFlags->fNsigma), end_sample=0;
            std::cout<<"KsTriggerModule::AnalyzeFlowEvent "<<i<<" start sample: "<<start_sample<<std::endl;
            if( start_sample > 0 )
               {
                  end_sample=trig(start_sample,fFlags->fNsigma);
                  std::cout<<"KsTriggerModule::AnalyzeFlowEvent "<<i<<" end sample: "<<end_sample<<std::endl;
                  if( end_sample>0 )
                     {
                        double amp=*(std::min_element(wf->begin()+start_sample,
                                                      wf->begin()+end_sample));
                        double duration=double(end_sample-start_sample);
                        double charge = std::accumulate(wf->begin()+start_sample,
                                                        wf->begin()+end_sample,0.0);
                        if(fFlags->fVerbose)
                           std::cout<<"KsTriggerModule ch: "<<dsch.index
                                    <<"   Hit Found ["<<start_sample<<","<<end_sample
                                    <<"] = "<<duration<<"   Amplitude: "<<amp<<" Charge: "<<charge<<std::endl;
                        TDSPulse aPulse(dsch.module,dsch.channel,dsch.board.c_str());
                        aPulse.charge=charge;
                        aPulse.time=start_sample;
                        aPulse.height=amp;
                        if( fFlags->fVerbose && 0 ) aPulse.Print();
                        wf_flow->pulses->push_back(aPulse);
                     }
               }
            if( adc_flow->getNchannels() > 0 ) 
               wf_flow->conf[adc_flow->ftype[0].c_str()].fNSamples = adc_flow->getNsamples(0);
            wf_flow->channels->push_back(dsch);

            if( fPlot && dsch.index == fChannel ) PlotHistos(wf, 
                                                             trig.GetFiltered(), trig.GetBaseline(),
                                                             trig.GetSigma(), trig.GetDeviation(), trig.GetResult());

            trig.reset();
         }
      
      flow=wf_flow;
      ++fCounter;
      return flow;
   }

   void PlotHistos(const std::vector<double>* wf, 
                   const std::vector<double>* f, const std::vector<double>* b, 
                   const std::vector<double>* s, const std::vector<double>* d, 
                   const std::vector<double>* r)
   {
      int Nsamples = wf->size();
      std::cout<<"KsTriggerModule::PlotHistos "<<fCounter<<" # of Samples: "<<Nsamples<<std::endl;
      if( !hwf )
         {
            std::string hname;
            std::string htitle;
            hname="hwf_CH"; hname+=std::to_string(fChannel);
            htitle="Waveform CH"; htitle+=std::to_string(fChannel);
            htitle+=" Event "; htitle+=std::to_string(fCounter);
            htitle+=";Sample;ADC";
            hwf = new TH1D(hname.c_str(),htitle.c_str(),Nsamples,0.,double(Nsamples));
            hwf->SetLineColor(kBlack);
            hwf->SetStats(0);

            hname="hf_CH"; hname+=std::to_string(fChannel);
            htitle="Filtered";// CH"; htitle+=std::to_string(fChannel);
            //htitle+="Event "; htitle+=std::to_string(fCounter);
            htitle+=";Sample;ADC";
            hf = new TH1D(hname.c_str(),htitle.c_str(),Nsamples,0.,double(Nsamples));
            hf->SetLineColor(kRed);
            hf->SetStats(0);

            hname="hb_CH"; hname+=std::to_string(fChannel);
            htitle="Baseline";// CH"; htitle+=std::to_string(fChannel);
            //            htitle+="Event "; htitle+=std::to_string(fCounter);
            htitle+=";Sample;ADC";
            hb = new TH1D(hname.c_str(),htitle.c_str(),Nsamples,0.,double(Nsamples));
            hb->SetLineColor(kBlue);
            hb->SetStats(0);

            hname="hs_CH"; hname+=std::to_string(fChannel);
            htitle="Sigma";// CH"; htitle+=std::to_string(fChannel);
            //htitle+="Event "; htitle+=std::to_string(fCounter);
            htitle+=";Sample;ADC";
            hs = new TH1D(hname.c_str(),htitle.c_str(),Nsamples,0.,double(Nsamples));
            hs->SetLineColor(kGreen);
            hs->SetStats(0);

            hname="hd_CH"; hname+=std::to_string(fChannel);
            htitle="Deviation";// CH"; htitle+=std::to_string(fChannel);
            //htitle+="Event "; htitle+=std::to_string(fCounter);
            htitle+=";Sample;ADC";
            hd = new TH1D(hname.c_str(),htitle.c_str(),Nsamples,0.,double(Nsamples));
            hd->SetLineColor(kRed);
            hd->SetStats(0);

            hname="hr_CH"; hname+=std::to_string(fChannel);
            htitle="Result";// CH"; htitle+=std::to_string(fChannel);
            //htitle+="Event "; htitle+=std::to_string(fCounter);
            htitle+=";Sample;ADC";
            hr = new TH1D(hname.c_str(),htitle.c_str(),Nsamples,0.,double(Nsamples));
            hr->SetLineColor(kBlack);
            hr->SetStats(0);
         }
      
      for(int i=0; i<Nsamples; ++i)
         {
            hwf->SetBinContent(i+1,wf->at(i)); // raw wf
            hf->SetBinContent(i+1,f->at(i));  // filtered wf
            hb->SetBinContent(i+1,b->at(i));  // baseline
            hd->SetBinContent(i+1,d->at(i));  // deviation
            hs->SetBinContent(i+1,s->at(i));  // sigma
            hr->SetBinContent(i+1,r->at(i));  // result
            //hp;  // pulse
         }
      
      fCanvas->cd(1);
      hwf->Draw("hist");
      hf->Draw("histsame");
      hb->Draw("histsame");
      gPad->Update();
      gPad->BuildLegend();

      fCanvas->cd(2);
      hd->Draw("hist");
      hs->Draw("histsame");
      hr->Draw("histsame");
      gPad->Update();
      gPad->BuildLegend();

      //      fCanvas->Modified();
      //      fCanvas->Draw();
      fCanvas->Update();
   }
};


class KsTriggerModuleFactory: public TAFactory
{
public:
   KsTriggerFlags fFlags;
   
public:
   void Usage()
   {
      printf("KsTriggerModuleFactory flags:\n");
      
   }

   void Init(const std::vector<std::string> &args)
   {
      printf("KsTriggerModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" )
               fFlags.fVerbose = true;
            // if( args[i] == "--Ks" )
            //    {
            //       // ++i;
            //       // fFlags.fTau_f = atoi( args[i].c_str() );
            //       ++i;
            //       fFlags.fTau_b = atoi( args[i].c_str() );
            //       ++i;
            //       fFlags.fTau_s = atoi( args[i].c_str() );
            //    }
            // if( args[i] == "--sigma" )
            //    {
            //       ++i;
            //       fFlags.fNsigma = atof( args[i].c_str() );
            //    }
         }
      
   }
   
   void Finish()
   {
      printf("KsTriggerModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("KsTriggerModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new KsTriggerModule(runinfo, &fFlags);
   }

};

static TARegister tar(new KsTriggerModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
