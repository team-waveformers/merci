/******************************************************************
 *  Pulse Fitter for DS PDU+ *
 *
 * A. Capra
 * May 2023
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "baselineflow.hxx"
#include "dsflow.hxx"
#include "dsdata.hxx"
#include "avgspe.hxx"

#include <iostream>
#include <fstream>
#include <vector>
#include <cassert>

#include "Minuit2/FCNBase.h"
#include "Minuit2/FunctionMinimum.h"
#include "Minuit2/MnUserParameterState.h"
#include "Minuit2/MnMigrad.h"

#include "json.hpp"
using json = nlohmann::json;


class AvgSPEfcn: public ROOT::Minuit2::FCNBase {

private:
   std::vector<double> fMeasurements;
   std::vector<double> fPositions;
   std::vector<double> fMVariances;

public:
   AvgSPEfcn(const std::vector<double> &meas, const std::vector<double> &pos,
           const std::vector<double> &mvar): fMeasurements(meas), fPositions(pos),
                                             fMVariances(mvar)
   { }

   AvgSPEfcn(const std::vector<double> &meas, const std::vector<double> &pos):
      fMeasurements(meas), fPositions(pos), fMVariances(meas.size(),1.)
   { }

   ~AvgSPEfcn() {}

   virtual double Up() const { return 1.; }
   virtual double operator()(const std::vector<double> &par) const
   {
      assert(par.size() == 4);

      AvgSPE f = AvgSPE(par[0], par[1], par[2], par[3]);

      double chi2 = 0.;
      int nmeas = fMeasurements.size();
      for(int n = 0; n < nmeas; n++)
         {
            double x = f(fPositions[n]) - fMeasurements[n];
            chi2 += (x * x / fMVariances[n]);
         }

      return chi2;
   }

   virtual double ErrorDef() const { return Up(); }

   inline std::vector<double> Measurements() const { return fMeasurements; }
   inline std::vector<double> Positions() const    { return fPositions; }
   inline std::vector<double> Variances() const    { return fMVariances; }
};


class FitPduPlusFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};


class FitPduPlusModule: public TARunObject
{
public:
   FitPduPlusFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors
   int fOkFit;
   int fNpulses;

private:
   bool fFiltered;
   int fBefore;
   int fTotal;

   bool fFloatPar;
   double fTauShort;
   double fTauLong;

public:
   FitPduPlusModule(TARunInfo* runinfo, FitPduPlusFlags* f): TARunObject(runinfo),
                                                     fFlags(f),fCounter(0),fError(0),
                                                     fOkFit(0),fNpulses(0),
                                                     fFiltered(false)
   {
      if(fFlags->fVerbose) std::cout<<"FitPduPlusModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="FitPduPlus";
#endif

      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"FitPduPlusModule Json parsing success!"<<std::endl;
      fin.close();

      fFiltered=settings["Fit"]["Filtered"].get<bool>();
      fBefore=settings["Fit"]["Before Pulse"].get<int>();
      fTotal=settings["Fit"]["Pulse Length"].get<int>();
      assert(fTotal>6);

      fFloatPar=settings["Fit"]["Floating Parameters"].get<bool>();
      fTauShort=settings["Fit"]["tau short"].get<double>();
      assert(fTauShort>0.);
      fTauLong=settings["Fit"]["tau long"].get<double>();
      assert(fTauLong>0.);
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"FitPduPlusModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"FitPduPlusModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"FitPduPlusModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<" ok: "<<fOkFit<<" total: "<<fNpulses<<std::endl;
   }


   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
#ifdef __PHAAR__
      BaselineEventFlow* base_flow = flow->Find<BaselineEventFlow>();
      if( !base_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
#endif

      if( fFlags->fVerbose )
         std::cout<<"FitPduPlusModule::AnalyzeFlowEvent # of ch: "<<wf_flow->GetNumberOfChannels()<<std::endl;

      for(int i=0;i<wf_flow->GetNumberOfPulses();++i)
         {
            const TDSPulse* dsp = wf_flow->GetDSpulse(i);
            double timestep = wf_flow->conf[dsp->board].fNanosecsPerSample;
            double maxA = wf_flow->conf[dsp->board].fResolution;
            ++fNpulses;

            // identify the start the pulse
            int start = dsp->time-fBefore;
            if( start < 0 ) start = 0;
            double tmin = double(start*timestep);
            int end = start + fTotal;
            if( fFlags->fVerbose )
               std::cout<<"FitPduPlusModule::   ch:"<<dsp->index<<"   pulse # "<<i
                        <<" start: "<<start<<" ("<<tmin<<"ns)";

            // fetch the waveform from the event flow
            const std::vector<double>* wf;
// #ifndef __PHAAR__
//             TDSChannel* dsch = wf_flow->GetDSchan(dsp->index);
            TDSChannel* dsch = wf_flow->GetChannel(dsp->index);
            if( dsch->unfiltered_wf.size() > 0 )
               wf = &dsch->unfiltered_wf;
            else if( (dsch->filtered_wf.size() > 0) && fFiltered )
               wf = &dsch->filtered_wf;
            else continue;
// #else
//             wf = base_flow->getChannelVector(dsp->index);
// #endif

            // identify the end of the pulse
            if( end > int(wf->size()) ) end = int(wf->size());
            double tmax = double(end*timestep);
            if( fFlags->fVerbose )
               std::cout<<" - end: "<<end<<" ("<<tmax
                        <<"ns)\t wf size: "<<wf->size()<<std::endl;

            // prepare the data to fit
            std::vector<double> meas(wf->begin()+start,wf->begin()+end);
            std::vector<double> pos;
            int nmeas = meas.size();
            assert(nmeas==fTotal);
            for(int n=0; n<nmeas; ++n)
               pos.push_back(double(start+n)*timestep);
            std::vector<double> err(nmeas,3.9);

            // create FCN function
            AvgSPEfcn fFCN(meas, pos, err);

            // initial guess of the floating parameters
            auto it_t = std::max_element(meas.begin(),meas.end());
            double A = *it_t;
            // size_t n_t = std::distance(meas.begin(),it_t); 
            // double t = pos[n_t];
            double t = dsp->time*timestep;
             if( fFlags->fVerbose )  // verbose output
                std::cout<<"FitPduPlusModule::AnalyzeFlowEvent Initial Fit Parameters A: "<<A
                         <<"  t0: "<<t<<" ns"<<std::endl;

            // create Minuit parameters with names
            ROOT::Minuit2::MnUserParameters upar;
            upar.Add("A",  A, 1, 0., maxA);
            upar.Add("t0", t, 1.e-3, tmin, tmax);
            if( fFloatPar ) // floating parameters with initial guess
               {
                  upar.Add("tau_short", fTauShort, 1.e-3, timestep, fTauLong);
                  upar.Add("tau_long",  fTauLong,  1.e-3, fTauShort, nmeas*timestep);
               }
            else // fixed parameters set in configuration file
               {
                  upar.Add("tau_short", fTauShort);
                  upar.Add("tau_long",  fTauLong);
               }

            // create Migrad minimizer
            ROOT::Minuit2::MnMigrad migrad(fFCN, upar);

            // if parameters are floating minimize the 4-parameters
            // function then fix them
            if( fFloatPar )
               {
                  // Minimize FCN with 4 parameters
                  ROOT::Minuit2::FunctionMinimum min1 = migrad();

                  if( fFlags->fVerbose )  // verbose output
                     std::cout<<"FitPduPlusModule::AnalyzeFlowEvent ch: "<<dsp->index
                              <<" Pulse # "<<i
                              <<" 1st minimum: " << min1 << std::endl;

                  if( !min1.IsValid() ) // verify that the fit is valid
                     {
                        if( fFlags->fVerbose )
                        std::cerr<<"FitPduPlusModule 1st Fit is not valid on ch: "<<dsp->index<<std::endl;
                        ++fError;
                        wf_flow->peaks->emplace_back(-1.,dsp->index, i,
                                                     tmin,tmax);
                        continue;
                     }
                  // Fix shape Parameters... to find accurate charge and falling edge
                  migrad.Fix("A");
                  migrad.Fix("t0");
               }

            // Minimize FCN with 2 parameters (charge and falling edge)
            ROOT::Minuit2::FunctionMinimum min2 = migrad();

            if( fFlags->fVerbose ) // verbose output
               std::cout<<"FitPduPlusModule::AnalyzeFlowEvent ch: "<<dsp->index
                        <<" Pulse # "<<i
                        <<" 2nd minimum: " << min2 << std::endl;

            if( !min2.IsValid() ) // verify that the fit is valid
               {
                  std::cerr<<"FitPduPlusModule 2nd Fit is not valid on ch: "<<dsp->index<<std::endl;
                  ++fError;
                  wf_flow->peaks->emplace_back(-2.,dsp->index, i,
                                               tmin,tmax);
                  continue;
               }
            else ++fOkFit;

            // degrees of freedom after fixing parameters
            double ndof = (double)nmeas-2.;
            // save fit results in event flow
            wf_flow->peaks->emplace_back(min2.UserState().Value("t0"),
                                         -1.,
                                         min2.UserState().Value("tau_short"),
                                         min2.UserState().Value("tau_long"),
                                         min2.UserState().Value("A"),
                                         -1.,
                                         min2.Fval()/ndof,
                                         dsp->index, i,
                                         tmin,tmax);
            if( fFlags->fVerbose ) // verbose output
               std::cout<<"=== Done with "<<i<<" ==="<<std::endl;
         }

      flow=wf_flow;
      ++fCounter;
      return flow;
   }
};


class FitPduPlusModuleFactory: public TAFactory
{
public:
   FitPduPlusFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      printf("FitPduPlusModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
   }

   void Finish()
   {
      printf("FitPduPlusModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("FitPduPlusModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new FitPduPlusModule(runinfo, &fFlags);
   }

};

static TARegister tar(new FitPduPlusModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
