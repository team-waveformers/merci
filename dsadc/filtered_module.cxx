/******************************************************************
 * Statistics of Filerted *
 * 
 * A. Capra
 * August 2022
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "baselineflow.hxx"
#include "dsflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>
#include <cassert>

#include "filter.hxx"

#include "json.hpp"
using json = nlohmann::json;


class FilteredFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};
  

class FilteredModule: public TARunObject
{
public:
   FilteredFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors

   // Filtered waveform baseline
   int fPedestal;
   int fShift;

public:
   FilteredModule(TARunInfo* runinfo, FilteredFlags* f): TARunObject(runinfo),
                                                     fFlags(f), fCounter(0), fError(0),
                                                     fPedestal(100)
   {
      if(fFlags->fVerbose) std::cout<<"FilteredModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Filtered";
#endif
      
      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"FilteredModule Json parsing success!"<<std::endl;
      fin.close();

      try
         {
            fPedestal=settings["Baseline"]["Pedestal"].get<int>();
         }
      catch(json::out_of_range& e) 
         {
            std::cerr<<e.what()<<std::endl;
         }

      std::string filter_name = settings["Filter"]["Name"].get<std::string>();
      if( filter_name == "Moving Average" )
         {
            fShift = settings["Filter"]["Par0"].get<int>();
         }
      else if( filter_name == "Exponential" )
         {
            fShift = 1;
         }
      else if( filter_name == "Exponential with Memory" )
         {
            fShift = settings["Filter"]["Par1"].get<int>();
         }
      else if( filter_name == "ARMA" )
         {
            fShift = 0;
         }
      else if( filter_name == "AR" )
         {
            fShift = 0;
         }
      else if( filter_name == "FIR" )
         {
            std::vector<int> a;
            runinfo->fOdb->RIA("/VX2740 defaults/User registers/FIR filter coefficients",&a);
            fShift = static_cast<int>(a.size());
         }
      else if( filter_name == "Cross-Correlation" )
         {
            try 
               {
                  std::string cfile=settings["Filter"]["Par0"].get<std::string>();
                  std::ifstream inFile(cfile); 
                  fShift = std::count(std::istreambuf_iterator<char>(inFile), 
                             std::istreambuf_iterator<char>(), '\n');
               }
            catch(json::type_error& e) 
               {
                  std::cerr<<e.what()<<" couldn't find path to template file, trying with sequence"<<std::endl;            
                  std::vector<double> a;
                  try 
                     {
                        a = settings["Filter"]["Par0"].get<std::vector<double>>();
                        fShift = static_cast<int>(a.size());
                     }
                  catch(json::type_error& e) 
                     {
                        std::cerr<<e.what()<<" coeffiecient sequence not available"<<std::endl;
                     }
                  
                  if( a.empty() ) fShift = 100;
                  else if( a.size() == 1 ) fShift = a[0];
               }
         }
      else
         {
            std::cerr<<"Unkwown filter "<<filter_name<<std::endl;
         }
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"FilteredModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"FilteredModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"FilteredModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      for(int i=0; i<wf_flow->GetNumberOfChannels(); ++i)
         {
            TDSChannel* dsch = wf_flow->GetDSchan(i);

#ifdef _HIT_FINDER_
            std::vector<double> filtered_waveform = dsch->peak;
#else
            std::vector<double> filtered_waveform = dsch->filtered_wf;
#endif
            
            // calculate the baseline and its rms of the filtered waveform
            double bline = std::accumulate(filtered_waveform.begin()+fShift,
                                           filtered_waveform.begin()+fShift+fPedestal,
                                           double(0))/(double)fPedestal;
            dsch->filter_baseline = bline;
            double frms = std::inner_product(filtered_waveform.begin()+fShift,
                                             filtered_waveform.begin()+fShift+fPedestal,
                                             filtered_waveform.begin()+fShift,double(0));
            frms = sqrt( (frms / (double) fPedestal) - (bline * bline) );
            dsch->filter_rms = frms;

            // calculate the standard deviation of the whole waveform
            // using the mean baseline of the fist nbase_samples
            double number_of_samples = (double) (filtered_waveform.size() - fShift);
            double wf_rms=std::inner_product(filtered_waveform.begin()+fShift,
                                             filtered_waveform.end(),
                                             filtered_waveform.begin(),double(0));
            wf_rms=sqrt( wf_rms / number_of_samples );
            dsch->filterwf_rms = wf_rms;

            if( fFlags->fVerbose )
               std::cout<<"FilteredModule::AnalyzeFlowEvent ch: "<<dsch->index
                        <<" wf rms: "<<wf_rms
                        <<"\tflt baseline: "<<bline
                        <<" RMS: "<<frms
                        <<std::endl;
         }
      
      flow=wf_flow;
      ++fCounter;
      return flow;
   }
};


class FilteredModuleFactory: public TAFactory
{
public:
   FilteredFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("FilteredModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
      
   }
   
   void Finish()
   {
      printf("FilteredModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("FilteredModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new FilteredModule(runinfo, &fFlags);
   }

};

static TARegister tar(new FilteredModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
