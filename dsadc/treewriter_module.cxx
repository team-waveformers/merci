/******************************************************************
 *  TTree Writer *
 *
 * A. Capra
 * August 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "adcflow.hxx"
#include "dsflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <fstream>

#include "TTree.h"
#include "TObjString.h"
#include "TError.h"

#include "json.hpp"
using json = nlohmann::json;

class TreeWriterFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};

class TreeWriterModule: public TARunObject
{
public:
   TreeWriterFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors

private:
  std::vector<int> fChanIndex;
  std::vector<double> fBaseline;
  std::vector<double> fNoise;
  std::vector<double> fMaxPH;
  std::vector<double> fFilterBaseline;
  std::vector<double> fFilterNoise;
  std::vector<double> fROIcharge;
  std::vector<double> fROIheight;
  std::vector<int>    fROItime;
  std::vector<double> ffilterROIcharge;
  std::vector<double> ffilterROIheight;
  std::vector<int>    ffilterROItime;
  std::vector<double> fwfrms;
  std::vector<double> ffwfrms;
  std::vector<unsigned int> fChanEventCounter;
  std::vector<double> fChanTriggerTime;
  int fNchan;
  TTree *ChanTree;

  std::vector<int> fPulseIndex;//this is the same as fChanIndex, the same index
  std::vector<double> fCharge;
  std::vector<double> fTime;
  std::vector<double> fAmplitude;
  std::vector<double> fTimeOverThreshold;
  std::vector<unsigned int> fPulseEventCounter;
  std::vector<double> fPulseTriggerTime;
  int fNpulse;
  TTree *PulseTree;

  std::vector<int> fPeakIndex;//this is the same as fChanIndex, the same index
  std::vector<double> fA;
  std::vector<double> fTime0;
  std::vector<double> fTauShort;
  std::vector<double> fTauLong;
  std::vector<double> fchi2;
  int fNpeak;
  TTree *PeakTree;

  double fmidasts;
  int fevent_number;

public:
   TreeWriterModule(TARunInfo* runinfo, TreeWriterFlags* f): TARunObject(runinfo),
							     fFlags(f),
							     fCounter(0), fError(0),
							     fNchan(0), ChanTree(0),
							     fNpulse(0), PulseTree(0),
                                 fmidasts(0.),fevent_number(-1)
   {
      if(fFlags->fVerbose) std::cout<<"TreeWriterModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="TreeWriter";
#endif
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"TreeWriterModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"TreeWriterModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;

      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory

      ChanTree = new TTree("TChan","Darkside Channels");
      ChanTree->Branch("index",&fChanIndex); // index = ADC# x chans per ADC + CH#
      ChanTree->Branch("baseline",&fBaseline); // baseline in ADC
      ChanTree->Branch("noise",&fNoise); // baseline rms
      ChanTree->Branch("maxph",&fMaxPH); // amplitude of WF in ADC
      ChanTree->Branch("filtbaseline",&fFilterBaseline); // baseline of filetered WF ~0
      ChanTree->Branch("filtnoise",&fFilterNoise); // baseline rms of filtered WF
      ChanTree->Branch("roiq",&fROIcharge); // integral
      ChanTree->Branch("roih",&fROIheight); // ADC
      ChanTree->Branch("roit",&fROItime); // number of samples
      ChanTree->Branch("filtroiq",&ffilterROIcharge); // intergral
      ChanTree->Branch("filtroih",&ffilterROIheight); // a.u.
      ChanTree->Branch("filtroit",&ffilterROItime); // number of samples
      ChanTree->Branch("wfrms",&fwfrms); // rms of the whole waveform
      ChanTree->Branch("filtwfrms",&ffwfrms); // rms of the whole filtered waveform
      ChanTree->Branch("nchan",&fNchan); // number of active channels
      ChanTree->Branch("mts",&fmidasts); // unix epoch
      ChanTree->Branch("eventcounter",&fChanEventCounter);
      ChanTree->Branch("trigtime",&fChanTriggerTime); // s
      ChanTree->Branch("Nevent",&fevent_number);


      PulseTree = new TTree("TPulse","Darkside Pulses");
      PulseTree->Branch("index",&fPulseIndex); // index = ADC# x chans per ADC + CH#
      PulseTree->Branch("charge",&fCharge); // integral
      PulseTree->Branch("time",&fTime); // number of samples
      PulseTree->Branch("height",&fAmplitude); // ADC
      PulseTree->Branch("ToT",&fTimeOverThreshold);// time over threshold in samples
      PulseTree->Branch("npulse",&fNpulse); // number of pulses found in the event
      PulseTree->Branch("mts",&fmidasts); // unix epoch
      PulseTree->Branch("eventcounter",&fPulseEventCounter);
      PulseTree->Branch("trigtime",&fPulseTriggerTime); // s
      PulseTree->Branch("Nevent",&fevent_number);

      PeakTree = new TTree("TPeak","Best-fit Pulses");
      PeakTree->Branch("index",&fPeakIndex);
      PeakTree->Branch("amplitude",&fA);
      PeakTree->Branch("time",&fTime0);
      PeakTree->Branch("taushort",&fTauShort);
      PeakTree->Branch("taulong",&fTauLong);
      PeakTree->Branch("chi2",&fchi2);
      PeakTree->Branch("npeak",&fNpeak);
      PeakTree->Branch("mts",&fmidasts);
      PeakTree->Branch("Nevent",&fevent_number);

      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"TreeWriterModule Json parsing success!"<<std::endl;
      fin.close();

      std::cout<<"TreeWriterModule::BeginRun Saving settings to rootfile... ";
      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      int error_level_save = gErrorIgnoreLevel;
      gErrorIgnoreLevel = kFatal;
      TObjString settings_obj(settings.dump().c_str());
      int bytes_written = gDirectory->WriteTObject(&settings_obj,"config");
      if( bytes_written > 0 )
         std::cout<<" DONE ("<<bytes_written<<")"<<std::endl;
      else
         std::cout<<" FAILED"<<std::endl;
      gErrorIgnoreLevel = error_level_save;

      uint32_t midas_start_time;
      runinfo->fOdb->RU32("Runinfo/Start time binary",(uint32_t*) &midas_start_time);
      time_t ts1 = midas_start_time;
      std::string string_time=std::to_string(ts1);
      std::cout<<"TreeWriterModule::BeginRun Saving start time to rootfile... ";
      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      error_level_save = gErrorIgnoreLevel;
      gErrorIgnoreLevel = kFatal;
      TObjString string_time_obj(string_time.c_str());
      bytes_written = gDirectory->WriteTObject(&string_time_obj,"BeginRunUnixTS");
      if( bytes_written > 0 )
         std::cout<<" DONE ("<<bytes_written<<")"<<std::endl;
      else
         std::cout<<" FAILED"<<std::endl;
      gErrorIgnoreLevel = error_level_save;

   }

   void EndRun(TARunInfo* runinfo)
   {
      uint32_t midas_end_time;
      runinfo->fOdb->RU32("Runinfo/Stop time binary",(uint32_t*) &midas_end_time);
      time_t ts1 = midas_end_time;
      std::string string_time=std::to_string(ts1);
      std::cout<<"TreeWriterModule::EndRun Saving end time to rootfile... ";
      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      int error_level_save = gErrorIgnoreLevel;
      gErrorIgnoreLevel = kFatal;
      TObjString string_time_obj(string_time.c_str());
      int bytes_written = gDirectory->WriteTObject(&string_time_obj,"EndRunUnixTS");
      if( bytes_written > 0 )
         std::cout<<" DONE ("<<bytes_written<<")"<<std::endl;
      else
         std::cout<<" FAILED"<<std::endl;
      gErrorIgnoreLevel = error_level_save;
      std::cout<<"TreeWriterModule::EndRun() run: "<<runinfo->fRunNo
               <<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }


   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow )
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            ++fError;
            return flow;
         }

      AdcEventFlow* adc_flow = flow->Find<AdcEventFlow>();
      if( !adc_flow ) fmidasts=-1.;
      else
         {
            fmidasts=adc_flow->fmidas_ts;
            fevent_number=adc_flow->getEventNumber();
         }

      if(fFlags->fVerbose)
         std::cout<<"TreeWriterModule::AnalyzeFlowEvent Writing "
                  <<wf_flow->GetNumberOfChannels()<<" channels"<<std::endl;
      fNchan=0;
      fChanIndex.clear();
      fBaseline.clear();
      fNoise.clear();
      fMaxPH.clear();
      fFilterBaseline.clear();
      fFilterNoise.clear();
      fROIcharge.clear();
      fROIheight.clear();
      fROItime.clear();
      ffilterROIcharge.clear();
      ffilterROIheight.clear();
      ffilterROItime.clear();
      fwfrms.clear();
      ffwfrms.clear();
      fChanEventCounter.clear();
      fChanTriggerTime.clear();
      for(int i=0; i<wf_flow->GetNumberOfChannels(); ++i)
         {
            TDSChannel* dsch = wf_flow->GetDSchan(i);
	    fChanIndex.push_back(dsch->index);

	    fBaseline.push_back(dsch->baseline);
	    fNoise.push_back(dsch->baseline_rms);
            fMaxPH.push_back(dsch->maxph);

	    fFilterBaseline.push_back(dsch->filter_baseline);
	    fFilterNoise.push_back(dsch->filter_rms);

            fROIcharge.push_back(dsch->roicharge);
            fROIheight.push_back(dsch->roipeak);
            fROItime.push_back(dsch->roitime);

            ffilterROIcharge.push_back(dsch->filter_roicharge);
            ffilterROIheight.push_back(dsch->filter_roipeak);
            ffilterROItime.push_back(dsch->filter_roitime);

            fwfrms.push_back(dsch->wf_rms);
            ffwfrms.push_back(dsch->filterwf_rms);

            fChanEventCounter.push_back(adc_flow->fEventCounters[dsch->module]);
            fChanTriggerTime.push_back(adc_flow->fTriggerTime[dsch->module]);

            //dsch->Print();
            // dsch->PrintROI();
	    ++fNchan;
         }
      ChanTree->Fill();

      if(fFlags->fVerbose)
         std::cout<<"TreeWriterModule::AnalyzeFlowEvent Writing "
                  <<wf_flow->GetNumberOfPulses()<<" pulses"<<std::endl;
      fNpulse=0;
      fPulseIndex.clear();
      fCharge.clear();
      fTime.clear();
      fAmplitude.clear();
      fTimeOverThreshold.clear();
      fPulseEventCounter.clear();
      fPulseTriggerTime.clear();
      for( int i=0; i<wf_flow->GetNumberOfPulses(); ++i)
	{
	   const TDSPulse* dsp =wf_flow->GetDSpulse(i);
	   fPulseIndex.push_back(dsp->index);
	   fCharge.push_back(dsp->charge);
	   fTime.push_back(dsp->time);
	   fAmplitude.push_back(dsp->height);
           fTimeOverThreshold.push_back(dsp->tot);
           fPulseEventCounter.push_back(adc_flow->fEventCounters[dsp->module]);
           fPulseTriggerTime.push_back(adc_flow->fTriggerTime[dsp->module]);
           //dsp->Print();
	   ++fNpulse;
	 }
      PulseTree->Fill();

      fNpeak=0;
      fPeakIndex.clear();
      fA.clear();
      fTime0.clear();
      fTauShort.clear();
      fTauLong.clear();
      fchi2.clear();
      for( int i=0; i<wf_flow->GetNumberOfPeaks(); ++i)
         {
            const TDSPeak* dspk = wf_flow->GetDSpeak(i);
            fPeakIndex.push_back(dspk->ChannelIndex);
            fA.push_back(dspk->Charge);
            fTime0.push_back(dspk->PulseTime);
            fTauShort.push_back(dspk->FallTimeShort);
            fTauLong.push_back(dspk->FallTimeLong);
            fchi2.push_back(dspk->chi2);
            ++fNpeak;
         }
      PeakTree->Fill();

      ++fCounter;
      return flow;
   }
};


class TreeWriterModuleFactory: public TAFactory
{
public:
   TreeWriterFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      printf("TreeWriterModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
             if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
	 }
   }

   void Finish()
   {
      printf("TreeWriterModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("TreeWriterModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new TreeWriterModule(runinfo, &fFlags);
   }

};

static TARegister tar(new TreeWriterModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
