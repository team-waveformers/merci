/******************************************************************
 *  Basic Filerting *
 * 
 * A. Capra
 * August 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "baselineflow.hxx"
#include "dsflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <fstream>
#include <vector>
#include <cmath>

#include <cassert>

#include "filter.hxx"

#include "json.hpp"
using json = nlohmann::json;

#include "TCanvas.h"
#include "TH1D.h"
#include "TLegend.h"


class FilterFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};
  

class FilterModule: public TARunObject
{
public:
   FilterFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors

   int fWindow=64;
   double fSmoothing=0.88;
   bool fMAF=false;
   bool fEXF=false;

   // experimental feature
   int fMemory=3;
   bool fEnF=false;

   // ARMA filter, see filter.hxx:ARMA
   bool fARMA=false;
   double fARMAtau=80.;
   double fARMAsigma=2.;
   double fARMAexp=1.;

  // AR filter
   bool fARF=false;

   // 'Generic' FIR
   bool fFIR=false;
   std::vector<double> fCoeff;

   // Cross-Correlation filter
   bool fCCF=false;
   std::string fTemplateFile;


   // Filter classes
   MAF avgfilter;
   EXF expfilter;
   EnF testfilter;
   ARMA armafilter;
   ARF arfilter;
   FIR firfilter;
   FIR xcorrfilter;

   // Filtered waveform baseline
   int fPedestal;

private:
   TH1D *hRawAvg, *hMAAvg, *hARAvg, *hARMAAvg;

public:
   FilterModule(TARunInfo* runinfo, FilterFlags* f): TARunObject(runinfo),
                                                     fFlags(f), fCounter(0), fError(0),
                                                     fPedestal(100)
   {
      if(fFlags->fVerbose) std::cout<<"FilterModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Filter";
#endif
      
      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"FilterModule Json parsing success!"<<std::endl;
      fin.close();

      try
         {
            fPedestal=settings["Baseline"]["Pedestal"].get<int>();
         }
      catch(json::out_of_range& e) 
         {
            std::cerr<<e.what()<<std::endl;
         }

      std::string filter_name = settings["Filter"]["Name"].get<std::string>();
      if( filter_name == "Moving Average" )
         {
            fMAF=true;
            fWindow=settings["Filter"]["Par0"].get<int>();
            std::cout<<filter_name<<" window: "<<fWindow<<std::endl;
            //avgfilter = MAF( fWindow );
            //            avgfilter.SetPedestal(fPedestal);
         }
      else if( filter_name == "Exponential" )
         {
            fEXF=true;
            fSmoothing=settings["Filter"]["Par0"].get<double>();
            std::cout<<filter_name<<" smoothing: "<<fSmoothing<<std::endl;
            expfilter = EXF( fSmoothing );
            //            expfilter.SetPedestal(fPedestal);
         }
      else if( filter_name == "Exponential with Memory" )
         {
            fEnF=true;
            fSmoothing=settings["Filter"]["Par0"].get<double>();
            fMemory=settings["Filter"]["Par1"].get<int>();
            std::cout<<filter_name<<" smoothing: "<<fSmoothing<<" memory: "<<fMemory<<std::endl;
            testfilter = EnF( fSmoothing, fMemory );
            //testfilter.SetPedestal(fPedestal);
         }
      else if( filter_name == "ARMA" )
         {
            fARMA=true;
            fARMAtau=settings["Filter"]["Par0"].get<double>();
            fARMAsigma=settings["Filter"]["Par1"].get<double>();
            fARMAexp=settings["Filter"]["Par2"].get<double>();
            std::cout<<filter_name<<" tau: "<<fARMAtau
                     <<" sigma_fp: "<<fARMAsigma<<" Afp_Aexp_ratio: "<<fARMAexp<<std::endl;
            //armafilter = ARMA( fARMAtau, fARMAsigma, fARMAexp );
            // armafilter.SetPedestal(fPedestal);
         }
      else if( filter_name == "AR" )
         {
            fARF=true;
            fARMAtau=settings["Filter"]["Par0"].get<double>();
            std::cout<<filter_name<<" filter tau: "<<fARMAtau<<std::endl;
            //arfilter = ARF( fARMAtau );
         }
      else if( filter_name == "FIR" )
         {
            fFIR=true;
            fCoeff=settings["Filter"]["Par0"].get<std::vector<double>>();
            if( fCoeff.empty() )
               fCoeff.assign(100,1);
            else if( fCoeff.size() == 1 )
               fCoeff.assign(fCoeff[0],1);

            firfilter = FIR(fCoeff);
            std::cout<<filter_name<<" filter with "<<firfilter.GetNumberOfCoefficients()<<" coefficients"<<std::endl;
         }
      else if( filter_name == "Cross-Correlation" )
         {
            fCCF=true;
            fTemplateFile=settings["Filter"]["Par0"].get<std::string>();
            xcorrfilter=FIR(fTemplateFile);
            std::cout<<filter_name<<" filter with "<<xcorrfilter.GetNumberOfCoefficients()<<" coefficients"<<std::endl;
         }
      else
         {
            std::cerr<<"Unkwown filter "<<filter_name<<std::endl;
         }

      avgfilter = MAF( fWindow );
      armafilter = ARMA( fARMAtau, fARMAsigma, fARMAexp );
      arfilter = ARF( fARMAtau );
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"FilterModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"FilterModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;

      int nsamples=2000;
      hRawAvg = new TH1D("hRawAvgCH0",";samples;ADC",nsamples,0,nsamples);
      hRawAvg->SetLineColor(kBlack);
      //  hRaw->SetFillColor(kBlack);
      hRawAvg->SetStats(0);

      hMAAvg = new TH1D("hMAAvgCH0",";samples;ADC",nsamples,0,nsamples);
      hMAAvg->SetLineColor(kRed);
      hMAAvg->SetLineWidth(2);
      hMAAvg->SetStats(0);
      
      hARAvg = new TH1D("hARAvgCH0",";samples;ADC",nsamples,0,nsamples);
      hARAvg->SetLineColor(kGreen);
      hARAvg->SetLineWidth(2);
      hARAvg->SetStats(0);

      hARMAAvg = new TH1D("hARMAAvgCH0",";samples;ADC",nsamples,0,nsamples);
      hARMAAvg->SetLineColor(kBlue);
      hARMAAvg->SetLineWidth(2);
      hARMAAvg->SetStats(0);
   }

   void EndRun(TARunInfo* runinfo)
   {
      hRawAvg->Scale(0.5/fCounter);
      hMAAvg->Scale(1./fCounter);
      hARAvg->Scale(1./fCounter);
      hARMAAvg->Scale(1./fCounter);

      TCanvas c("cAvgCH0","cAvgCH0",2000,1300);
      hRawAvg->Draw("HIST");
      hMAAvg->Draw("HISTsame");
      hARAvg->Draw("HISTsame");
      hARMAAvg->Draw("HISTsame");
      TLegend* leg = new TLegend(0.75,0.2,0.95,0.47);
      leg->AddEntry(hRawAvg,"raw","l");
      leg->AddEntry(hMAAvg,"average","l");
      leg->AddEntry(hARAvg,"exponential","l");
      leg->AddEntry(hARMAAvg,"ARMA","l");
      leg->Draw("same");
      c.SaveAs(".png");
      std::cout<<"FilterModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      BaselineEventFlow* base_flow = flow->Find<BaselineEventFlow>();
      if( !base_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      if( fFlags->fVerbose )
         std::cout<<"FilterModule::AnalyzeFlowEvent : "<<base_flow->getNchannels()<<std::endl;
      
      DSProcessorFlow* wf_flow = new DSProcessorFlow(flow);
      wf_flow->conf[base_flow->fTypes[0]].fNSamples=base_flow->getNsamples(0);
      // wf_flow->fEventNumber=adc_flow->getEventNumber();
      int shift=0;
      
      for(int i=0; i<base_flow->getNchannels(); ++i)
         {
            TDSChannel dsch( base_flow->getADCmodule(i), base_flow->getADCchannel(i),
                             base_flow->fTypes.at(i).c_str() );
            dsch.unfiltered_wf = *(base_flow->getChannelVector(i));
            dsch.baseline = base_flow->getBaseline(i);
            dsch.baseline_rms = base_flow->getBaselineRMS(i);
            if( fMAF )
               {
                  dsch.filtered_wf = std::vector<double>(*avgfilter( &dsch.unfiltered_wf ));
                  shift=fWindow;
               }
            else if( fEXF )
               {
                  dsch.filtered_wf = std::vector<double>(*expfilter( &dsch.unfiltered_wf ));
                  shift=1;
               }
            else if( fEnF )
               {
                  dsch.filtered_wf = std::vector<double>(*testfilter( &dsch.unfiltered_wf ));
                  shift=fMemory;
               }
            else if( fARMA )
               {
                  dsch.filtered_wf = std::vector<double>(*armafilter( &dsch.unfiltered_wf ));
               }
            else if( fARF )
               {
                  dsch.filtered_wf = std::vector<double>(*arfilter( &dsch.unfiltered_wf ));
               }
            else if( fFIR )
               {
                  dsch.filtered_wf = std::vector<double>(*firfilter( &dsch.unfiltered_wf ));
                  shift=firfilter.GetNumberOfCoefficients();
               }
            else if( fCCF )
               {
                  dsch.filtered_wf = std::vector<double>(*xcorrfilter( &dsch.unfiltered_wf ));
                  shift=xcorrfilter.GetNumberOfCoefficients();
               }
            else
               {
                  if( fFlags->fVerbose )
                     std::cout<<"FilterModule::AnalyzeFlowEvent WARNING: the filtered waveform will be unfiltered"<<std::endl;
                  dsch.filtered_wf = std::vector<double>(dsch.unfiltered_wf);
               }

            if( dsch.index == 0 )
               {
#ifdef MODULE_MULTITHREAD
                  std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
                  int nsamples = dsch.unfiltered_wf.size();
                  std::vector<double> bins(nsamples);
                  std::iota(bins.begin(),bins.end(),1);

                  std::vector<double> raw(nsamples);
                  std::transform(dsch.unfiltered_wf.begin(),dsch.unfiltered_wf.end(),raw.begin(),
                                 [](double x)-> double { return -1.*x;} );
                  hRawAvg->FillN(nsamples,bins.data(),raw.data());
                  TString hname=TString::Format("hWFCH%d",dsch.index);
                  TH1D* hRaw = new TH1D(hname,";samples;ADC",nsamples,0,nsamples);
                  hRaw->SetLineColor(kBlack);
                  hRaw->SetStats(0);
                  hRaw->FillN(nsamples,bins.data(),raw.data());
                  
                  std::vector<double> ma = std::vector<double>(*avgfilter( &raw ));
                  hMAAvg->FillN(nsamples,bins.data(),ma.data());
                  hname=TString::Format("hMACH%d",dsch.index);
                  TH1D* hMA = new TH1D(hname,";samples;ADC",nsamples,0,nsamples);
                  hMA->SetLineColor(kRed);
                  hMA->SetLineWidth(2);
                  hMA->SetStats(0);
                  hMA->FillN(nsamples,bins.data(),ma.data());

                  std::vector<double> ar = std::vector<double>(*arfilter( &raw ));
                  hARAvg->FillN(nsamples,bins.data(),ar.data());
                  hname=TString::Format("hARCH%d",dsch.index);
                  TH1D* hAR = new TH1D(hname,";samples;ADC",nsamples,0,nsamples);
                  hAR->SetLineColor(kGreen);
                  hAR->SetLineWidth(2);
                  hAR->SetStats(0);
                  hAR->FillN(nsamples,bins.data(),ar.data());

                  std::vector<double> arma = std::vector<double>(*armafilter( &raw ));
                  hARMAAvg->FillN(nsamples,bins.data(),arma.data());  
                  hname=TString::Format("hARMACH%d",dsch.index);
                  TH1D* hARMA = new TH1D(hname,";samples;ADC",nsamples,0,nsamples);
                  hARMA->SetLineColor(kBlue);
                  hARMA->SetLineWidth(2);
                  hARMA->SetStats(0);
                  hARMA->FillN(nsamples,bins.data(),arma.data());

                  TString cname=TString::Format("cfltCH%dEV%d",dsch.index,fCounter);
                  TCanvas* c1 = new TCanvas(cname,cname,2000,1300);
                  hRaw->Draw("HIST");
                  hMA->Draw("HISTsame");
                  hAR->Draw("HISTsame");
                  hARMA->Draw("HISTsame");
                  TLegend* leg1 = new TLegend(0.75,0.2,0.95,0.47);
                  leg1->AddEntry(hRaw,"raw","l");
                  leg1->AddEntry(hMA,"average","l");
                  leg1->AddEntry(hAR,"exponential","l");
                  leg1->AddEntry(hARMA,"ARMA","l");
                  leg1->Draw("same");
                  c1->SaveAs(".png");
               }

            // calculate the baseline and its rms of the filtered waveform
            double bline = std::accumulate(dsch.filtered_wf.begin()+shift,
                                           dsch.filtered_wf.begin()+shift+fPedestal,
                                           double(0))/(double)fPedestal;
            dsch.filter_baseline = bline;
            double frms = std::inner_product(dsch.filtered_wf.begin()+shift,
                                             dsch.filtered_wf.begin()+shift+fPedestal,
                                             dsch.filtered_wf.begin()+shift,double(0));
            frms = sqrt( (frms / (double) fPedestal) - (bline * bline) );
            dsch.filter_rms = frms;

            // calculate the standard deviation of the whole waveform
            // using the mean baseline of the fist nbase_samples
            double number_of_samples = (double) (dsch.filtered_wf.size() - shift);
            double wf_rms=std::inner_product(dsch.filtered_wf.begin()+shift,
                                             dsch.filtered_wf.end(),
                                             dsch.filtered_wf.begin(),double(0));
            //wf_rms=sqrt( (wf_rms / number_of_samples) - (bline*bline) );
            wf_rms=sqrt( wf_rms / number_of_samples );
            dsch.filterwf_rms = wf_rms;
            if( fFlags->fVerbose )
               std::cout<<"FilterModule::AnalyzeFlowEvent ch: "<<dsch.index
                        <<" wf rms: "<<wf_rms
                        <<"\tflt baseline: "<<bline
                        <<" RMS: "<<frms
                        <<std::endl;

            wf_flow->channels->push_back(dsch);
         }
      
      flow=wf_flow;
      ++fCounter;
      return flow;
   }

};


class FilterModuleFactory: public TAFactory
{
public:
   FilterFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("FilterModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
      
   }
   
   void Finish()
   {
      printf("FilterModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("FilterModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new FilterModule(runinfo, &fFlags);
   }

};

static TARegister tar(new FilterModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
