#ifndef __DSDATA__
#define __DSDATA__

#include <iostream>
#include <iomanip>
#include <vector>

/// Class to store a pulse
class TDSPulse
{
public:
  TDSPulse(int imodule = -1, int ichannel = -1,
	   const char* iboard="V1725" ):board(iboard),
                                        module(imodule),
                                        channel(ichannel),
                                        time(0.),height(0.),
                                        charge(0.),tot(0.)
   
  {
    ADCchan=0;
    if( board == "VX2740" )
      ADCchan=64;
    else if( board == "V1725" || board == "V1730" )
      ADCchan=16;
    index = module*ADCchan + channel;
  }
  std::string board="";
  int module;
  int channel;
  int ADCchan;
  int index;
  double time;
  double height;
  double charge;
  double tot;
   
   void Print() const
   {
      std::cout<<"TDSPulse :"<<board<<"\t"<<module<<"\t"<<channel<<"\t"<<index
               <<"\t Q: "<<charge<<" T: "<<time<<" H: "<<height<<std::endl;
   }
};


/// Class to store info about a waveform (ie baseline)
class TDSChannel
{
public:
  TDSChannel(int imodule = -1, int ichannel = -1,
	     const char* iboard="V1725" ):event_counter(0),
                                          baseline(0.),baseline_rms(0.),maxph(0.),
                                          filter_baseline(0.),filter_rms(0.),
                                          roicharge(0.),roipeak(0.),roitime(0.),
                                          filter_roicharge(-1.),filter_roipeak(-1.),
                                          filter_roitime(0.),
                                          wf_rms(-1.),filterwf_rms(-1.),
                                          board(iboard),module(imodule),
                                          channel(ichannel)
  {
    unfiltered_wf.clear();
    filtered_wf.clear();
     ADCchan=0;
    if( board == "VX2740" )
      ADCchan=64;
    else if( board == "V1725" || board == "V1730" )
      ADCchan=16;
    index = module*ADCchan + channel;
  }

  unsigned int event_counter;
  double baseline;
  double baseline_rms;
  std::vector<double> unfiltered_wf;
  std::vector<double> filtered_wf;
#ifdef _HIT_FINDER_
  std::vector<double> peak;
#endif
  double maxph;
  double filter_baseline;
  double filter_rms;
  double roicharge;
  double roipeak;
  int roitime;
  double filter_roicharge;
  double filter_roipeak;
  int filter_roitime;
  double wf_rms;
  double filterwf_rms;
  std::string board=""; // ADC model
  int module; // sequential ADC number
  int channel;// ADC channel number
  int ADCchan;// number of ADC channels
  int index;// sequential channel number

   void Print() const
   {
      std::cout<<"TDSChannel: "<<board<<"\t"<<module<<"\t"<<channel<<"\t"<<index
               <<"\tb-line: "<<baseline<<" b-line rms: "<<baseline_rms
               <<"\tAmplitude: "<<maxph<<" wf rms: "<<wf_rms<<std::endl;
   }
   void PrintFiltered() const
   {
      std::cout<<"TDSChannel (filtered): "<<board<<"\t"<<module<<"\t"<<channel<<"\t"<<index
               <<"\t b: "<<filter_baseline<<" rms: "<<filter_rms
         //<<"\tAmplitude: "<<maxph
               <<std::endl;
   }
   void PrintROI() const
   {
      std::cout<<"TDSChannel idx: "<<std::setw(3)<<index<<" ROI: "<<std::setw(6)<<roipeak<<" at "<<std::setw(6)<<roitime<<" samples\tAfter filter: "<<std::setw(6)<<filter_roipeak<<" at "<<std::setw(6)<<filter_roitime<<" samples"<<std::endl;
   }
};


struct Proto0MB2map
{
public:
   std::map<int, std::pair<int, int> > mb2_map;

   Proto0MB2map()
   {
      // Map from (board*16 + channel) to (x, y)
      mb2_map[0] = std::make_pair(4,0);
      mb2_map[2] = std::make_pair(4,1);
      mb2_map[4] = std::make_pair(4,2);
      mb2_map[6] = std::make_pair(4,3);
      mb2_map[8] = std::make_pair(4,4);
      mb2_map[10] = std::make_pair(3,0);
      mb2_map[12] = std::make_pair(3,1);
      mb2_map[16] = std::make_pair(3,2);
      mb2_map[18] = std::make_pair(3,3);
      mb2_map[20] = std::make_pair(3,4);
      mb2_map[22] = std::make_pair(2,0);
      mb2_map[24] = std::make_pair(2,1);
      mb2_map[26] = std::make_pair(2,2);
      mb2_map[32] = std::make_pair(2,3);
      mb2_map[34] = std::make_pair(2,4);
      mb2_map[36] = std::make_pair(1,0);
      mb2_map[38] = std::make_pair(1,1);
      mb2_map[40] = std::make_pair(1,2);
      mb2_map[42] = std::make_pair(1,3);
      mb2_map[48] = std::make_pair(1,4);
      mb2_map[50] = std::make_pair(0,0);
      mb2_map[52] = std::make_pair(0,1);
      mb2_map[54] = std::make_pair(0,2);
      mb2_map[56] = std::make_pair(0,3);
      mb2_map[58] = std::make_pair(0,4);
   }
};

class ADCconf
{
public:
   std::string fModel;
   int fNumberChannelPerModule;
   double fNanosecsPerSample; // ns
   int fNSamples;
   int fResolution;
   double fDynamicRange; // mV

public:
   ADCconf(): fModel("null"),fNumberChannelPerModule(0),
              fNanosecsPerSample(0.0),fNSamples(0),fResolution(0),fDynamicRange(0.)
   {}
   ADCconf(std::string s): fModel(s),fNSamples(0)
   {
      if(fModel=="VX2740")
         {
            fNumberChannelPerModule=64;
            fNanosecsPerSample=8.0; // 125 MS/s = 8 ns
            fResolution=0xffff; // 16 bits
            fDynamicRange=2e3; // mV
         }
      else if(fModel=="VX2745")
         {
            fNumberChannelPerModule=64;
            fNanosecsPerSample=8.0; // 125 MS/s = 8 ns
            fResolution=0xffff; // 16 bits
            fDynamicRange=4e3; // mV
         }
      else if(fModel=="V1725")
         {
            fNumberChannelPerModule=16;
            fNanosecsPerSample=4.0; // 250 MS/s = 4 ns
            fResolution=0x3fff; // 14 bits
            fDynamicRange=2e3; // mV
         }
      else if(fModel=="V1730")
         {
            fNumberChannelPerModule=16;
            fNanosecsPerSample=2.0; // 500 MS/s = 2 ns
            fResolution=0x3fff; // 14 bits
            fDynamicRange=2e3; // mV
         }
      else
         std::cerr<<"ADCconf:: Unknown ADC type "<<s<<std::endl;
   }

   void print() const
   {
      std::cout<<fModel<<" # of channels: "<<fNumberChannelPerModule
               <<" Sampling period: "<<fNanosecsPerSample<<" ns"
               <<" Number of Samples: "<<fNSamples<<std::endl;
   }
};

class TDSPeak // class to store the fit results
{
public:
   double PulseTime;
   double RiseTime;
   double FallTimeShort;
   double FallTimeLong;
   double Charge;
   double kappa;
   double chi2;
   int ChannelIndex;
   int Counter;

   double MinValue;
   double MaxValue;

   TDSPeak(double t0, double tR, double tS, double tL,
           double A, double k, double c, 
           int i, int n,
           double tmin, double tmax):PulseTime(t0),
                                     RiseTime(tR),
                                     FallTimeShort(tS),FallTimeLong(tL),
                                     Charge(A),kappa(k),chi2(c),
                                     ChannelIndex(i),Counter(n),
                                     MinValue(tmin), MaxValue(tmax)
   { }

   TDSPeak(double c, int i, int n,
           double tmin, double tmax):PulseTime(-1.),
                                     RiseTime(-1.),
                                     FallTimeShort(-1.),FallTimeLong(-1.),
                                     Charge(-1.),kappa(-1.),chi2(c),
                                     ChannelIndex(i),Counter(n),
                                     MinValue(tmin), MaxValue(tmax)
   { }

   void Print() const
   {
      std::cout<<"TDSPeak # "<<Counter<<" in ch: "<<ChannelIndex<<"\tA = "<<Charge<<" t0 = "<<PulseTime<<" ns"<<std::endl;
      
   }
};

#endif


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
