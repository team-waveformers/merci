/******************************************************************
 *  History Writer for Napoli test setup 1 *
 * 
 * B. Smith
 * A. Capra
 * October 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "dsflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <fstream>

#include "TTree.h"
#include "TObjString.h"
#include "TError.h"

#include "json.hpp"
using json = nlohmann::json;

class HistorywriterFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};

class HistorywriterModule: public TARunObject
{
public:
   HistorywriterFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors

private:
  std::vector<float> fTemperatures;
  TTree *RunTree;

  double fmidasts;

public:
   HistorywriterModule(TARunInfo* runinfo, 
                       HistorywriterFlags* f): TARunObject(runinfo),
                                               fFlags(f),
                                               fCounter(0),fError(0),
                                               RunTree(0)
   {
      if(fFlags->fVerbose) std::cout<<"HistorywriterModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Historywriter";
#endif
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"HistorywriterModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"HistorywriterModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;

      MVOdbError error;
      runinfo->fOdb->RFA("Equipment/Lakeshore-336/Variables/TEMP", &fTemperatures,false,0,&error);
      
      if( error.fError )
         {
            std::cerr<<error.fErrorString<<std::endl;
         }
      else
         {
            runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
            RunTree = new TTree("TRun", "Darkside run info");
            if (fTemperatures.size() != 4) {
               printf("HistorywriterModule::BeginRun() Unable to read Lakeshore temperatures. Setting to -9999\n");
               fTemperatures.resize(4);
               fTemperatures[0] = -9999;
               fTemperatures[1] = -9999;
               fTemperatures[2] = -9999;
               fTemperatures[3] = -9999;
            }
            
            RunTree->Branch("cryo_temp_0",&fTemperatures[0]);
            RunTree->Branch("cryo_temp_1",&fTemperatures[1]);
            RunTree->Branch("cryo_temp_2",&fTemperatures[2]);
            RunTree->Branch("cryo_temp_3",&fTemperatures[3]);

            std::cout<<"HistorywriterModule::BeginRun() PT100 Lakeshore readings:"<<std::endl;
            for(size_t i=0; i<fTemperatures.size(); ++i)
               std::cout<<"\t ["<<i<<"] = "<<fTemperatures[i]<<" K"<<std::endl;
            
            RunTree->Fill();
         }
      
      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"HistorywriterModule Json parsing success!"<<std::endl;
      fin.close();
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"HistorywriterModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* /*flags*/, TAFlowEvent* flow)
   {
      ++fCounter;
      return flow;
   }
};


class HistorywriterModuleFactory: public TAFactory
{
public:
   HistorywriterFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("HistorywriterModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
             if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
	 }
   }
   
   void Finish()
   {
      printf("HistorywriterModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("HistorywriterModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new HistorywriterModule(runinfo, &fFlags);
   }

};

static TARegister tar(new HistorywriterModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
