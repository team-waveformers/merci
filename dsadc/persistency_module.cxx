/******************************************************************
 * Waveform sum  *
 * 
 * A. Capra
 * July 2022
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "dsflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

#include "TH1D.h"

#include "json.hpp"
using json = nlohmann::json;

class PersistencyFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};
  

class PersistencyModule: public TARunObject
{
public:
   PersistencyFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors

   int fPersistencyEvents;
   bool fDisabled;

   TDirectory* fPercyDir;
   std::map<int,TH1D*> fhPersistency; // the persistency histograms
   std::map<int,TH1D*> fhRawPersistency; // the persistency histograms
   std::map<int,int> fNhits; // the persistency histograms

   std::vector<int> fFilteredChannels;

   double fWFcut_lower;
   double fWFcut_upper;

public:
   PersistencyModule(TARunInfo* runinfo, PersistencyFlags* f): TARunObject(runinfo),fFlags(f),
                                                               fCounter(0),fError(0),
                                                               fPersistencyEvents(0),
                                                               fDisabled(false),
                                                               fPercyDir(0)
   {
      if(fFlags->fVerbose) std::cout<<"PersistencyModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Persistency";
#endif
      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"FilterModule Json parsing success!"<<std::endl;
      fin.close();

      fPersistencyEvents = settings["Plot"]["Persistency Events Limit"].get<int>();

      try
         {
            fFilteredChannels = settings["Filter"]["Skip"].get<std::vector<int>>();
            std::cout<<"PersistencyModule::ctor Already filtered channels:";
            for(auto& ch: fFilteredChannels) std::cout<<" "<<ch;
            std::cout<<"\n";
         }
      catch(json::type_error& e) 
         {
            std::cerr<<"PersistencyModule::ctor reading skip channels: "<<e.what()<<std::endl;
         }

      fWFcut_upper = DBL_MAX;
      fWFcut_lower = -1.*fWFcut_upper;
      try
         {
            fWFcut_lower = settings["Pulse"]["Amplitude"].get<double>()*0.5/0.6;
            fWFcut_upper = 3.*fWFcut_lower;
            std::cout<<"PersistencyModule ctor: Threshold for WF sum: "<<fWFcut_lower<<std::endl;
         }
      catch(json::type_error& e) 
         {
            std::cerr<<"PersistencyModule::ctor no threshold "<<e.what()<<std::endl;
         }
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty())
         std::cout<<"PersistencyModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"PersistencyModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
      
      if( runinfo->fFileName.empty() )
         {
            runinfo->fRoot->fgDir->cd();
            gDirectory->cd("..");
            fPercyDir = new TDirectory("WFaverage", "Persistency Histograms");
         }
      else
         {
            runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
            fPercyDir=runinfo->fRoot->fOutputFile->mkdir("WFaverage");
         }
   }

   void EndRun(TARunInfo* runinfo)
   {
      for(auto it = fhRawPersistency.begin(); it !=  fhRawPersistency.end(); ++it )
         {
            it->second->Scale(1./double(fNhits[it->first]));
         }

      for(auto it = fhPersistency.begin(); it !=  fhPersistency.end(); ++it )
         {
            it->second->Scale(1./double(fNhits[it->first]));
         }

      if( runinfo->fFileName.empty() )
         {
            runinfo->fRoot->fgDir->cd();
            gDirectory->cd("..");
            if( fPercyDir ) delete fPercyDir;
         }

      std::cout<<"PersistencyModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }
  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {      
      if( fDisabled ) return flow;
      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow ) 
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      int NumberOfChannels = wf_flow->GetNumberOfChannels();
      if( fFlags->fVerbose )
         std::cout<<"PersistencyModule::AnalyzeFlowEvent Number of Channels: "<<NumberOfChannels<<std::endl;

      TDirectory* dir = runinfo->fRoot->fgDir;    
      for(int ich=0; ich<NumberOfChannels; ++ich)
         {
            const TDSChannel* dschan = wf_flow->GetDSchan(ich);
            dir->cd();// RootApp:/manalyzer  
            //gDirectory->pwd();
            if( fFlags->fVerbose )
               std::cout<<"PersistencyModule::AnalyzeFlowEvent Channel at "<<ich
                        <<" ds: "<<dschan->index<<std::endl;
            if( fCounter < fPersistencyEvents )
               {
                  if( runinfo->fgFileList.size() )
                     runinfo->fRoot->fOutputFile->cd(); // <filename>:/
                  else
                     dir->cd("..");
                  fPercyDir->cd();
                  PersistencyPlot(dschan);
               }
            else fDisabled=true;
         }

      if( fFlags->fVerbose )
         std::cout<<"PersistencyModule::AnalyzeFlowEvent DONE"<<std::endl;

      ++fCounter;
      return flow;
   }

   void PersistencyPlot( const TDSChannel* dschan )
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock); // PersistencyPlot
#endif
      int idx = dschan->index;
      if(fFlags->fVerbose)
         {
            std::cout<<"PersistencyModule::PersistencyPlot "<<idx<<" \n";
            //gDirectory->pwd();
         }

      if( (dschan->filter_roipeak > fWFcut_lower) && (dschan->filter_roipeak < fWFcut_upper) ) 
         {
            if(fFlags->fVerbose) 
               std::cout<<"PersistencyModule::PersistencyPlot skip, filtered ROI prom. is "
                        <<dschan->filter_roipeak<<" \n";
            return;
         }
 
      const std::vector<double> wf = dschan->unfiltered_wf;
      const std::vector<double> wff = dschan->filtered_wf;
      int nsamples = (int)wf.size();
      if( !fhPersistency.count(idx) )
         {
            std::string hname="hWFFsumCH"+std::to_string(idx);
            if(fFlags->fVerbose)
               std::cout<<"PersistencyModule::PersistencyPlot count "<<hname<<std::endl;
            std::string htitle="Persistency Plot for filtered channel "+std::to_string(idx)+";Samples;ADC";
            fhPersistency[idx] = new TH1D(hname.c_str(),htitle.c_str(),nsamples,0.,(double)nsamples);
            fhPersistency[idx]->SetStats(0);

            hname="hWFsumCH"+std::to_string(idx);
            htitle="Persistency Plot for filtered channel "+std::to_string(idx)+";Samples;ADC";
            fhRawPersistency[idx] = new TH1D(hname.c_str(),htitle.c_str(),nsamples,0.,(double)nsamples);
            fhRawPersistency[idx]->SetStats(0);

            fNhits[idx]=0;
         }
 
      ++fNhits[idx];
      for(int i=0; i<nsamples; ++i)
         {
            fhRawPersistency[idx]->AddBinContent(i+1,-1.*wf.at(i));
            fhPersistency[idx]->AddBinContent(i+1,-1.*wff.at(i));
         }
      if(fCounter==(fPersistencyEvents-1) && fFlags->fVerbose) 
         std::cout<<"PersistencyModule::PersistencyPlot for channel:"<<idx<<" Last WF."<<std::endl;
   }

   bool IsFiltered(TDSChannel* dsch)
   {
      for( auto& ich: fFilteredChannels )
         {
            if( ich == dsch->index ) return true;
         }
      return false;
   }

};


class PersistencyModuleFactory: public TAFactory
{
public:
   PersistencyFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("PersistencyModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
   }
   
   void Finish()
   {
      printf("PersistencyModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("PersistencyModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new PersistencyModule(runinfo, &fFlags);
   }
};

static TARegister tar(new PersistencyModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
