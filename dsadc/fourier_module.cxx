/******************************************************************
 *
 * Perform Fourier Transform of Waveforms *
 * 
 * A. Capra
 * August 2022
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "dsflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>
#include <functional>
#include <cmath>

#include "TVirtualFFT.h"
#include "TGraph.h"
#include "TCanvas.h"

#include "json.hpp"
using json = nlohmann::json;


class FourierFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};


class FourierModule: public TARunObject
{
public:
   FourierFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors

private:
   std::map<int,std::vector<double>> fFFT; 
   double fTimeBin; // seconds
   double fADCmV; // mV

   void get_wf_mV(const std::vector<double>* in, std::vector<double>* out)
   {
      std::transform(in->begin(), in->end(), out->begin(),
                     std::bind(std::multiplies<double>(), std::placeholders::_1, fADCmV));
   }
      
   void InitializeFFT(int idx,size_t number_of_samples)
   {
      fFFT[idx].assign(number_of_samples,double(0));
   }

   TGraph* ConvertVector2Graph(const std::vector<double>* v, int chan)
   {
      size_t nsamples = v->size();
      double xmin = 1./(double(nsamples)*fTimeBin);
      double xmax = 1./fTimeBin;
      double df = (xmax-xmin)/nsamples;
      TGraph* g = new TGraph(nsamples);
      std::string gname("gFFTMagCH"); gname+=std::to_string(chan);
      g->SetName(gname.c_str());
      std::string gtitle("DFT Channel "); gtitle+=std::to_string(chan);
      //gtitle+=";Frequency [Hz];Amplitude [mV^{2}]";
      gtitle+=";Frequency [Hz];Amplitude [a.u.]";
      g->SetTitle(gtitle.c_str());
      g->SetLineColor(kBlue);
      g->SetMarkerColor(kBlue);
      for(size_t i=0; i<v->size(); ++i)
         {
            double x = i*df;
            double y = v->at(i);
            if( fCounter > 0 ) y/=static_cast<double>(fCounter);
            g->SetPoint(i,x,y);
         }
      return g;
   }

public:
   FourierModule(TARunInfo* runinfo, FourierFlags* f): TARunObject(runinfo),
						       fFlags(f), fCounter(0), fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"FourierModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Fourier";
#endif
      fTimeBin = 8.e-9; // s
      fADCmV = 2000./(pow(2,16)-1.); // mV

      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"FourierModule Json parsing success!"<<std::endl;
      fin.close();
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"FourierModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"FourierModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"FourierModule::EndRun() Plotting"<<std::endl;
      std::string cname, ctitle;
      for( auto& f: fFFT )
         {
            runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
            if( !runinfo->fRoot->fOutputFile->cd("FFT") )
               runinfo->fRoot->fOutputFile->mkdir("FFT")->cd();
            TGraph* g = ConvertVector2Graph( &f.second, f.first );
            cname="cFFTch"; cname+=std::to_string(f.first);
            cname+="Run"; cname+=std::to_string(runinfo->fRunNo);
            ctitle="Magnitude of FFT for Channel "; ctitle+=std::to_string(f.first);
            TCanvas* c = new TCanvas(cname.c_str(),ctitle.c_str(),2100,1400);
            g->Draw("ALP");
            gPad->Update();
            gPad->SetGridx(1); //set the grid on the X axis
            gPad->SetLogy();
            c->SaveAs(".pdf");
            //delete g;
            delete c;
            g->Write();
         }
      std::cout<<"FourierModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow ) 
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      if( fCounter < 100 )
         {
            fTimeBin = wf_flow->conf[wf_flow->GetDSchan(0)->board].fNanosecsPerSample * 1.e-9;
            fADCmV = wf_flow->conf[wf_flow->GetDSchan(0)->board].fDynamicRange/static_cast<double>(wf_flow->conf[wf_flow->GetDSchan(0)->board].fResolution);
            if( fFlags->fVerbose )
               std::cout<<"FourierModule::AnalyzeFlowEvent for "<<wf_flow->GetDSchan(0)->board
                        <<" bin: "<<fTimeBin
                        <<" s  -- ADC range: "<<wf_flow->conf[wf_flow->GetDSchan(0)->board].fDynamicRange
                        <<" mV    ADC converstion: "<<fADCmV<<std::endl;
         }
      if( fFlags->fVerbose )
         std::cout<<"FourierModule::AnalyzeFlowEvent Number of Channels: "<<wf_flow->GetNumberOfChannels()
                  <<" Sampling period: "<<fTimeBin<<" s"<<std::endl;
      
      for(int i=0; i<wf_flow->GetNumberOfChannels(); ++i)
         {
            TDSChannel* dsch = wf_flow->GetDSchan(i);
            int nsamples = static_cast<int>(dsch->unfiltered_wf.size()); // ask ROOT why...
            TVirtualFFT *fftr2c = TVirtualFFT::FFT(1,           // number of transform dimensions 
                                                   &nsamples,   // sizes of each dimension (an array at least ndim long) 
                                                   "R2C M K");  // "R2C" a real-input/complex-output discrete Fourier transform (DFT) in one or more dimensions,
                                                                // "M" (from "measure") some time spend in finding the optimal way to do the transform
                                                                // "K" (from "keep") creates and returns a new TVirtualFFT*. User is then responsible for deleting it.
            std::vector<double>* wf = new std::vector<double>(dsch->unfiltered_wf.size());
            get_wf_mV(&dsch->unfiltered_wf,wf);
            fftr2c->SetPoints( wf->data() );
            // std::vector<double> wf(dsch->unfiltered_wf.begin(),dsch->unfiltered_wf.end());
            // fftr2c->SetPoints( wf.data() );
            fftr2c->Transform();
            std::vector<double> mag;
            double re,im,norm=1./sqrt(nsamples);
            for( int i=0; i<nsamples; ++i )
               {
                  fftr2c->GetPointComplex(i,re,im);
                  mag.push_back( sqrt(re*re+im*im)*norm );// normalizaztion: nsamples^(-1/2)
               }
            delete fftr2c; // delete transform
            delete wf;

            if( !fFFT.count(dsch->index) ) InitializeFFT(dsch->index,dsch->unfiltered_wf.size());
            // sum magnitude of the transform
            std::transform(fFFT[dsch->index].begin(), fFFT[dsch->index].end(), mag.begin(), fFFT[dsch->index].begin(), std::plus<double>());
         }

      flow=wf_flow;
      ++fCounter;
      return flow;
   }
};


class FourierModuleFactory: public TAFactory
{
public:
   FourierFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("FourierModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
   }
   
   void Finish()
   {
      printf("FourierModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("FourierModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new FourierModule(runinfo, &fFlags);
   }

};

static TARegister tar(new FourierModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
