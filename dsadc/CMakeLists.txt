# CMake for DS Analysis
#
#
cmake_minimum_required(VERSION 3.12 FATAL_ERROR)
project(dsadcana)

option(VX2740 "Enable VX2740 unpacker" ON)
option(V1725 "Enable V1725 unpacker" ON)
option(FFT "Enable FFT module" OFF)
option(HITFIND "Enable HitFinder" OFF)
option(KSTRIG "Enable K-sigma trigger module (WIP)" OFF)
option(FIT "Enable pulse fitting module" OFF)
option(LASER "Enalbe analysis of a Region of Interest" ON)
option(SUM "Sum all waveforms" OFF)
option(PLOT "Enable waveform plotting" ON)
option(DSHISTO "Enable hits histogramming" OFF)
option(TTREE "Enable ROOT output" ON)
option(DSHISTORY "Enable Writing MIDAS History Variables" OFF)
option(WFASCII "Save all the waveforms to ASCII" OFF)

message(STATUS "${PROJECT_NAME} installation path: ${CMAKE_INSTALL_PREFIX} ")

##########################################
# Configure current project
message(STATUS "Configuring ${PROJECT_NAME}")

if( NOT V1725 AND NOT VX2740 )
  message(FATAL_ERROR
    "This target cannot be built without any ADC module
     Please use -DV1725=ON and/or -DVX2740=ON")
endif()

set(Modules ${CMAKE_SOURCE_DIR}/src/baseline_module.cxx ${CMAKE_CURRENT_SOURCE_DIR}/stat_module.cxx)
if(V1725)
  list(INSERT Modules 0 ${CMAKE_SOURCE_DIR}/src/unpackV1725_module.cxx)
endif(V1725)
if(VX2740)
  list(INSERT Modules 0 ${CMAKE_SOURCE_DIR}/src/unpackVX2740_module.cxx)
endif(VX2740)

if(FFT)
  list(APPEND Modules ${CMAKE_CURRENT_SOURCE_DIR}/fourier_module.cxx)
endif(FFT)

if(KSTRIG)
  list(APPEND Modules ${CMAKE_CURRENT_SOURCE_DIR}/KsTrigger_module.cxx)
  set(HITFIND OFF)
else(KSTRIG)

  list(APPEND Modules ${CMAKE_CURRENT_SOURCE_DIR}/filter_module.cxx)

  if(HITFIND)
    list(APPEND Modules ${CMAKE_SOURCE_DIR}/src/hitfinder_module.cxx)
  else(HITFIND)
    list(APPEND Modules ${CMAKE_SOURCE_DIR}/src/pulsefinder_module.cxx)
  endif(HITFIND)

  if(FIT) 
    list(APPEND Modules ${CMAKE_CURRENT_SOURCE_DIR}/fitpduplus_module.cxx)
  endif(FIT)

  list(APPEND Modules ${CMAKE_CURRENT_SOURCE_DIR}/filtered_module.cxx)

endif(KSTRIG)

if(LASER)
  list(APPEND Modules ${CMAKE_CURRENT_SOURCE_DIR}/laser_module.cxx)
endif(LASER)

if(SUM)
  list(APPEND Modules ${CMAKE_CURRENT_SOURCE_DIR}/persistency_module.cxx)
endif(SUM)

if(DSHISTO)
  list(APPEND Modules ${CMAKE_CURRENT_SOURCE_DIR}/histo_module.cxx)
endif(DSHISTO)

if(PLOT)
  list(APPEND Modules ${CMAKE_CURRENT_SOURCE_DIR}/plot_module.cxx)
endif(PLOT)

if( NOT DSHISTO AND NOT TTREE )
  message(WARNING
    "This target has NO OUTPUT
    Please use either -DDSHISTO=ON or DTTREE=ON, or both")
endif()

if(TTREE)
  list(APPEND Modules ${CMAKE_CURRENT_SOURCE_DIR}/treewriter_module.cxx)
endif(TTREE)

if(DSHISTORY)
 list(APPEND Modules ${CMAKE_CURRENT_SOURCE_DIR}/historywriter_NA-setup-1_module.cxx)
endif(DSHISTORY)

##########################################
# Configure target
set(bin "dsanana.exe")
set(Sources ${CMAKE_SOURCE_DIR}/src/TV1725RawData.cxx ${CMAKE_SOURCE_DIR}/src/TVX2740RawData.cxx ${Modules})
add_executable(${bin} ${Sources})
if(WARNINGS_ALL)
  target_compile_options(${bin} PUBLIC -Wall -Wextra -pedantic)
  message(STATUS "Enabling strict compilation rules")
endif(WARNINGS_ALL)

target_include_directories(${bin} PUBLIC ${CMAKE_SOURCE_DIR}/src ${CMAKE_CURRENT_SOURCE_DIR})

target_compile_definitions(${bin} PUBLIC ${ROOT_CCX_FLAGS} HAVE_ROOT HAVE_ROOT_HTTP HAVE_THTTP_SERVER MODULE_MULTITHREAD)
if(WFASCII)
  target_compile_definitions(${bin} PUBLIC EXPORT_ASCII)
  message(STATUS "Enabling waveform dump")
endif(WFASCII)
if(HITFIND)
  target_compile_definitions(${bin} PUBLIC _HIT_FINDER_)
endif(HITFIND)

target_include_directories(${bin} PUBLIC ${MANA_PATH_DIR}/manalyzer ${MANA_PATH_DIR}/midasio ${MANA_PATH_DIR}/mvodb ${MANA_PATH_DIR}/mjson ${MANA_PATH_DIR}/mxml)
target_include_directories(${bin} PUBLIC ${ROOT_INCLUDE_DIRS})

if( MIDAS_FOUND )
  target_link_directories(${bin} PUBLIC ${MIDAS_LIBRARY_DIRS})
  target_include_directories(${bin} PUBLIC ${MIDAS_INCLUDE_DIRS})
endif( MIDAS_FOUND )

target_link_libraries(${bin} manalyzer manalyzer_main ${MIDAS_LIBRARIES} ${ROOT_LIBRARIES} )

##########################################
install(TARGETS ${bin} DESTINATION ${CMAKE_INSTALL_PREFIX})

#set(DEFAULT_CONFIGURATION ${CMAKE_CURRENT_SOURCE_DIR}/config/master.json)
#install(FILES ${DEFAULT_CONFIGURATION} DESTINATION ${CMAKE_INSTALL_PREFIX}) 

message(STATUS "[${PROJECT_NAME}] ${CMAKE_BUILD_TYPE} build")
message(STATUS "[${PROJECT_NAME}] ${bin} will be installed in ${CMAKE_INSTALL_PREFIX}")
message(STATUS "[${PROJECT_NAME}] ${DEFAULT_CONFIGURATION} will be installed in ${CMAKE_INSTALL_PREFIX}")
