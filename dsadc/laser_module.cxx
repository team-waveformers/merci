/******************************************************************
 *  Region of Interest Analysis *
 * 
 * A. Capra
 * August 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "dsflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <fstream>
#include <cmath>
#include <cassert>

#include "json.hpp"
using json = nlohmann::json;


class LaserFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};
  

class LaserModule: public TARunObject
{
public:
   LaserFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors
 
   int roi_inf;
   int roi_charge_sup;
   int roi_charge_len;
   int roi_peak_sup;
   int roi_peak_len;

   int filter_roi_inf;
   int filter_roi_charge_sup;
   int filter_roi_charge_len;
   int filter_roi_peak_sup;
   int filter_roi_peak_len;

   std::vector<int> fTrigChans{-1};

public:
   LaserModule(TARunInfo* runinfo, LaserFlags* f): TARunObject(runinfo),
                                                   fFlags(f), fCounter(0), fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"LaserModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Laser";
#endif
      
      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"LaserModule Json parsing success!"<<std::endl;
      fin.close();

      try
         {
            fTrigChans=settings["ROI"]["trig chan"].get<std::vector<int>>();
            std::cout<<"LaserModule::ctor Laser Trigger Channel(s):";
            for(auto& ch: fTrigChans) std::cout<<" "<<ch;
            std::cout<<"\n";
         }
      catch(json::type_error& e) 
         {
            std::cerr<<"LaserModule::ctor reading skip channels: "<<e.what()<<std::endl;
         }

      roi_inf=settings["ROI"]["min"].get<int>();
      assert(roi_inf>=0);
      roi_charge_sup=settings["ROI"]["max charge"].get<int>();
      roi_charge_len=roi_charge_sup-roi_inf;
      assert(roi_charge_len>0);
      roi_peak_sup=settings["ROI"]["max peak"].get<int>();
      roi_peak_len=roi_peak_sup-roi_inf;
      assert(roi_peak_len>0);
      
      filter_roi_inf=settings["ROI"]["filter min"].get<int>();
      assert(filter_roi_inf>=0);
      filter_roi_charge_sup=settings["ROI"]["filter max charge"].get<int>();
      filter_roi_charge_len=filter_roi_charge_sup-filter_roi_inf;
      assert(filter_roi_charge_len>0);
      filter_roi_peak_sup=settings["ROI"]["filter max peak"].get<int>();
      filter_roi_peak_len=filter_roi_peak_sup-filter_roi_inf;
      assert(filter_roi_peak_len>0);
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"LaserModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"LaserModule::BeginRun() run: "<<runinfo->fRunNo
                  <<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"LaserModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow ) 
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      if( fFlags->fVerbose )
         std::cout<<"LaserModule::AnalyzeFlowEvent : "<<wf_flow->GetNumberOfChannels()<<std::endl;

      if( !(wf_flow->GetNumberOfChannels()>0) ) return flow;
      if( !wf_flow->GetDSchan(0) )
         {
            std::cerr<<"LaserModule::AnalyzeFlowEvent : No Channels?"<<std::endl;
            return flow;
         }
      if( !wf_flow->conf.count( wf_flow->GetDSchan(0)->board ) )
         {
            std::cerr<<"LaserModule::AnalyzeFlowEvent : No Board Configuration?"<<std::endl;
            return flow;
         }
      int NumberOfSamples = wf_flow->conf[wf_flow->GetDSchan(0)->board].fNSamples;
      if( fFlags->fVerbose )
         std::cout<<"LaserModule::AnalyzeFlowEvent Number of Samples: "<<NumberOfSamples<<std::endl;
      if( NumberOfSamples > 0 )
         CheckROI(NumberOfSamples);
      else
         {
            std::cerr<<"LaserModule::AnalyzeFlowEvent : No Samples?"<<std::endl;
            return flow;
         }

      for(int i=0; i<wf_flow->GetNumberOfChannels(); ++i)
         {
            TDSChannel* dschan = wf_flow->GetDSchan(i);
            if( fFlags->fVerbose ) dschan->Print();

            // analyze the Region of Interest -- unfiltered waveform
            // calculate the charge in the ROI

            auto start_unf=dschan->unfiltered_wf.begin()+roi_inf;
            auto end_unf_q=dschan->unfiltered_wf.begin()+roi_charge_sup;
            double roi_unfiltered_charge = std::accumulate(start_unf,end_unf_q,double(0));
            // determine the pulse of height (of a positive pulse) and pulse time
            auto end_unf_ph=dschan->unfiltered_wf.begin()+roi_peak_sup;
            auto timebin_unf = std::max_element(start_unf,end_unf_ph);
            int roi_unfiltered_time = std::distance(dschan->unfiltered_wf.begin(),timebin_unf);
            double roi_unfiltered_peak=*timebin_unf;

            // Store results
            dschan->roicharge = roi_unfiltered_charge;
            dschan->roipeak = roi_unfiltered_peak;
            dschan->roitime = roi_unfiltered_time;
            
            // analyze the Region of Interest -- filtered waveform
            // #ifdef _HIT_FINDER_
            //std::vector<double> filtered_waveform = dschan->peak;
            //#else
            std::vector<double> filtered_waveform = dschan->filtered_wf;
            //#endif
            if( !(filtered_waveform.size()>0) ) continue;
            auto start_f=filtered_waveform.begin()+filter_roi_inf;

            // calculate the charge in the ROI
            auto end_f_q=filtered_waveform.begin()+filter_roi_charge_sup;
            double roi_filtered_charge = std::accumulate(start_f,end_f_q,double(0));
            // determine the pulse of height (of a positive pulse) and pulse time
            auto end_f_ph=filtered_waveform.begin()+filter_roi_peak_sup;
            auto timebin_f = std::max_element(start_f,end_f_ph);
            int roi_filtered_time = std::distance(filtered_waveform.begin(),timebin_f);
            double roi_filtered_peak=*timebin_f;

            // Store results
            dschan->filter_roicharge = roi_filtered_charge;
            dschan->filter_roipeak = roi_filtered_peak;
            dschan->filter_roitime = roi_filtered_time;

            if( fFlags->fVerbose ) dschan->PrintROI();
         }
      
      flow=wf_flow;
      ++fCounter;
      return flow;
   }

   void CheckROI(int& nsamples)
   {
      //      std::cout<<"LaserModule::CheckROI validity"<<std::endl;
      if( roi_peak_sup > nsamples )
         {
            std::cerr<<"LaserModule::AnalyzeFlowEvent : Upper bound of ROI "<<roi_peak_sup<<" exceeds number of samples... ";
            roi_peak_sup=nsamples;
            std::cerr<<" changed to "<<roi_peak_sup<<" equals to the number of samples"<<std::endl;
            if( roi_inf >= roi_peak_sup )
               {
                  std::cerr<<"LaserModule::AnalyzeFlowEvent : Lower bound of ROI "<<roi_inf<<" is greater than Upper bound ROI... ";
                  roi_inf=roi_peak_sup-roi_peak_len;
                  if( roi_inf < 0 ) roi_inf=0;
                  std::cerr<<" changed to "<<roi_inf<<std::endl;
               }
         }

      if( filter_roi_peak_sup > nsamples )
         {
            std::cerr<<"LaserModule::AnalyzeFlowEvent : Upper bound of Filtered ROI "<<filter_roi_peak_sup<<" exceeds number of samples... ";
            filter_roi_peak_sup=nsamples;
            std::cerr<<" changed to "<<filter_roi_peak_sup<<" equals to the number of samples"<<std::endl;
            if( filter_roi_inf >= filter_roi_peak_sup )
               {
                  std::cerr<<"LaserModule::AnalyzeFlowEvent : Lower bound of Filtered ROI "<<filter_roi_inf<<" is greater than Upper bound ROI... ";
                  filter_roi_inf=filter_roi_peak_sup-filter_roi_peak_len;
                  if( filter_roi_inf < 0 ) filter_roi_inf=0;
                  std::cerr<<" changed to "<<filter_roi_inf<<std::endl;
               }
         }

      if( roi_charge_sup > nsamples )
         {
            std::cerr<<"LaserModule::AnalyzeFlowEvent : Upper bound of ROI "<<roi_charge_sup<<" exceeds number of samples... ";
            roi_charge_sup=nsamples;
            std::cerr<<" changed to "<<roi_charge_sup<<" equals to the number of samples"<<std::endl;
            if( roi_inf >= roi_charge_sup )
               {
                  std::cerr<<"LaserModule::AnalyzeFlowEvent : Lower bound of ROI "<<roi_inf<<" is greater than Upper bound ROI... ";
                  roi_inf=roi_charge_sup-roi_charge_len;
                  if( roi_inf < 0 ) roi_inf=0;
                  std::cerr<<" changed to "<<roi_inf<<std::endl;
               }
         }

      if( filter_roi_charge_sup > nsamples )
         {
            std::cerr<<"LaserModule::AnalyzeFlowEvent : Upper bound of Filtered ROI "<<filter_roi_charge_sup<<" exceeds number of samples... ";
            filter_roi_charge_sup=nsamples;
            std::cerr<<" changed to "<<filter_roi_charge_sup<<" equals to the number of samples"<<std::endl;
            if( filter_roi_inf >= filter_roi_charge_sup )
               {
                  std::cerr<<"LaserModule::AnalyzeFlowEvent : Lower bound of Filtered ROI "<<filter_roi_inf<<" is greater than Upper bound ROI... ";
                  filter_roi_inf=filter_roi_charge_sup-filter_roi_charge_len;
                  if( filter_roi_inf < 0 ) filter_roi_inf=0;
                  std::cerr<<" changed to "<<filter_roi_inf<<std::endl;
               }
         }
   }
};


class LaserModuleFactory: public TAFactory
{
public:
   LaserFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("LaserModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
      
   }
   
   void Finish()
   {
      printf("LaserModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("LaserModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new LaserModule(runinfo, &fFlags);
   }

};

static TARegister tar(new LaserModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
