# PHAAR Experiments @ TRIUMF

Using CAEN DT5730B as V1730

## Build

```
git clone git@bitbucket.org:team-waveformers/merci.git
cd merci
mkdir build && cd build
cmake ..
cmake --build . -- install -j
```

The executable `haicuana.exe` is installed in the `bin` folder.

Sample configuration file is also installed in the same folder, e.g., `vera.json`.


## Build without fitting module

```
cd build	
cmake -DFIT=OFF ..
cmake --build . -- install
```

## Build with filter module

```
cd build	
cmake -DDSFILTER=ON ..
cmake --build . -- install
```

## Build without plot module

```
cd build	
cmake -DPLOT=OFF ..
cmake --build . -- install
```


## Run example

```
./phaarana.exe --mt /daq/daqstore/vera/data2/run00727.mid.gz -- --conf vera.json
./haicuana.exe --midas-progname haicu_analyzer -i -g -- --conf haicu.json
```
