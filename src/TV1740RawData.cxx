#include "TV1740RawData.hxx"

#include <iomanip>
#include <iostream>



void TV1740RawData::Print(){

  std::cout << "V1740 decoder for bank " << GetName().c_str() << std::endl;
  std::cout << "Bank size: " << GetEventSize() << std::endl;
  std::cout << "Channel Mask : " << GetGroupMask() << std::endl;
  std::cout << "Event counter : " << GetEventCounter() << std::endl;
  std::cout << "Trigger tag: " << GetTriggerTag() << std::endl;

  std::cout << "Number of channels with data: " << GetNChannels() << std::endl;
  for(int i = 0 ; i < GetNChannels(); i++){
    TV1740RawChannel channelData = GetChannelData(i);
  }

}

//Helper method for decoding uncompressed data
void TV1740RawData::HandleUncompressedData(){

    int nitems = GetSize();
    if(fVerbose>0) {
      std::cout << "V1740 size : " << nitems << std::endl;
      if (nitems < 0) { printf("V1740 decode; warning empty bank\n"); return;}
    }

    //if we got a bank with only a header
    if (nitems == 4)
    {
      int event_number = GetEventCounter();
      if(fVerbose>0) std::cout << "Event# "<< event_number <<", Header only bank"<<std::endl;
      return;
    }

    // save some information from header.
    uint32_t eventID = GetEventCounter();
    uint32_t timeTag = GetTriggerTag();

    if(fVerbose > 0) printf("V1740_ Evt#:%d timeTag:%x nwords:%d\n", eventID, timeTag, nitems);

    // Check the group mask, see how many active groups
    uint32_t grMask = GetGroupMask();
    int nActiveGroups=0;
    for(int iCh=0; iCh<8; iCh++){
      if( fVerbose > 0 ) std::cout << nActiveGroups <<std::endl;
      if(grMask & (1<<iCh))
        nActiveGroups++;
    }
    if(fVerbose>0) std::cout << "Group mask = 0x" << std::hex <<  grMask << std::dec << ", number of active groups = " << nActiveGroups << std::endl;

    if(nActiveGroups == 0) return;

    // Check that size of data bank looks correct;
    int nblocks = ((nitems - 4)/nActiveGroups) / 9 / 4;
    //int nblocks = ((nitems - 4)/nActiveGroups) / 9;
    //int nblocks = (nitems - 4) / (9 * nActiveGroups);
    int remainder = ((nitems - 4)/nActiveGroups) % 9;
    if(remainder != 0 && fVerbose > 0) printf("V1740 decoder; bank size is unexpected; size of %i does not satisfy (size-4) %% 9 == 0\n",nitems);

    if(fVerbose > 0) std::cout << "Data looks okay: nblocks = " << nblocks << ", remainder = " << remainder << std::endl;

    //This check is explicitly for LoLX Data to remove fragmentented events from trigger errors
    if (nblocks*3 < 180) {
      if(fVerbose > 0) std::cout << "Error: Unexpected number of samples "<<std::endl;
      return;
    }
    // Keep track of which active group we are on.
    int nGr = 0;
    // Loop over active groups
    for(int gr = 0; gr < 8; gr++){
      // Structure to store waveforms for group of 8 channels.
      std::vector<std::vector<double> > samples;
      for(int i = 0; i < 8; i++){
        samples.push_back(std::vector<double>());
      }
      // Check if there are samples for this group
      if(! ((grMask >> gr) & 0x1)){
        if(fVerbose > 0) std::cout << "No samples for group " << gr << std::endl;
        continue;
      }
        // Loop over all the 9-word blocks
        for(int i = 0; i < nblocks; i++){
        	// Figure out where to start looking in data
        	int offset = 4 + i*9 + (nGr * nblocks * 9);
        	// write out the decoding for blocks of 9 words explicitly, otherwise hard to understand this bank decoding
        	samples[0].push_back(((GetData32()[offset] & 0xfff)));
        	samples[0].push_back(((GetData32()[offset] & 0xfff000) >> 12 ));
        	samples[0].push_back(((GetData32()[offset] & 0xff000000) >> 24) + ((GetData32()[offset+1] & 0xf) <<8) );

        	samples[1].push_back(((GetData32()[offset+1] & 0xfff0) >> 4));
        	samples[1].push_back(((GetData32()[offset+1] & 0xfff0000) >> 16 ));
        	samples[1].push_back(((GetData32()[offset+1] & 0xf0000000) >> 28) + ((GetData32()[offset+2] & 0xff) <<4) );

        	samples[2].push_back(((GetData32()[offset+2] & 0xfff00)>>8));
        	samples[2].push_back(((GetData32()[offset+2] & 0xfff00000) >> 20 ));

        	offset += 3;

        	samples[2].push_back(((GetData32()[offset] & 0xfff)));

        	samples[3].push_back(((GetData32()[offset] & 0xfff000) >> 12 ));
        	samples[3].push_back(((GetData32()[offset] & 0xff000000) >> 24) + ((GetData32()[offset+1] & 0xf) <<8) );
        	samples[3].push_back(((GetData32()[offset+1] & 0xfff0) >> 4));

        	samples[4].push_back(((GetData32()[offset+1] & 0xfff0000) >> 16 ));
        	samples[4].push_back(((GetData32()[offset+1] & 0xf0000000) >> 28) + ((GetData32()[offset+2] & 0xff) <<4) );
        	samples[4].push_back(((GetData32()[offset+2] & 0xfff00)>>8));

        	samples[5].push_back(((GetData32()[offset+2] & 0xfff00000) >> 20 ));

        	offset += 3;

        	samples[5].push_back(((GetData32()[offset] & 0xfff)));
        	samples[5].push_back(((GetData32()[offset] & 0xfff000) >> 12 ));

        	samples[6].push_back(((GetData32()[offset] & 0xff000000) >> 24) + ((GetData32()[offset+1] & 0xf) <<8) );
        	samples[6].push_back(((GetData32()[offset+1] & 0xfff0) >> 4));
        	samples[6].push_back(((GetData32()[offset+1] & 0xfff0000) >> 16 ));

        	samples[7].push_back(((GetData32()[offset+1] & 0xf0000000) >> 28) + ((GetData32()[offset+2] & 0xff) <<4) );
        	samples[7].push_back(((GetData32()[offset+2] & 0xfff00)>>8));
        	samples[7].push_back(((GetData32()[offset+2] & 0xfff00000) >> 20 ));
        }
        nGr++;

      // Add the objects for each channel.  If the group is empty there won't be any samples.
      for(int i = 0; i < 8; i++){
        int ch = gr*8 + i;
        TV1740RawChannel channel(ch);
        channel.AddSamples(samples[i]);
        fMeasurements.push_back(channel);
        //      if(samples[i].size() != 0) std::cout << "Number of samples for channel " << ch << " is " << samples[i].size() << std::endl;
      }

    }//End loop over groups

}


TV1740RawData::TV1740RawData(int bklen, int bktype, const char* name, void *pdata) :
    TGenericData(bklen, bktype, name, pdata)
{

  fVerbose = 0;
  //Read the V1740 header data
  fGlobalHeader0 = GetData32()[0];
  fGlobalHeader1 = GetData32()[1];
  fGlobalHeader2 = GetData32()[2];
  fGlobalHeader3 = GetData32()[3];
  int event_size = GetData32()[0] & 0x0FFFFFFF;

  if(event_size > bklen || event_size==0){
    static int failed_v1740_decode = 0;
    failed_v1740_decode++;
    if(failed_v1740_decode <= 5){
      std::cerr << "TV1740RawData::TV1740RawData : "
                << "Something wrong with this V1740 bank.  \n"
                << "The first word is " << event_size
                << " which is larger than MIDAS bank size " << bklen << ".\n"
                << "Warning " << failed_v1740_decode << " of 5." << std::endl;
    }

    return;
  }

  HandleUncompressedData();
}


TV1740RawData::~TV1740RawData(){

}
