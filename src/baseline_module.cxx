/******************************************************************
 * Baseline module
 *
 * A. Capra, D. Gallacher
 * October 2021
 * Performs baseline subtraction and restoration if required, see LoLX/README.md for details
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include <iostream>
#include <fstream>
#include <cmath>
#include <numeric>
#include <functional>
#include <cstdlib>


#include "adcflow.hxx"
#include "baselineflow.hxx"
#include "TWDRawData.hxx"

#include "utils.hxx"

#include "json.hpp"
using json = nlohmann::json;

#include "TGraph.h"
#include "TGraphSmooth.h"

//Read preprocessor flag value as string literal
#define STRING(s) #s
#define STRSTRING(s) STRING(s)

class BaselineFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};


class BaselineModule: public TARunObject
{
public:
   BaselineFlags* fFlags;

protected:
  int fCounter; // counts the number of processed events
  int fError;   // counts the number of encountered errors
  std::string fBaselineMode; //Simple subtraction ==  0, LoLX-style restoration ==1, Simple subtraction no

  //Number of samples at the start of the baseline window to use for calculations
  int nbase_samples;

  double fBaseline;//Baseline: Average, Mode or Median
  double fBaseline_rms;//Baseline RMS
 
  //LoLX-style specific members
  double fBaseline_tau;
  double fBaseline_t0_thresh;
  double fNanosecsPerSample;
  int fBinWindow;///Window for the baseline restoration algorithm, number of bins at end of trace to average for offset
  int fBinJump; //Jump number of bins back if there is a peak at end of the trace.
  double fSlope;//Slope of baseline to jump away from
 
  //Utility class with analysis methods
  Utils *utils;

  TGraph *gSmoothed = NULL;//Smoothed curve of baseline oscillation
  int fSmoothChan;//Channe to use for smoothing

public:
   BaselineModule(TARunInfo* runinfo, BaselineFlags* f): TARunObject(runinfo),
                                                         fFlags(f), fCounter(0), fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"BaselineModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Baseline";
#endif
      std::string config_path;
#ifdef CONFIG_PATH
      config_path = STRSTRING(CONFIG_PATH) + fFlags->fConfigName;//Read config file path from CMAKE source dir (lolx)
#else
      config_path = fFlags->fConfigName;//If path isn't defined look in flags only
#endif

      std::cout << "Reading config file from path = "<< config_path << "\n";
      std::ifstream fin(config_path);
      if(fFlags->fVerbose)
        std::cout<<"BaselineModule Json parsing: "<<config_path<<std::endl;
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"BaselineModule Json parsing success!"<<std::endl;
      fin.close();
      fBaselineMode = settings["Baseline"]["Mode"].get<std::string>();
      nbase_samples=settings["Baseline"]["Pedestal"].get<int>();
      if( fBaselineMode == "simple" || fBaselineMode == "mode" || fBaselineMode == "median" )
	{
	  std::cout<<"BaselineModule:: Mode: "<<fBaselineMode<<std::endl;
	}
      else
	{
	  fBaseline_tau = settings["Baseline"]["DecayTau"].get<double>();
	  fBaseline_t0_thresh = settings["Baseline"]["Threshold"].get<double>();
	  fNanosecsPerSample = settings["Baseline"]["NanoSecPerSample"].get<double>();
	  fBinWindow = settings["Baseline"]["RestorationWindow"].get<int>();
	  fBinJump = settings["Baseline"]["BinJumpWindow"].get<int>();
	  fSlope = settings["Baseline"]["BinJumpSlope"].get<double>();
	}
      if (fBaselineMode == "oscillating")
	{
	  fSmoothChan = settings["Baseline"]["SmoothingChan"].get<int>();
	}

      //if utils doesn't exist, create an instance of it
      utils = new Utils();
      utils->SetVerbosity(int(fFlags->fVerbose));
   }

  ~BaselineModule()
  {
    if(utils) delete utils; 
  }


   void BeginRun(TARunInfo* runinfo)
   {
      if( runinfo->fFileName.empty() )
         std::cout<<"BaselineModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
	{ 
	  std::cout<<"BaselineModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
	}
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"BaselineModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }


   //AnalyzeFlowEvent is where the action happens
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
     AdcEventFlow* adc_flow = flow->Find<AdcEventFlow>();
     if( !adc_flow )
        {
         ++fError;
     #ifdef HAVE_MANALYZER_PROFILER
           *flags|=TAFlag_SKIP_PROFILE;
     #endif
           return flow;
        }


     if( !adc_flow->getNchannels() ){
       if(fFlags->fVerbose) std::cerr << "No ADC Channels found for event #"<< fCounter <<std::endl;
       return flow;
     }

     if( fFlags->fVerbose ){
       std::cout<<"BaselineModule::AnalyzeFlowEvent Event: "<<adc_flow->getEventNumber()<<" ("<<fCounter<<")   # of channels: "
		<<adc_flow->getNchannels()
		<<", # of samples per channel: "<<adc_flow->getNsamples(0)<<std::endl;
     } else if( (fCounter%1000)==0 ) std::cout<<"AnalyzeFlowEvent #"<<std::setw(8)<<fCounter<<std::endl;
     if( adc_flow->getNsamples(0) < nbase_samples )
        {
           std::cerr<<"BaselineModule::AnalyzeFlowEvent pedestal length exceeds number of samples"<<std::endl;
           return flow;
        }

     BaselineEventFlow* the_flow = new BaselineEventFlow(flow,adc_flow->getEventNumber());

      //baseline oscillation correction by subtracting smoothed curve of empty channel from other channels
      if(fBaselineMode == "oscillating"){
        //Get the right index for this channel
        // // //Header info
        // int dchan = -1;
        // for(int ic = 0 ; ic < adc_flow-> getNchannels(); ++i){
        //   if( adc_flow->getChannelNumber == fSmoothChan){//Look for channel
        //     dchan = ic;
        //     break;
        //   }
        // }
        const int dchan = adc_flow->getChannelNumber(fSmoothChan);
        const TWDRawChannel *chwdaq = (const TWDRawChannel*) adc_flow->getChannel(dchan);
        if(!chwdaq){
          std::cout<< "BaselineModule: Error no channel data "<< std::endl;
          return flow;
        }
        const std::vector<double> *wftimes = chwdaq->GetTimes();
        const std::vector<double>* rawwf = adc_flow->getVector(dchan);
        TH1D hSmoothed("","",rawwf->size(),0,rawwf->size()*fNanosecsPerSample);
        if(wftimes->size() != 0){
          utils->ConvertRawWF_WDAQ(hSmoothed,rawwf,wftimes,fSmoothChan);
        }else {
          if(fFlags->fVerbose) std::cout << "Missing time vector, using constant timing from config"<<std::endl;
          utils->ConvertRawWF(hSmoothed,rawwf,dchan,fNanosecsPerSample);
        }
        //Smooth the target channel with kernel smoothing (Large bandwith ~1/4 the waveform)
        TGraph gin(&hSmoothed);
        TGraphSmooth gs("normal");
        TGraph *gout = gs.SmoothKern(&gin,"normal",double(adc_flow->getNsamples(0)/4.));
        gSmoothed = new TGraph(*gout);//Save smoothed waveform for this event to subtract off of non-empty channels
      }

      //Loop over channels
      for(int n=0; n<adc_flow->getNchannels(); ++n){
	// this is only the number of channels in module
	const int daq_chan = adc_flow->getChannelNumber(n);
	// module number
	int module = adc_flow->getBoard(n);
	
        if( fFlags->fVerbose ){
          std::cout<<"BaselineModule::AnalyzeFlowEvent "<<std::setw(4)<<n<<") "<<adc_flow->ftype[n]<<" board: "<<adc_flow->getBoard(n)<<" ch: "<<daq_chan<<std::endl;
        }

	//Baseline corrected wf vector
	std::vector<double> corrWF;
#ifdef USE_WDAQ
	std::vector<double> corrWFTimes;
#endif

	double baseline = utils->calculate_baseline(adc_flow->getVector(n),nbase_samples);

	//Calculate simple baseline parameters from raw WF
	//Simple baseline subtraction
	if( fBaselineMode == "simple" ){ // average over n samples
	  fBaseline = baseline;
	  const std::vector<double>* raw = adc_flow->getVector(n);
	  BaselineCorrectionDefault(fBaseline,raw,corrWF,-1);//Negative polarity
	} else if( fBaselineMode == "mode" ){ // most probable value
	  fBaseline = utils->calculate_mode(adc_flow->getVector(n));
	  const std::vector<double>* raw = adc_flow->getVector(n);
	  BaselineCorrectionDefault(fBaseline,raw,corrWF,-1);//Negative polarity
	} else if( fBaselineMode == "median" ){ // median value
	  fBaseline = utils->calculate_median(adc_flow->getVector(n));
	  const std::vector<double>* raw = adc_flow->getVector(n);
	  BaselineCorrectionDefault(fBaseline,raw,corrWF,-1);//Negative polarity
	} else if (fBaselineMode == "lolx" || fBaselineMode == "oscillating"){
	  //LoLX method
	  fBaseline = baseline;
#ifdef USE_WDAQ
	  BaselineCorrectionLoLX(adc_flow,corrWF,daq_chan,n,corrWFTimes);
#else
	  std::vector<double> corrwft_dummy;//if we don't have timing, then pass a dummy vector and move on
	  BaselineCorrectionLoLX(adc_flow,corrWF,daq_chan,n,corrwft_dummy);
	  corrwft_dummy.clear();
#endif
	} else if( fBaselineMode == "simple_neg" ){
	  fBaseline = baseline;
	  const std::vector<double>* raw = adc_flow->getVector(n);
	  BaselineCorrectionDefault(fBaseline,raw,corrWF,1);//Positive polarity
	} else { //Do nothing to the waveform
	  corrWF.assign(adc_flow->getVector(n)->begin(),adc_flow->getVector(n)->end());
	}

	//Add to the baselineflowEvent
	std::string board_type = adc_flow->ftype[n];
#ifdef USE_WDAQ
	if(fBaselineMode != "lolx"){
	  const TWDRawChannel *ch_wdaq = (const TWDRawChannel*) adc_flow->getChannel(n);
	  const std::vector<double> *v_times = ch_wdaq->GetTimes();
	  corrWFTimes = *v_times;
	}
	the_flow->addWF(corrWF,corrWFTimes,daq_chan,board_type.c_str());
#else
	the_flow->add(corrWF,daq_chan,module,board_type.c_str());
#endif
	// calculate baseline RMS
	fBaseline_rms = utils->calculate_baseline_rms(adc_flow->getVector(n),baseline,nbase_samples);
	// add baseline info to the flow
	the_flow->addBaselines(fBaseline,fBaseline_rms,fBaselineMode);
	if( fFlags->fVerbose )
	  std::cout<< "Baseline: "<<fBaseline<<" RMS: "<<fBaseline_rms<<std::endl;
      }//End loop over channels

      if(gSmoothed != NULL) delete gSmoothed;//Delete the smoothed curve (only non-NULL if baselinemode==4)
      if(fFlags->fVerbose) std::cout << "Leaving baseline_module"<<std::endl;
      flow = the_flow;
      ++fCounter;
      return flow;
    }//End AnalyzeFlowEvent

    //Default baseline corr
    void BaselineCorrectionDefault(double baseline, const std::vector<double> *raw_wf, std::vector<double> &corr_wf, int polarity)
    {
      corr_wf.resize(raw_wf->size());
      //This flips pulses (DS standard)
      std::transform(raw_wf->begin(),raw_wf->end(),corr_wf.begin(),
      [baseline,polarity](double y){ return polarity*(y-baseline);});
    }

    /// LoLX Style baseline restoration
    void BaselineCorrectionLoLX(AdcEventFlow *rawflow,std::vector<double> &corr_wf,int daqchan,int n, std::vector<double> &corr_wf_t)
    {
      //Convert to histogram for some analysis
      TH1D hWF;
      #ifdef USE_WDAQ
          //Wavedaq has variable time binning
          const TWDRawChannel *ch_wdaq = (const TWDRawChannel*) rawflow->getChannel(n);
          const std::vector<double> *v_times = ch_wdaq->GetTimes();
          corr_wf_t = *v_times;
          if(v_times->size() != 0){
          //  if(v_times->size()==0 || adc_flow->getVector(n)->size()==0)continue;
            utils->ConvertRawWF_WDAQ(hWF,rawflow->getVector(n),v_times,daqchan);
          }else{
            if(fFlags->fVerbose) std::cout << "Missing time vector, using constant timing from config"<<std::endl;
            utils->ConvertRawWF(hWF,rawflow->getVector(n),daqchan,fNanosecsPerSample);
          }
          if(fBaselineMode == "oscillating"){
            if( gSmoothed != NULL ){
              for(int iB=1;iB <= hWF.GetNbinsX();iB++) {
                double smoothed = gSmoothed->Eval(hWF.GetBinCenter(iB));
                hWF.SetBinContent(iB,hWF.GetBinContent(iB)-smoothed);
              }
            }
          }
      #else
            utils->ConvertRawWF(hWF,rawflow->getVector(n),daqchan,fNanosecsPerSample);
      #endif

      if(hWF.GetNbinsX() == 1 ) return;//Empty WF
      //Baseline restoration assumes baseline is 0
      utils->RemoveBaseline(hWF,nbase_samples); //baseline removal
      //Only if baseline is near-ish to 0, do the restoration
      if( fabs(utils->CalculateBaseline(hWF,5,5+nbase_samples)) < 100 ){
        Double_t tZero = utils->GetT0Raw(hWF,fBaseline_t0_thresh);
        if( tZero > 0.0 && tZero < hWF.GetBinCenter(hWF.GetNbinsX())*0.95 ){ //Make sure we have a physical t0 time
          utils->BaselineRestoration(hWF,fBaseline_tau,tZero,fBinWindow, fBinJump,fSlope); //baseline restoration algorithm
          utils->RemoveBaseline(hWF,nbase_samples);
        }
      }
      //Fill corrected vector
      for(int iBin = 0;iBin < hWF.GetNbinsX();iBin++){
        corr_wf.push_back(hWF.GetBinContent(iBin));
      }
    }
};


class BaselineModuleFactory: public TAFactory
{
public:
   BaselineFlags fFlags;

public:
   void Usage()
   {
      printf("Modules options flags:\n");
      printf("\n");
      printf("\t--config, --conf, -c </path/to/json_file>\n\t\tspecify path json configuration file\n");
      printf("\t--verbose, -v\n\t\tprint status information\n");
   }

   void Init(const std::vector<std::string> &args)
   {
      printf("BaselineModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
   }

   void Finish()
   {
      printf("BaselineModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("BaselineModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new BaselineModule(runinfo, &fFlags);
   }


};

static TARegister tar(new BaselineModuleFactory);
