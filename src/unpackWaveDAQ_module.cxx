/******************************************************************
 * Unpack MEGII WaveDAQ data *
 *
 * D. Gallacher
 * October 2021
 *
******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "adcflow.hxx"
#include "TWDRawData.hxx"

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <time.h>
#include <cstdlib>

#include "TTimeStamp.h"

#include "json.hpp"
using json = nlohmann::json;

//Read preprocessor flag value as string literal
#define STRING(s) #s
#define STRSTRING(s) STRING(s)

class UnpackAdcFlagsWD
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};


class UnpackAdcModuleWD: public TARunObject
{
public:
   UnpackAdcFlagsWD* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors
   const int numberChannelPerModule = 18;
   const double NanosecsPerSample = 1.0;//Not static
   int fTotNumBoards = 2;//For LoLX
   //delta-time vector from DRST, defined for first event only
   std::map<int, std::vector<double>> fDeltaTimes;//Key is channel ID not index

public:
   UnpackAdcModuleWD(TARunInfo* runinfo, UnpackAdcFlagsWD* f): TARunObject(runinfo),
                                                         fFlags(f), fCounter(0), fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"UnpackAdcModuleWD ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="UnpackWDAQ";
#endif

      std::string config_path;
#ifdef CONFIG_PATH
      config_path = STRSTRING(CONFIG_PATH) + fFlags->fConfigName;//Read config file path from CMAKE source dir (lolx)
#else
      config_path = fFlags->fConfigName;//If path isn't defined look in flags only
#endif
      std::cout << "Reading config file from path = "<< config_path << "\n";
      std::ifstream fin(config_path);
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"UnpackWD Json parsing success!"<<std::endl;
      fin.close();

      fTotNumBoards=settings["ADC"]["Number of WD Boards"].get<int>();

      std::cout<<"Total number of WD Boards for run "<<runinfo->fRunNo
               <<" is "<<fTotNumBoards<<std::endl;
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"UnpackAdcModuleWD::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"UnpackAdcModuleWD::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"UnpackAdcModuleWD::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

   TAFlowEvent* Analyze(TARunInfo* /*runinfo*/, TMEvent* midasevent,
                        TAFlags* /*flags*/, TAFlowEvent* flow)
   {
      if( midasevent->error )
         {
            ++fError;
            return flow;
         }

      int event_num = midasevent->event_id;
      int trigger_mask = midasevent->trigger_mask;

      //Use generic ADC flow module
      AdcEventFlow* eflow = flow->Find<AdcEventFlow>();
      if( !eflow )
         {
            eflow = new AdcEventFlow(flow, midasevent->time_stamp,event_num);
            flow = eflow;
         }

      eflow->setTriggerMask(trigger_mask);

      if( fFlags->fVerbose )
          std::cout<<"UnpackAdcModuleWD::Analyze ID: "<< event_num
                   <<" S/N: "<<midasevent->serial_number
                   <<" TS: "<<midasevent->time_stamp
                   <<" Trigger mask: "<<trigger_mask<<std::endl;

      std::string bankname = "DRSV"; //Get the adc data
      TMBank* bankADC = midasevent->FindBank(bankname.c_str());
      if( !bankADC ){
        std::cout << "Error UnpackAdcModuleWD bank "<< bankname << " not found "<< std::endl;
        fError++;
        return flow;
      }
      if( fFlags->fVerbose )
         std::cout<<"UnpackAdcModuleWD::Analyze bank: "<<bankname << std::endl;
      TWDRawData* raw = new TWDRawData(event_num,(int)bankADC->data_size,(int)bankADC->type,
                                               bankADC->name.c_str(),
                                               midasevent->GetBankData(bankADC));

      //Set counter for both modules
      eflow->fEventCounters[0] = raw->GetEventCounter();
      eflow->fEventCounters[1] = raw->GetEventCounter();

      bankname = "DRST";//Get the timing
      TMBank* bankTime = midasevent->FindBank(bankname.c_str());

      raw->AddDRSTBank(fDeltaTimes,(int)bankTime->data_size,(int)bankTime->type,
                                               bankTime->name.c_str(),
                                               midasevent->GetBankData(bankTime));


      for( int chan = 0; chan < raw->GetNChannels(); chan++ )
         {
            TWDRawChannel channelData = raw->GetChannelData(chan);
            if( channelData.IsEmpty() ) continue;
            int board_idx = -1;
            chan < 16 ? board_idx = 123 : board_idx = 124;
            channelData.SetBoardNumber(board_idx);
            channelData.SetIndex(board_idx*numberChannelPerModule+chan);
            if( fFlags->fVerbose ) channelData.Print();
            eflow->add(&channelData,"WDAQ");
         }
      delete raw;


      if( fFlags->fVerbose )
         std::cout<<"UnpackAdcModuleWD::Analyze Event: "<<fCounter
                  <<" # of channels: "<<eflow->getNchannels()<<std::endl;

      ++fCounter;
      return flow;
   }
};


class UnpackAdcModuleWDFactory: public TAFactory
{
public:
   UnpackAdcFlagsWD fFlags;

public:
   void Usage()
   {
      printf("UnpackADCModuleWDFactory flags:\n");
      printf("--verbose\tprint status information\n");
   }

   void Init(const std::vector<std::string> &args)
   {
      printf("UnpackAdcModuleWDFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
   }

   void Finish()
   {
      printf("UnpackAdcModuleWDFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("UnpackAdcModuleWDFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new UnpackAdcModuleWD(runinfo, &fFlags);
   }

};

static TARegister tar(new UnpackAdcModuleWDFactory);
