/******************************************************************
 * Flow Object(s) for ADC analysis *
 *
 * A. Capra, D. Gallacher
 * August 2021
 *
******************************************************************/

#ifndef __ADCFLOW__
#define __ADCFLOW__

#include "manalyzer.h"

#include <map>
#include <vector>
#include <string>
#include <stdexcept>

#include "TRawChannel.hxx"

#include "TTimeStamp.h"


class AdcEventFlow: public TAFlowEvent
{
public:
  const double fmidas_ts;
  std::map<int, unsigned int> fEventCounters; // Board ID -> event counter
  std::map<int, double> fTriggerTime; // Board ID -> (extended) trigger time tag
  std::vector<std::string> ftype;

private:
  std::vector<TRawChannel> fChannels;
  int fNchannels;
  const int fEventNumber; // midas event serial number
  uint64_t fTriggerMask;

public:
   AdcEventFlow(TAFlowEvent* flow,
                  double ts, int event_number):TAFlowEvent(flow),
                                               fmidas_ts(ts),fNchannels(0),
                                               fEventNumber(event_number)
   {}

  ~AdcEventFlow()
   {
      fEventCounters.clear();
      fTriggerTime.clear();
      fChannels.clear();
   }

  void add(TRawChannel* ch)
  {
     fChannels.push_back(*ch);
     ++fNchannels;
     //ch->Print();
  }

   void add(TRawChannel* ch, const char* type)
  {
     fChannels.push_back(*ch);
     ftype.emplace_back(type);
     ++fNchannels;
     //ch->Print();
  }
  void setTriggerMask(int tmask){ fTriggerMask = tmask; }

  const TRawChannel* getChannel(int ch) const
  {
    if( ch >= fNchannels )
      throw std::out_of_range("out of range!");

    if( fChannels.empty() )
      throw std::out_of_range("empty!");

    return &fChannels.at(ch);
  }

  inline int getNchannels() const
  {
    return fNchannels;
  }

  inline int getNsamples(int ch) const
  {
    return (int) fChannels.at(ch).GetNSamples();
  }

  inline int getSample(int ch, int s) const
  {
    return fChannels.at(ch).GetADCSample(s);
  }

  inline int getBoard(int ch) const
  {
    return fChannels.at(ch).GetBoardNumber();
  }

  inline int getChannelNumber(int ch) const
  {
     return fChannels.at(ch).GetChannelNumber();
  }

  inline int getChannelIndex(int ch) const
  {
     return fChannels.at(ch).GetIndex();
  }

  inline const std::vector<double>* getVector(int ch) const
  {
      return fChannels.at(ch).GetMeasurement();
  }

  inline const double* getWaveform(int ch) const
  {
     return fChannels.at(ch).GetWaveform();
  }

  inline int getEventNumber() const
  {
     return fEventNumber;
  }
  inline int getTriggerMask()
  {
    return fTriggerMask;
  }

};

#endif
