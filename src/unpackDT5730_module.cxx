/******************************************************************
 * Unpack CAEN DT5730 data *
 *
 * D. Gallacher, based on unpack_v2740_module
 * February 2022
 *
******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "adcflow.hxx"
#include "TDT5730RawData.hxx"

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <cstdlib>

//Read preprocessor flag value as string literal
#define STRING(s) #s
#define STRSTRING(s) STRING(s)


#include "json.hpp"
using json = nlohmann::json;

class UnpackAdcFlagsDT5730
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};


class UnpackAdcModuleDT5730: public TARunObject
{
public:
   UnpackAdcFlagsDT5730* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors
   const int numberChannelPerModule = 8;
   const double NanosecsPerSample = 2.0; //ADC clock runs at 500 Mhz on the DT5730 = units of 2 nsecs
   int fTotNumBoards=1;//

public:
   UnpackAdcModuleDT5730(TARunInfo* runinfo, UnpackAdcFlagsDT5730* f): TARunObject(runinfo),
                                                         fFlags(f), fCounter(0), fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"UnpackAdcModuleDT5730 ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="UnpackAdcDT5730";
#endif
      std::string config_path;
#ifdef CONFIG_PATH
      config_path = STRSTRING(CONFIG_PATH) + fFlags->fConfigName;//Read config file path from CMAKE source dir (lolx)
#else
      config_path = fFlags->fConfigName;//If path isn't defined look in flags only
#endif
      std::cout << "Reading config file from path = "<< config_path << "\n";
      std::ifstream fin(config_path);
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"UnpackDT5730 Json parsing success!"<<std::endl;
      fin.close();

      fTotNumBoards=settings["ADC"]["Number of DT5730"].get<int>();

      std::cout<<"Total number of DT5730 boards for run "<<runinfo->fRunNo
               <<" is "<<fTotNumBoards<<std::endl;
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"UnpackAdcModuleDT5730::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"UnpackAdcModuleDT5730::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"UnpackAdcModuleDT5730::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

   TAFlowEvent* Analyze(TARunInfo* /*runinfo*/, TMEvent* midasevent,
                        TAFlags* /*flags*/, TAFlowEvent* flow)
   {
      if( midasevent->error )
         {
            ++fError;
            return flow;
         }

      int fe_idx = midasevent->trigger_mask;
      if( fFlags->fVerbose )
         std::cout<<"UnpackAdcModuleDT5730::Analyze ID: "<<midasevent->event_id
                  <<" S/N: "<<midasevent->serial_number
                  <<" TS: "<<midasevent->time_stamp
                  <<" Trigger mask: "<<fe_idx<<std::endl;

      //Use generic ADC flow module
      AdcEventFlow* eflow = flow->Find<AdcEventFlow>();
      if( !eflow )
         {
            eflow = new AdcEventFlow(flow, midasevent->time_stamp,midasevent->event_id);
            flow = eflow;
         }


      for (int board_idx = 0; board_idx < fTotNumBoards; ++board_idx)
         {
            std::string bankname="DT57";//At McGill
            TMBank* bank = midasevent->FindBank(bankname.c_str());
            if( !bank ) continue;
            if( fFlags->fVerbose )
               std::cout<<"UnpackAdcModuleDT5730::Analyze DT5730 bank "<<bankname
                        <<" found... Board: "<<board_idx<<std::endl;
            TDT5730RawData* raw = new TDT5730RawData((int)bank->data_size,(int)bank->type,
                                                     bank->name.c_str(),
                                                     midasevent->GetBankData(bank));

            eflow->fEventCounters[board_idx] = raw->GetEventCounter();
            if(fFlags->fVerbose) raw->Print();

            for( int chan = 0; chan < raw->GetNChannels(); ++chan )
               {
                  TDT5730RawChannel channelData = raw->GetChannelData(chan);
                  if( channelData.IsEmpty() ) continue;
                  channelData.SetBoardNumber(board_idx);
                  channelData.SetIndex(board_idx*numberChannelPerModule+chan);
                  if( fFlags->fVerbose ) channelData.Print();
                  eflow->add(&channelData,"DT5730");
               }
            delete raw;
         }
      if( fFlags->fVerbose )
         std::cout<<"UnpackAdcModuleDT5730::Analyze Event: "<<fCounter
                  <<" # of channels: "<<eflow->getNchannels()<<std::endl;

      ++fCounter;
      return flow;
   }
};


class UnpackAdcModuleDT5730Factory: public TAFactory
{
public:
   UnpackAdcFlagsDT5730 fFlags;

public:
   // void Usage()
   // {
   //    printf("UnpackAdcModuleDT5730Factory flags:\n");
   //    printf("--verbose\tprint status information\n");
   // }

   void Init(const std::vector<std::string> &args)
   {
      printf("UnpackAdcModuleDT5730Factory::Init!\n");

      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
         //fFlags.fVerbose = true;

   }

   void Finish()
   {
      printf("UnpackAdcModuleDT5730Factory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("UnpackAdcModuleDT5730Factory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new UnpackAdcModuleDT5730(runinfo, &fFlags);
   }

};

static TARegister tar(new UnpackAdcModuleDT5730Factory);
