/******************************************************************
 * Unpack CAEN V1725 data *
 * 
 * A. Capra
 * August 2021
 *
******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "adcflow.hxx"
#include "TV1725RawData.hxx"

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>

#include "json.hpp"
using json = nlohmann::json;


class UnpackAdcFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
   int fSkipEvents=-1;
};
  

class UnpackAdcModule: public TARunObject
{
public:
   UnpackAdcFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors
   const int numberChannelPerModule = 16;
   const double NanosecsPerSample = 4.0; //ADC clock runs at 250Mhz on the v1725 = units of 4 nsecs
   int fTotNumBoards=4;
   int fNumberSamples;

public:
   UnpackAdcModule(TARunInfo* runinfo, UnpackAdcFlags* f): TARunObject(runinfo),
                                                           fFlags(f), 
                                                           fCounter(0), fError(0),
                                                           fNumberSamples(-1)
   {
      if(fFlags->fVerbose) std::cout<<"UnpackAdcModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="UnpackAdc";
#endif

      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"UnpackV1725 Json parsing success!"<<std::endl;
      fin.close();

      fTotNumBoards=settings["ADC"]["Number of V1725"].get<int>();
      
      std::cout<<"Total number of V1725 boards for run "<<runinfo->fRunNo
               <<" is "<<fTotNumBoards<<std::endl;
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"UnpackAdcModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"UnpackAdcModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"UnpackAdcModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }
  
   TAFlowEvent* Analyze(TARunInfo* /*runinfo*/, TMEvent* midasevent,
                        TAFlags* /*flags*/, TAFlowEvent* flow)
   {
      if( midasevent->error )
         {
            ++fError;
            return flow;
         }

      // allow the user to fast forward to the desired event
      if( fCounter < fFlags->fSkipEvents)
         {
            if( fFlags->fVerbose )
               std::cout<<"UnpackADCModule Skip Event: "<<fCounter<<std::endl;
            ++fCounter;
            return flow;
         }

      int fe_idx = midasevent->trigger_mask;
      if( fFlags->fVerbose )
         std::cout<<"UnpackADCModule::Analyze ID: "<<midasevent->event_id
                  <<" S/N: "<<midasevent->serial_number
                  <<" TS: "<<midasevent->time_stamp
                  <<" Trigger mask: "<<fe_idx<<std::endl;

      AdcEventFlow* eflow = flow->Find<AdcEventFlow>();
      if( !eflow )
         {
            eflow = new AdcEventFlow(flow, midasevent->time_stamp,fCounter);
            flow=eflow;
         }
           
      for (int board_idx = 0; board_idx < fTotNumBoards; ++board_idx)
         {
            std::stringstream ss;
            ss<<"W2"<<std::setfill('0')<<std::setw(2)<<std::to_string(board_idx);
            std::string bankname=ss.str();
            TMBank* bank = midasevent->FindBank(bankname.c_str());
            if( !bank ) continue;
            if( fFlags->fVerbose )
               std::cout<<"UnpackADCModule::Analyze V1725 bank "<<bankname
                        <<" found... Board: "<<board_idx<<std::endl;
            TV1725RawData* raw = new TV1725RawData((int)bank->data_size,(int)bank->type,
                                                     bank->name.c_str(),
                                                     midasevent->GetBankData(bank));
            if( raw->IsZLECompressed() )
               {
                  //std::cout<<"UnpackADCModule::Analyze WARNING! ZLE unpacking is not implemented"<<std::endl;
                  std::cout<<"UnpackADCModule::Analyze WARNING! Using ZLE unpacking"<<std::endl;
                  // continue; // Maybe implemented
               }
            if( fFlags->fVerbose )
               raw->Print();

            eflow->fEventCounters[board_idx] = raw->GetEventCounter();
            eflow->fTriggerTime[board_idx] = raw->GetTriggerTimeStamp();
            if( fFlags->fVerbose )
               std::cout<<"UnpackADCModule::Analyze V1725 ETTT: "<<eflow->fTriggerTime[board_idx]
                        <<" s\tEvent Counter: "<<eflow->fEventCounters[board_idx]<<std::endl;

            // loop over channels
            for( int chan = 0; chan < raw->GetNChannels(); ++chan )
               {
                  TV1725RawChannel& channelData = raw->GetChannelData(chan);
                  if( channelData.IsEmpty() ) continue;
                  if( !IsValid(channelData) )
                     {
                        delete raw;
                        std::cerr<<"UnpackADCModule::Analyze V1725 SKIPPING MIDAS EVENT S/N "<<midasevent->serial_number<<std::endl;
                        ++fError;
                        return flow;
                     }
                  channelData.SetBoardNumber(board_idx);
                  channelData.SetIndex(board_idx*numberChannelPerModule+chan);
                  if( fFlags->fVerbose && 0 ) channelData.Print();
                  eflow->add(&channelData,"V1725");
               } // loop over channels
            delete raw;
         }// loop over boards
      if( fFlags->fVerbose )
         std::cout<<"UnpackAdcModule::Analyze Event: "<<fCounter
                  <<" # of channels: "<<eflow->getNchannels()<<std::endl;
     
      ++fCounter;
      return flow;
   } // analyze
   
   bool IsValid(TV1725RawChannel& ch)
   {
      if( fNumberSamples < 0 )
         {
            fNumberSamples = ch.GetNSamples();
         }
      
      if( fNumberSamples != ch.GetNSamples() )
         {
            std::cerr<<"UnpackADCModule::IsValid ERROR: number of samples changed from "
                     <<fNumberSamples<<" to "<<ch.GetNSamples()<<std::endl;
            return false;            
         }
      return true;
   }
};


class UnpackAdcModuleFactory: public TAFactory
{
public:
   UnpackAdcFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("UnpackAdcModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
            if( args[i] == "--skip-events" ||
                args[i] == "-s" )
               fFlags.fSkipEvents=std::stoi(args[i+1]);
         }
      
   }
   
   void Finish()
   {
      printf("UnpackAdcModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("UnpackAdcModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new UnpackAdcModule(runinfo, &fFlags);
   }

};

static TARegister tar(new UnpackAdcModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
