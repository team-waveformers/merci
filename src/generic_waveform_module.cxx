/******************************************************************
 *  Waveform display for baselineflow, adcflow and oulseflow*
 *
 * D. Gallacher
 * March 2022
 * Displays waveforms and pulse finder output (Raw and corrected)
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "baselineflow.hxx"
#include "adcflow.hxx"
#include "pulseflow.hxx"
#include "utils.hxx"

#include "TMath.h"
#include "TH1D.h"
#include "TH1.h"
#include "TCanvas.h"
#include "TH2D.h"
#include "TLine.h"
#include "TF1.h"
#include "TPad.h"
#include "TGraph.h"
#include <TROOT.h>
#include <TStyle.h>

#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <cassert>
#include <cstdlib>


#include "json.hpp"
using json = nlohmann::json;

class GenericWaveformFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};

//Read preprocessor flag value as string literal
#define STRING(s) #s
#define STRSTRING(s) STRING(s)

class GenericWaveformModule: public TARunObject
{
public:
   GenericWaveformFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors

private:

  bool fSave; // save PDFs of canvases
  int fSaveEvents; // limit of events to save
  int fEnabled; // Whether to display things at all

  bool fPersistency; // enable summation of waveforms
  int fPersistencyChannel; // visualize a special channel
  int fPersistencyEvents; // terminate the summation at this event

  double fWFmin; // Minimum of raw waveform display
  double fWFmax; //Maximum of raw waveform display
  double fNanosecPerSample;//Number of nanoseconds per sample

  double fPersistencyMax;//Maximum of Y-axis for persistency plot
  double fPersistencyMin;//Minimum of y-axis for persistency plot

  int fNumChannels; //Number of channels for the raw and corrected waveforms
  std::map<int,TH1D*> fhWFRaw; // raw waveforms
  std::map<int,TH1D*> fhWFCorr; // baseline subtracted waveforms
  std::map<int,TH1D*> fhPulses; // found pulse location and size

  std::map<int,TCanvas*> fCRaw; // the canvases for the raw waveforms
  std::map<int,TCanvas*> fCCorr; // the canvases for the corrected waveforms+pulses

  TCanvas* fCP; // the canvas for the persistency plot for the selected channel

  TH2D* fhPersistency; // the persistency histogram

  TDirectory* fRawDir;//Directory to hold raw plots
  TDirectory* fCorrDir;//Directory to hold corr/pulse plots

  Utils *util;

public:
   GenericWaveformModule(TARunInfo* runinfo, GenericWaveformFlags* f): TARunObject(runinfo),
   fFlags(f), fCounter(0), fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"GenericWaveformModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="GenericWaveforms";
#endif
      std::string config_path;
#ifdef CONFIG_PATH
      config_path = STRSTRING(CONFIG_PATH) + fFlags->fConfigName;//Read config file path from CMAKE source dir (lolx)
#else
      config_path = fFlags->fConfigName;//If path isn't defined look in flags only
#endif
      std::cout << "Reading config file from path = "<< config_path << "\n";
      std::ifstream fin(config_path);
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"GenericWaveformModule Json parsing success!"<<std::endl;
      fin.close();


      fEnabled = settings["Waveform"]["Enabled"].get<int>();
      fWFmin = settings["Waveform"]["WF Minimum"].get<double>();
      fWFmax = settings["Waveform"]["WF Maximum"].get<double>();
      fSave = settings["Waveform"]["Save PDF"].get<bool>();
      fSaveEvents = settings["Waveform"]["Save Events Limit"].get<int>();
      fPersistency = settings["Waveform"]["Persistency"].get<bool>();
      fPersistencyChannel = settings["Waveform"]["Persistency Channel"].get<int>();
      fPersistencyEvents = settings["Waveform"]["Persistency Events Limit"].get<int>();
      fPersistencyMin = settings["Waveform"]["Persistency Max"].get<double>();
      fPersistencyMax = settings["Waveform"]["Persistency Min"].get<double>();

      util = new Utils();
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"GenericWaveformModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"GenericWaveformModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;

         if( fEnabled ){
           int runn=runinfo->fRunNo;
           for(int iCh=0;iCh<fNumChannels;iCh++){
             std::string cname = Form("Run# %i: Raw_Canvas_%i",runn,iCh);
             if(fFlags->fVerbose&&0) std::cout<<"\tnew canvas: "<<cname<<std::endl;
             fCRaw[iCh] = new TCanvas(cname.c_str(),cname.c_str(),2000,1500);
             fCRaw.at(iCh)->ToggleEventStatus();
           }//end loop over channels

           //Make directories for the channels
           if( runinfo->fFileName.empty() ){
               gDirectory->cd("RootApp:/");
               fRawDir = runinfo->fRoot->fgDir->mkdir("RawChannels");
               fCorrDir = runinfo->fRoot->fgDir->mkdir("CorrChannels");
            } else {
                 runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
                 fRawDir = runinfo->fRoot->fOutputFile->mkdir("RawChannels");
                 fCorrDir = runinfo->fRoot->fOutputFile->mkdir("CorrChannels");
            }//End online check
          }//End enable check

         if(fPersistency){
           std::string cname="cPersistencyR"+std::to_string(runinfo->fRunNo)+"CH"+std::to_string(fPersistencyChannel);
           fCP = new TCanvas(cname.c_str(),cname.c_str(),900,700);
           if( runinfo->fFileName.empty() ){
             gDirectory->cd("RootApp:/");
           } else {
              runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
          }
        }//End persistency check
   }//End begin run

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"GenericWaveformModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;

      //Cleanup
      for(auto it=fhWFRaw.begin();it!=fhWFRaw.end();++it){
        if(it->second) delete it->second;
      }
      fhWFRaw.clear();
      for(auto it=fhWFCorr.begin();it!=fhWFCorr.end();++it){
        if(it->second) delete it->second;
      }
      fhWFCorr.clear();
      for(auto it=fhPulses.begin();it!=fhPulses.end();++it){
        if(it->second) delete it->second;

      }
      fhPulses.clear();

      if( fEnabled ){
            for(auto it=fCRaw.begin(); it!=fCRaw.end(); ++it){
              if(it->second) delete it->second;
            }
            fCRaw.clear();
            for(auto it=fCCorr.begin(); it!=fCCorr.end(); ++it){
              if(it->second) delete it->second;
            }
            fCCorr.clear();
         }

      if(fPersistency && fSave)
         {
            fCP->SaveAs(".pdf");
            delete fCP;
         }
   }

   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {

      BaselineEventFlow* corr_flow = flow->Find<BaselineEventFlow>();
      if( !corr_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      AdcEventFlow* adc_flow = flow->Find<AdcEventFlow>();
      if( !adc_flow )
          {
            ++fError;
       #ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
       #endif
            return flow;
          }

      PulseFlow* pulse_flow = flow->Find<PulseFlow>();
         if( !pulse_flow )
          {
               ++fError;
      #ifdef HAVE_MANALYZER_PROFILER
               *flags|=TAFlag_SKIP_PROFILE;
      #endif
               return flow;
          }

      if( fFlags->fVerbose ){
        std::cout<<"GenericWaveformModule::AnalyzeFlowEvent # of ch: "<<corr_flow->getNchannels()<<std::endl;
      }

         fNanosecPerSample = pulse_flow->conf->nanoSecsPerSample;

         ResetHisto();
         //Get the actual number of channels
         fNumChannels = adc_flow->getNchannels();
         TDirectory* dir = runinfo->fRoot->fgDir;
         //loop over channels and do stuff
         for(int ich = 0; ich < fNumChannels; ++ich){
           const std::vector<double> *raw_wf = adc_flow->getVector(ich);
           if(raw_wf->empty()) continue; //Skip empty waveforms
           const std::vector<double>* corr_wf = corr_flow->getChannelVector(ich);
           dir->cd();// RootApp:/manalyzer
           int daq_channel = pulse_flow->results.at(ich)->chanNum;
           PlotChannel(raw_wf,corr_wf,daq_channel);

           if( fPersistency && (fCounter < fPersistencyEvents) ){
             if( runinfo->fgFileList.size() )
                runinfo->fRoot->fOutputFile->cd(); // <filename>:/
             else
                gDirectory->cd("RootApp:/");
             PersistencyPlot(corr_wf,daq_channel);
          }
        }

        //Format display
         double max_height=1.1;
         for( int iChan=0; iChan < pulse_flow->nChannels; ++iChan){
             PulseFinderResults* pulseResult = pulse_flow->results.at(iChan);
             int daq_channel = pulseResult->chanNum;
             PlotPulse(pulseResult,daq_channel);

             if(pulseResult->GetNPulses()) {
               max_height = pulseResult->GetAmplitude(0);
             } else{
               max_height = corr_flow->getBaselineRMS(iChan)*3;
             }
        }
         max_height*=2.0;

         // if this module is enabled, run until here
         // i.e., make waveforms available in JSROOT
         if( !fEnabled )
            {
               ++fCounter;
               return flow;
            }

         // if graphics is available on the host
         // run with -g and select a valid channel
       #ifdef MODULE_MULTITHREAD
         std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
       #endif

         int runNum = runinfo->fRunNo;
         ShowPlots(max_height,runNum);
         ShowRaw(runNum);

         // read and plot here histos saved
         // in subdirectories
         if( runinfo->fgFileList.size() )
            runinfo->fRoot->fOutputFile->cd(); // <filename>:/
         else
            gDirectory->cd("RootApp:/");

         if( fSave && (fCounter < fSaveEvents) ){
               // TString sname(fC->GetName());
               // sname+="_Event"+std::to_string(fCounter)+".pdf";
               // fC->SaveAs(sname); fC->SaveAs(sname);
               // sname=fCA->GetName();
               // sname+="_Event"+std::to_string(fCounter)+".pdf";
               // fCA->SaveAs(sname); fCA->SaveAs(sname);
        }

      flow = pulse_flow;
      ++fCounter;
      return flow;
   }

   void PlotChannel(const std::vector<double> *raw_wf, const std::vector<double>* corr_wf,const int index)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      int Nbins = raw_wf->size();
      if( fFlags->fVerbose && 0 )
         std::cout<<"LoLXWaveformModule::PlotChannels   # of bins: "<<Nbins<<std::endl;
      std::string hname;
      std::string htitle;

      //Switch to the correct directory
      fRawDir->cd();

      if( !fhWFRaw.count(index) ){
        fhWFRaw[index] = new TH1D(Form("hRaw_%i",index),"Raw Waveform; Time (ns);Intensity (ADC/V)",Nbins,0,Nbins*fNanosecPerSample);
      }

      util->ConvertRawWF(*fhWFRaw[index],raw_wf,index,fNanosecPerSample);
      fhWFRaw[index]->SetName(Form("hRaw_%i",index));
      fhWFRaw[index]->SetTitle("Raw Waveform; Time (ns);Intensity (ADC/V)");

      //Test baseline restoration with smoothed curve

      //Switch to the correct directory
      fCorrDir->cd();

      if( !fhWFCorr.count(index) ){
        fhWFCorr[index] = new TH1D(Form("hCorr_%i",index),"Corrected Waveform;Time (ns);Intensity (ADC/V)",Nbins,0,Nbins*fNanosecPerSample);
      }
      util->ConvertRawWF(*fhWFCorr[index],corr_wf,index,fNanosecPerSample);
      fhWFCorr[index]->SetName(Form("hCorr_%i",index));
      fhWFCorr[index]->SetTitle("Corrected Waveform;Time (ns);Intensity (ADC/V)");

      //if(index!=29) fhWFCorr[index]->Add(fSmoothed,-1);

      if( !fhPulses.count(index) )
         {
            fhPulses[index] = new TH1D(Form("hPulse_%i",index),"Pulse Waveform;Time (ns);Intensity (ADC/V)",Nbins,0,Nbins*fNanosecPerSample);
         }
      for(int iB = 1; iB < Nbins+2;iB++) fhPulses.at(index)->SetBinContent(iB,0.0);
      fhPulses[index]->SetLineColor(kRed+2);
      fhPulses[index]->SetLineWidth(2);
   }

   //Get pulse information and display output
   void PlotPulse(PulseFinderResults *pRes,int index)
   {
     #ifdef MODULE_MULTITHREAD
           std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
     #endif
     std::cout << "Waveform Module Plot Pulse" <<std::endl;
     // Fill histogram with pulses, pulse width based on time and pulse height based on amplitude
     const int np = pRes->GetNPulses();
     if(!np) return;
     for(int iP =0; iP < np ;iP++){
       double pulseStart = pRes->GetStartTime(iP);
       double pulseEnd = pulseStart + pRes->GetTimeOverThreshold(iP);
       double pulseHeight = pRes->GetAmplitude(iP);
       int startBin = fhPulses[index]->FindBin(pulseStart);
       int endBin = fhPulses[index]->FindBin(pulseEnd);
        std::cout << "Setting pulse properties: "<< Form("height = %f, start = %f",pulseHeight,pulseStart)<<std::endl;
       //Loop over "pulse bins" and set them all to the pulse height
       for(int iB = startBin; iB < endBin;iB++){
         fhPulses[index]->SetBinContent(iB,-pulseHeight);
       }//End loop over pulse bins
     }//End loop over pulses

   }

   //Display corr waveforms
   void ShowPlots( double& pulse_max, int& runn)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif

      if( fFlags->fVerbose ) std::cout<<"GenericWaveformModule::ShowPlots "<<std::endl;
      for(int i=0;i<fNumChannels;i++){
        bool isPresent = fCCorr.count(i);
        if( !isPresent ){
              std::string cname = Form("Run #%i : Corr_Canvas_%i",runn,i);
              fCCorr[i] = new TCanvas(cname.c_str(),cname.c_str(),2000,1500);
              fCCorr.at(i)->ToggleEventStatus();
        }

        fCCorr.at(i)->cd();
        gPad->SetGrid();
        fhWFCorr.at(i)->Draw("hist");
        fhPulses.at(i)->Draw("histsame");

        fCCorr.at(i)->Modified();
        fCCorr.at(i)->Draw();
        fCCorr.at(i)->Update();

      }
   }

   //Display raw waveforms
   void ShowRaw(int &run_num)
   {
 #ifdef MODULE_MULTITHREAD
       std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
 #endif

       for(int i=0;i<fNumChannels;i++){
         bool isPresent = fCRaw.count(i);
         if( !isPresent ){
               std::string cname = Form("Run# %i: Raw_Canvas_%i",run_num,i);
               fCRaw[i] = new TCanvas(cname.c_str(),cname.c_str(),2000,1500);
               fCRaw[i]->ToggleEventStatus();
         }

         fCRaw[i]->cd();
         gPad->SetGrid();
         fhWFRaw[i]->Draw("hist");
         fCRaw[i]->Modified();
         fCRaw[i]->Draw();
         fCRaw[i]->Update();

         gPad->SetGrid();
         fhWFRaw.at(i)->Draw("hist");
      }
   }

   void PersistencyPlot( const std::vector<double>* corr_wf,int index)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      if(!(fFlags->fVerbose))
         {
            std::cout<<"GenericWaveformModule::PersistencyPlot "<<index<<" ";
         }
      int nsamples = (int) corr_wf->size();
      if( !fhPersistency ){
        std::string hname="hPersistencyCH"+std::to_string(index);
        std::string htitle="Persistency Plot for channel "+std::to_string(index)+";Time (ns);ADC";
        fhPersistency = new TH2D(hname.c_str(),htitle.c_str(),nsamples,fWFmin,fWFmax,nsamples,fPersistencyMin,fPersistencyMax);
        fhPersistency->SetStats(0);
    }
      //Fill the th2d persistency histogram
      for(int iS=0; iS< nsamples; ++iS){
            double wfVal = corr_wf->at(iS);
            double binNumber = fhPersistency->FindBin(wfVal,iS*fNanosecPerSample);
            double binContent = fhPersistency->GetBinContent(binNumber);
            fhPersistency->SetBinContent(binNumber,binContent+wfVal);
      }

      if( index == fPersistencyChannel ){
            gStyle->SetPalette(kViridis);
            fhPersistency->Draw("colz");
      }

      if(fCounter == (fPersistencyEvents-1) && fFlags->fVerbose) std::cout<<"GenericWaveformModule::PersistencyPlot for channel:"<<index<<" Last WF."<<std::endl;
   }

   //Reset Histograms
   void ResetHisto()
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      if(fFlags->fVerbose) std::cout<<"Resetting Event: "<<fCounter<<std::endl;
      for(auto it=fhWFRaw.begin();it!=fhWFRaw.end();++it){
        if(it->second) it->second->Reset();
      }

      for(auto it=fhWFCorr.begin();it!=fhWFCorr.end();++it){
        if(it->second) it->second->Reset();
      }
      for(auto it=fhPulses.begin();it!=fhPulses.end();++it){
        if(it->second) it->second->Reset();
      }

   }//End ResetHisto


};//End GenericWaveformModule


class GenericWaveformFactory: public TAFactory
{
public:
   GenericWaveformFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      printf("GenericWaveformFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
         fFlags.fVerbose = true;
   }

   void Finish()
   {
      printf("GenericWaveformFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("GenericWaveformFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new GenericWaveformModule(runinfo, &fFlags);
   }

};

static TARegister tar(new GenericWaveformFactory);
