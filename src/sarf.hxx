#ifndef __SARF__
#define __SARF__

#include <cmath>

class SARF
{
private:
   double fA;
   double ft0;
   double fk;
   double fsigma;
   double ftauS;
   double ftauL;
   
public:
   SARF(double amplitude, double time, double kappa, double s, double ts, double tl):
      fA(amplitude),ft0(time),fk(kappa),fsigma(s),ftauS(ts),ftauL(tl)
   {}

   double A() const { return fA; }
   double t0() const { return ft0; }
   double k() const { return fk; }
   double sigma() const { return fsigma; }
   double tau_short() const { return ftauS; }
   double tau_long() const { return ftauL; }

   double operator()(double t) const
   {
      double sq2i=1./sqrt(2.);
      double short_=exp( (0.5*fsigma*fsigma/ftauS/ftauS) - ((t-ft0)/ftauS)) *
         erfc( fsigma*sq2i/ftauS - ((t-ft0)*sq2i/fsigma) );
      double long_=exp( (0.5*fsigma*fsigma/ftauL/ftauL) - ((t-ft0)/ftauL)) *
         erfc( fsigma*sq2i/ftauL - ((t-ft0)*sq2i/fsigma) );
      return fA*( ((1-fk)*0.5/ftauS*short_) + (fk*0.5/ftauL*long_) );
   }
};
#endif


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
