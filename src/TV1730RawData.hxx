#ifndef TV1730RawData_hxx_seen
#define TV1730RawData_hxx_seen

#include <vector>

#include "TGenericData.hxx"
#include "TRawChannel.hxx"

/// Class for each channel measurement
/// For the definition of obscure variables see the CAEN V1730 manual (for raw (non-DPP) readout).
class TV1730RawChannel: public TRawChannel 
{
public:
   /// Constructor; need to pass in header and measurement.
   TV1730RawChannel(int chan, int nsamp):TRawChannel(chan,nsamp)
   {   }
   /// Constructor
   TV1730RawChannel(int chan):TRawChannel(chan,0)
   {   }
};


/// Class to store raw data from CAEN V1730 (for raw readout, no-DPP).
class TV1730RawData: public TGenericData {

public:

   /// Constructor
   TV1730RawData(int bklen, int bktype, const char* name, void *pdata);


   /// Get Event Counter
   uint32_t GetEventCounter() const {return (fGlobalHeader[2] & 0xffffff);};

   /// Get Event Counter
   uint32_t GetEventSize() const {return (fGlobalHeader[0] & 0xfffffff);};

   /// Get Geographical Address
   uint32_t GetGeoAddress() const {return (fGlobalHeader[1] & 0xf8000000) >> 27 ;};

   /// Get the extended trigger time tag
   uint32_t GetTriggerTimeTag() const {return fGlobalHeader[3];};

   /// Get channel mask
   uint32_t GetChMask(){return (fGlobalHeader[1] & 0xff) + ((fGlobalHeader[2] & 0xff000000) >> 16);};

   void Print();

   /// Get the Vector of TDC Measurements.
   std::vector<TV1730RawChannel>& GetMeasurements() {return fMeasurements;}

private:
  
   // We have vectors of the headers/trailers/etc, since there can be 
   // multiple events in a bank.

   /// The overall global header
   std::vector<uint32_t> fGlobalHeader;  

   /// Vector of V1730 Measurements.
   std::vector<TV1730RawChannel> fMeasurements;

};

#endif


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
