#include "TWDRawData.hxx"



void TWDRawData::Print(){

  std::cout << "WD decoder for bank " << GetName().c_str() << std::endl;
  // std::cout << "Bank size: " << GetEventSize() << std::endl;
  // std::cout << "Channel Mask : " << GetGroupMask() << std::endl;
  // std::cout << "Event counter : " << GetEventCounter() << std::endl;
  // std::cout << "Trigger tag: " << GetTriggerTag() << std::endl;

  std::cout << "Number of channels with data: " << GetNChannels() << std::endl;
  for(int i = 0 ; i < GetNChannels(); i++){
    TWDRawChannel channelData = GetChannelData(i);
  }

}

//Method for decoding uncompressed data
void TWDRawData::HandleUncompressedData(){

  int nitems = GetSize();//Event size in bytes
  fEvSize = nitems/4;
  if(fVerbose > 0) {
    std::cout << "DRSV size : " << nitems << std::endl;
    if (nitems < 0) { printf("WDAQ decode; warning empty bank\n"); return;}
  }

  //if we got a bank with only a header
  if (nitems == 5)
  {
    std::cout << "Error: Event# "<< event_counter <<", Header only bank"<<std::endl;
    return;
  }

  //Number of 16 bit words in this event
  int nwords = int(nitems/2);
  if(fVerbose > 0) std::cout << "DRSV nwords(16 bit): "<< nwords <<std::endl;

  //DRSV header struct
  unsigned short flags;       // 0-2: header type, 3-4: drs version, 5-9: encoding mode, 10-15: spare
  unsigned short address;     // DRS4  : 0-3: drs chn,  4: drs chip, 5-9: slot, 10-15: crate
  unsigned short data_size;   // size of waveform in bytes
  unsigned short first_bin;   // first bin which gets encoded, can be used to define ROI
  short          temperature; // temperature of the WD board in a unit of 0.01 degC

  //Parsed information from bit encoding in header
  int enc_mode, htype, drs_vers, channel, chip, slot, crate;

  // Structure of DRSV Bank
  // Loops through 8 channels per chip,
  // BANK[] = {HEADER[0],...,HEADER[N],WFDATA[0],WFDATA[1],...,WFDATA[N]...,HEADER[0],...,WFDATA[0],...,WFDATA[N],HEADER[].. }
  // HEADER is 10 bytes (5 words)

  //To hold sample wfs
  std::vector<std::vector<double> > samples;
  int nChannels = 0;

  //Loop over words
  for(int iWord = 0 ; iWord < nwords; /* Iterate manually inside loop */ )
  {
    //Decode header first
    flags = GetData16()[iWord++];//Read word then iterate to next word
    address = GetData16()[iWord++];
    data_size = GetData16()[iWord++];
    first_bin = GetData16()[iWord++];
    temperature = GetData16()[iWord++];

    //Parse bit encoding, from drs_encode.h library
    htype = flags & 0x0007;
    drs_vers = flags & 0x0018;
    enc_mode = flags & 0x03E0;
    channel = (address & 0x000F) >>  0;
    chip = (address & 0x0010) >>  4;
    slot = (address & 0x01E0) >>  5;
    crate = (address & 0xFE00) >>  9;

    int wf_size = int(data_size / 2); //Number of 16 bit words

    if(fVerbose > 1){
      std::cout << "====== WDAQ DRSV Header Summary ====== "<<std::endl;
      std::cout << "enc_mode: "<< enc_mode << ", header_type: "<< htype << std::endl;
      std::cout << "drs_version: "<< drs_vers << ", data_size: " << data_size
      << " , wf_size: " <<wf_size<<std::endl;
      std::cout << "first_bin: "<< first_bin << ", chip: " << chip << std::endl;
      std::cout << "slot: "<< slot << ", channel: "<< channel << std::endl;
      std::cout << "crate: "<< crate <<", temperature: " << temperature<< std::endl;
    }

    if( wf_size != 1024 ){
        std::cout << "Error: Unexpected waveform size of " << wf_size << " skipping channel"<<std::endl;
        iWord+=wf_size;
        continue;
    }

    //Don't record WD126, nothing plugged in
    if(slot == 3){
      if(fVerbose > 1) std::cout << "Slot == 3, Skipping WD126 channels"<<std::endl;
      iWord+=wf_size;
      continue;
    }

    samples.push_back(std::vector<double>());

    //Read sample for this waveform
    for(int nsample = 0; nsample < wf_size; nsample++ )
    {
      short sVolt = (GetData16()[iWord]);
      double dVolt = double(sVolt)*(1.0/DRS_CF_V_MODE00);
      samples[nChannels].push_back(dVolt);//Add voltage
      iWord++;
    }//End loop over wf samples

    //Get ID as diplayed on the front-panel of the WDB
    int chan_id = chip*8 + channel;
    //Put timing channels at 100 & 101 for slot 1 and 200 & 201 for slot 2
    channel == 8 && slot == 0? chan_id = 100 + chip : chan_id = chan_id;
    channel == 8 && slot == 1? chan_id = 200 + chip : chan_id = chan_id;
    slot == 0 ? chan_id = chan_id : chan_id += 16; //Increment for slot 1, to become channels 16-31

    //Create raw channels and add waveforms to them
    TWDRawChannel wd_channel(chan_id);
    wd_channel.AddSamples(samples[nChannels]);
    nChannels++;
    if(chan_id < 32) fMeasurements.push_back(wd_channel);//Skip timing channels for now

  }//End loop over words


}//End handle uncompressed data


//Decode DRST Bank
// BANK[] = {HEADER[0],...,HEADER[N],DTDATA[0],..DTDATA[N],HEADER[0]}// Only headers after event 0
// HEADER is 12 bytes (3 words)
void TWDRawData::AddDRSTBank(std::map<int, std::vector<double>> &fDeltaTimes,int bklen, int bktype, const char* /*name*/, void *pdata)
{

  //Cast data to 16 bit words, copied from TGenericData
  const uint16_t* wdata = reinterpret_cast<const uint16_t*>(pdata);
  //Cast to float, copied from TGenericData
  const float* fldata = reinterpret_cast<const float*>(pdata);
  int offset = 3; //Header offset is 6 shorts (12 bytes) so 3 floats

  int nitems = bklen;//Event size in bytes
  if(fVerbose>0) {
    std::cout << "DRST size : " << nitems <<" type: "<< bktype<< std::endl;
    if (nitems < 0) { printf("WDAQ decode; warning empty bank\n"); return;}
  }

  //if we got a bank with only a header
  if (nitems == 6)
  {
    std::cout << "Error: Event# "<< event_counter <<", Header only bank"<<std::endl;
    return;
  }

  if(fDeltaTimes.empty() && event_counter != 1){
    std::cout <<"Error: Non-first Event# "<<event_counter <<", and deltat vector empty"<<std::endl;
    //return;
  }

  //Number of 16 bit words in this event
  int nwords = int(nitems/2);

  //DRST Header
  unsigned short flags;       // 0-2: header type, 3-4: drs version, 5-9: encoding mode, 10-15: spare
  unsigned short address;     // DRS4   : 0-3: drs chn,   4: drs chip, 5-9: slot, 10-15: crate
  unsigned short data_size;   // size of waveform in bytes
  unsigned short first_bin;   // first bin which gets encoded, can be used to define ROI
  unsigned short trigger_cell;// bin in which trigger occured
  unsigned short dt;          // nominal bin width in ps

  //Parsed information from bit encoding in header
  int enc_mode, htype, drs_vers, channel, chip, slot, crate;

  std::map<int,int>trigger_bins;//Map of channel id to trigger bin

  for(int iWord = 0; iWord < nwords;){

    flags = wdata[iWord++];//Read word then iterate to next word
    address = wdata[iWord++];
    data_size = wdata[iWord++];
    first_bin = wdata[iWord++];
    trigger_cell = wdata[iWord++];
    dt = wdata[iWord++];

    //Parse bit encoding, from drs_encode.h library
    htype = flags & 0x0007;
    drs_vers = flags & 0x0018;
    enc_mode = flags & 0x03E0;
    channel = (address & 0x000F) >>  0;
    chip = (address & 0x0010) >>  4;
    slot = (address & 0x01E0) >>  5;
    crate = (address & 0xFE00) >>  9;

    int dt_size = data_size / 4; //Number of floats per channel

    //Print header info for each channel
    if(fVerbose > 0){
      std::cout << "====== WDAQ DRST Header Summary ====== "<<std::endl;
      std::cout << "enc_mode: "<< enc_mode << ", header_type: "<< htype << std::endl;
      std::cout << "drs_version: "<< drs_vers << ", data_size: " << data_size << std::endl;
      std::cout << "first_bin: "<< first_bin << ", chip: " << chip << std::endl;
      std::cout << "slot: "<< slot << ", channel: "<< channel << ", crate: "<< crate <<std::endl;
      std::cout << "dt: "<< dt << ", trigger_cell: " << trigger_cell<< std::endl;
    }

    //Don't record WD126, nothing plugged in
    if(slot == 3){
      if(fVerbose > 0) std::cout << "Slot == 3, Skipping WD126 channels"<<std::endl;
      iWord += int(data_size/2);
      offset = offset+3+dt_size;
      continue;
    }

    //Get ID as diplayed on the front-panel of the WDB
    int chan_id = GetChannelIndex(chip,slot,channel);

    //Insert trigger cell into map for later
    trigger_bins.insert(std::pair<int,int>(chan_id,trigger_cell));

    //If we have the first event, parse for the delta-t array
    if(event_counter == 1){
      //Create vector pair
      fDeltaTimes.insert(std::pair<int,std::vector<double>>(chan_id, std::vector<double>()));
      //Get v_deltat for each channel and save in map
      for(int iSample = offset;iSample < dt_size+offset; iSample++){
        float fTime = fldata[iSample];
        fDeltaTimes[chan_id].push_back(fTime);//Insert deltat into vector for channel
        iWord+=2;//Iterate for headers
      }//End loop over samples
    }//End first event condition
    //Increment offset in float data with another header and the size of the waveform
    offset = offset+3+dt_size;
  }//End loop over nwords


  //Add time bins to TWDRawChannels
  for(unsigned int iChan = 0; iChan < fMeasurements.size(); iChan++){
    int chan_index = fMeasurements[iChan].GetChannelNumber();
    //std::cout << "Adding timing array for chan_index: "<< chan_index << std::endl;
    std::vector<double> vDt = fDeltaTimes[chan_index]; //Delta time array
    std::vector<double> vTimes(vDt.size(),0); //Initialize time array to 0
    //Start from 1 since bin zero == 0 seconds
    for(unsigned int iBin = 1; iBin < vDt.size(); iBin++){
      //Which bins to start reading in the loop of 1024 bins
      int tBin = (iBin+trigger_bins[chan_index]) % 1024;
      double tLast = vTimes[iBin-1]; //Time from last bin
      // If tBin == 0 then the bin we want to read is 1024
      tBin == 0 ? tBin = 1024 : tBin = tBin;
      tLast += vDt[tBin-1]; //Add delta time between tBin N and tBin N+1
      vTimes[iBin] = tLast;
    }
    fMeasurements[iChan].SetADCTimes(vTimes);
  }

}//End AddDRSTBank

//Method to get unique DAQ ID, taking in header data from WDB
int TWDRawData::GetChannelIndex(int chip, int slot, int channel){

  int chan_id = chip*8 + channel;
  //Put timing channels at 100 & 101 for slot 1 and 200 & 201 for slot 2
  channel == 8 && slot == 0? chan_id = 100 + chip : chan_id = chan_id;
  channel == 8 && slot == 1? chan_id = 200 + chip : chan_id = chan_id;
  slot == 0 ? chan_id = chan_id : chan_id += 16; //Increment for slot 1, to become channels 16-31
  return chan_id;
}

TWDRawData::TWDRawData(int event_num, int bklen, int bktype, const char* name, void *pdata) :
    TGenericData(bklen, bktype, name, pdata)
{

  fVerbose = 0;
  event_counter = event_num;
  if(fVerbose > 0) std::cout << "Event num = "<<event_counter <<std::endl;


  HandleUncompressedData();
}


TWDRawData::~TWDRawData(){

}
