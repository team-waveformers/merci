/******************************************************************
 * Baseline subtracted flow object(s) for analysis *
 *
 * D. Gallacher
 * October 2021
 *
******************************************************************/

#ifndef __BASELINEFLOW__
#define __BASELINEFLOW__

#include "manalyzer.h"

#include <map>
#include <vector>
#include <string>
#include <stdexcept>
#include <iostream>


class BaselineEventFlow: public TAFlowEvent
{
public:

  std::map<int, unsigned int> fChannelMap; // Map of Channels, Vector Index->DAQ Channel
  std::vector<std::string> fTypes; //Channel type list

private:

  /// Number of channels
  int fNchannels;

  /// lists of baselines for each channel
  std::vector<double> fBaselines;
  std::vector<double> fBaselines_rms;
  /// List of baseline modes, tracking what algorithm was used
  std::vector<std::string> fBaselineModes;
  
  std::vector<double> fROIq;
  std::vector<double> fROIh;
  std::vector<double> fROIt;

  /// List of baseline corrected waveforms 1 for each channel
  std::vector<std::vector<double>> fChannelSamples;
  std::vector<std::vector<double>> fChannelTimes;

  ///
  std::vector<int> fModules;
  std::vector<int> fChannels;

  const int fEventNumber;

public:
  BaselineEventFlow(TAFlowEvent* flow):TAFlowEvent(flow),fNchannels(0),fEventNumber(0)
  {}

  BaselineEventFlow(TAFlowEvent* flow, int ev):TAFlowEvent(flow),fNchannels(0),fEventNumber(ev)
  {}

  ~BaselineEventFlow()
   {
     fChannelSamples.clear();
     fChannelTimes.clear();
     fBaselines.clear();
     fBaselines_rms.clear();
     fROIq.clear() ; 
     fROIh.clear() ; 
     fROIt.clear() ; 
     fTypes.clear();
     fBaselineModes.clear();
   }

   /// Add the baseline corrected wf to this flow event, including the daq channel from TRawChannel->GetChannelNumber()
  void add(std::vector<double> wf, int daq_chan, int mod)
  {
     fChannelSamples.push_back(wf);
     // Keep track of the index of this wF
     fChannels.push_back(daq_chan);
     fModules.push_back(mod);
     ++fNchannels;
  }

  /// Add the baseline corrected wf to this flow event, also add the type
  void add(std::vector<double> wf, int daq_chan, int module, const char* type)
  {
     fChannelSamples.push_back(wf);
     fTypes.emplace_back(type);
     fChannelMap.insert({fNchannels,daq_chan});
     fTypes.emplace_back(type);
     fChannels.push_back(daq_chan);
     fModules.push_back(module);
     ++fNchannels;
  }

  /// Add the baseline corrected wf to this flow event, including the daq channel from TRawChannel->GetChannelNumber()
 void addWF(std::vector<double> wf, std::vector<double> time, int daq_chan, const char* type)
 {
    fChannelSamples.push_back(wf);
    fChannelTimes.push_back(time);
    // Keep track of the index of this wF
    fChannelMap.insert({fNchannels,daq_chan});
    fTypes.emplace_back(type);
    ++fNchannels;
 }

  /// Add channel baselines and baseline rms's
  void addBaselines(double baseline, double baseline_rms,std::string baseline_mode)
  {
    fBaselines.push_back(baseline);
    fBaselines_rms.push_back(baseline_rms);
    fBaselineModes.push_back(baseline_mode);
  }

  /// Add channel ROIs
  void addROIs( double fROIqval, double fROIhval, double fROItval=0. )
  {
    fROIq.push_back(fROIqval);
    fROIh.push_back(fROIhval);
    fROIt.push_back(fROItval);
  }

  /// Get baseline RMS
  double getBaselineRMS(int chan){
    if(chan>fNchannels){
      printf("Error: BaselineRMS channel requested greater than NChannels \n");
      return -1.0;
    }
    return fBaselines_rms[chan];
  }
  double getBaseline(int chan){
    if(chan>fNchannels){
      printf("Error: Baseline channel requested greater than NChannels \n");
      return -1.0;
    }
    return fBaselines[chan];
  }

  //PA 
  double getROIq(int chan){
    if( !(fROIq.size() > 0) ) return -1.;
    if(chan>fNchannels){
      printf("Error: ROI channel requested greater than NChannels");
      return -1.0;
    }
    return fROIq[chan];
  }

  double getROIh(int chan){
    if( !(fROIh.size() > 0) ) return -1.;
    if(chan>fNchannels){
      printf("Error: ROI channel requested greater than NChannels");
      return -1.0;
    }
    return fROIh[chan];
  }

  // AC
  double getROIt(int chan){
    if( !(fROIt.size() > 0) ) return -1.;
    if(chan>fNchannels){
      printf("Error: ROI channel requested greater than NChannels");
      return -1.0;
    }
    return fROIt[chan];
  }

  std::string getBaselineMode(int chan){
    if(chan>fNchannels){
      printf("Error: Baseline Mode channel requested greater than NChannels \n");
      return "NDEF";
    }
    return fBaselineModes[chan];
  }


  ///Get a pointer to the data, but can't modify it
  const std::vector<double>* getChannelVector(int ch) const
  {
    if( ch >= fNchannels )
      throw std::out_of_range("out of range!");

    if( fChannelSamples.empty() )
      throw std::out_of_range("empty!");

    return &fChannelSamples.at(ch);

  }

  ///Get a pointer to the data, but can't modify it
  const std::vector<double>* getChannelTimes(int ch) const
  {
    if( ch >= fNchannels )
      throw std::out_of_range("out of range!");

    if( fChannelTimes.empty() )
      throw std::out_of_range("empty!");

    return &fChannelTimes.at(ch);
  }

  inline const double* getChannelWaveform(int ch) const
  {
    if( ch >= fNchannels )
      throw std::out_of_range("out of range!");

    if( fChannelSamples.empty() )
      throw std::out_of_range("empty!");
    return fChannelSamples.at(ch).data();

  }

  inline int getNchannels() const
  {
    return fNchannels;
  }

  inline int getNsamples(int ch) const
  {
    return (int) fChannelSamples.at(ch).size();
  }

  inline double getSample(int ch, int s) const
  {
    if(s > getNsamples(ch) ){
      throw std::out_of_range("out of range");
    }

    return fChannelSamples.at(ch)[s];
  }

  inline int getADCchannel(int ch) const
  {
    return fChannels.at(ch);
  }

  inline int getADCmodule(int ch) const
  {
    return fModules.at(ch);
  }

  inline int getEventNumber() const
  {
    return fEventNumber;
  }

};

#endif
