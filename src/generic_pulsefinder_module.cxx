/******************************************************************
 *  Simple Pulse Finder *
 *
 * D. Gallacher
 * Feb 2022
 * Finds pulses with an absolute threhsold, looks for sub-peaks by finding zero-crossings in derivative space
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "baselineflow.hxx"
#include "adcflow.hxx"
#include "pulseflow.hxx"
#include "utils.hxx"

#include "TMath.h"
#include "TH1D.h"
#include "TH1.h"

#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <cassert>
#include <cstdlib>

//Read preprocessor flag value as string literal
#define STRING(s) #s
#define STRSTRING(s) STRING(s)

#include "json.hpp"
using json = nlohmann::json;


class GenericPulseFinderFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};


class GenericPulseFinderModule: public TARunObject
{
public:
   GenericPulseFinderFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors

private:
   int fNtotPulses;

   Utils *util;

   //Pulse finder variable
   double fAmplitude; //Skip pulses with less than this height
   double fMaxAmplitude; //Skip waveforms with more than this amplitude(Noisy)
   double fWidth; //Skip pulses that are shorter than this value
   double fNsigma;  //Threshold for pulse finding in multiples of
   double fEndThresh; //Threshold of baseline RMS's to be at "end of pulse"
   int fPedestal;  //Number of samples for derivative baseline rms
   double fThreshold;//Threshold in ADC for pulsefinding
   int fRebinning; //Rebinning factor for the derviative used in pulse finding
   int fDerWindow;// +/- derWindow  = size of window to calculate finite difference derivative
   int fPeakScanWidth;// How far ahead to look for the peak from leading edge (in bins)
   int fNmins;// Number of minimums to scan for
   int fResetWindow;// Number of bins to reset when scanning for minimums
   int fNanosecsPerSample;//nano secs per sample
   int fPolarity; //Are the pulses +ve or -ve? (+/- 1)


   std::string fBoardType;//Type of board,read from config

public:
   GenericPulseFinderModule(TARunInfo* runinfo, GenericPulseFinderFlags* f): TARunObject(runinfo),
                                                               fFlags(f), fCounter(0), fError(0),
                                                               fNtotPulses(0)
   {
      if(fFlags->fVerbose) std::cout<<"GenericPulseFinderModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="PulseFinder";
#endif
      std::string config_path;
#ifdef CONFIG_PATH
      config_path = STRSTRING(CONFIG_PATH) + fFlags->fConfigName;//Read config file path from CMAKE source dir (lolx)
#else
      config_path = fFlags->fConfigName;//If path isn't defined look in flags only
#endif
      std::cout << "Reading config file from path = "<< config_path << "\n";
      std::ifstream fin(config_path);
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"GenericPulseFinderModule Json parsing success!"<<std::endl;
      fin.close();


      fAmplitude = settings["Pulse"]["Amplitude"].get<double>();
      fMaxAmplitude = settings["Pulse"]["MaxAmplitude"].get<double>();
      fNsigma = settings["Pulse"]["Sigma"].get<double>();
      fPedestal = settings["Pulse"]["Pedestal"].get<int>();
      fEndThresh = settings["Pulse"]["EndThresh"].get<double>();
      fWidth = settings["Pulse"]["Width"].get<double>();
      fNmins = settings["Pulse"]["NumMins"].get<int>();
      fDerWindow = settings["Pulse"]["DerivativeWindow"].get<int>();
      fPeakScanWidth = settings["Pulse"]["PeakScan"].get<int>();
      fResetWindow = settings["Pulse"]["ResetWindow"].get<int>();
      fThreshold = settings["Pulse"]["Threshold"].get<double>();

      fBoardType = settings["Pulse"]["BoardType"].get<std::string>();
      fPolarity = settings["Pulse"]["Polarity"].get<int>();

      util = new Utils();
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"GenericPulseFinderModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"GenericPulseFinderModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"GenericPulseFinderModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<" Total number of Pulses: "<<fNtotPulses<<std::endl;
   }

   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {

      BaselineEventFlow* corr_flow = flow->Find<BaselineEventFlow>();
      if( !corr_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      AdcEventFlow* adc_flow = flow->Find<AdcEventFlow>();
      if( !adc_flow )
          {
            ++fError;
       #ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
       #endif
            return flow;
          }

      int nChan = corr_flow->getNchannels();
      if( fFlags->fVerbose  )
         std::cout<<"GenericPulseFinderModule::AnalyzeFlowEvent # of ch: "<<nChan<<std::endl;

      PulseFlow* pulse_flow  = new PulseFlow(flow,fBoardType,nChan);
      int nSamples = corr_flow->getNsamples(0);
      //pulse_flow->conf->

      fNanosecsPerSample = pulse_flow->conf->nanoSecsPerSample;

      for(int iChan=0; iChan < nChan; iChan++){
        TH1D hCorr;
        const std::vector<double> *wf = corr_flow->getChannelVector(iChan);
        double rms = corr_flow->getBaselineRMS(iChan);
        if(nSamples == 0 ){
          if(fFlags->fVerbose) std::cout << "No samples in WF# " << iChan << std::endl;
          continue;
        }
        util->ConvertRawWF(hCorr,wf,iChan,double(fNanosecsPerSample));
        if(fPolarity == 1) hCorr.Add(&hCorr,-2);//Flip because pulse finder assumes -ve
        if(hCorr.GetMaximum() > fMaxAmplitude) continue;//Skip noisy pulses
        PulseFinder(pulse_flow,hCorr,iChan);
      }

      if(fFlags->fVerbose) std::cout << "Leaving Generic PulseFinder Module AnalyzeFlowEvent" << std::endl;
      flow = pulse_flow;
      ++fCounter;
      return flow;
   }

   //David's Generic Pulsefinder, not fast or great
    void PulseFinder(PulseFlow *lFlow,TH1D &hCorr,int chan_num){
      int offset = 5;
      double baselineRMS = util->CalculateBaselineRMS(hCorr,offset,offset+fPedestal);
      double aBaseline = util->CalculateBaseline(hCorr,offset,offset+fPedestal);
      const int width = int(300.0/hCorr.GetBinWidth(1));
      const int nBins = hCorr.GetNbinsX();
      //FindMinimum parameters
      //pars[derivativeWindow,resetWindow,peakScanWindow,num_mins,baselineWindow,nSigmas]
      std::vector<double> findMinPars = {double(fDerWindow),double(fResetWindow),double(fPeakScanWidth),double(fNmins),double(fPedestal),fNsigma,aBaseline};
      // list of peak-times of subpeaks and voltages
      // list of bin numbers for t0's of pulse-candidates
      std::vector<double> pulseEdge;
      // List of bin numbers corresponding to the end of the pulse
      std::vector<double> pulseEnd;
      //Container for subpeak times and voltages <bin number,voltage>
      std::vector<std::vector<std::pair<int,double>>> subpeaks;
      //List of baselines before pulses
      std::vector<double> pulsePreBaselines;

      //Loop over the derivative to find candidate pulses
      for(int iBin = offset ; iBin < nBins ;iBin++){
        // Did we find the end of the pulse? If not pulse end = peakTime+300ns
        bool foundEnd  = false;
        //Scan over all bins in the derivative trace and determine which are below threshold
        if(hCorr.GetBinContent(iBin) < fThreshold && hCorr.GetBinContent(iBin+1) < fThreshold){
          double pTime = hCorr.GetBinCenter(iBin);
          pulseEdge.push_back(pTime);

          //Find baseline
          double prePulseBaseline = util->CalculateBaseline(hCorr,iBin-offset-fPedestal,iBin-offset);
          findMinPars[6] = prePulseBaseline;
          pulsePreBaselines.push_back(prePulseBaseline);
          //Arbitrarily set for now, this will be reset to iBin+300ns if we fail the endBin scan
          int endBin = iBin+offset;
          //Loop over bins in the trace to find time corresponding to the pulse-end
          //If the bin is close to the
          for(int iPT = iBin+2; iPT < nBins; iPT++){
            //Are we close to the baseline?
            if((TMath::Abs(hCorr.GetBinContent(iPT)) - baselineRMS*fEndThresh) < 0.0 ){
              pTime = hCorr.GetBinCenter(iPT);
              pulseEnd.push_back(pTime);
              endBin = iPT;
              foundEnd = true;
              goto next;
            }//End end of pulse check
          }//End loop for end of pulse
          next:

          //Edge-case handling
          if(endBin >= nBins) endBin = nBins;
          //FindMinimums returns a list of std::pairs<int,double> with the bin number of the peak and
          // the value at the peak - value at the leading edge of the pulse
          subpeaks.push_back(util->FindMinimums(hCorr,iBin-fPedestal,endBin,findMinPars));
          if(foundEnd){
            //Start looking after the end of the first pulse
            iBin = endBin + 1;
          }else {
            pulseEnd.push_back(hCorr.GetBinContent(iBin)+300);
            iBin+=width-1; // Jump ahead so we don't double count
          }
        }//End conditional for threshold
      }//End loop over all bins

      if(fFlags->fVerbose) std::cout << "Found " << pulseEdge.size()<<" pulses.."<<std::endl;

      //Loop over pulses found and add properties to DS
      for(unsigned int iPulse = 0; iPulse < pulseEdge.size(); iPulse++){
        double tStart = pulseEdge[iPulse];
        double tEnd = pulseEnd[iPulse];
        if(subpeaks[iPulse].empty()){
          if(fFlags->fVerbose) std::cout << "No peaks found above threshold"<<std::endl;
          continue;
        }
        double tPeak = hCorr.GetBinCenter(subpeaks[iPulse][0].first); //First entry is bin number of minimum
        double vPeak = fabs(subpeaks[iPulse][0].second); //Second is value at minimum - leading edge
        double deltaT = tEnd - tStart;
        if(deltaT < 0) continue; //Skip non-physical pulses
        int startIntegral = hCorr.FindBin(pulseEdge[iPulse]);
        int endIntegral = hCorr.FindBin(pulseEnd[iPulse]);
        if(endIntegral < startIntegral) endIntegral = hCorr.GetNbinsX();
        double totalQ = fabs(hCorr.Integral(startIntegral,endIntegral)); //This is not actually Q yet, needs ADC rescaling

        if(fabs(vPeak) < fAmplitude) continue; //Skip non-physical pulses
        if(deltaT < fWidth) continue; //Skip non-physical pulses
        if(fFlags->fVerbose) std::cout << "After cut Pulse properties: "<< Form("Channel #%i: tStart = %f, tEnd=%f, tPeak=%f, vPeak=%f, totalQ =%f",chan_num,tStart,tEnd,tPeak,vPeak,totalQ)<<std::endl;

        //Only add main peaks for now
        lFlow->results.at(chan_num)->pulseResults.fPulseStart.push_back(tStart);
        lFlow->results.at(chan_num)->pulseResults.fPulseEnd.push_back(tEnd);
        lFlow->results.at(chan_num)->pulseResults.fPulseWidth.push_back(deltaT);
        lFlow->results.at(chan_num)->pulseResults.fPeakTime.push_back(tPeak);
        lFlow->results.at(chan_num)->pulseResults.fAmplitude.push_back(vPeak);
        lFlow->results.at(chan_num)->pulseResults.fCharge.push_back(totalQ);
        fNtotPulses++;
      }//End loop over pulses
    }//End pulsefinder
};//End GenericPulseFinderModule


class GenericPulseFinderModuleFactory: public TAFactory
{
public:
   GenericPulseFinderFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      printf("GenericPulseFinderModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
         //fFlags.fVerbose = true;
   }

   void Finish()
   {
      printf("GenericPulseFinderModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("GenericPulseFinderModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new GenericPulseFinderModule(runinfo, &fFlags);
   }

};

static TARegister tar(new GenericPulseFinderModuleFactory);
