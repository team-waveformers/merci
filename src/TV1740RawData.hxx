#ifndef TV1740Data_hxx_seen
#define TV1740Data_hxx_seen

#include <vector>

#include "TGenericData.hxx"
#include "TRawChannel.hxx"

/// This class stores the information for
/// a single V1740 channel, in particular the waveform.

//Inherit from TRawChannel, add additional V1740 depedent things here
class TV1740RawChannel: public TRawChannel{

public:

  TV1740RawChannel(int channel, int baseline=-1):
    TRawChannel(channel,0),
    fVerbose(0),
    fBaseline(baseline)
  { }

  int GetBaseline()const {return fBaseline;};

  /// Add all samples to wavefrom
  void AddSamples(std::vector<double> samples){ fWaveform = samples; }

 private:
  //Baseline for this channel
  int fVerbose;
  int fBaseline;

};

/// Class to store data from CAEN V1740.
///
/// Class stores information for data from a single board (ie single MIDAS bank).
/// Data is stored as a vector of TV1740RawChannel's
///
/// Implementation detail:
///  For the short term this class is just a thin wrapper around Alex's
/// TV1740Handler; we need to figure out how to resolve this in the longer
/// term.
class TV1740RawData: public TGenericData {

public:

  /// Constructor
  TV1740RawData(int bklen, int bktype, const char* name, void *pdata);

  ~TV1740RawData();

  /// Get the number of 32-bit words in bank.
  uint32_t GetEventSize() const {return (fGlobalHeader0 & 0x0fffffff);};
  /// Get the channel mask; ie, the set of channels for which we
  /// have data for this event.
  uint32_t GetGroupMask() const {return (fGlobalHeader1 & 0x000000ff);};
   /// Get event counter
  uint32_t GetEventCounter() const {return ((fGlobalHeader2) & 0x00ffffff);};
  /// Get trigger tag
  uint32_t GetTriggerTag() const {return ((fGlobalHeader3) & 0xffffffff);};
  /// Get Number of channels in this bank.
  int GetNChannels() const {return fMeasurements.size();}

  /// Get Channel Data
  TV1740RawChannel GetChannelData(int i) {
    if(i >= 0 && i < (int)fMeasurements.size())
      return fMeasurements[i];

    return TV1740RawChannel(i);
  }


  void Print();


private:

  /// Helper method to handle uncompressed data.
  void HandleUncompressedData();

  /// The overall global headers
  uint32_t fGlobalHeader0;
  uint32_t fGlobalHeader1;
  uint32_t fGlobalHeader2;
  uint32_t fGlobalHeader3;

    /// Vector of V1740 measurements
  std::vector<TV1740RawChannel> fMeasurements;

  int fVerbose;
};

#endif
