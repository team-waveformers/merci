/******************************************************************
 *  Hit finder *
 * 
 * A. Capra
 * August 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "baselineflow.hxx"
#include "dsflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <cassert>
#include <iomanip>

#include "hitfinder.hxx"

#include "json.hpp"
using json = nlohmann::json;


class PulseFinderFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};


class PulseFinderModule: public TARunObject
{
public:
   PulseFinderFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors

private:
   int fNtotPulses;

   bool fFixed;
   double fAmplitude;
   double fNsigma;
   int fShortPulse;
   int fStartIntegration;
   int fTotalIntegration;
   std::string fTimingMode; //TBD

   int fTriggerChannel;

public:
   PulseFinderModule(TARunInfo* runinfo, PulseFinderFlags* f): TARunObject(runinfo),
                                                               fFlags(f), fCounter(0), fError(0),
                                                               fNtotPulses(0), 
                                                               fAmplitude(1.), fNsigma(1.5)
   {
      if(fFlags->fVerbose) std::cout<<"PulseFinderModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="PulseFinder";
#endif

      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"PulseFinderModule Json parsing success!"<<std::endl;
      fin.close();

      fFixed=settings["Pulse"]["Fixed"].get<bool>();
      if( fFixed )
         {
            fAmplitude=settings["Pulse"]["Amplitude"].get<double>();
            std::cout<<"PulseFinderModule ctor: Using Fixed Threshold "<<fAmplitude<<std::endl;
         }
      else
         {
            fNsigma=settings["Pulse"]["Sigma"].get<double>();
            std::cout<<"PulseFinderModule ctor: Using Threshold as "<<fNsigma<<" sigma above baseline"<<std::endl;
         }
      fShortPulse=settings["Pulse"]["Duration"].get<int>(); // Time Over Threshold
      std::cout<<"PulseFinderModule ctor: Time over Threshold: "<<fShortPulse<<" samples"<<std::endl;

      // limit for charge integration
      fStartIntegration=settings["Pulse"]["Charge"]["before"].get<int>();
      fTotalIntegration=settings["Pulse"]["Charge"]["total"].get<int>();
      
      fTimingMode="thr";
      try
         {
            if( settings["Filter"]["Name"].get<std::string>() == "ARMA" ||
                settings["Filter"]["Name"].get<std::string>() == "AR" || 
                settings["Filter"]["Name"].get<std::string>() == "Cross-Correlation")
               fTimingMode="avg";
         }
      catch(...) 
         {
            std::cout<<"PulseFinderModule:: Filtering algorithm is not set"<<std::endl;
         }
      std::cout<<"PulseFinderModule:: timing mode "<<fTimingMode<<std::endl;

      try
         {
            fTriggerChannel=settings["Laser"]["Trigger Channel"].get<int>();
         }
      catch(...) 
         {
            fTriggerChannel=-1;
         }
      std::cout<<"PulseFinderModule:: Laser trigger channel (index) is "<<fTriggerChannel<<std::endl;
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"PulseFinderModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"PulseFinderModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"PulseFinderModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<" Total number of Pulses: "<<fNtotPulses<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      int number_of_channels=0;
      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
     if( wf_flow ) 
         {
            number_of_channels = wf_flow->GetNumberOfChannels();
            if( fFlags->fVerbose )
               std::cout<<"PulseFinderModule::AnalyzeFlowEvent # of ch: "<<number_of_channels
                        <<" with DSProcessor"<<std::endl;
         }
      else
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      // get the number of channels per ADC board
      int ADCchan =  wf_flow->GetConf()->fNumberChannelPerModule;
 
      double threshold=fAmplitude;
      int npevt=0;
      for(int i=0; i<number_of_channels; ++i)
         {
            const std::vector<double>* wf;
            double rms=0.;
            int Mod=-1,Chan=-1;
            std::string Type="";

            TDSChannel* dsch = wf_flow->GetDSchan(i);
            if( dsch->filtered_wf.size() > 0 )
               {
                  wf=&dsch->filtered_wf;
                  rms=dsch->filter_rms; 
                  if( fFlags->fVerbose ) dsch->PrintFiltered();
               }
            else
               {
                  if( fFlags->fVerbose ) dsch->Print();
                  //std::cout<<"PulseFinderModule::AnalyzeFlowEvent with DSProcessor WARNING: you are using unfiltered waveforms for pulse finding..."<<std::endl;
                  wf=&dsch->unfiltered_wf;
                  rms=dsch->baseline_rms;
               }
            Mod = dsch->module;
            Chan = dsch->channel;
            Type = dsch->board.c_str();

            // do not apply pulse finder to trigger channel
            int ChannelIndex = Mod * ADCchan + Chan;
            if( ChannelIndex == fTriggerChannel ) continue;

            if( fFlags->fVerbose )
               std::cout<<"PulseFinderModule::AnalyzeFlowEvent ch: "<<i
                        <<" size: "<<wf->size()<<std::endl;

            if( !fFixed ) threshold = rms * fNsigma; // threshold based on baseline rms
            PulseFinder find( threshold );           // pulse finder - Time Over Threshold
            find.SetMinimumDuration( fShortPulse );  // set the minimum Time Over Threshold
            find.SetIntegrationWindow( fStartIntegration, fTotalIntegration );
            int np = find(wf); // find pulses
            if( fFlags->fVerbose )
               {
                  if( np>0 ) find.print(true);
                  else std::cout<<"No pulses found above threshold: "<<threshold
                               <<" in channel: "<<Chan<<" in module: "<<Mod<<std::endl;
               }

            for(int i=0; i<np; ++i) // store the pulses in the flow
               {
                  TDSPulse aPulse(Mod,Chan,Type.c_str()); // DS-type object
                  aPulse.charge=find.GetCharge(i);
                  if( fTimingMode == "avg" )
                     aPulse.time=find.GetAveragePeakTime(i);
                  else if( fTimingMode == "thr" )
                     aPulse.time=(double)find.GetStartTime(i);
                  else
                     aPulse.time=(double)find.GetPeakTime(i);
                  aPulse.height=find.GetAmplitude(i);
                  aPulse.tot=(double)find.GetTimeOverThreshold(i);
                  if( fFlags->fVerbose && 0 ) aPulse.Print();
                  wf_flow->pulses->push_back(aPulse);
               }

            fNtotPulses+=np;
            npevt+=np;
         }

      if( fFlags->fVerbose )
         std::cout<<"PulseFinderModule::AnalyzeFlowEvent # of pulses: "<<npevt<<std::endl;
      
      flow=wf_flow;
      ++fCounter;
      return flow;
   }
};


class PulseFinderModuleFactory: public TAFactory
{
public:
   PulseFinderFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("PulseFinderModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
   }
   
   void Finish()
   {
      printf("PulseFinderModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("PulseFinderModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new PulseFinderModule(runinfo, &fFlags);
   }

};

static TARegister tar(new PulseFinderModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
