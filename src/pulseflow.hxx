/******************************************************************
 * Flow Object(s) for generic pulse finding *
 *
 * D. Gallacher
 * Feb 2022
 *
******************************************************************/

#ifndef __PULSEFLOW__
#define __PULSEFLOW__

#include "manalyzer.h"

#include <map>
#include <vector>
#include <string>

class PulseFinderResults
{
private:
   int fNpulses;
   double fThreshold;
public:

  virtual ~PulseFinderResults(){ Reset();}

  //Struct to hold pulse finder results
  struct PulseFinderOutput{
    std::vector<double> fPulseStart;
    std::vector<double> fPulseEnd;
    std::vector<double> fPulseWidth;
    std::vector<double> fPeakTime;
    std::vector<double> fAmplitude;
    std::vector<double> fCharge;
  };

  PulseFinderOutput pulseResults;
  int chanNum;

  inline void SetThreshold(double t) { fThreshold = t; }
  inline PulseFinderOutput GetPulseFinderOutput(){return pulseResults;}
  inline int GetStartTime(int i) const { return pulseResults.fPulseStart.at(i); }
  inline int GetPeakTime(int i) const { return pulseResults.fPeakTime.at(i); }
  inline double GetAmplitude(int i) const { return pulseResults.fAmplitude.at(i); }
  inline double GetCharge(int i) const { return pulseResults.fCharge.at(i); }
  inline int GetTimeOverThreshold(int i) const { return pulseResults.fPulseWidth.at(i); }
  inline int GetNPulses() const { return pulseResults.fPulseStart.size(); }


  inline void Reset()
  {
     fNpulses = 0;
     pulseResults = {};
  }

  void print(bool all=true) const{
    //Not implemented yet
  }


};

//Settings here
class PulseConfig {

  public:
    int nChannels;
    double nanoSecsPerSample;

    PulseConfig(){ init(); }
    PulseConfig(std::string name){
      if(name=="DT5730"){
        nChannels = 8;
        nanoSecsPerSample = 2.0;
      }
      else if(name=="VX2740"){
        nChannels = 64;
        nanoSecsPerSample = 8.0;
      } else {
        std::cout << "PulseConfig name not found"<<std::endl;
      }
    }

    void print() const
    {
      std::cout << "Printing PulseConfig Settings .. ";
      std::cout << Form ("\n Number of channels = %i,",nChannels);
      std::cout << "\n";
    }

    void init(){

      nChannels =0;

    }

};


class PulseFlow: public TAFlowEvent
{
public:
  PulseConfig *conf;
  std::vector<PulseFinderResults*> results;
  int nChannels;
  std::string confName;

public:
  PulseFlow(TAFlowEvent* flow,std::string name):TAFlowEvent(flow)
  {
    conf = new PulseConfig(name);
    nChannels = conf->nChannels;

    //Initialize
    for(int i=0;i<nChannels;i++){
      results.push_back(new PulseFinderResults());
      results.at(i)->chanNum = i;
    }
  }

  //Constructor with nchannels
  PulseFlow(TAFlowEvent* flow,std::string name,int num_channels):TAFlowEvent(flow)
  {
    conf = new PulseConfig(name);
    nChannels =num_channels;
    //Initialize
    for(int i=0;i<nChannels;i++){
      results.push_back(new PulseFinderResults());
      results.at(i)->chanNum = i;
    }
  }


  ~PulseFlow()
  {
    delete conf;
    for(uint iChannel =0;iChannel<nChannels;iChannel++){
      delete (results[iChannel]);
    }
    results.clear();
  }

};

#endif
