/******************************************************************
 * Unpack CAEN V1730 data *
 *
 * A. Capra
 * November 2021
 *
******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "adcflow.hxx"
#include "TV1730RawData.hxx"

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>

#include "json.hpp"
using json = nlohmann::json;


class UnpackV1730Flags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
   int fSkipEvents=-1;
};


class UnpackV1730Module: public TARunObject
{
public:
   UnpackV1730Flags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors
   const int numberChannelPerModule = 16;
   const double NanosecsPerSample = 2.0; //ADC clock runs at 500MHz on the v1730 = units of 2 nsecs
   int fTotNumBoards=4;

public:
   UnpackV1730Module(TARunInfo* runinfo, UnpackV1730Flags* f): TARunObject(runinfo),
                                                         fFlags(f), fCounter(0), fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"UnpackV1730Module ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="UnpackV1730";
#endif

      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"UnpackV1730 Json parsing success!"<<std::endl;
      fin.close();

      fTotNumBoards=settings["ADC"]["Number of V1730"].get<int>();

      std::cout<<"Total number of V1730 boards for run "<<runinfo->fRunNo
               <<" is "<<fTotNumBoards<<std::endl;
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"UnpackV1730Module::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"UnpackV1730Module::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"UnpackV1730Module::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

   TAFlowEvent* Analyze(TARunInfo* /*runinfo*/, TMEvent* midasevent,
                        TAFlags* /*flags*/, TAFlowEvent* flow)
   {
      if( midasevent->error )
         {
            ++fError;
            return flow;
         }
      
      if( fFlags->fVerbose )
         std::cout<<"UnpackV1730Module::Analyze ID: "<<midasevent->event_id
                  <<" S/N: "<<midasevent->serial_number
                  <<" TS: "<<midasevent->time_stamp
                  <<" Trigger mask: "<<midasevent->trigger_mask<<std::endl;

      if( midasevent->event_id != 1 ) return flow; // non-ADC event
 
      // allow the user to fast forward to the desired event
      if( fCounter < fFlags->fSkipEvents)
         {
            if( fFlags->fVerbose )
               std::cout<<"UnpackV1730Module Skip Event: "<<fCounter<<std::endl;
            ++fCounter;
            return flow;
         }

      AdcEventFlow* eflow = flow->Find<AdcEventFlow>();
      if( !eflow )
         {
            eflow = new AdcEventFlow(flow, midasevent->time_stamp,midasevent->serial_number);
            //std::cout<<"UnpackV1730Module New ADC Flow Event!!"<<std::endl;
            flow=eflow;
         }
      //std::cout<<"UnpackV1730Module Event: "<<eflow->getEventNumber()<<std::endl;

      for (int board_idx = 0; board_idx < fTotNumBoards; ++board_idx)
         {
            std::stringstream ss;
            ss<<"V73"<<std::to_string(board_idx);
            std::string bankname=ss.str();
            TMBank* bank = midasevent->FindBank(bankname.c_str());
            if( !bank ) continue;
            if( fFlags->fVerbose )
               std::cout<<"UnpackV1730Module::Analyze V1730 bank "<<bankname
                        <<" found... Board: "<<board_idx<<std::endl;
            TV1730RawData* raw = new TV1730RawData((int)bank->data_size,(int)bank->type,
                                                     bank->name.c_str(),
                                                     midasevent->GetBankData(bank));

            eflow->fEventCounters[board_idx] = raw->GetEventCounter();
            //            eflow->fTriggerTime[board_idx] = raw->GetTriggerTimeStamp();
            eflow->fTriggerTime[board_idx] = raw->GetTriggerTimeTag();
            if( fFlags->fVerbose )
               std::cout<<"UnpackV1730Module::Analyze V1730 ETTT: "<<eflow->fTriggerTime[board_idx]
                        <<" s\tEvent Counter: "<<eflow->fEventCounters[board_idx]<<std::endl;

            // loop over channels
            int chan=0;
            for( auto channelData: raw->GetMeasurements() )
               {
                  if( channelData.IsEmpty() ) continue;
                  channelData.SetBoardNumber(board_idx);
                  channelData.SetIndex(board_idx*numberChannelPerModule+chan);
                  if( fFlags->fVerbose && 0 ) channelData.Print();
                  eflow->add(&channelData,"V1730");
                  ++chan;
               }
            delete raw;
         }
      if( fFlags->fVerbose )
         std::cout<<"UnpackV1730Module::Analyze Event: "<<fCounter
                  <<" # of channels: "<<eflow->getNchannels()<<std::endl;

      ++fCounter;
      return flow;
   }
};


class UnpackV1730ModuleFactory: public TAFactory
{
public:
   UnpackV1730Flags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      printf("UnpackV1730ModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
            if( args[i] == "--skip-events" ||
                args[i] == "-s" )
               fFlags.fSkipEvents=std::stoi(args[i+1]);
         }

   }

   void Finish()
   {
      printf("UnpackV1730ModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("UnpackV1730ModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new UnpackV1730Module(runinfo, &fFlags);
   }

};

static TARegister tar(new UnpackV1730ModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
