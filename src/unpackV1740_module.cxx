/******************************************************************
 * Unpack CAEN V1740 data *
 *
 * D. Gallacher, based on unpack_v2740_module
 * October 2021
 *
******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "adcflow.hxx"
#include "TV1740RawData.hxx"

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include <cstdlib>

#include "json.hpp"
using json = nlohmann::json;

class UnpackAdcFlagsV1740
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};

//Read preprocessor flag value as string literal
#define STRING(s) #s
#define STRSTRING(s) STRING(s)

class UnpackAdcModuleV1740: public TARunObject
{
public:
   UnpackAdcFlagsV1740* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors
   const int numberChannelPerModule = 64;
   const double NanosecsPerSample = 16.0; //ADC clock runs at 62.5Mhz on the v1740 = units of 16 nsecs
   int fTotNumBoards=1;//For LoLX

public:
   UnpackAdcModuleV1740(TARunInfo* runinfo, UnpackAdcFlagsV1740* f): TARunObject(runinfo),
                                                         fFlags(f), fCounter(0), fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"UnpackAdcModuleV1740 ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="UnpackAdcV1740";
#endif
      std::string config_path;
#ifdef CONFIG_PATH
      config_path = STRSTRING(CONFIG_PATH) + fFlags->fConfigName;//Read config file path from CMAKE source dir (lolx)
#else
      config_path = fFlags->fConfigName;//If path isn't defined look in flags only
#endif
      std::cout << "Reading config file from path = "<< config_path << "\n";
      std::ifstream fin(config_path);
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"UnpackV1740 Json parsing success!"<<std::endl;
      fin.close();

      fTotNumBoards=settings["ADC"]["Number of V1740"].get<int>();

      std::cout<<"Total number of V1740 boards for run "<<runinfo->fRunNo
               <<" is "<<fTotNumBoards<<std::endl;
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"UnpackAdcModuleV1740::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"UnpackAdcModuleV1740::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"UnpackAdcModuleV1740::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

   TAFlowEvent* Analyze(TARunInfo* /*runinfo*/, TMEvent* midasevent,
                        TAFlags* /*flags*/, TAFlowEvent* flow)
   {
      if( midasevent->error )
         {
            ++fError;
            return flow;
         }

      int fe_idx = midasevent->trigger_mask;
      if( fFlags->fVerbose )
         std::cout<<"UnpackAdcModuleV1740::Analyze ID: "<<midasevent->event_id
                  <<" S/N: "<<midasevent->serial_number
                  <<" TS: "<<midasevent->time_stamp
                  <<" Trigger mask: "<<fe_idx<<std::endl;

      //Use generic ADC flow module
      AdcEventFlow* eflow = flow->Find<AdcEventFlow>();
      if( !eflow )
         {
            eflow = new AdcEventFlow(flow, midasevent->time_stamp,midasevent->event_id);
            flow=eflow;
         }


      for (int board_idx = 0; board_idx < fTotNumBoards; ++board_idx)
         {
            std::stringstream ss;
            ss<<"W4"<<std::setfill('0')<<std::setw(2)<<std::to_string(board_idx);
            std::string bankname=ss.str();
            TMBank* bank = midasevent->FindBank(bankname.c_str());
            if( !bank ) continue;
            if( fFlags->fVerbose )
               std::cout<<"UnpackAdcModuleV1740::Analyze V1740 bank "<<bankname
                        <<" found... Board: "<<board_idx<<std::endl;
            TV1740RawData* raw = new TV1740RawData((int)bank->data_size,(int)bank->type,
                                                     bank->name.c_str(),
                                                     midasevent->GetBankData(bank));

            eflow->fEventCounters[board_idx] = raw->GetEventCounter();

            for( int chan = 0; chan < raw->GetNChannels(); ++chan )
               {
                  TV1740RawChannel channelData = raw->GetChannelData(chan);
                  if( channelData.IsEmpty() ) continue;
                  channelData.SetBoardNumber(board_idx);
                  channelData.SetIndex(board_idx*numberChannelPerModule+chan);
                  if( fFlags->fVerbose && 0 ) channelData.Print();
                  eflow->add(&channelData,"V1740");
               }
            delete raw;
         }
      if( fFlags->fVerbose )
         std::cout<<"UnpackAdcModuleV1740::Analyze Event: "<<fCounter
                  <<" # of channels: "<<eflow->getNchannels()<<std::endl;

      ++fCounter;
      return flow;
   }
};


class UnpackAdcModuleV1740Factory: public TAFactory
{
public:
   UnpackAdcFlagsV1740 fFlags;

public:
   // void Usage()
   // {
   //    printf("UnpackAdcModuleV1740Factory flags:\n");
   //    printf("--verbose\tprint status information\n");
   // }

   void Init(const std::vector<std::string> &args)
   {
      printf("UnpackAdcModuleV1740Factory::Init!\n");

      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }

   }

   void Finish()
   {
      printf("UnpackAdcModuleV1740Factory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("UnpackAdcModuleV1740Factory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new UnpackAdcModuleV1740(runinfo, &fFlags);
   }

};

static TARegister tar(new UnpackAdcModuleV1740Factory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
