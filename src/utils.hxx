#ifndef Utils_h
#define Utils_h

/******************************************************************
 * Utility functions for baseline and pulse analysis
 *
 * D. Gallacher
 * October 2021
 *
 *
 ******************************************************************/

#include "TH1D.h"
#include "TH1.h"
#include "TF1.h"
#include "TGraph.h"
#include "TMath.h"
#include <Math/Functor.h>
#include <Math/RichardsonDerivator.h>


#include <map>
#include <numeric>
#include <vector>
#include <utility>

class Utils {

public:
  int verbose;

  Utils() : verbose(0){}
  ~Utils(){}


  void SetVerbosity(int v){verbose = v;}

  //Calculate a baseline from the vector of ADC counts,
  // within a window of nbase_samples at the start of the waveform
  double calculate_baseline(const std::vector<double>* vADC, int& window)
  {
    double baseline = std::accumulate(vADC->begin(),vADC->begin()+window,double(0));
    baseline /= (double) window;
    return baseline;
  }

  // Calculate median of waveform vector -- AC
  double calculate_median(const std::vector<double>* vADC)
  { 
    std::vector<double> w(*vADC); // since we are modifying the vector later, we make a local copy
    size_t n = w.size() / 2; // position of the median
    // Rearranges the elements in the range such that the second argument
    // is the element that would be in that position in a sorted sequence.
    // https://www.cplusplus.com/reference/algorithm/nth_element/
    std::nth_element(w.begin(), w.begin()+n, w.end());
    return w[n]; // the median
  }

  // Calculate mode, aka 'most probable value', of waveform vector -- AC
  double calculate_mode(const std::vector<double>* vADC)
  {
    // create a counter, i.e., a map from the vector element to its (absolute) frequency
    std::map<double, int> counters;
    // count how many times an item appears
    for(auto& i: *vADC) ++counters[i];

    // avoids writing "std::pair<int,int>" later on
    using pair_type = decltype(counters)::value_type; 
    // get the map value that is most frequent
    auto pr = max_element( counters.begin(), counters.end(),
                        [] (const pair_type& p1, const pair_type& p2) { // lambda function
                        return p1.second < p2.second;}  // strict ordering
        );
    // mode of the vector:
    return pr->first;
  }

  //Calculate the rms of the  baseline from the vector of ADC counts,
  // within a window of nbase_samples at the start of the waveform
  double calculate_baseline_rms(const std::vector<double>* vADC, double& baseline, int& window)
  {
    double baseline_rms = std::inner_product(vADC->begin(),
					     vADC->begin()+window,
					     vADC->begin(),double(0));
    baseline_rms = sqrt( (baseline_rms / (double) window) - (baseline * baseline) );
    return baseline_rms;
  }

  //PA -- Calculate the charge in ROI from the vector of ADC counts,
  // within a window [start:end]. You  need to know the trigger position. 
  double calculate_ROI_charge(const std::vector<double>* vADC, int& start, int& end)
  {
    double roicharge = std::accumulate(vADC->begin()+start,
                                       vADC->begin()+end,
                                       double(0));
    return roicharge;
  }

  //PA -- Calculate the height in ROI from the vector of ADC counts,
  // within a window [start:end]. You  need to know the trigger position. 
  double calculate_ROI_height(const std::vector<double>* vADC, int& start, int& end)
  {
    return *std::max_element(vADC->begin()+start,
			     vADC->begin()+end);
  }

  //PA+AC -- Calculate the height in ROI from the vector of ADC counts,
  // within a window [start:end]. You  need to know the trigger position. 
  double calculate_ROI_height(const std::vector<double>* vADC, int& start, int& end, double& t)
  {
    auto pos = std::max_element(vADC->begin()+start,
				vADC->begin()+end);
    t = static_cast<double>( std::distance( vADC->begin(), pos ) );
    return *pos;
  }
  
  //Method to convert a raw ADC object into a TH1D
  void ConvertRawWF(TH1D &hWF,const std::vector<double>* vADC, const int chan, double nanoSecPerSample){
    uint nsamp = vADC->size();
    if (nsamp == 0) {
      if(verbose > 0) std::cout << "Error in Utils: No samples found for channel# "<< chan <<std::endl;
      return;
    }

    std::string name = Form("chan_%i",chan);
    std::string title = Form("corrected waveform %i",chan);
    hWF.SetBins(nsamp,0,nsamp*nanoSecPerSample);
    hWF.SetName(name.c_str());
    hWF.SetTitle(title.c_str());
    for(uint iB = 1; iB < nsamp+2;iB++) hWF.SetBinContent(iB,(vADC->data())[iB-1]);//Set content from vector
  }

  //Method to convert a raw WDAQ waveform into a TH1D with variable bins
  void ConvertRawWF_WDAQ(TH1D &hWF,const std::vector<double>* vADC, const std::vector<double>* vTime, const int chan){

    const uint nsamp = vTime->size();
    if (nsamp == 0) {
      if(verbose > 0) std::cout << "Error in Utils: No samples found for channel# "<< chan <<std::endl;
      return;
    }
    //Variable bin width
    std::vector<double> lowEdges(nsamp+1,0.0);
    //convert time to ns from s and fill bins
    for(uint iB = 0;iB<nsamp;iB++) {
      lowEdges[iB] = vTime->data()[iB]/1.0e-9;
    }
    //Ensure increasing bins
    lowEdges[nsamp] = lowEdges[nsamp-1] + 1;
    std::string name = Form("chan_%i",chan);
    std::string title = Form("corrected waveform %i",chan);
    hWF.SetBins(nsamp,&lowEdges[0]);
    hWF.SetName(name.c_str());
    hWF.SetTitle(title.c_str());
    //Set content from vector
    for(uint iB = 1; iB < nsamp + 2;iB++) hWF.SetBinContent(iB,(vADC->data())[iB-1]);
  }

  //Perfrom a basic baseline subtraction, look at first N+5 bins to get an average baseline then subtract
  void RemoveBaseline(TH1D &h ,int win=5){
    double baseline = 0.0;
    for(int iBin = 5 ; iBin < win+5 ; iBin++){
      baseline += h.GetBinContent(iBin);
    }
    baseline /= double(win); // Mean baseline
    //Subtract average baseline
    for(int iBin = 1; iBin < h.GetNbinsX()+1; iBin++){
      double bAmp = h.GetBinContent(iBin);
      h.SetBinContent(iBin,bAmp-baseline); // Shift down bin contents by baseline average
    }
  }


  //David's Baseline restoration algorithm, assumes baseline is zero,
  //Also requires end of waveform to be flat, a pulse at the end can
  //cause a slope in the correction, add in error handling for this -- DG
  void BaselineRestoration(TH1D &hCorr, double tau,double t0,int binWindow, int binJump,double slope)
  {
    double binsize = hCorr.GetBinWidth(1); // Bin width in nano-seconds
    std::vector<double> vShift;
    vShift.push_back(0.0);// Initial condition
    int binstart = 1;
    int n = 1;

    //Recursive shift
    for(int iB=binstart;iB<hCorr.GetNbinsX();iB++){
      double input = hCorr.GetBinContent(iB);//VExp(n)
      double correction = input-vShift[n-1]*exp(-binsize/tau);//Vinp(n)
      double shift = vShift[n-1]*exp(-binsize/tau)-correction*binsize/tau; // Vshift(n)
      vShift.push_back(shift);
      hCorr.SetBinContent(iB,correction);
      n++;

    }
    vShift.clear();

    int nbins = hCorr.GetNbinsX();
    //Calculate DC offset at end of pulse
    double offset = 0.0;
    int bstart = nbins-binWindow;//Number of bins to calculate the DC offset at end of trace
    int bend = nbins-2;
    std::vector<double> check;
    for(int iB =bstart;iB<bend;iB++)
    {
      offset+=hCorr.GetBinContent(iB);
      check.push_back(hCorr.GetBinContent(iB));
    }

    double minCheck = *std::min_element(check.begin(),check.end());
    double maxCheck = *std::max_element(check.begin(),check.end());
    //std::cout << "Warning in Utils: Slanted baseline!!! diff = " << fabs(maxCheck-minCheck)<<std::endl;
    if(fabs(maxCheck-minCheck) > slope){
       if(verbose > 0 ) std::cout << "Warning in Utils: Slanted baseline!!! diff = " << fabs(maxCheck-minCheck)<<std::endl;
        offset = CalculateBaseline(hCorr,bstart-binJump-binWindow,bstart-binJump);
    } else {
        offset/=double(bend-bstart);
    }

    //T0 of first pulse
    bstart = hCorr.FindBin(t0);
    //Shift down post-pulse baseline
    for(int iB=bstart;iB<nbins;iB++)
    {
      double bincontent = hCorr.GetBinContent(iB);
      hCorr.SetBinContent(iB,bincontent-offset);
    }

  }


  //Take the finite differences derivative of the waveform
  //not the best way to get a first derivative, but works
  TH1D GetDerivativeFinite(TH1D &hCorr, int binWidth)
  {
      double deltaV = 0.0;
      int nBins = hCorr.GetNbinsX();
      TH1D hDTrace("","Trace Derivative;time[ns];1st Der[AU]",nBins,0,hCorr.GetBinLowEdge(nBins)+hCorr.GetBinWidth(1));
      for(int iBin = 1; iBin < nBins; iBin++){
        // Be careful near the edges of the trace
        if(iBin<=binWidth){
          deltaV = 0;
        }
        if(iBin<=nBins){
          deltaV = (hCorr.GetBinContent(iBin+binWidth)-hCorr.GetBinContent(iBin-binWidth))/((binWidth+1)*hCorr.GetBinWidth(iBin));
        }
        else{
          deltaV = (hCorr.GetBinContent(iBin)-hCorr.GetBinContent(iBin-binWidth))/((binWidth+1)*hCorr.GetBinWidth(iBin));
        }
        hDTrace.SetBinContent(iBin,deltaV);
      }
      // std::cout << "Leaving GetDerivativeFinite "<<std::endl;
      return hDTrace;
  }

  void GetDerivativeROOT(TH1D &hDCorr, TH1D &hCorr,double eps)
  {
    //Use TGraph to interpolate better than histograms
    TGraph gWF;
    for(int iP=0;iP<hCorr.GetNbinsX();iP++) gWF.SetPoint(iP,hCorr.GetBinCenter(iP+1),hCorr.GetBinContent(iP+1));
    int nBins = hCorr.GetNbinsX();
    double lowEdge = hCorr.GetBinLowEdge(1);
    double highEdge = hCorr.GetBinLowEdge(nBins+1);

    //Define TF1's for derivating with Lambdas
    //Make the waveform a tf1
    TF1 fWF("fWF",[&](double*x, double *p){ p[0]=0; return gWF.Eval(x[0]); }, lowEdge, highEdge, 1);
    //Use the derivative function to make a derivative of the waveform
    TF1 fDWF("fDWF",[&](double*x, double *p){ p[0]=0; return fWF.Derivative(x[0],0,eps); }, lowEdge, highEdge, 1);

    //Return the waveform derivative as a histogram
    hDCorr.SetTitle("Trace Derivative;time[s];1st Der[AU]");
    hDCorr.SetBins(nBins,lowEdge,highEdge);
    for(int iB=0;iB<nBins;iB++){
        double dWF = fDWF.Eval(hDCorr.GetBinCenter(iB));
        hDCorr.SetBinContent(iB,dWF);
    }
  }

  int MinimumScan(TH1D &hCorr,int startBin,int window){
    int minBin = startBin;
    double minVal = hCorr.GetBinContent(minBin);
    //Edge-case handling
    if(startBin-window < 1 || hCorr.GetNbinsX() <= (startBin+window) ){
      // std::cout << "Window specified is outside of histogram limits.. Returning startBin value" <<std::endl;
      return minBin;
    }

    //Scan for updated minimum
    for(int iB = startBin-window ; iB < startBin+window;iB++){
      double binVal = hCorr.GetBinContent(iB);
      if(binVal < minVal){
        minBin = iB;
        minVal = binVal;
      }
    }
    return minBin;
  }

  //Only looks forward, doesn't scan around central value
  double FindMinimum(TH1D &hCorr, int startBin, int window){
    int endBin = startBin+window;//in bin count
    double min = 1e10;
    int minBin = startBin+1;
    if(startBin-window < 1 || hCorr.GetNbinsX() <= (startBin+window) ){
      // std::cout << "Window specified is outside of histogram limits.. Returning startBin value" <<std::endl;
      return minBin;
    }

    for(int iB =startBin;iB<endBin;iB++){
      double binVal = hCorr.GetBinContent(iB);
      if(binVal < min) {
        minBin = iB;
        min = binVal;
      }
    }//End loop over bins
    return minBin;
  }//End function

  //Find RMS of histogram in a window [bStart,bEnd]
  double CalculateBaselineRMS(TH1D &hCorr, int bStart, int bEnd)
  {
    double bRMS = 0.0; // Initialize to 0
    double width = bEnd-bStart;
    for(int iBin = bStart; iBin < bEnd; iBin++){
      double bVal = hCorr.GetBinContent(iBin);
      bRMS += bVal*bVal;
    }
    bRMS/=width;
    bRMS = sqrt(bRMS);
    return bRMS;
  }

  //Find average baseline
  double CalculateBaseline(TH1D &hCorr, int bStart, int bEnd)
  {
    double baseline = 0.0; // Initialize to 0
    double width = bEnd-bStart;
    for(int iBin = bStart; iBin < bEnd; iBin++){
      double bVal = hCorr.GetBinContent(iBin);
      baseline += bVal;
    }
    baseline/=width;
    return baseline;
  }

  // Basic t0 algorithm, looks at the derivative of the trace and finds the point of threshold crossing
  // threshold given in scalar multiples of derivative baseline RMS's
  double GetT0(TH1D &hDRaw,double thresh,int win){
    double t0 = 20.0;
    double RMSLevel = CalculateBaselineRMS(hDRaw,10,10+win);
    for(int iBin=hDRaw.FindBin(t0);iBin<hDRaw.GetNbinsX();iBin++){
      if(hDRaw.GetBinContent(iBin) < -1.0*RMSLevel*thresh){
        t0 = hDRaw.GetBinLowEdge(iBin);
        return t0;
      }
    }
    return -1.0;

  }//End T0

  //Simpler t0 finder, look for pulse under threshold (2 bins required)
  double GetT0Raw(TH1D &hRaw,double thresh){
    double t0 = 20.0;
    for(int iBin=hRaw.FindBin(t0);iBin<hRaw.GetNbinsX();iBin++){
      if(hRaw.GetBinContent(iBin) < thresh && hRaw.GetBinContent(iBin+2) < thresh){
        t0 = hRaw.GetBinLowEdge(iBin);
        return t0;
        }
      }
    return -1.0;
  }//End T0

  //Method to scan a histogram in a window around a pulse, to find the minimum values
  //Test a minimum finder inside window [bin_start,bin_end], returns bin numbers of minimums
  std::vector<std::pair<int,double>> FindMinimums(TH1D &hCorr, const int start,int end,const std::vector<double> par){

    //Vector to hold minimum bins
    if(par.size()!=7){
      std::cout << "Error in Utils: Unexpected number of input parameters requested for MinimumSearch, dying now.."<<std::endl;
      exit(1);
    }
    std::vector<std::pair<int,double>> minimums;
    int binWidth = par[0]; //10;//Number of bins to average over
    int resetbins = par[1]; //50;//Number of bins to reset
    int peakScanWidth = par[2]; //50;//Number of bins to scan forward over for zero-crossing
    int nMins = par[3]; //5;//Number of minimums to look for
    int baseWin = par[4]; //Number of bins to use before start_bin for baseline rms
    double thresh = par[5];//Threshold in number of sigmas
    double base = par[6];//Baseline before pulse

    std::vector<double> finiteDifferences; //Derivative of wf, but only in window[start,end]
    std::vector<double> waveform;
    int nBins = end-start;
    if(nBins<0){
      std::cout << "Error in Utils: End requested before start"<<std::endl;
      exit(1);
    }
    if(end > hCorr.GetNbinsX()){
      if(verbose>0) std::cout << "Error in Utils: End bin outside of waveform, using last bin as end"<<std::endl;
      end = hCorr.GetNbinsX();
    }


    //Calculate derivative using a finite difference window scan
    int nb=0;
    for(int iBin = start; iBin <= end; iBin++){
          double deltaV = 0.0;
          nb++;
          // Be careful near the edges of the trace
          if(iBin <= binWidth){
            deltaV = 0.0;
          } if(iBin < end) {
            deltaV = (hCorr.GetBinContent(iBin+binWidth)-hCorr.GetBinContent(iBin-binWidth))/((binWidth+1)*hCorr.GetBinWidth(iBin));
          } else {
            deltaV = (hCorr.GetBinContent(iBin)-hCorr.GetBinContent(iBin-binWidth))/((binWidth+1)*hCorr.GetBinWidth(iBin));
          }
          finiteDifferences.push_back(deltaV);
          waveform.push_back(hCorr.GetBinContent(iBin));
    }//End derivative

    //Find rms of derivative baseline before pulse
    int bsup = baseWin-5;
    double bline = 0.;
    double brms = calculate_baseline_rms(&finiteDifferences,bline,bsup);
    //Find Minimums, up to N, by looking at derivative and finding

    std::vector<double>::iterator wf_min = std::min_element(std::begin(waveform),std::end(waveform));
    //First minimum is the main pulse peak,
    int wf_min_bin = std::distance(std::begin(waveform),wf_min);
    double wf_min_val = hCorr.GetBinContent(start+wf_min_bin) - base; //Subtract off rolling baseline
    std::pair<int,double> firstpeak = std::make_pair(wf_min_bin+start,wf_min_val);
    minimums.push_back(firstpeak);//Push back first

    for(int iMin = 0; iMin < nMins ; iMin++){
      std::vector<double>::iterator aMin = std::min_element(std::begin(finiteDifferences), std::end(finiteDifferences));
        //First find minimum of derivative corresponding to leading edges of sub-pulse candidates
       if (std::end(finiteDifferences)!=aMin && *aMin < -thresh*brms){
         int theMinBin = std::distance(finiteDifferences.begin(), aMin);
         int threshBin = theMinBin-1;
         //Then Look for zero-crossing after leading edge corresponding to peak of actual waveform
         for(int iZ = theMinBin; iZ < theMinBin+peakScanWidth; iZ++){
           if(finiteDifferences[iZ] < 0 && finiteDifferences[iZ+1] > 0){
             theMinBin = iZ;
             break;
           }
         }//End loop over bins

         //Get Minimum values
         double subpeakV = hCorr.GetBinContent(theMinBin+start) - hCorr.GetBinContent(threshBin+start-2);//Min content = value at start vs peak bin
         std::pair<int,double> minimumPair = std::make_pair(theMinBin+start,subpeakV);
         minimums.push_back(minimumPair);
         unsigned int position = std::distance(finiteDifferences.begin(),aMin)+resetbins;
         int startReset = std::distance(finiteDifferences.begin(),aMin - baseWin);
         if(position < finiteDifferences.size()) {
           if(startReset < 0) { std::fill(aMin,aMin+resetbins,0.0); }//Set values to 0
           else {std::fill(aMin-baseWin,aMin+resetbins,0.0);}//Set values to 0
         } else { //If we're here we've reached the end of the scan
            break;
         }
       }//End Threshold check
    }//End minimum search

    return minimums;
  }//End MinimumSearch


};

#endif
