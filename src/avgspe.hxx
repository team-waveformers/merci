#ifndef __AVGSPE__
#define __AVGSPE__

#include <cmath>

class AvgSPE
{
private:
   double fA;
   double ft0;
   double ftauS;
   double ftauL;
   
public:
   AvgSPE(double amplitude, double time, double ts, double tl):
      fA(amplitude),ft0(time),ftauS(ts),ftauL(tl)
   {}

   double A() const { return fA; }
   double t0() const { return ft0; }
   double tau_short() const { return ftauS; }
   double tau_long() const { return ftauL; }

   double operator()(double t) const
   {
      if( t < ft0 ) return 0.;
      return fA * ( exp( (ft0-t)/ftauL ) - exp( (ft0-t)/ftauS ) );
   }
};
#endif


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
