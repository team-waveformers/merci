#ifndef TWDData_hxx_seen
#define TWDData_hxx_seen

#include <vector>
#include <map>
#include <stdio.h>
#include <iomanip>
#include <iostream>

#include "TGenericData.hxx"
#include "TRawChannel.hxx"

/*******************************************************************************
 * DRS voltage encoding mode 0
 * no compression
 * voltage is stored in short [x0.1mV]
 *******************************************************************************/
const double DRS_CF_V_MODE00 = 1e4;     // conversion factor for voltage


/// This class stores the information for
/// a single WDAQ channel, in particular the waveform.

//Inherit from TRawChannel
class TWDRawChannel: public TRawChannel{

public:

  TWDRawChannel(int channel, int baseline=-1):
    TRawChannel(channel,0),
    fVerbose(0),
    fBaseline(baseline)
  { }

  int GetBaseline()const {return fBaseline;};

  /// Add all ADC samples to wavefrom
  void AddSamples(std::vector<double> samples){ fWaveform = samples; }

  /// Add time low-edge bins for samples to waveform
  void SetADCTimes(std::vector<double> times){ fWaveformTimes = times; }


 private:
  //Baseline for this channel
  int fVerbose;
  int fBaseline;


};

/// Class to store data from WDAQ
/// Class stores information for data from a single bank (all boards)
/// Data is stored as a vector of TWDRawChannel's
///

class TWDRawData: public TGenericData {

public:

  /// Constructor
  TWDRawData(int event_num, int bklen, int bktype, const char* name, void *pdata);

  ~TWDRawData();

  //Update these to more useful info -- DG

  /// Get the number of 32-bit words in bank.
  uint32_t GetEventSize() const {return fEvSize;}
  /// Get event counter
  uint32_t GetEventCounter() const {return event_counter;}

  /// Get Number of channels in this bank.
  int GetNChannels() const {return fMeasurements.size();}

  /// Method to determine unique channels corresponding to WDB info
  int GetChannelIndex(int chip, int slot , int channel);


  /// Get Channel Data
  TWDRawChannel GetChannelData(int i) {
    if(i >= 0 && i < (int)fMeasurements.size())
      return fMeasurements[i];

    return TWDRawChannel(i);
  }


  //Method to decode DRST Bank separately, if event==0, vDeltat is filled,
  //otherwise its just read to make the time vector for each TWDRawChannel
  void AddDRSTBank(std::map<int, std::vector<double>> &fDeltaTimes,int bklen, int bktype, const char* name, void *pdata);

  void Print();


private:

  /// Helper method to handle uncompressed data.
  void HandleUncompressedData();


  /// The overall global headers
  uint32_t fEvSize;

  /// Vector of WDAQ measurements
  std::vector<TWDRawChannel> fMeasurements;

  //Track event number
  int event_counter;

  int fVerbose;
};

#endif
