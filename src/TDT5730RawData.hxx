#ifndef TDT5730RawData_hxx_seen
#define TDT5730RawData_hxx_seen

#include <vector>

#include "TGenericData.hxx"
#include "TRawChannel.hxx"


/// Class to store information from a single DT5730 channel.
class TDT5730RawChannel: public TRawChannel{

 public:

  /// constructor
  TDT5730RawChannel(int channel, int baseline=-1):
    TRawChannel(channel,0),
    fVerbose(0),
    fBaseline(baseline)
  { }


  void AddSamples(std::vector<double> Samples){
    fWaveform = Samples;
  }


 private:

  //Baseline for this channel
  int fVerbose;
  int fBaseline;


};


/// This class encapsulates the data from a single V1740 board (in a single MIDAS bank).
class TDT5730RawData: public TGenericData {

public:

  /// Constructor
  TDT5730RawData(int bklen, int bktype, const char* name, void *pdata);

  /// Get the number of 32-bit words in bank.
  uint32_t GetEventSize() const {return (fGlobalHeader0 & 0xfffffff);};

  /// Get the channel mask; ie, the set of channels for which we
  /// have data for this event.
  uint32_t GetChannelMask() const {return (fGlobalHeader1 & 0xff);};

  /// Get event counter
  uint32_t GetEventCounter() const {return ((fGlobalHeader2) & 0xffffff);};

  /// Get trigger tag
  uint32_t GetTriggerTag() const {return ((fGlobalHeader3) & 0xffffffff);};


  /// Get Number of channels in this bank.
  int GetNChannels() const {return fMeasurements.size();}

  /// Get Channel Data
  TDT5730RawChannel GetChannelData(int i) {
    if(i >= 0 && i < (int)fMeasurements.size())
      return fMeasurements[i];

    return TDT5730RawChannel(-1);
  }


  void Print();


private:

  /// Helper method to handle uncompressed data.
  void HandlUncompressedData();

  /// The overall global headers
  uint32_t fGlobalHeader0;
  uint32_t fGlobalHeader1;
  uint32_t fGlobalHeader2;
  uint32_t fGlobalHeader3;


  /// Vector of DT5730 measurements
  std::vector<TDT5730RawChannel> fMeasurements;

};

#endif
