/******************************************************************
 *  Hit finder *
 * 
 * A. Capra
 * August 2022
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "baselineflow.hxx"
#include "dsflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <cassert>
#include <iomanip>

#include "hitfinder.hxx"

#include "json.hpp"
using json = nlohmann::json;


class HitFinderFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};


class HitFinderModule: public TARunObject
{
public:
   HitFinderFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors

private:
   int fNtotPulses;

   bool fFixed;

   double fAmplitude;
   uint32_t fMAgate;
   double fMinCharge;

   unsigned fWindow;
   unsigned fNsigma;

   HitFinder hf;

public:
   HitFinderModule(TARunInfo* runinfo, HitFinderFlags* f): TARunObject(runinfo),
                                                           fFlags(f), fCounter(0), fError(0),
                                                           fNtotPulses(0), fAmplitude(1.)
   {
      if(fFlags->fVerbose) std::cout<<"HitFinderModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="HitFinder";
#endif

      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"HitFinderModule Json parsing success!"<<std::endl;
      fin.close();

      try 
         {
            fFixed = settings["Pulse"]["Fixed"].get<bool>(); // whether the hit finder uses a fixed threshold
         } 
      catch(json::type_error& e) 
         {
            fFixed = true; // default methode is "fixed threshold"
            std::cerr<<"HitFinderModule::ctor Using fixed threshold: "<<e.what()<<std::endl;
         }
      if( fFixed )
         {
            try 
               {
                  fAmplitude=settings["Pulse"]["Amplitude"].get<double>(); // fixed threshold
               } 
            catch(json::type_error& e) 
               {
                  fAmplitude = 10;// default fixed threshold
                  std::cerr<<"HitFinderModule::ctor threshold not set using default value: "<<e.what()<<std::endl;
               }
            std::cout<<"HitFinderModule Threshold Amplitude: "<<fAmplitude<<std::endl;
         }
      else
         {
            fWindow = settings["Pulse"]["Pedestal"].get<unsigned>(); // number of sample to calculate the baseline
            std::cout<<"HitFinderModule pedestal length: "<<fWindow<<std::endl;
            fNsigma = settings["Pulse"]["Sigma"].get<unsigned>(); // number of std.dev. for peak detection
            std::cout<<"HitFinderModule N-sigma: "<<fNsigma<<std::endl;
         }
      fMAgate = settings["Pulse"]["MAgate"].get<uint32_t>(); // number of samples for moving average
      std::cout<<"HitFinderModule MA window: "<<fMAgate<<std::endl;
      fMinCharge = settings["Pulse"]["MinCharge"].get<double>(); // charge/amplitude to accept a peak as a hit
      std::cout<<"HitFinderModule Cut on Charge/Prominence: "<<fMinCharge<<std::endl;
 
      // hit finder class
      hf = HitFinder(); // default ctor
      // set common parameters
      hf.SetMAWindow(fMAgate);
      hf.SetMinIntegral(fMinCharge);
      if( fFixed ) // set parameters for fixed-threshold hit-finding
         {
            hf.UnsetDynamicThreshold();
            hf.SetPedestal(fWindow);
            hf.SetNsigma(fNsigma);
         }
      else // set parameters for dynamic-threshold hit-finding
         {
            hf.SetDynamicThreshold();
            hf.SetThreshold(fAmplitude);
         }
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"HitFinderModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"HitFinderModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"HitFinderModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<" Total number of Pulses: "<<fNtotPulses<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      int number_of_channels=0;
#ifndef __PHAAR__
      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
     if( wf_flow ) 
         {
            number_of_channels = wf_flow->GetNumberOfChannels();
            if( fFlags->fVerbose )
               std::cout<<"HitFinderModule::AnalyzeFlowEvent # of ch: "<<number_of_channels
                        <<" with DSProcessor"<<std::endl;
         }
      else
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
#else
      BaselineEventFlow* base_flow = flow->Find<BaselineEventFlow>();
         if( base_flow )
            {
               number_of_channels = base_flow->getNchannels();
               if( fFlags->fVerbose )
                  std::cout<<"HitFinderModule::AnalyzeFlowEvent # of ch: "<<number_of_channels
                        <<" with Base"<<std::endl;
            }
         else
            {
               ++fError;
#ifdef HAVE_MANALYZER_PROFILER
               *flags|=TAFlag_SKIP_PROFILE;
#endif
               return flow;
            }
         DSProcessorFlow* wf_flow = new DSProcessorFlow(flow);
         wf_flow->conf[base_flow->fTypes[0]].fNSamples=base_flow->getNsamples(0);
#endif
 
      double threshold=fAmplitude;
      int npevt=0;
      for(int i=0; i<number_of_channels; ++i)
         {
            const std::vector<double>* wf;
            double rms;
            int Mod=-1,Chan=-1;
            std::string Type="";
#ifndef __PHAAR__
            TDSChannel* dsch = wf_flow->GetDSchan(i);
            if( dsch->filtered_wf.size() > 0 )
               {
                  wf=&dsch->filtered_wf;
                  if( fFlags->fVerbose ) dsch->PrintFiltered();
               }
            else
               {
                  if( fFlags->fVerbose ) dsch->Print();
                  wf=&dsch->unfiltered_wf;
               }
            Mod = dsch->module;
            Chan = dsch->channel;
            Type = dsch->board.c_str();
            if( fFlags->fVerbose )
               std::cout<<"HitFinderModule::AnalyzeFlowEvent (DS) ch: "<<i
                        <<" size: "<<wf->size()<<std::endl;
#else
            wf = base_flow->getChannelVector(i);
            rms = base_flow->getBaselineRMS(i);
            Mod = base_flow->getADCmodule(i);
            Chan = base_flow->getADCchannel(i);
            Type = base_flow->fTypes.at(i).c_str();
            if( fFlags->fVerbose )
               std::cout<<"HitFinderModule::AnalyzeFlowEvent (PHAAR) ch: "<<i
                        <<" size: "<<wf->size()<<std::endl;
#endif
                  
            hf(wf);
            int np = hf.GetNumberOfHits();
            if( fFlags->fVerbose )
               {
                  if( np>0 ) hf.Print();
                  else std::cout<<"No pulses found above threshold: "<<threshold
                               <<" in channel: "<<Chan<<" in module: "<<Mod<<std::endl;
               }

            for(int i=0; i<np; ++i)
               {
                  TDSPulse aPulse(Mod,Chan,Type.c_str());
                  aPulse.charge = static_cast<double>( hf.GetQ(i) );
                  aPulse.time   = static_cast<double>( hf.Gett(i) );
                  // aPulse.height = wf->at(hf.Gett(i));
                  aPulse.height = static_cast<double>( hf.Getk(i) );
                  aPulse.tot    = static_cast<double>( hf.GetDt(i) );
                  if( fFlags->fVerbose ) aPulse.Print();
                  wf_flow->pulses->push_back(aPulse);
               }
#ifndef __PHAAR__
            dsch->peak.assign( hf.GetWF()->begin(), hf.GetWF()->end() );
#endif

            fNtotPulses+=np;
            npevt+=np;
            
            hf.Reset();
         }

      if( fFlags->fVerbose )
         std::cout<<"HitFinderModule::AnalyzeFlowEvent # of pulses: "<<npevt<<std::endl;
      
      flow=wf_flow;
      ++fCounter;
      return flow;
   }
};


class HitFinderModuleFactory: public TAFactory
{
public:
   HitFinderFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("HitFinderModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
   }
   
   void Finish()
   {
      printf("HitFinderModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("HitFinderModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new HitFinderModule(runinfo, &fFlags);
   }

};

static TARegister tar(new HitFinderModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
