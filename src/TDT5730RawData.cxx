#include "TDT5730RawData.hxx"

#include <iomanip>
#include <iostream>

void TDT5730RawData::HandlUncompressedData(){

  //decoder from rootana lib for dt5730

  // Do some sanity checking.
  // Make sure first word has right identifier
  if( (GetData32()[0] & 0xf0000000) != 0xa0000000)
    std::cerr << "First word has wrong identifier; first word = 0x"
	      << std::hex << GetData32()[0] << std::dec << std::endl;

	int counter = 4;
	int number_available_channels = 0;
	for(int ch = 0; ch < 16; ch++){
		if((1<<ch) & GetChannelMask()){
			number_available_channels++;
		}
	}

	int nwords_per_channel = (GetEventSize() - 4)/number_available_channels;

	// Loop over channel data
	for(int ch = 0; ch < 8; ch++){
		if((1<<ch) & GetChannelMask()){
			std::vector<double> Samples;
			for(int i = 0; i < nwords_per_channel; i++){
				double sample = double((GetData32()[counter] & 0x3fff));
				Samples.push_back(sample);
				sample = double((GetData32()[counter] & 0x3fff0000) >> 16);
				Samples.push_back(sample);
				counter++;
			}
			TDT5730RawChannel meas = TDT5730RawChannel(ch);
			meas.AddSamples(Samples);
			fMeasurements.push_back(meas);
		}
	}

}//End HandlUncompressedData


TDT5730RawData::TDT5730RawData(int bklen, int bktype, const char* name, void *pdata):
    TGenericData(bklen, bktype, name, pdata)
{

  fGlobalHeader0 = GetData32()[0];
  fGlobalHeader1 = GetData32()[1];
  fGlobalHeader2 = GetData32()[2];
  fGlobalHeader3 = GetData32()[3];

  HandlUncompressedData();

}

void TDT5730RawData::Print(){

  std::cout << "DT5730 decoder for bank " << GetName().c_str() << std::endl;
  std::cout << "EventSize = "<< GetEventSize()<<"\n";
  std::cout << "ChannelMask = "<< GetChannelMask()<<"\n";
  std::cout << "EventCounter = "<< GetEventCounter()<<"\n";
  std::cout << "TriggerTag = "<< GetTriggerTag()<<"\n";
  std::cout << "NChannels = "<< GetNChannels()<<"\n";


}
