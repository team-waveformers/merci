/******************************************************************
 *  Filtering algorithms *
 * 
 * A. Capra
 * January 2022
 *
 ******************************************************************/


#include <vector>
#include <numeric>
#include <fstream>
#include <string>
#include <cassert>

class Filter
{
public:
   Filter()
   {}

   virtual std::vector<double>* operator()(std::vector<double>* adc_samples)
   {
      Reset(0);
      std::for_each(adc_samples->begin(),adc_samples->end(),
                    [this](double& x) { hit.push_back(x); } );
      return &hit;
   }

public:
   std::vector<double> hit;

   void Reset(int nsamples)
   {
      hit.resize(nsamples);
   }
};

class MAF: public Filter
{
public:
   MAF():Filter(),w(1){} // null filter

   MAF(int k):Filter(),w(k)
   {
      assert(w>0);
      fWindow=1./double(w);
   }

   std::vector<double>* operator()(std::vector<double>* adc_samples)
   {
      int n=adc_samples->size();
      Reset(n);
      double sum=0.;
      for(int i=0; i<=w; ++i)
         {
            sum+=adc_samples->at(i);
            hit[i]=sum/double(i+1);
         }
      for( int i=w+1;i<n;++i )
         {
            hit[i]=hit[i-1]+(adc_samples->at(i)-adc_samples->at(i-w-1))*fWindow;
         }
      return &hit;
   }

   int GetGate() const { return w; }

private:
   int w;
   double fWindow;
};

class EXF: public Filter
{
public:
   EXF():Filter(),w(1),w1(0){} //null filter
   
   EXF(double a):Filter()
   {
      assert((a>0.) && (a<1.));
      w=a;
      w1=1.-w;
   }

   virtual std::vector<double>* operator()(std::vector<double>* adc_samples)
   {
      int n=adc_samples->size();
      Reset(n);
      hit[0]=adc_samples->at(0);
      for(int i=1;i<n;++i)
         {
            hit[i]=w*adc_samples->at(i)+w1*hit[i-1];
         }
      return &hit;
   }

private:
   double w;
   double w1;
};


// this is an experimental filter
class EnF: public Filter
{
public:
   EnF():Filter(),w(1.),w1(0.),m(0) {} //null filter
   
   EnF(double a,int k):Filter(),m(k)
   { 
      assert((a>0.) && (a<1.));
      w=a;
      w1=1.-w;
   }

   std::vector<double>* operator()(std::vector<double>* adc_samples)
   {
      int n=adc_samples->size();
      Reset(n);
      for(int i=0;i<m;++i)
         hit[i]=adc_samples->at(i);
      for(int i=m;i<n;++i)
         {
            hit[i]=w*adc_samples->at(i)+w1*hit[i-m];
         }
      return &hit;
   }

   int GetMemory() const { return m; }

private:
   double w;
   double w1;
   int m;
};


class ARF: public Filter
{
public:
  ARF():Filter(),ftau(0.)
  {
    FixParameters();
  }
   
  ARF(double t):Filter(),ftau(t)
  {
    FixParameters();
  }

  std::vector<double>* operator()(const std::vector<double>* adc_samples)
  {
    Reset(adc_samples->size());

    // prepare AR kernel
    size_t nsamples=adc_samples->size();
    AR.resize(nsamples);
    AR[0]=adc_samples->back();
      
    size_t i;
    // Auto-Recursive filtering for the exponential tail
    for( i=1; i<nsamples; ++i)
      {
         AR[i] = adc_samples->at( nsamples-1-i ) + _a*AR[i - 1];
      }
    
    // create output
    for(i = 0; i<nsamples; ++i)
      {
         hit[i]= AR[nsamples-1-i] * _tau ;
      }
    return &hit;
  }

private:
  void FixParameters()
  {
    _tau =  1./ftau;
    _a = 1. - _tau;
  }

  
private:
  // filter parameters
  double ftau;

  // convenience variables
  double _a;
  double _tau;

  // Auto-Recursive vector
  std::vector<double> AR;
};

class ARMA: public Filter
{
public:
  ARMA():Filter(),ftau(0.),fsigma(0.),ffp_exp_ratio(0.)
  {
    kgsize=0;
    FixParameters();
  }
   
  ARMA(double t, double s, double k):Filter(),ftau(t),fsigma(s),ffp_exp_ratio(k)
  {
    gen_gaus_kernel();
    if( !(kgsize > 0) ) std::cerr<<"ARMA ctor: something wrong with the Gaussian kernel"<<std::endl;
    FixParameters();
  }

  std::vector<double>* operator()(const std::vector<double>* adc_samples)
  {
    Reset(adc_samples->size());
 
    // prepare filter kernels
    size_t nsamples=adc_samples->size();
    MA.resize(nsamples);
    AR.resize(nsamples);
    AR[0]=adc_samples->back();
      
    size_t i, j;
    double tmp;
    for( i=0; i<nsamples; ++i)
      {
	if( (int(i - ceiled) >= 0) && (i + halfsize < nsamples) )  // take care of waveform edges
	  {
	    tmp = 0.; // zero partial summation 
	    for(j=0; j<kgsize; ++j) // cross-correlation filtering for the fast peak
	      {
		tmp += adc_samples->at( i + halfsize - j ) * gker[j];
	      }
	    MA[i] = tmp; 
	  }
	else
	  MA[i] = 0.;

	// Auto-Recursive filtering for the exponential tail
	if( i ) AR[i] = adc_samples->at(nsamples-1-i) + _a*AR[i - 1];
      }

    // ARMA: put together
    for(i = 0; i<nsamples; ++i)
      {
	hit[i] = _scale_factor * AR[nsamples-1-i] * _tau  + scale_factor * MA[i] ;
      }
    return &hit;
  }

  virtual inline void Reset(size_t nsamples)
  {
    if( nsamples <= kgsize )
      std::cerr<<"ARMA Reset: Number of samples: "<<nsamples
	       <<" smaller than the Gaussian kernel size: "<<kgsize<<std::endl;
    hit.resize(nsamples);
  }

  inline void SetScaleFactor(double f) { scale_factor = f; _scale_factor = 1. - scale_factor; }


private:
  void gen_gaus_kernel()
  {
    gker.clear();
    kgsize = static_cast<int>(8*fsigma);
    for(size_t i=0; i<kgsize; ++i)
      gker.push_back(exp( -0.5*pow( (i-4.*fsigma)/fsigma,2) ) / (fsigma*sqrt(2*M_PI)) );
  }
  
  void FixParameters()
  {
    ceiled = ceil(kgsize / 2 ) + 1;
    halfsize = kgsize / 2;
    
    scale_factor = ffp_exp_ratio/ftau*(sqrt(2*M_PI)*fsigma);
    _scale_factor = 1. - scale_factor;
    _tau =  1./ftau;
    _a = 1. - _tau;
  }

  
private:
  // filter parameters
  double ftau; // in samples
  double fsigma; // in samples
  double ffp_exp_ratio;

  // convenience variables
  double _a;
  double _tau; // 1/tau : 1/samples

  // combine (1-scale_factor)*AR+scale_factor*MA
  double scale_factor;
  double _scale_factor;

  // gaussian kernel vector
  std::vector<double> gker;
  size_t kgsize;
  size_t ceiled;
  size_t halfsize;

  // finite response vector
  std::vector<double> MA;
  // Auto-Recursive vector
  std::vector<double> AR;
};

class FIR: public Filter
{
public:
   FIR():Filter(),fNcoeff(0),fNorm(0){}

   FIR(unsigned int n, double* c):Filter(),fNcoeff(n),fCoeff(c,c+n/sizeof(double) )
   {
      fNorm=0.;
      for(unsigned i=0; i<n; ++n) fNorm+=c[i];
   }

   FIR(std::vector<double> c):Filter(),fCoeff(c)
   {
      fNcoeff=fCoeff.size();
      fNorm=std::accumulate(c.begin(),c.end(),double(0));
   }

   FIR(std::string fname,bool norm=false):Filter(),fNorm(1.) // Cross-Correlation Filter
   {
      if( fname != "" )
         CoeffLoader(fname);
      else
         CoeffLoader();
      fNcoeff=fCoeff.size();
      if( norm ) fNorm=static_cast<double>(fNcoeff);
   }

   virtual std::vector<double>* operator()(const std::vector<double>* adc_samples)
   {
      size_t n=adc_samples->size();
      Reset(n);
      double sum=0.;
      for(size_t i=0;i<=fNcoeff;++i)
         {
            sum+=adc_samples->at(i);
            hit[i]=sum/double(i+1);
         }
 
      auto it=adc_samples->begin();
      for(size_t i=fNcoeff+1;i<n;++i)
         {
            hit[i]=std::inner_product(fCoeff.begin(),fCoeff.end(),it,double(0))/fNorm;
            ++it;
         }
      
      return &hit;
   }

   void SetODBcoefficients(std::vector<int> c)
   {
      fCoeff.clear();
      for( auto& a : c )
         {
            fCoeff.push_back( static_cast<double>(a) );
         }
      fNcoeff=fCoeff.size();
      fNorm=std::accumulate(fCoeff.begin(),fCoeff.end(),double(0));
   }

   void Print()
   {
      std::cout<<"FIR coefficients: ";
      for( auto& a : fCoeff )
         {
            std::cout<<a<<" ";
         }
      std::cout<<"\n";
   }

   int GetNumberOfCoefficients() const { return fNcoeff; }

private:
   void CoeffLoader(std::string fname="../dsadc/templates/Normalized_Template_CH0_PDUplus.dat")
   {
      std::ifstream fin(fname);
      assert(fin.is_open());
      std::cout<<"FIR template file: "<<fname<<std::endl;
      double y;
      while(fin>>y)
         {
            fCoeff.push_back(y);
         }
      fin.close();
   }

private:
   size_t fNcoeff;
   std::vector<double> fCoeff;
   double fNorm;
};

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
