/******************************************************************
 *  Hit Finder algorithms *
 * 
 * A. Capra
 * August 2021 - 1st version
 * April 2022  - new hit finder (pyreco-style)
 * August 2022 - this version
 *
 ******************************************************************/


// *****************************************************************
// * dsana-style finder *
// *
// * Time-Over-Threshold algorithm
// * 
// *****************************************************************

class PulseFinder
{
private:
   int fNpulses;
   double fThreshold;
   std::vector<int> fPulseStart; // time of pulse crossing thr
   std::vector<int> fPulseEnd;   // time of pulse no longer over thr
   std::vector<int> fPeakTime;   // time of max p.h.
   std::vector<double> fAveragePeakTime; // time of max p.h. smoothed-out
   std::vector<double> fAmplitude; // pulse height - peak
   std::vector<double> fCharge; // integral over fixed range
   std::vector<int> fDuration; // time over threshold
   int fRejectShort; // cut on time over threshold

   // charge integration over a fixed
   // range around the pulse peak
   int fIntBefore; // samples before peak pulse 
   int fIntAfter;  // samples after peak pulse
   int fIntTotal;  // total number of samples in the integration

   // calculate weighted average
   // time around peak height
   int fTimeAverageBefore; // samples before peak
   int fTimeAverageAfter;  // samples after peak
   int fTimeAverageTotal;  // total number of samples in average
   
public:
   PulseFinder():fNpulses(0),fThreshold(0),
                 fRejectShort(1),
                 fIntBefore(100),fIntTotal(250),
                 fTimeAverageBefore(5),fTimeAverageAfter(6)
   {
      fIntAfter=fIntTotal-fIntBefore;
      fTimeAverageTotal=fTimeAverageBefore+fTimeAverageAfter;
   }
   PulseFinder(double& s, double& n):fNpulses(0),fThreshold(s*n),
                                     fRejectShort(1),
                                     fIntBefore(100),fIntTotal(250),
                                     fTimeAverageBefore(5),fTimeAverageAfter(6)
   {
      fIntAfter=fIntTotal-fIntBefore;
      fTimeAverageTotal=fTimeAverageBefore+fTimeAverageAfter;
   }
   PulseFinder(double& t):fNpulses(0),fThreshold(t),
                          fRejectShort(1),
                          fIntBefore(100),fIntTotal(250),
                          fTimeAverageBefore(5),fTimeAverageAfter(6)
   {
      fIntAfter=fIntTotal-fIntBefore;
      fTimeAverageTotal=fTimeAverageBefore+fTimeAverageAfter;
   }

   inline int GetStartTime(int i) const { return fPulseStart.at(i); }
   inline int GetPeakTime(int i) const { return fPeakTime.at(i); }
   inline double GetAveragePeakTime(int i) const { return fAveragePeakTime.at(i); }
   inline double GetAmplitude(int i) const { return fAmplitude.at(i); }
   inline double GetCharge(int i) const { return fCharge.at(i); }
   inline int GetTimeOverThreshold(int i) const { return fDuration.at(i); }

   inline void SetThreshold(double t) { fThreshold=t; }
   inline void SetMinimumDuration(int d) { fRejectShort=d; }
   inline void SetIntegrationWindow(int before, int tot)
   {
      fIntBefore=before;fIntTotal=tot;
      fIntAfter=fIntTotal-fIntBefore;
      assert(fIntAfter>0);
   }

   inline void Reset()
   {
      fNpulses=0;
      fPulseStart.clear();
      fPulseEnd.clear();
      fPeakTime.clear();
      fAveragePeakTime.clear();
      fAmplitude.clear();
      fCharge.clear();
      fDuration.clear();
   }
   
   int operator()(const std::vector<double>* adc_samples)
   {
      std::vector<double> abs_adc_samples(adc_samples->size());
      // Here I assume that all the pulses are negative this to
      // simplify the algorithm, I flip the waveform
      // One could capture a parameter in the lambda-function
      // to make change this at, e.g., runtime.
      std::transform(adc_samples->begin(),adc_samples->end(),
                     abs_adc_samples.begin(),
                     [](double y){return fabs(y);});
      
      std::vector<double>::iterator it = abs_adc_samples.begin();
      const double thr=fThreshold;
      while(true)
         {
            // find where waveform is above thr
            it = std::find_if( it, abs_adc_samples.end(),
                               [thr](double const& r)
                               { return (r>thr); } );
            // if it's never above thr, quit
            if( it==abs_adc_samples.end() ) break;
            int iStart = (int) std::distance(abs_adc_samples.begin(),it);
            fPulseStart.push_back( iStart );
            // std::cout<<"\t"<<iStart;

            // find where waveform goes back below thr
            it = std::find_if( it, abs_adc_samples.end(),
                               [thr](double const& r)
                               { return (r<thr); } );
            int iEnd = (int) std::distance(abs_adc_samples.begin(),it);
            // std::cout<<" "<<iEnd;

            // if the pulse is too short, might just a fluke
            // skip
            int duration = iEnd-iStart;
            if( duration < fRejectShort )
               {
                  fPulseStart.pop_back();
                  ++it;
                  //  std::cout<<" too short"<<std::endl;
                  continue;
               }
            fPulseEnd.push_back(iEnd);
            fDuration.push_back(duration);

            // find peak pulse
            std::vector<double>::iterator max_pos = std::max_element(abs_adc_samples.begin()+iStart,
                                                                     abs_adc_samples.begin()+iEnd);
            int max_timebin=(int)std::distance(abs_adc_samples.begin(),max_pos);
            // std::cout<<" "<<max_timebin<<std::endl;
            fPeakTime.push_back(max_timebin);
            double a = *max_pos; fAmplitude.push_back(a);
            // std::cout<<" "<<a;

            // calculate weighted average time around peak height
            double t0 = (double) max_timebin;
            if( (max_timebin+fTimeAverageAfter) < (int) abs_adc_samples.size() &&
                (max_timebin-fTimeAverageBefore) >= 0 )
               {
                  std::vector<double> time( fTimeAverageTotal ); // prepare time axis
                  std::iota(time.begin(),time.end(),
                            max_timebin-fTimeAverageBefore); // fill-in the timebin
                  double w=std::accumulate(max_pos-fTimeAverageBefore, 
                                           max_pos+fTimeAverageAfter,double(0));
                  // weighted average of time
                  t0 = std::inner_product( max_pos-fTimeAverageBefore, 
                                           max_pos+fTimeAverageAfter,
                                           time.begin(), double(0) ) / w;
               }
            fAveragePeakTime.push_back(t0);
            
            // charge integration over a fixed range
            // around the pulse peak
            int maxn=(int)std::distance(abs_adc_samples.begin(),max_pos);
            int bef=maxn-fIntBefore;
            if( bef<0 ) bef=0;
            int aft=maxn+fIntAfter;
            if( aft>int(abs_adc_samples.size()) )
               aft=int(abs_adc_samples.size());
            double q = std::accumulate(abs_adc_samples.begin()+bef,
                                       abs_adc_samples.begin()+aft,0.0);
            fCharge.push_back(q);
            //std::cout<<" "<<q<<std::endl;
            
            ++fNpulses;
            ++it;
         }
      return (int)fAmplitude.size();
   }

   void print(bool all=true) const
   {
      std::cout<<"Pulse Finder found "<<fNpulses
               <<" pulse(s) above threshold: "<<fThreshold<<std::endl;
      if( !all ) return;
      for(size_t i=0;i<fAmplitude.size();++i)
         {
            std::cout<<std::setw(3)<<i<<") at ["<<std::setw(6)<<fPulseStart.at(i)
                     <<","<<std::setw(6)<<fPulseEnd.at(i)
                     <<"] = "<<std::setw(4)<<fPulseEnd.at(i)-fPulseStart.at(i)
                     <<" samples\tamplitude: "<<std::setw(6)<<fAmplitude.at(i)
                     <<" charge: "<<std::setw(6)<<fCharge.at(i)
                     <<" Time over Thr: "<<std::setw(6)<<fDuration.at(i)<<" samples"<<std::endl;
         }
   }

};

// *****************************************************************
// *****************************************************************
// * pyreco-style hit finder
// * 
// * Requires input signal filtered with Auto-Recursive method
// * Example: ARMA filter, see filter.hxx
// * 
// *****************************************************************

class HitFinder
{
public:
   HitFinder():fWindow(100),fPedestal(600),
	       fThreshold(10.),fNsigma(3),fMinIntegral(100.),
	       fDynamicThreshold(true)
   {
      fWd = static_cast<double>(fWindow);
      fHalfWindow = fWindow/2;
      Reset();
      std::cout<<"PeakFinder default ctor"<<std::endl;
   }
  
   HitFinder(uint32_t w, double thr, double mq):fWindow(w),fPedestal(0),
						fThreshold(thr),fNsigma(0),fMinIntegral(mq),
						fDynamicThreshold(false)
   {
      fWd = static_cast<double>(fWindow);
      fHalfWindow = fWindow/2;
      Reset();
      std::cout<<"PeakFinder with fixed threshold"<<std::endl;
   }

   HitFinder(uint32_t w, uint32_t bl, uint16_t ns, double mq):fWindow(w),fPedestal(bl),
							      fNsigma(ns),fMinIntegral(mq),
							      fDynamicThreshold(true)
   {
      fThreshold = 0;
      fWd = static_cast<double>(fWindow);
      fHalfWindow = fWindow/2;
      Reset();
      std::cout<<"PeakFinder with dynamic threshold"<<std::endl;
   }


   void moving_average_subtraction(const std::vector<double>* adc_samples)
   {
      if( adc_samples->size() < fWindow ) return;
      fOut.resize( adc_samples->size() );

      // partial sum
      double asum = 0.;
      // indexes
      size_t i=0,j=1;
    
      // first point
      for( ; i<fHalfWindow; ++i) asum += static_cast<double>(2*adc_samples->at(i));
      fOut[0] = static_cast<double>(adc_samples->at(0)) - asum/fWd;

      // first half window
      for( i=1; i<=fHalfWindow; ++i )
         {
            asum += static_cast<double>(adc_samples->at( i + fHalfWindow - 1 ) - adc_samples->at( fHalfWindow - i ));
            fOut[i] = static_cast<double>(adc_samples->at(i)) - asum/fWd;
         }

      // first from half window to number of samples minus half window
      for( i=fHalfWindow+1; i<=adc_samples->size()-fHalfWindow; ++i )
         {
            asum += static_cast<double>(adc_samples->at( i + fHalfWindow - 1 ) - adc_samples->at( i - fHalfWindow - 1 ));
            fOut[i] = static_cast<double>(adc_samples->at(i)) - asum/fWd;
         }

      // from number of samples minus half window to number of samples
      for( i=adc_samples->size()-fHalfWindow+1; i<adc_samples->size(); ++i )
         {
            asum += static_cast<double>(adc_samples->at( adc_samples->size() - j++ ) - adc_samples->at( i - fHalfWindow - 1 ));
            fOut[i] = static_cast<double>(adc_samples->at(i)) - asum/fWd;
         }
   }

   void peak_finder()
   {
      auto it = fOut.begin();
      double thr=fThreshold;
      while(true)
         {
            // find where waveform is above thr
            it = std::find_if( it, fOut.end(),
                               [thr](double const& r)
                               { return (r>thr); } );
            // if it's never above thr, quit
            if( it==fOut.end() ) break;
	
            uint32_t iStart = std::distance(fOut.begin(),it);
            //std::cout<<"\ts: "<<iStart;

            // find where waveform goes back below thr
            it = std::find_if( it, fOut.end(),
                               [thr](double const& r)
                               { return (r<thr); } );
            uint32_t iEnd = std::distance(fOut.begin(),it);
            //std::cout<<" e: "<<iEnd;

            // find peak pulse
            std::vector<double>::iterator max_pos = std::max_element(fOut.begin()+iStart,
                                                                     fOut.begin()+iEnd);
            double A = *max_pos;
            //std::cout<<" A: "<<A<<" = "<<static_cast<uint16_t>(A);
	
            double Q = std::accumulate(fOut.begin()+iStart,fOut.begin()+iEnd,double(0));
            //std::cout<<" Q: "<<Q<<"\tcut: "<<Q/A;
            if( Q/A < fMinIntegral )
               {
                  ++it;
                  //std::cout<<" too small"<<std::endl;
                  continue;
               }
            //else std::cout<<"\n";

            // peak-width
            uint32_t duration = static_cast<uint32_t>(iEnd-iStart);
            // peak time (sample)
            uint32_t smax=std::distance(fOut.begin(),max_pos);
            fHits.emplace_back(static_cast<uint16_t>(A),smax,duration,static_cast<int32_t>(Q));
            ++it;
         }
   }

   void ComputeThreshold()
   {
      uint32_t len = fPedestal;
      if( fPedestal >= fOut.size() ) len = fOut.size()-1;
      double den = static_cast<double>(len);
      // calculate the baseline and its rms of the filtered waveform
      double bline = std::accumulate(fOut.begin(),
                                     fOut.begin()+len,
                                     double(0))/den;
      double rms = std::inner_product(fOut.begin(),
                                      fOut.begin()+len,
                                      fOut.begin(),double(0));
      // rms = sqrt( (rms / den) - (bline * bline) );
      double m2 = bline * bline / den;
      rms = sqrt( (rms - m2) / den );
      fThreshold = bline + fNsigma * rms;
      //std::cout<<"Baseline: "<<bline<<"  RMS:"<<rms<<"  Threshold: "<<fThreshold<<std::endl;
   }

   void operator()(const std::vector<double>* adc_samples)
   {
      moving_average_subtraction( adc_samples );
      if( fDynamicThreshold ) ComputeThreshold();
      peak_finder();
   }

   // setters
   inline void SetMAWindow(unsigned w)    { fWindow=w; 
                                            fWd = static_cast<double>(fWindow);
                                            fHalfWindow=fWindow/2; }
   inline void SetPedestal(unsigned p)    { fPedestal=p;}
   inline void SetThreshold(double t)     { fThreshold=t; }
   inline void SetNsigma(unsigned n)      { fNsigma=n; }
   inline void SetMinIntegral(double q)   { fMinIntegral=q; }
   inline void SetDynamicThreshold()      { fDynamicThreshold=true; }
   inline void UnsetDynamicThreshold()    { fDynamicThreshold=false; }

   // getters
   inline const std::vector<double>* GetWF() const { return &fOut; }

   struct SingleHit
   {
      uint16_t k;
      uint32_t t;
      uint32_t Dt;
      int32_t Q;
    
      SingleHit(uint16_t a, uint32_t b, uint32_t c):k(a),t(b),Dt(c),Q(0)
      {}
    
      SingleHit(uint16_t a, uint32_t b, uint32_t c, int32_t d):k(a),t(b),Dt(c),Q(d)
      {}
   };

   // getters
   inline uint16_t Getk(size_t i)  const { return fHits[i].k; }
   inline uint32_t Gett(size_t i)  const { return fHits[i].t; }
   inline uint32_t GetDt(size_t i) const { return fHits[i].Dt; }
   inline int32_t GetQ(size_t i)   const { return fHits[i].Q; }
   inline size_t GetNumberOfHits() const { return fHits.size(); }

   void Reset()
   {
      fHits.clear();
   }

   void Print()
   {
      for(auto& h: fHits)
         std::cout<<"k: "<<h.k<<" t: "<<h.t<<" Dt: "<<h.Dt<<" Q: "<<h.Q<<"\tcut: "<<h.Q/h.k<<std::endl;
   }

private:
   uint32_t fWindow;
   uint32_t fHalfWindow;
   double fWd;
   uint32_t fPedestal;
   double fThreshold;
   uint16_t fNsigma;
   double fMinIntegral;
   bool fDynamicThreshold;
   std::vector<double> fOut;
   std::vector<SingleHit> fHits;
};


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
