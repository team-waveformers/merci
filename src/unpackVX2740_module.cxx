/******************************************************************
 * Unpack CAEN VX2740 data *
 * 
 * A. Capra
 * August 2021
 *
******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "adcflow.hxx"
#include "TVX2740RawData.hxx"

#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>

//Read preprocessor flag value as string literal
#define STRING(s) #s
#define STRSTRING(s) STRING(s)

#include "json.hpp"
using json = nlohmann::json;


class UnpackVXFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
   int fSkipEvents=-1;
};


class UnpackVXModule: public TARunObject
{
public:
   UnpackVXFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors
   const int numberChannelPerModule = 64;
   const double NanosecsPerSample = 8.0; //ADC clock at 125Mhz = 8 nsecs
   int fTotNumBoards=1;
   int fNumberSamples;
   bool isVX2745=false;

public:
   UnpackVXModule(TARunInfo* runinfo, UnpackVXFlags* f): TARunObject(runinfo),
                                                         fFlags(f), fCounter(0), fError(0),
                                                         fNumberSamples(-1)
   {
      if(fFlags->fVerbose) std::cout<<"UnpackVXModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="UnpackVX";
#endif
      std::string config_path;
#ifdef CONFIG_PATH
      config_path = STRSTRING(CONFIG_PATH) + fFlags->fConfigName;//Read config file path from CMAKE source dir (lolx)
#else
      config_path = fFlags->fConfigName;//If path isn't defined look in flags only
#endif
      std::cout << "Reading config file from path = "<< config_path << "\n";
      std::ifstream fin(config_path);
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"UnpackVX2740 Json parsing success!"<<std::endl;
      fin.close();

      fTotNumBoards=settings["ADC"]["Number of VX2740"].get<int>();
      std::cout<<"Total number of VX2740 boards for run "<<runinfo->fRunNo
               <<" is "<<fTotNumBoards<<std::endl;
      if( fTotNumBoards==0 )
         {
            try
               {
                  fTotNumBoards=settings["ADC"]["Number of VX2745"].get<int>();
                  std::cout<<"Total number of VX2745 boards for run "<<runinfo->fRunNo
                           <<" is "<<fTotNumBoards<<std::endl;
                  if( fTotNumBoards > 0 ) isVX2745=true;
               }
            catch(json::type_error& e)
               {
                  if( fFlags->fVerbose )
                     std::cout<<"No VX2745: why should be?"<<std::endl;
               }
         }
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"UnpackVXModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"UnpackVXModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"UnpackVXModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

   TAFlowEvent* Analyze(TARunInfo* /*runinfo*/, TMEvent* midasevent,
                        TAFlags* /*flags*/, TAFlowEvent* flow)
   {
      if( midasevent->error ) // skip events with errors
         {
            ++fError;
            return flow;
         }

      // allow the user to fast forward to the desired event
      if( fCounter < fFlags->fSkipEvents)
         {
            if( fFlags->fVerbose )
                std::cout<<"UnpackVX2740Module Skip Event: "<<fCounter<<std::endl;
            ++fCounter;
            return flow;
         }

      int fe_idx = midasevent->trigger_mask;
      if( fFlags->fVerbose )
         std::cout<<"UnpackVX2740Module::Analyze ID: "<<midasevent->event_id
                  <<" S/N: "<<midasevent->serial_number
                  <<" TS: "<<midasevent->time_stamp
                  <<" Trig mask: "<<fe_idx<<std::endl;

      AdcEventFlow* eflow = flow->Find<AdcEventFlow>();
      if( !eflow )
         {
            eflow = new AdcEventFlow(flow, midasevent->time_stamp, fCounter);
            flow=eflow;
         }

      for( int board_idx = 0; board_idx<fTotNumBoards; ++board_idx )
         {
            std::stringstream ss;
            ss<<"D"<<std::setfill('0')<<std::setw(3)<<std::to_string(board_idx);
            std::string bankname=ss.str();
            TMBank* bank = midasevent->FindBank(bankname.c_str());
            if( !bank ) continue;
            //int bid = std::stoi(bank->name+1);
            if( fFlags->fVerbose )
               std::cout<<"UnpackVX2740Module::Analyze VX2740 bank "<<bankname<<" found... Board: "<<board_idx<<std::endl;
            TVX2740RawData* raw = new TVX2740RawData((int)bank->data_size,
                                                     (int)bank->type,
                                                     bank->name.c_str(),
                                                     midasevent->GetBankData(bank));
            if( raw->GetHeader().format != 0x10 )
               {
                  if( fFlags->fVerbose )
                     std::cout<<"UnpackVX2740Module::Analyze Not scope-data format"<<std::endl;
                  continue;
               }
            if( fFlags->fVerbose )
               raw->Print();

            eflow->fEventCounters[board_idx] = raw->GetHeader().event_counter;
            eflow->fTriggerTime[board_idx] = raw->GetTriggerTimeStamp();

            if( fFlags->fVerbose )
               std::cout<<"UnpackVXModule::Analyze VX2740 ETTT: "<<eflow->fTriggerTime[board_idx]
                        <<" s\tEvent Counter: "<<eflow->fEventCounters[board_idx]<<std::endl;

            // loop over channels
            for( int chan = 0; chan < raw->GetNChannels(); ++chan )
               {
                  TVX2740RawChannel* channelData = raw->GetChannelData(chan);
                  if( channelData->IsEmpty() ) continue;
                  if( !IsValid(channelData) )
                     {
                        delete raw;
                        std::cerr<<"UnpackADCModule::Analyze VX2740 SKIPPING MIDAS EVENT S/N "<<midasevent->serial_number<<std::endl;
                        ++fError;
                        return flow;
                     }
                  channelData->SetBoardNumber(board_idx);
                  channelData->SetIndex(board_idx*numberChannelPerModule+chan);
                  if( fFlags->fVerbose ) channelData->Print();
                  if( isVX2745 ) eflow->add(channelData,"VX2745");
                  else eflow->add(channelData,"VX2740");
               } // loop over channels
            delete raw;
         }// loop over boards

      if( fFlags->fVerbose )
         std::cout<<"UnpackVXModule::Analyze Event: "<<fCounter
                  <<" # of channels: "<<eflow->getNchannels()<<std::endl;
      ++fCounter;
      return flow;
   }// analyze

   bool IsValid(TVX2740RawChannel* ch)
   {
      if( fNumberSamples < 0 )
         {
            fNumberSamples = ch->GetNSamples();
         }

      if( fNumberSamples != ch->GetNSamples() )
         {
            std::cerr<<"UnpackVXModule::IsValid ERROR: number of samples changed from "
                     <<fNumberSamples<<" to "<<ch->GetNSamples()<<std::endl;
            return false;           
         }
      return true;
   }
};


class UnpackVXModuleFactory: public TAFactory
{
public:
   UnpackVXFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      printf("UnpackVXModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
            if( args[i] == "--skip-events" ||
                args[i] == "-s" )
               fFlags.fSkipEvents=std::stoi(args[i+1]);
         }
   }

   void Finish()
   {
      printf("UnpackVXModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("UnpackVXModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new UnpackVXModule(runinfo, &fFlags);
   }

};

static TARegister tar(new UnpackVXModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
