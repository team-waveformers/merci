/******************************************************************
 *  Binary Search of Pulses  *
 * 
 * A. Capra
 * November 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "baselineflow.hxx"
#include "phaarflow.hxx"
#include "phaar_data.hxx"

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#include "json.hpp"
using json = nlohmann::json;

class BinarySearchPulseFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};


class BinarySearchPulseModule: public TARunObject
{
public:
   BinarySearchPulseFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors

private:
   double fAmplitude;
   int fDuration;
   std::vector<int> bin;
   std::vector<double> amplitude;


public:
   BinarySearchPulseModule(TARunInfo* runinfo, BinarySearchPulseFlags* f): TARunObject(runinfo),
                                                         fFlags(f),fCounter(0),fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"BinarySearchPulseModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="BinarySearchPulse";
#endif

      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"BinarySearchPulseModule Json parsing success!"<<std::endl;
      fin.close();

      fAmplitude=settings["Pulse"]["Amplitude"].get<double>();
      fDuration=settings["Pulse"]["Duration"].get<int>();
   }

   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"BinarySearchPulseModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"BinarySearchPulseModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"BinarySearchPulseModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      BaselineEventFlow* base_flow = flow->Find<BaselineEventFlow>();
      if( !base_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      PHAARflow* fflow = new PHAARflow(flow);
      for(int iChan=0; iChan<base_flow->getNchannels(); ++iChan)
         {
            const std::vector<double>* wf = base_flow->getChannelVector(iChan);
            std::cout<<"BinarySearchPulseModule::AnalyzeFlowEvent ch: "<<iChan
                     <<" size: "<<wf->size()<<std::endl;
            bin.clear(); amplitude.clear();
            BinarySearch(wf->begin(),wf->end());
            for(size_t i=0; i<bin.size(); ++i)
               {
                  fflow->pulses->emplace_back(amplitude.at(i),bin.at(i),iChan);
               }
         }
      flow=fflow;
      ++fCounter;
      return flow;
   }

   void BinarySearch(std::vector<double>::const_iterator start, 
                     std::vector<double>::const_iterator stop)
   {
      auto t0 = std::max_element(start,stop);
      if( t0 == stop ) return;
      double y0 = *t0;
      if( y0 > fAmplitude )
         {
            int b = (int) std::distance(start,t0);
            std::cout<<"BinarySearch("<<*start<<","<<*stop<<") bin: "<<b<<" amp: "<<y0<<std::endl;
            bin.push_back(b);
            amplitude.push_back(y0);
            BinarySearch(start,t0-5);
            BinarySearch(t0+fDuration,stop);
         }
      return;
   }
							    
};


class BinarySearchPulseModuleFactory: public TAFactory
{
 public:
   BinarySearchPulseFlags fFlags;
   
 public:
   void Init(const std::vector<std::string> &args)
   {
      printf("BinarySearchPulseModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
   }
   
   void Finish()
   {
      printf("BinarySearchPulseModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("BinarySearchPulseModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new BinarySearchPulseModule(runinfo, &fFlags);
   }

};

static TARegister tar(new BinarySearchPulseModuleFactory);


/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
