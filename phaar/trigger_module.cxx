/******************************************************************
 *  Trigger Channel Analysis *
 * 
 * A. Capra
 * May 2022
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "baselineflow.hxx"
#include "dsflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <fstream>
#include <cmath>
#include <cassert>

#include "json.hpp"
using json = nlohmann::json;


class TriggerFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};
  

class TriggerModule: public TARunObject
{
public:
   TriggerFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors

   int fTrigChan;
   double fTrigThrs;
   int fTrigWdth;

public:
   TriggerModule(TARunInfo* runinfo, TriggerFlags* f): TARunObject(runinfo),
                                                   fFlags(f), fCounter(0), fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"TriggerModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Trigger";
#endif
      
      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"TriggerModule Json parsing success!"<<std::endl;
      fin.close();

      fTrigChan = settings["Laser"]["Trigger Channel"].get<int>();
      fTrigThrs = settings["Laser"]["Trigger Threshold"].get<double>();
      fTrigWdth = settings["Laser"]["Trigger Width"].get<int>();
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"TriggerModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"TriggerModule::BeginRun() run: "<<runinfo->fRunNo
                  <<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"TriggerModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      BaselineEventFlow* base_flow = flow->Find<BaselineEventFlow>();
      if( !base_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      // get the number of channels per ADC board
      int ADCchan = wf_flow->conf[base_flow->fTypes[0]].fNumberChannelPerModule;
      int Mod, Chan, ChannelIndex;
 
      std::vector<double> fTrigTime; // average time of trg
      const double thr=fTrigThrs;
      for(int i=0; i<base_flow->getNchannels(); ++i)
         {
            Mod          = base_flow->getADCmodule(i);
            Chan         = base_flow->getADCchannel(i);
            ChannelIndex = Mod * ADCchan + Chan;
            if( ChannelIndex != fTrigChan ) continue;
            
            const std::vector<double>* wf = base_flow->getChannelVector(i);
            if( fFlags->fVerbose )
               std::cout<<"TriggerModule::AnalyzeFlowEvent number "<<base_flow->getEventNumber()
                        <<" Trigger found at ch: "<<i<<" size: "<<wf->size()<<std::endl;

            std::vector<double>::const_iterator it = wf->begin();
            while(true)
               {
                  // find where waveform is above thr
                  it = std::find_if( it, wf->end(),
                                     [thr](double const& r)
                                     { return (r>thr); } );
                
                  // if it's never above thr, quit
                  if( it==wf->end() ) break;

                  // time of the crossing
                  int iPos = (int) std::distance(wf->begin(),it);
                   
                  // calculate weighted average time around peak height
                  std::vector<double> time( fTrigWdth ); // prepare time axis
                  std::iota(time.begin(), time.end(), iPos); // fill-in the timebin
                  double w = std::accumulate(it,it+fTrigWdth,double(0)); // weights
                  // weighted average of time
                  double t0 = std::inner_product( it, it+fTrigWdth, time.begin(), double(0) ) / w;
                  fTrigTime.push_back(t0);

                  TDSPulse aPulse(Mod,Chan,base_flow->fTypes.at(i).c_str());
                  aPulse.time = t0;
                  aPulse.height =* std::max_element(it,it+fTrigWdth);
                  aPulse.tot = fTrigWdth;
                  aPulse.charge = fTrigWdth*fTrigThrs*10.;

                  if( fFlags->fVerbose ) aPulse.Print();
                  wf_flow->pulses->push_back(aPulse);

                  it += (fTrigWdth+1);
               }
         }

      if( fFlags->fVerbose )
         {
            if( fTrigTime.size() > 1 )
               {
                  std::cout<<"TriggerModule::AnalyzeFlowEvent Trigger frequency: ";
                  std::vector<double> dffs; dffs.resize(fTrigTime.size());
                  std::adjacent_difference(fTrigTime.begin(),fTrigTime.end(),dffs.begin());
                  double period = std::accumulate(dffs.begin()+1,dffs.end(),double(0))/double(dffs.size()-1);
                  period *= wf_flow->conf[base_flow->fTypes[0]].fNanosecsPerSample;
                  double freq = 1e-3/(period*1e-9); // ns^-1 -> kHz
                  std::cout<<freq<<" kHz  (number of pulses: "<<fTrigTime.size()<<")"<<std::endl;
               }
            else if( fTrigTime.size() == 1 )
               {
                  std::cout<<"TriggerModule::AnalyzeFlowEvent Trigger found at "<<fTrigTime[0]<<" samples"<<std::endl;
               }
            else
               {
                  std::cout<<"TriggerModule::AnalyzeFlowEvent NO Trigger FOUND"<<std::endl;
               }
         }
      
      flow = wf_flow;
      ++fCounter;
      return flow;
   }
};


class TriggerModuleFactory: public TAFactory
{
public:
   TriggerFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("TriggerModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
      
   }
   
   void Finish()
   {
      printf("TriggerModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("TriggerModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new TriggerModule(runinfo, &fFlags);
   }

};

static TARegister tar(new TriggerModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
