/******************************************************************
 *  TTree Writer *
 * 
 * A. Capra
 * August 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "adcflow.hxx"
#include "dsflow.hxx"
#include "baselineflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <fstream>

#include "TTree.h"
#include "TObjString.h"
#include "TError.h"

#include "json.hpp"
using json = nlohmann::json;

class phaarTreeFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};

class phaarTreeModule: public TARunObject
{
public:
   phaarTreeFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors

private:
  std::vector<int> fChanIndex;
  std::vector<double> fBaseline;
  std::vector<double> fNoise;
  std::vector<double> fmaxph;
  std::vector<double> fwfrms;
  std::vector<double> fROIcharge;
  std::vector<double> fROIheight;
  std::vector<double> fROItime;
  int fNchan;
  TTree *ChanTree;
  
  std::vector<int> fPulseIndex;//this is the same as fChanIndex, the same index
  std::vector<double> fCharge;
  std::vector<double> fTime;
  std::vector<double> fAmplitude;
  std::vector<double> fTimeOverThreshold;
  std::vector<unsigned int> fPulseEventCounter;
  std::vector<double> fPulseTriggerTime;
  int fNpulse;
  TTree *PulseTree;

  std::vector<int> fPeakIndex;//this is the same as fChanIndex, the same index
  std::vector<double> fArea;
  std::vector<double> fTime0;
  std::vector<double> fKappa;
  std::vector<double> fRiseTime;
  std::vector<double> fFallTimeShort;
  std::vector<double> fFallTimeLong;
  std::vector<double> fchi2;
  int fNpeak;
  TTree *PeakTree;

  double fmidasts;
  int fevent_number;

public:
   phaarTreeModule(TARunInfo* runinfo, phaarTreeFlags* f): TARunObject(runinfo),
                                                           fFlags(f),
                                                           fCounter(0), fError(0),
                                                           fNchan(0), ChanTree(0),
                                                           fNpulse(0), PulseTree(0)
   {
      if(fFlags->fVerbose) std::cout<<"phaarTreeModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="phaarTree";
#endif
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"phaarTreeModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"phaarTreeModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;

      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      
      ChanTree = new TTree("TChan","PHAAR Channels");
      ChanTree->Branch("index",&fChanIndex);
      ChanTree->Branch("baseline",&fBaseline);
      ChanTree->Branch("noise",&fNoise);
      ChanTree->Branch("maxph",&fmaxph);
      ChanTree->Branch("wfrms",&fwfrms);
      ChanTree->Branch("roiq",&fROIcharge);
      ChanTree->Branch("roih",&fROIheight);
      ChanTree->Branch("roit",&fROItime);
      ChanTree->Branch("nchan",&fNchan);
      ChanTree->Branch("mts",&fmidasts);

      PulseTree = new TTree("TPulse","PHAAR Pulses");
      PulseTree->Branch("index",&fPulseIndex); // index = ADC# x chans per ADC + CH#
      PulseTree->Branch("charge",&fCharge); // integral
      PulseTree->Branch("time",&fTime); // number of samples
      PulseTree->Branch("height",&fAmplitude); // ADC
      PulseTree->Branch("ToT",&fTimeOverThreshold);// time over threshold in samples
      PulseTree->Branch("npulse",&fNpulse); // number of pulses found in the event
      PulseTree->Branch("mts",&fmidasts); // unix epoch
      PulseTree->Branch("eventcounter",&fPulseEventCounter);
      PulseTree->Branch("trigtime",&fPulseTriggerTime); // s
      PulseTree->Branch("Nevent",&fevent_number);

      PeakTree = new TTree("TPeak","PHAAR best-fit Pulses");
      PeakTree->Branch("index",&fPeakIndex);
      PeakTree->Branch("charge",&fArea);
      PeakTree->Branch("time",&fTime0);
      PeakTree->Branch("risetime",&fRiseTime);
      PeakTree->Branch("falltimeshort",&fFallTimeShort);
      PeakTree->Branch("falltimelong",&fFallTimeLong);
      PeakTree->Branch("kappa",&fKappa);
      PeakTree->Branch("chi2",&fchi2);
      PeakTree->Branch("npeak",&fNpeak);
      PeakTree->Branch("mts",&fmidasts);

      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"phaarTreeModule Json parsing success!"<<std::endl;
      fin.close();

      std::cout<<"phaarTreeModule::BeginRun Saving settings to rootfile... ";
      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      int error_level_save = gErrorIgnoreLevel;
      gErrorIgnoreLevel = kFatal;
      TObjString settings_obj(settings.dump().c_str());
      int bytes_written = gDirectory->WriteTObject(&settings_obj,"config");
      if( bytes_written > 0 )
         std::cout<<" DONE ("<<bytes_written<<")"<<std::endl;
      else
         std::cout<<" FAILED"<<std::endl;
      gErrorIgnoreLevel = error_level_save;

      uint32_t midas_start_time;
      runinfo->fOdb->RU32("Runinfo/Start time binary",(uint32_t*) &midas_start_time);
      time_t ts1 = midas_start_time;
      std::cout<<"Begin of run time: "<<std::asctime(std::localtime(&ts1));
      std::string string_time=std::to_string(ts1);
      std::cout<<"phaarTreeModule::BeginRun Saving start time to rootfile... ";
      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      error_level_save = gErrorIgnoreLevel;
      gErrorIgnoreLevel = kFatal;
      TObjString string_time_obj(string_time.c_str());
      bytes_written = gDirectory->WriteTObject(&string_time_obj,"BeginRunUnixTS");
      if( bytes_written > 0 )
         std::cout<<" DONE ("<<bytes_written<<")"<<std::endl;
      else
         std::cout<<" FAILED"<<std::endl;
      gErrorIgnoreLevel = error_level_save;
   }

   void EndRun(TARunInfo* runinfo)
   {
      uint32_t midas_end_time;
      runinfo->fOdb->RU32("Runinfo/Stop time binary",(uint32_t*) &midas_end_time);
      time_t ts1 = midas_end_time;
      std::cout<<"End of run time: "<<std::asctime(std::localtime(&ts1));
      std::string string_time=std::to_string(ts1);
      std::cout<<"TreeWriterModule::EndRun Saving end time to rootfile... ";
      runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
      int error_level_save = gErrorIgnoreLevel;
      gErrorIgnoreLevel = kFatal;
      TObjString string_time_obj(string_time.c_str());
      int bytes_written = gDirectory->WriteTObject(&string_time_obj,"EndRunUnixTS");
      if( bytes_written > 0 )
         std::cout<<" DONE ("<<bytes_written<<")"<<std::endl;
      else
         std::cout<<" FAILED"<<std::endl;
      gErrorIgnoreLevel = error_level_save;
      std::cout<<"TreeWriterModule::EndRun() run: "<<runinfo->fRunNo
               <<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow ) 
         {
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            ++fError;
            return flow;
         }

      AdcEventFlow* adc_flow = flow->Find<AdcEventFlow>();
      if( !adc_flow ) fmidasts=-1.;
      else fmidasts=adc_flow->fmidas_ts;

      BaselineEventFlow* baseflow = flow->Find<BaselineEventFlow>();
      if( !baseflow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
         
      if(fFlags->fVerbose)
         std::cout<<"phaarTreeModule::AnalyzeFlowEvent Writing "
                  <<wf_flow->GetNumberOfChannels()<<" channels"<<std::endl;

      fevent_number = fCounter;
      
      fNchan=0;
      fChanIndex.clear();
      fBaseline.clear();
      fNoise.clear();
      fmaxph.clear();
      fwfrms.clear();
      fROIcharge.clear();
      fROIheight.clear();
      fROItime.clear();
      for(int i=0; i<baseflow->getNchannels(); ++i)
         {
            fChanIndex.push_back(baseflow->getADCchannel(i));
            fBaseline.push_back(baseflow->getBaseline(i));
            fNoise.push_back(baseflow->getBaselineRMS(i));
            fROIcharge.push_back(baseflow->getROIq(i)); //PA
            fROIheight.push_back(baseflow->getROIh(i));//PA
            fROItime.push_back(baseflow->getROIt(i));//AC
            const TDSChannel* ch = wf_flow->GetChannel(i);
            fmaxph.push_back(ch->maxph);
            fwfrms.push_back(ch->wf_rms);
            ++fNchan;
         }
      ChanTree->Fill();

      if(fFlags->fVerbose)
         std::cout<<"phaarTreeModule::AnalyzeFlowEvent Writing "
                  <<wf_flow->GetNumberOfPulses()<<" pulses"<<std::endl;
      fNpulse=0;
      fPulseIndex.clear();
      fCharge.clear();
      fTime.clear();
      fAmplitude.clear();
      fTimeOverThreshold.clear();
      fPulseEventCounter.clear();
      fPulseTriggerTime.clear();
      for( int i=0; i<wf_flow->GetNumberOfPulses(); ++i)
         {
            const TDSPulse* dsp = wf_flow->GetDSpulse(i);
            fPulseIndex.push_back(dsp->index);
            fCharge.push_back(dsp->charge);
            fTime.push_back(dsp->time);
            fAmplitude.push_back(dsp->height);
            fTimeOverThreshold.push_back(dsp->tot);
            fPulseEventCounter.push_back(adc_flow->fEventCounters[dsp->module]);
            fPulseTriggerTime.push_back(adc_flow->fTriggerTime[dsp->module]);
            //dsp->Print();
            ++fNpulse;
	 }
      PulseTree->Fill();

      if(fFlags->fVerbose)
         std::cout<<"phaarTreeModule::AnalyzeFlowEvent Writing "
                  <<wf_flow->GetNumberOfPeaks()<<" peaks"<<std::endl;

      fNpeak=0;
      fPeakIndex.clear();
      fArea.clear();
      fTime0.clear();
      fKappa.clear();
      fRiseTime.clear();
      fFallTimeShort.clear();
      fFallTimeLong.clear();
      fchi2.clear();
      for( int i=0; i<wf_flow->GetNumberOfPeaks(); ++i)
         {
            const TDSPeak* dspk = wf_flow->GetDSpeak(i);
            fPeakIndex.push_back(dspk->ChannelIndex);
            fArea.push_back(dspk->Charge);
            fTime0.push_back(dspk->PulseTime);
            fKappa.push_back(dspk->kappa);
            fRiseTime.push_back(dspk->RiseTime);
            fFallTimeShort.push_back(dspk->FallTimeShort);
            fFallTimeLong.push_back(dspk->FallTimeLong);
            fKappa.push_back(dspk->kappa);
            fchi2.push_back(dspk->chi2);
            ++fNpeak;
         }
      PeakTree->Fill();
      
      ++fCounter;
      return flow;
   }
};


class phaarTreeModuleFactory: public TAFactory
{
public:
   phaarTreeFlags fFlags;
   
public:
   void Usage()
   {
      printf("phaarTreeModuleFactory flags:\n");
      printf("--verbose\tprint status information\n");
   }

   void Init(const std::vector<std::string> &args)
   {
      printf("phaarTreeModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
             if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
	 }
   }
   
   void Finish()
   {
      printf("phaarTreeModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("phaarTreeModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new phaarTreeModule(runinfo, &fFlags);
   }

};

static TARegister tar(new phaarTreeModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
