/******************************************************************
 * Plot Waveforms *
 *
 * A. Capra
 * November 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "adcflow.hxx"
#include "baselineflow.hxx"
#include "dsflow.hxx"
#include "dsdata.hxx"
#include "sarf.hxx"

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <fstream>

#include "TCanvas.h"
#include "TH1D.h"
#include "TLine.h"

#ifdef EXPORT_ASCII
#include <sys/stat.h>
#endif

#include "json.hpp"
using json = nlohmann::json;

class WfviewFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};


class WfviewModule: public TARunObject
{
public:
   WfviewFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors

   bool fEnabled; // enable canvases creation
   int fChannel;  // special channel to plot separately

   bool fSave; // save PDFs of canvases
   int fSaveEvents; // limit of events to save

   bool fPersistency; // enable summation of waveforms
   int fPersistencyChannel; // visualize a special channel
   int fPersistencyEvents; // terminate the summation at this event

   TCanvas* fC; // the canvas for the special channel
   TCanvas* fCA; // the canvas for the first 16 channels
   std::map<int,TH1D*> fhwf; // raw waveforms
   double fRawWFmin;
   double fRawWFmax;
   std::map<int,TH1D*> fhwfb; // baseline subtracted waveforms
   std::map<int,TH1D*> fhwff; // baseline subtracted filtered waveforms
   std::map<int,TH1D*> fhpulse; // found pulse location and size
   std::map<int,TH1D*> ffpulse; // fit pulse

   bool fFoundPulse;
   bool fGoodFit;

   int fROImin;
   int fROImaxq;
   int fROImaxp;
   TLine* fROIlow; // line to identify the lower bound of the ROI in the plots
   TLine* fROIupq; // line to identify the upper bound of the ROI in the plots
   TLine* fROIupp; // line to identify the upper bound of the ROI in the plots

#ifdef EXPORT_ASCII
   std::ofstream fout;   // this feature can be only be enabled from cmake
   std::string fdirname; // save ASCII WF to this directory
#endif

   std::map<int,TCanvas*> fCraw; // the canvases for the raw waveforms

public:
   WfviewModule(TARunInfo* runinfo, WfviewFlags* f): TARunObject(runinfo),fFlags(f),
                                                     fCounter(0),fError(0),
                                                     fEnabled(false),fSave(false),
                                                     fPersistency(false),
                                                     fC(0),fCA(0),
                                                     fFoundPulse(false),fGoodFit(false),
                                                     /*fPercyDir(0),fCP(0),*/
                                                     fROImin(-1),fROImaxq(-1),fROImaxp(-1)
   {
      if(fFlags->fVerbose) std::cout<<"WfviewModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Wfview";
#endif

      fhwf.clear();
      fhwfb.clear();
      fhwff.clear();
      fhpulse.clear();
      ffpulse.clear();
      //      fhPersistency.clear();
      fCraw.clear();
      
      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"WfviewModule Json parsing success!"<<std::endl;
      fin.close();

      fEnabled=settings["Plot"]["Canvas"].get<bool>();
      fChannel=settings["Plot"]["Channel"].get<int>();
      fSave=settings["Plot"]["Save PDF"].get<bool>();
      fSaveEvents=settings["Plot"]["Save Events Limit"].get<int>();

      try {
         fROImin=settings["ROI"]["min"].get<int>();
         fROImaxq=settings["ROI"]["max charge"].get<int>();
         fROImaxp=settings["ROI"]["max peak"].get<int>();
      } catch(json::out_of_range& e) {
         std::cerr<<"WfviewModule::WfviewModule(...) ROI bounds: "<<e.what()<<std::endl;
      }

      fPersistency=settings["Plot"]["Persistency"].get<bool>();
      fPersistencyChannel=settings["Plot"]["Persistency Channel"].get<int>();
      fPersistencyEvents=settings["Plot"]["Persistency Events Limit"].get<int>();

      fRawWFmin=settings["Plot"]["Raw WF Minimum"].get<double>();
      fRawWFmax=settings["Plot"]["Raw WF Maximum"].get<double>();

      fROIlow = new TLine(fROImin,-1.,fROImin,1.);
      fROIlow->SetLineColor(1);  fROIlow->SetLineStyle(1);
      fROIupp = new TLine(fROImaxp,-1.,fROImaxp,1.);
      fROIupp->SetLineColor(1);  fROIupp->SetLineStyle(7);
      fROIupq = new TLine(fROImaxq,-1.,fROImaxq,1.);
      fROIupq->SetLineColor(1);  fROIupq->SetLineStyle(9);
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty())
         std::cout<<"WfviewModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"WfviewModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;

      if( fEnabled )
         {
            int runn=runinfo->fRunNo;

            std::string cname="Wfview_ADC_channel"+std::to_string(fChannel)
               +"_Run"+std::to_string(runn);
            std::string ctitle="Wfview ADC channel "+std::to_string(fChannel)
               +" Run "+std::to_string(runn);
            fC=new TCanvas(cname.c_str(),ctitle.c_str(),1000,750);
            fC->ToggleEventStatus();

            cname="Wfview_ADC_Run"+std::to_string(runn)+"_Channels";
            ctitle="Wfview ADC Run "+std::to_string(runn);
            fCA = new TCanvas(cname.c_str(),ctitle.c_str(),2000,1500);
            fCA->ToggleEventStatus();
            fCA->Divide(1,2);
         }

#ifdef EXPORT_ASCII
      fdirname="./WFASCIIR"+std::to_string(runinfo->fRunNo);
      int status = mkdir(fdirname.c_str(),0777);
      if(status==-1)
         std::cout<<"WfviewModule::BeginRun Cannot create directory "<<fdirname<<std::endl;
      else
         std::cout<<"WfviewModule::BeginRun "<<fdirname<<" succesfully created"<<std::endl;
#endif
   }

   void EndRun(TARunInfo* runinfo)
   {
      for(auto it=fhwf.begin();it!=fhwf.end();++it)
         if(it->second) delete it->second;
      fhwf.clear();
      for(auto it=fhwfb.begin();it!=fhwfb.end();++it)
         if(it->second) delete it->second;
      fhwfb.clear();
      for(auto it=fhwff.begin();it!=fhwff.end();++it)
         if(it->second) delete it->second;
      fhwff.clear();
      for(auto it=fhpulse.begin();it!=fhpulse.end();++it)
         if(it->second) delete it->second;
      fhpulse.clear();
      for(auto it=ffpulse.begin();it!=ffpulse.end();++it)
         if(it->second) delete it->second;
      ffpulse.clear();


      if( fEnabled )
         {
            if(fROIlow) delete fROIlow;
            if(fROIupp) delete fROIupp;
            if(fROIupq) delete fROIupq;
            fROIlow=fROIupp=fROIupq=0;

            // if(ffROIlow) delete ffROIlow;
            // if(ffROIupp) delete ffROIupp;
            // if(ffROIupq) delete ffROIupq;
            // ffROIlow=ffROIupp=ffROIupq=0;

            if(fC) delete fC;
            fC=0;
            if(fCA) delete fCA;
            fCA=0;

            for(auto it=fCraw.begin(); it!=fCraw.end(); ++it)
               if(it->second) delete it->second;
            fCraw.clear();
         }

      std::cout<<"WfviewModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }


   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      ResetHisto();
      AdcEventFlow* adc_flow = flow->Find<AdcEventFlow>();
      if( !adc_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      BaselineEventFlow* baseflow = flow->Find<BaselineEventFlow>();
      if( !baseflow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow ) 
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      int NumberOfChannels=adc_flow->getNchannels();
      fFoundPulse=fGoodFit=false;

      TDirectory* dir = runinfo->fRoot->fgDir;
      double plot_height=100.,wfmax;
      double chan_height=plot_height;
      for(int ich=0; ich<NumberOfChannels; ++ich)
         {
            const TRawChannel* adc_raw = adc_flow->getChannel(ich);
            if( adc_raw->IsEmpty() ) continue;
            dir->cd();// RootApp:/manalyzer
            //gDirectory->pwd();

            wfmax = *std::max_element(baseflow->getChannelVector(ich)->begin(),
                                      baseflow->getChannelVector(ich)->end());
            plot_height = plot_height>wfmax?plot_height:wfmax;
            if( ich == fChannel ) chan_height = chan_height>wfmax?chan_height:wfmax;
            std::cout<<"WfviewModule::AnalyzeFlowEvent "<<ich<<"\t"<<adc_flow->getChannelIndex(ich)<<"\t Max Height: "<<wfmax<<std::endl;

#ifndef __PHAAR__
            const TDSChannel* dschan = wf_flow->GetDSchan(ich);
            PlotChannel(adc_raw, ich, &dschan->unfiltered_wf, &dschan->filtered_wf);
            std::cout<<"WfviewModule::AnalyzeFlowEvent "<<dschan->unfiltered_wf.size()
                     <<"\t"<<dschan->filtered_wf.size()<<std::endl;
#else
            PlotChannel(adc_raw, ich, baseflow->getChannelVector(ich));
#endif
         }
      plot_height*=1.1;
      chan_height*=1.1;

      if( fFlags->fVerbose )
         std::cout<<"WfviewModule::AnalyzeFlowEvent Plotting "<<wf_flow->GetNumberOfPulses()
                  <<" pulse(s) in "<<NumberOfChannels<<" ("<<wf_flow->GetNumberOfChannels()<<") channel(s)"
                  <<" for "<<fhpulse.size()<<" plots with "<<wf_flow->GetNumberOfPeaks()<<" fits"<<std::endl;
      for( int ip=0; ip<wf_flow->GetNumberOfPulses(); ++ip)
         {
            const TDSPulse* dsp = wf_flow->GetDSpulse(ip);
            if( fFlags->fVerbose && 0 ) dsp->Print();
            PlotPulse(dsp);
            fFoundPulse=true;
            if( wf_flow->GetNumberOfPeaks() > 0 )
               {
                  double timestep=wf_flow->conf[dsp->board].fNanosecsPerSample;
                  const TDSPeak* pp = wf_flow->GetDSpeak(ip);
                  if( fFlags->fVerbose && 0 ) pp->Print();
                  if( pp->chi2 >= 0. )
                     {
                        PlotFit(pp,timestep);
                        fGoodFit=true;
                     }
               }
         }

      // if this module is enabled, run until here
      // i.e., make waveforms available in JSROOT
      if( !fEnabled )
         {
            ++fCounter;
            return flow;
         }
      // if graphics is available on the host
      // run with -g and select a valid channel

      if( fFlags->fVerbose )
         std::cout<<"WfviewModule::AnalyzeFlowEvent Plotting Channel: "<<fChannel<<std::endl;

      if( !fhwf[fChannel] && wf_flow->fEventNumber<3 )
         {
            std::cerr<<"WfviewModule::Analyze() channel "<<fChannel
                     <<" not present in waveform container"<<std::endl;
         }

      std::string runstring(std::to_string(runinfo->fRunNo));
      std::string evtstring(std::to_string(adc_flow->getEventNumber()));
      ShowPlots(NumberOfChannels, plot_height, chan_height, runstring, evtstring);
      ShowRaw(NumberOfChannels, runstring, evtstring);

      if( fSave && (fCounter < fSaveEvents) ) SaveCanvases(evtstring);

      ++fCounter;
      return flow;
   }

   void PlotChannel(const TRawChannel* raw, const unsigned int index,
                    const std::vector<double>* wf, const std::vector<double>* fwf=0)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock); // PlotChannel
#endif
      int Nbins = raw->GetNSamples();
      if( fFlags->fVerbose )
         std::cout<<"WfviewModule::PlotChannels CH index: "<<index
                  <<"  # of bins: "<<Nbins<<" ("<<wf->size()<<")"<<std::endl;
      std::string hname;
      std::string htitle;
      std::string board=std::to_string(raw->GetBoardNumber());

      if( !fhwf.count(index) )
         {
            hname="hwf_ADC";
            hname+=board;
            hname+="_CH"; hname+=std::to_string(raw->GetChannelNumber());
            if( fFlags->fVerbose ) std::cout<<hname<<std::endl;
            htitle="Waveform ";
            htitle+=board+" ";
            htitle+="ADC: "; htitle+=board;
            htitle+=" CH: "; htitle+=std::to_string(raw->GetChannelNumber());
            htitle+=";Sample;ADC";
            fhwf[index] = new TH1D(hname.c_str(),htitle.c_str(),
                                   Nbins,0.,double(Nbins));
            fhwf[index]->SetStats(0);
            fhwf[index]->SetLineColor(kBlack);
            fhwf[index]->SetMinimum(fRawWFmin);
            fhwf[index]->SetMaximum(fRawWFmax);
         }
      for(int i=0; i<Nbins; ++i)
         {
            fhwf[index]->SetBinContent(i+1,(double)raw->GetADCSample(i));
         }

      if( !fhwfb.count(index) )
         {
            hname="hchan_ADC";
            hname+=board;
            hname+="_CH"; hname+=std::to_string(raw->GetChannelNumber());
            if( fFlags->fVerbose ) std::cout<<hname<<std::endl;
            htitle="Wavefrom Pedestal Subtracted ";
            htitle+="ADC: "; htitle+=board;
            htitle+=" CH: "; htitle+=std::to_string(raw->GetChannelNumber());
            htitle+=";Sample;ADC";
            fhwfb[index] = new TH1D(hname.c_str(),htitle.c_str(),Nbins,0.,double(Nbins));
            fhwfb[index]->SetStats(0);
            fhwfb[index]->SetLineColor(kBlue);
            fhwfb[index]->SetMinimum(-32000.);
            fhwfb[index]->SetMaximum(32000.);
         }
#ifdef EXPORT_ASCII
      if( fChannel == int(index) || fChannel == -1 )
         {
            std::string foutname=fdirname+"/WFadc"+std::to_string(raw->GetBoardNumber())+"ch"+std::to_string(raw->GetChannelNumber())+"_"+std::to_string(fCounter)+".dat";
            fout.open(foutname,std::ofstream::out);
            std::cout<<"Saving... "<<foutname<<std::endl;
         }
#endif
      for(int i=0; i<Nbins; ++i)
         {
            fhwfb[index]->SetBinContent(i+1,-1.*wf->at(i));
#ifdef EXPORT_ASCII
            if( fChannel == int(index) || fChannel == -1 ) fout<<i<<","<<wf->at(i)<<"\n";
#endif
         }
#ifdef EXPORT_ASCII
      if( fChannel == int(index) || fChannel == -1 ) fout.close();
#endif

      if( fwf )
         {
            if( !fhwff.count(index) )
               {
                  hname="hchan_Filtered";
                  hname+=board;
                  hname+="_CH"; hname+=std::to_string(raw->GetChannelNumber());
                  if( fFlags->fVerbose && 0 ) std::cout<<hname<<std::endl;
                  htitle="Waveform Filtered ";
                  htitle+="ADC: "; htitle+=board;
                  htitle+=" CH: "; htitle+=std::to_string(raw->GetChannelNumber());
                  htitle+=";Sample;ADC";
                  fhwff[index] = new TH1D(hname.c_str(),htitle.c_str(),
                                          Nbins,0.,double(Nbins));
                  fhwff[index]->SetStats(0);
                  fhwff[index]->SetLineColor(kBlack);
                  fhwff[index]->SetLineWidth(2);
                  fhwff[index]->SetMinimum(-16384.);
                  fhwff[index]->SetMaximum(16384.);
               }
            for(int i=0; i<Nbins; ++i)
               {
                  fhwff[index]->SetBinContent(i+1,(double)fwf->at(i));
               }
            if( fFlags->fVerbose && 0 ) // extreme debug of the filter
               std::cout<<"WfviewModule::PlotChannel(...) "
                        <<fhwff[index]->GetName()<<" "
                        <<fhwff[index]->GetEntries()<<" "
                        <<fhwff[index]->GetMaximumBin()<<" "
                        <<fhwff[index]->GetBinContent( fhwff[index]->GetMaximumBin() )
                        <<std::endl;
         }

      if( !fhpulse.count(index) )
         {
            hname="hpulse_";
            hname+=board;
            hname+="_CH"; hname+=std::to_string(raw->GetChannelNumber());
            if( fFlags->fVerbose ) std::cout<<hname<<std::endl;
            htitle="Pulses";
            htitle+="ADC: "; htitle+=board;
            htitle+=" CH: "; htitle+=std::to_string(raw->GetChannelNumber());
            htitle+=";Sample;ADC";
            fhpulse[index] = new TH1D(hname.c_str(),htitle.c_str(),Nbins,0.,double(Nbins));
            fhpulse[index]->SetStats(0);
            fhpulse[index]->SetLineColor(6);
         }
      for(int i=0;i<Nbins;++i) fhpulse[index]->SetBinContent(i+1,0.0);

      if( !ffpulse.count(index) )
         {
            hname="hpulsefit_";
            hname+=board;
            hname+="_CH"; hname+=std::to_string(raw->GetChannelNumber());
            if( fFlags->fVerbose && 0 ) std::cout<<hname<<std::endl;
            htitle="Pulses";
            htitle+="ADC: "; htitle+=board;
            htitle+=" CH: "; htitle+=std::to_string(raw->GetChannelNumber());
            htitle+=";Sample;ADC";
            ffpulse[index] = new TH1D(hname.c_str(),htitle.c_str(),Nbins,0.,double(Nbins));
            ffpulse[index]->SetStats(0);
            ffpulse[index]->SetLineColor(2);
            ffpulse[index]->SetLineWidth(2);
         }
      for(int i=0;i<Nbins;++i) ffpulse[index]->SetBinContent(i+1,0.0);
   }

   void PlotPulse(const TDSPulse* p)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock); // PlotPulse
#endif
      if( fFlags->fVerbose && 0 )
         std::cout<<"WfviewModule::PlotPulse "<<p->channel
                  <<" at "<<p->index
                  <<" in "<<fhpulse[p->index]->GetName()<<std::endl;
      int start=((int)p->time)+1;
      int len = start+p->tot;
      for( int b=start; b<len; ++b )
         {
            //            std::cout<<"("<<b<<","<<p->height<<")";
            fhpulse[p->index]->SetBinContent( b, p->height );
         }
      //std::cout<<"\n";
   }

   void PlotFit(const TDSPeak* p, double& bin_size)
   {
      if( fFlags->fVerbose && 0 )
         std::cout<<"WfviewModule::PlotFit "<<p->Counter
                  <<" at "<<p->ChannelIndex
                  <<" in "<<ffpulse[p->ChannelIndex]->GetName()<<std::endl;
      SARF func(p->Charge,p->PulseTime,p->kappa,
                p->RiseTime,p->FallTimeShort, p->FallTimeLong);
      for( int b=1; b<=ffpulse[p->ChannelIndex]->GetNbinsX(); ++b )
         {
            double t = fhpulse[p->ChannelIndex]->GetBinCenter(b)*bin_size;
            if( t < p->MinValue ) continue;
            if( t > p->MaxValue ) break;
            double y = func(t);
            //std::cout<<"("<<b<<","<<t<<","<<y<<")";
            ffpulse[p->ChannelIndex]->SetBinContent( b, -y );
            //ffpulse[p->ChannelIndex]->Fill( t, y );
         }
   }

   void ShowPlots(int& NumberOfChannels, 
                  double& plot_max, double& chan_max,
                  std::string& runn, std::string& evtn)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock); // ShowPlots
#endif

      TLine* fROIlow_clone = (TLine*) fROIlow->Clone();
      TLine* fROIupp_clone = (TLine*) fROIupp->Clone();
      TLine* fROIupq_clone = (TLine*) fROIupq->Clone();
      // TLine* ffROIlow_clone = (TLine*) ffROIlow->Clone();
      // TLine* ffROIupp_clone = (TLine*) ffROIupp->Clone();
      // TLine* ffROIupq_clone = (TLine*) ffROIupq->Clone();

      double plot_min = -1.*plot_max;
      double chan_min = -1.*chan_max;
      std::string ctitle;
      if( fhwfb[fChannel] )
         {
            TH1D* h_baseline_sub = (TH1D*) fhwfb[fChannel]->Clone();
            ctitle="Wfview ADC channel "+std::to_string(fChannel)
               +" Run "+runn+" Event "+evtn;
            fROIlow_clone->SetY1(chan_min); fROIlow_clone->SetY2(chan_max); 
            fROIupp_clone->SetY1(chan_min); fROIupp_clone->SetY2(chan_max);
            fROIupq_clone->SetY1(chan_min); fROIupq_clone->SetY2(chan_max);
            // ffROIlow_clone->SetY1(chan_min); ffROIlow_clone->SetY2(chan_max); 
            // ffROIupp_clone->SetY1(chan_min); ffROIupp_clone->SetY2(chan_max);
            // ffROIupq_clone->SetY1(chan_min); ffROIupq_clone->SetY2(chan_max);
            if( fFlags->fVerbose ) std::cout<<"WfviewModule::ShowPlots "<<ctitle<<" in range ["<<chan_min<<","<<chan_max<<"]"<<std::endl;
            fC->SetTitle(ctitle.c_str());
            fC->cd();
            h_baseline_sub->Draw("hist");
            // fhwf[fChannel]->Draw("histsame");
            // fC->Modified();
            // fC->Update();
            if(fhwff[fChannel]) fhwff[fChannel]->Draw("histsame");
            if(fFoundPulse) fhpulse[fChannel]->Draw("histsame");
            if(fGoodFit) ffpulse[fChannel]->Draw("histsame");
            fROIlow_clone->Draw("same");
            fROIupp_clone->Draw("same");
            fROIupq_clone->Draw("same");
            // ffROIlow_clone->Draw("same");
            // ffROIupp_clone->Draw("same");
            // ffROIupq_clone->Draw("same");
            h_baseline_sub->GetYaxis()->SetRangeUser(chan_min,chan_max);
            fC->Modified();
            fC->Draw();
            fC->SetGridy(1);
            fC->Update();
         }

      fROIlow->SetY1(plot_min); fROIlow->SetY2(plot_max); 
      fROIupp->SetY1(plot_min); fROIupp->SetY2(plot_max);
      fROIupq->SetY1(plot_min); fROIupq->SetY2(plot_max);

      ctitle="Wfview ADC Run "+runn+" Event "+evtn;
      if( fFlags->fVerbose ) std::cout<<"WfviewModule::ShowPlots "<<ctitle<<std::endl;
      fCA->SetTitle(ctitle.c_str());
      fCA->cd();
      int ipad=1;
      int iplot=0;
      int ichan=0;
      while(ipad<3)
         {
            if(fhwfb[iplot])
               {
                  fCA->cd(ipad);
                  
                  fhwfb[iplot]->Draw("hist");
                  fhwfb[iplot]->GetYaxis()->SetRangeUser(plot_min,plot_max);
                  
                  if(fhwff[iplot]) fhwff[iplot]->Draw("histsame");
                  if(fFoundPulse) fhpulse[iplot]->Draw("histsame");
                  if(fGoodFit) ffpulse[iplot]->Draw("histsame");

                  fROIlow->Draw("same");
                  fROIupp->Draw("same");
                  fROIupq->Draw("same");
                  gPad->SetGridy(1);
                  // ffROIlow->Draw("same");
                  // ffROIupp->Draw("same");
                  // ffROIupq->Draw("same");
                  ++ipad;
                  ++ichan;
               }
            ++iplot;
            if( ichan == NumberOfChannels ) break;
         }
      fCA->Modified();
      fCA->Draw();
      fCA->Update();
   }

   void ShowRaw(int& nchan, std::string& runn, std::string& evtn)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock); // ShowRaw
#endif
      int ncanvases = std::ceil(nchan/16.0);
      if(fFlags->fVerbose)
         std::cout<<"WfviewModule::ShowRaw("<<nchan<<",...) Number of canvanses: "
                  <<ncanvases<<std::endl;
      int ichan=0;
      for(int icanv=0; icanv<ncanvases; ++icanv )
         {
            std::string ctitle="Wfview Raw ADC Run "+runn
               +" Event "+evtn+" Section"
               +std::to_string(icanv+1);
            if( fFlags->fVerbose ) std::cout<<"WfviewModule::ShowRaw"<<ctitle<<std::endl;
            bool isPresent=fCraw.count(icanv);
            if( !isPresent )
               {
                  std::string cname="Wfview_RawADC_Run"+runn+"_section"+std::to_string(icanv+1);
                  if(fFlags->fVerbose&&0) std::cout<<"\tnew canvas: "<<cname<<std::endl;
                  fCraw[icanv] = new TCanvas(cname.c_str(),ctitle.c_str(),2000,1500);
                  fCraw[icanv]->ToggleEventStatus();
                  fCraw[icanv]->Divide(1,2);
                  fCraw[icanv]->Iconify();
               }
            else
               fCraw[icanv]->SetTitle(ctitle.c_str());
            int ipad=1;
            int iplot=icanv*16;
            while(ipad<17)
               {
                  if(fhwf[iplot])
                     {
                        fCraw[icanv]->cd(ipad);
                        fhwf[iplot]->Draw("hist");
                        fhwf[iplot]->GetYaxis()->SetRangeUser(fRawWFmin,fRawWFmax);
                        ++ipad;
                        ++ichan;
                     }
                  ++iplot;
                  if( ichan == nchan ) break;
               }
            fCraw[icanv]->Modified();
            fCraw[icanv]->Draw();
            fCraw[icanv]->Update();
         }
   }

   void SaveCanvases(std::string& evtn)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock); // SaveCanvases
#endif
      TString sname(fC->GetName());
      sname+="_Event"+evtn+".png";
      fC->SaveAs(sname); fC->SaveAs(sname);
      sname=fCA->GetName();
      sname+="_Event"+evtn+".png";
      fCA->SaveAs(sname); fCA->SaveAs(sname);
   }

   void ResetHisto()
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      for(auto it=fhwf.begin();it!=fhwf.end();++it)
         {
            if(it->second)
               {
                  it->second->Reset();
                  it->second->SetMinimum(fRawWFmin);
                  it->second->SetMaximum(fRawWFmin);
               }
         }
      for(auto it=fhwfb.begin();it!=fhwfb.end();++it)
         {
            if(it->second)
               {
                  it->second->Reset();
                  it->second->SetMinimum(-16384.);
                  it->second->SetMaximum(16384.);
               }
         }
      for(auto it=fhwff.begin();it!=fhwff.end();++it)
         {
            if(it->second)
               {
                  it->second->Reset();
                  it->second->SetMinimum(-16384.);
                  it->second->SetMaximum(16384.);
               }
         }
      for(auto it=fhpulse.begin();it!=fhpulse.end();++it)
         if(it->second) it->second->Reset();
      for(auto it=ffpulse.begin();it!=ffpulse.end();++it)
         if(it->second) it->second->Reset();
   }
};


class WfviewModuleFactory: public TAFactory
{
public:
   WfviewFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      printf("WfviewModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
   }

   void Finish()
   {
      printf("WfviewModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("WfviewModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new WfviewModule(runinfo, &fFlags);
   }

};

static TARegister tar(new WfviewModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
