/******************************************************************
 *  Region of Interest Analysis *
 * 
 * A. Capra
 * May 2022
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "baselineflow.hxx"
#include "dsflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <fstream>
#include <cmath>
#include <cassert>

#include "utils.hxx"

#include "json.hpp"
using json = nlohmann::json;


class RoiFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};
  

class RoiModule: public TARunObject
{
public:
   RoiFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors
 
   // raw wf ROI
   int roi_inf;
   int roi_charge_sup;
   int roi_charge_len;
   int roi_peak_sup;
   int roi_peak_len;
   // filtered wf ROI (unused  -- AC 6/2/2023)
   int filter_roi_inf;
   int filter_roi_charge_sup;
   int filter_roi_charge_len;
   int filter_roi_peak_sup;
   int filter_roi_peak_len;

   int fTrigChan;
   int fTrigDelay;
   bool fRelative;

   unsigned int    fPostTrigger ; 

   //Utility class with analysis methods
   Utils *utils;

public:
   RoiModule(TARunInfo* runinfo, RoiFlags* f): TARunObject(runinfo),
                                               fFlags(f), fCounter(0), fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"RoiModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Roi";
#endif
      
      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"RoiModule Json parsing success!"<<std::endl;
      fin.close();

      // read ROI parameters from config file
      // this refers to the raw waveform
      // lower bound
      roi_inf = settings["ROI"]["min"].get<int>();
      assert(roi_inf>=0);
      // upper bound for charge intergration
      roi_charge_sup = settings["ROI"]["max charge"].get<int>();
      roi_charge_len = roi_charge_sup-roi_inf;
      assert(roi_charge_len>0);
      std::cout<<"ROI charge: ["<<roi_inf<<","<<roi_charge_sup<<"] = "<<roi_charge_len<<" samples"<<std::endl;
      // upper bound for pulse height calculation
      roi_peak_sup = settings["ROI"]["max peak"].get<int>();
      roi_peak_len = roi_peak_sup-roi_inf;
      assert(roi_peak_len>0);
      std::cout<<"ROI height: ["<<roi_inf<<","<<roi_peak_sup<<"] = "<<roi_peak_len<<" samples"<<std::endl;

      try 
         {
            fTrigChan = settings["Laser"]["Trigger Channel"].get<int>();
         }
      catch(...)
         {
            fTrigChan = -1;
         }
      
      // try
      //    {
      //       filter_roi_inf=settings["ROI"]["filter min"].get<int>();
      //       assert(filter_roi_inf>=0);
      //       filter_roi_charge_sup=settings["ROI"]["filter max charge"].get<int>();
      //       filter_roi_charge_len=filter_roi_charge_sup-filter_roi_inf;
      //       assert(filter_roi_charge_len>0);
      //       filter_roi_peak_sup=settings["ROI"]["filter max peak"].get<int>();
      //       filter_roi_peak_len=filter_roi_peak_sup-filter_roi_inf;
      //       assert(filter_roi_peak_len>0);
      //    }
      // catch(...)
      //    {
      filter_roi_inf=0;
      filter_roi_charge_sup = filter_roi_charge_len = filter_roi_peak_sup = filter_roi_peak_len = 1000;
      //    }

      fRelative = false;
      try 
         {
            fRelative = settings["ROI"]["Relative"].get<bool>();
            if( fRelative ) std::cout<<"RoiModule ctor: Using ROI relative to Trigger"<<std::endl;
            else std::cout<<"RoiModule ctor: Using fixed ROI"<<std::endl;
         }
      catch(...) 
         { 
            std::cout<<"RoiModule ctor: Using fixed ROI"<<std::endl;  
         }

      fTrigDelay = 15;
      try 
         {
            fTrigDelay = settings["ROI"]["Delay"].get<int>();
            if( fRelative ) std::cout<<"RoiModule ctor: Using trigger delay "<<fTrigDelay<<" bins"<<std::endl;
            else std::cerr<<"RoiModule ctor: Trigger delay not used because the ROI is not relative to the trigger pulse"<<std::endl;
         }
      catch(...)
         {
            if( fRelative ) std::cout<<"RoiModule ctor: Using trigger delay "<<fTrigDelay<<" bins"<<std::endl;
         }

      //if utils doesn't exist, create an instance of it
      utils = new Utils();
      utils->SetVerbosity(int(fFlags->fVerbose));
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"RoiModule::BeginRun() run: "<<runinfo->fRunNo<<" online";
      else
         std::cout<<"RoiModule::BeginRun() run: "<<runinfo->fRunNo
                  <<" midasfile: "<<runinfo->fFileName.c_str();

      uint32_t ttt ; 
      runinfo->fOdb->RU32("/Equipment/FEV1730USB/Settings/V1730/Post Trigger",(uint32_t*)  &ttt) ;
      fPostTrigger = ttt; 
      std::cout << ", reading the Post Trigger Setting " << fPostTrigger <<  std::endl ; 
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"RoiModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      BaselineEventFlow* base_flow = flow->Find<BaselineEventFlow>();
      if( !base_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      if( fFlags->fVerbose )
         std::cout<<"RoiModule::AnalyzeFlowEvent "<<base_flow->getEventNumber()
                  <<" Number of Channels: "<<base_flow->getNchannels()<<std::endl;

      int start_time,stop_time_charge,stop_time_peak;
      if( fRelative )
         {
            for( int i=0; i<wf_flow->GetNumberOfPulses(); ++i)
               {
                  const TDSPulse* dsp = wf_flow->GetDSpulse(i);
                  if( dsp->index != fTrigChan ) continue; // only one trig channel
                  // roi_inf;
                  start_time       = static_cast<int>(dsp->time) + fTrigDelay;
                  // roi_charge_len;
                  stop_time_charge = static_cast<int>(start_time + roi_charge_len);
                  // roi_peak_len;
                  stop_time_peak   = static_cast<int>(start_time + roi_peak_len);
                  break; // if trig channel found, quit
               }
            if( fFlags->fVerbose )
               std::cout<<"RoiModule::AnalyzeFlowEvent ROI relative to trigger pulse - start: "<<start_time
                        <<"   stop charge: "<<stop_time_charge<<"   stop peak: "<<stop_time_peak<<std::endl;
         }
      else
         {
            start_time       = roi_inf;
            stop_time_charge = roi_charge_sup;
            stop_time_peak   = roi_peak_sup;
         }

      double roiq,roih,roit;
      for(int i=0; i<base_flow->getNchannels(); ++i)
         {
            const std::vector<double>* wf = base_flow->getChannelVector(i);
            int nsamp = wf->size();
            if( fFlags->fVerbose )
               std::cout<<"RoiModule::AnalyzeFlowEvent number "
                        <<" ch: "<<i<<" size: "<<nsamp<<std::endl;

            CheckROI( nsamp );
            
            roiq = utils->calculate_ROI_charge( wf, start_time, stop_time_charge );
            roih = utils->calculate_ROI_height( wf, start_time, stop_time_peak, roit);

            base_flow->addROIs( roiq, roih, roit );

            if( fFlags->fVerbose )
               std::cout<<"RoiModule::AnalyzeFlowEvent CH:"<<base_flow->getADCchannel(i)
                        <<"\tQ: "<<roiq<<"\tH: "<<roih<<"\tT: "<<roit<<std::endl;
         }

      ++fCounter;
      return flow;
   }

   void CheckROI(int& nsamples)
   {
      //      std::cout<<"ROIModule::CheckROI validity"<<std::endl;
      if( roi_peak_sup > nsamples )
         {
            std::cerr<<"ROIModule::AnalyzeFlowEvent : Upper bound of ROI "<<roi_peak_sup<<" exceeds number of samples... ";
            roi_peak_sup=nsamples;
            std::cerr<<" changed to "<<roi_peak_sup<<" equals to the number of samples"<<std::endl;
            if( roi_inf >= roi_peak_sup )
               {
                  std::cerr<<"ROIModule::AnalyzeFlowEvent : Lower bound of ROI "<<roi_inf<<" is greater than Upper bound ROI... ";
                  roi_inf=roi_peak_sup-roi_peak_len;
                  if( roi_inf < 0 ) roi_inf=0;
                  std::cerr<<" changed to "<<roi_inf<<std::endl;
               }
         }

      if( filter_roi_peak_sup > nsamples )
         {
            std::cerr<<"ROIModule::AnalyzeFlowEvent : Upper bound of Filtered ROI "<<filter_roi_peak_sup<<" exceeds number of samples... ";
            filter_roi_peak_sup=nsamples;
            std::cerr<<" changed to "<<filter_roi_peak_sup<<" equals to the number of samples"<<std::endl;
            if( filter_roi_inf >= filter_roi_peak_sup )
               {
                  std::cerr<<"ROIModule::AnalyzeFlowEvent : Lower bound of Filtered ROI "<<filter_roi_inf<<" is greater than Upper bound ROI... ";
                  filter_roi_inf=filter_roi_peak_sup-filter_roi_peak_len;
                  if( filter_roi_inf < 0 ) filter_roi_inf=0;
                  std::cerr<<" changed to "<<filter_roi_inf<<std::endl;
               }
         }

      if( roi_charge_sup > nsamples )
         {
            std::cerr<<"ROIModule::AnalyzeFlowEvent : Upper bound of ROI "<<roi_charge_sup<<" exceeds number of samples... ";
            roi_charge_sup=nsamples;
            std::cerr<<" changed to "<<roi_charge_sup<<" equals to the number of samples"<<std::endl;
            if( roi_inf >= roi_charge_sup )
               {
                  std::cerr<<"ROIModule::AnalyzeFlowEvent : Lower bound of ROI "<<roi_inf<<" is greater than Upper bound ROI... ";
                  roi_inf=roi_charge_sup-roi_charge_len;
                  if( roi_inf < 0 ) roi_inf=0;
                  std::cerr<<" changed to "<<roi_inf<<std::endl;
               }
         }

      if( filter_roi_charge_sup > nsamples )
         {
            std::cerr<<"ROIModule::AnalyzeFlowEvent : Upper bound of Filtered ROI "<<filter_roi_charge_sup<<" exceeds number of samples... ";
            filter_roi_charge_sup=nsamples;
            std::cerr<<" changed to "<<filter_roi_charge_sup<<" equals to the number of samples"<<std::endl;
            if( filter_roi_inf >= filter_roi_charge_sup )
               {
                  std::cerr<<"ROIModule::AnalyzeFlowEvent : Lower bound of Filtered ROI "<<filter_roi_inf<<" is greater than Upper bound ROI... ";
                  filter_roi_inf=filter_roi_charge_sup-filter_roi_charge_len;
                  if( filter_roi_inf < 0 ) filter_roi_inf=0;
                  std::cerr<<" changed to "<<filter_roi_inf<<std::endl;
               }
         }
   }
};


class RoiModuleFactory: public TAFactory
{
public:
   RoiFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("RoiModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
      
   }
   
   void Finish()
   {
      printf("RoiModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("RoiModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new RoiModule(runinfo, &fFlags);
   }

};

static TARegister tar(new RoiModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
