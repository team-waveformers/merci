/******************************************************************
 * Waveforms *
 *
 * D. Gallacher
 * A. Capra and P. A. Amaudruz
 * October 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "adcflow.hxx"

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <fstream>

#include "TCanvas.h"
#include "TH1D.h"

#include "json.hpp"
using json = nlohmann::json;

class WFFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};

//Read preprocessor flag value as string literal
#define STRING(s) #s
#define STRSTRING(s) STRING(s)


class wfmodule: public TARunObject
{
public:
   WFFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors

   bool fEnabled; // enable canvases creation

   bool fSave; // save PDFs of canvases
   int fSaveEvents; // limit of events to save

   bool fPersistency; // enable summation of waveforms
   int fPersistencyChannel; // visualize a special channel
   int fPersistencyEvents; // terminate the summation at this event

   double fRawWFmin;
   double fRawWFmax;
   double fNanosecPerSample;//Number of nanoseconds per sample


   int fNumChannels; //Number of channels for the raw and corrected waveforms
   std::map<int,TH1D*> fhWFRaw; // raw waveforms
   std::map<int,TCanvas*> fCRaw; // the canvases for the raw waveforms
   TDirectory* fPercyDir;
   TCanvas* fCP; // the canvas for the persistency plot for the selected channel
   TCanvas *fCRawAll;//All Raw waveforms on one canvas
   std::map<int,TH1D*> fhPersistency; // the persistency histograms

   TDirectory* fRawDir;//Directory to hold raw plots

   int fPedestal;//Bins for showing baselines


#ifdef EXPORT_ASCII
   std::ofstream fout; // this feature can be only be enabled from cmake
#endif




public:
   wfmodule(TARunInfo* runinfo, WFFlags* fFlags): TARunObject(runinfo),fFlags(fFlags),
                                                  fCounter(0),fError(0),
                                                  fEnabled(false),fSave(false),
                                                  fPersistency(false)
   {
      if(fFlags->fVerbose) std::cout<<"wfmodule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Waveform";
#endif
      std::string config_path;
#ifdef CONFIG_PATH
    config_path = STRSTRING(CONFIG_PATH) + fFlags->fConfigName;//Read config file path from CMAKE source dir (lolx)
#else
    config_path = fFlags->fConfigName;//If path isn't defined look in flags only
#endif
      std::cout << "Reading config file from path = "<< config_path << "\n";
      //Change this to the new map-plots (single channels)
      fhWFRaw.clear();

      fhPersistency.clear();
      fCRaw.clear();
 
      std::ifstream fin(config_path);
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"wfmodule Json parsing success!"<<std::endl;
      fin.close();

      fEnabled = settings["Waveform"]["Canvas"].get<bool>();
      fSave = settings["Waveform"]["Save PDF"].get<bool>();
      fSaveEvents = settings["Waveform"]["Save Events Limit"].get<int>();
      fPersistency = settings["Waveform"]["Persistency"].get<bool>();
      fPersistencyChannel = settings["Waveform"]["Persistency Channel"].get<int>();
      fPersistencyEvents = settings["Waveform"]["Persistency Events Limit"].get<int>();
      fNumChannels = settings["Waveform"]["Num Channels"].get<int>();
      fRawWFmin = settings["Waveform"]["Raw WF Minimum"].get<double>();
      fRawWFmax = settings["Waveform"]["Raw WF Maximum"].get<double>();
      
      fNanosecPerSample = 16.;
      // fEnabled = true;
      // fSave = false;
      // fSaveEvents = 1;
      // fPersistency = false;
      // fPersistencyChannel = 0;
      // fPersistencyEvents = 1000;
      // fNumChannels = 16;
      // fRawWFmin = 0;
      // fRawWFmax = 4000;

   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty())
         std::cout<<"wfmodule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"wfmodule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;

      if( fEnabled )
         {
           fCRawAll = new TCanvas("Raw_Waveforms","All Raw Waveforms",2000,1500);
           fCRawAll->Divide(8,4);
           fCRawAll->ToggleEventStatus();
  

            int runn=runinfo->fRunNo;
            for(int iCh=0;iCh<fNumChannels;iCh++){
              std::string cname = Form("Run# %i: Raw_Canvas_%i",runn,iCh);
              if(fFlags->fVerbose&&0) std::cout<<"\tnew canvas: "<<cname<<std::endl;
              fCRaw[iCh] = new TCanvas(cname.c_str(),cname.c_str(),2000,1500);
              fCRaw.at(iCh)->ToggleEventStatus();
            }//end loop over channels


            if( runinfo->fFileName.empty() )
               {
                  gDirectory->cd("RootApp:/");
                  fRawDir = runinfo->fRoot->fgDir->mkdir("RawChannels");
               }
            else
               {
                  runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
                  fRawDir = runinfo->fRoot->fOutputFile->mkdir("RawChannels");
               }


         }

      if(fPersistency)
         {
            std::string cname="cPersistencyR"+std::to_string(runinfo->fRunNo)+"CH"+std::to_string(fPersistencyChannel);
            fCP = new TCanvas(cname.c_str(),cname.c_str(),900,700);
            if( runinfo->fFileName.empty() )
               {
                  gDirectory->cd("RootApp:/");
                  fPercyDir = new TDirectory("Persistency", "Persistency Histograms");
               }
            else
               {
                  runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
                  fPercyDir=runinfo->fRoot->fOutputFile->mkdir("Persistency");
               }
         }
   }

   void EndRun(TARunInfo* runinfo)
   {
      for(auto it=fhWFRaw.begin();it!=fhWFRaw.end();++it)
         if(it->second) delete it->second;
      fhWFRaw.clear();

      if( fEnabled )
         {
            for(auto it=fCRaw.begin(); it!=fCRaw.end(); ++it)
               if(it->second) delete it->second;
            fCRaw.clear();
         }


      if(fPersistency)
         {
            fCP->SaveAs(".pdf");
            delete fCP;
            if( runinfo->fFileName.empty() )
               {
                  gDirectory->cd("RootApp:/");
                  if( fPercyDir ) delete fPercyDir;
               }
         }
      std::cout<<"wfmodule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }


   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      AdcEventFlow* adc_flow = flow->Find<AdcEventFlow>();
      if( !adc_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }


      ResetHisto();

      //Get the actual number of channels
      fNumChannels = adc_flow->getNchannels();

      TDirectory* dir = runinfo->fRoot->fgDir;
      for(int ich=0; ich<fNumChannels; ++ich)
         {
            //const TV1740RawChannel* adc_raw = (TV1740RawChannel*) adc_flow->getChannel(ich);
            const std::vector<double> *raw_wf = adc_flow->getVector(ich);
            if(raw_wf->empty()) continue;//Skip empty waveforms

            dir->cd();// RootApp:/manalyzer

            PlotChannel(ich,raw_wf);
            
            if( fPersistency && (fCounter < fPersistencyEvents) )
               {
                  if( runinfo->fgFileList.size() )
                     runinfo->fRoot->fOutputFile->cd(); // <filename>:/
                  else
                     gDirectory->cd("RootApp:/");
                  fPercyDir->cd();
                  //gDirectory->pwd();
                  PersistencyPlot(raw_wf,ich);
               }
         }

      if( fFlags->fVerbose ){
        //Print stuff here
      }

      // if this module is enabled, run until here
      // i.e., make waveforms available in JSROOT
      if( !fEnabled )
         {
            ++fCounter;
            return flow;
         }
      // if graphics is available on the host
      // run with -g and select a valid channel

#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif

      if( fFlags->fVerbose ){
        //Print stuff here
      }
      int runNum = runinfo->fRunNo;
      ShowRaw(runNum);

      // read and plot here histos saved
      // in subdirectories
      if( runinfo->fgFileList.size() )
         runinfo->fRoot->fOutputFile->cd(); // <filename>:/
      else
         gDirectory->cd("RootApp:/");

      if( fSave && (fCounter < fSaveEvents) )
         {
            // TString sname(fC->GetName());
            // sname+="_Event"+std::to_string(fCounter)+".pdf";
            // fC->SaveAs(sname); fC->SaveAs(sname);
            // sname=fCA->GetName();
            // sname+="_Event"+std::to_string(fCounter)+".pdf";
            // fCA->SaveAs(sname); fCA->SaveAs(sname);
         }

      ++fCounter;
      return flow;
   }


    void PlotChannel(const int index, const std::vector<double> *raw_wf)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      int Nbins = raw_wf->size();
      if( fFlags->fVerbose && 0 )
         std::cout<<"wfmodule::PlotChannels   # of bins: "<<Nbins<<std::endl;
      std::string hname;
      std::string htitle;

      //Switch to the correct directory
      fRawDir->cd();

      if( !fhWFRaw.count(index) ){
        fhWFRaw[index] = new TH1D(Form("hRaw_%i",index),"Raw Waveform; Time (ns);Intensity (ADC/V)",Nbins,0,Nbins*fNanosecPerSample);
      }

      //utils->ConvertRawWF(*fhWFRaw[index],raw_wf,index,fNanosecPerSample);
      fhWFRaw[index]->SetName(Form("hRaw_%i",index));
      fhWFRaw[index]->SetTitle("Raw Waveform; Time (ns);Intensity (ADC/V)");
      fhWFRaw[index]->SetLineColor(GetChannelColor(index));
      for(int i=0; i<Nbins; ++i)
         {
             fhWFRaw[index]->SetBinContent(i+1,raw_wf->at(i));
         }
   }

   void ShowRaw(int &run_num)
   {
 #ifdef MODULE_MULTITHREAD
       std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
 #endif

       for(int i=0;i<fNumChannels;i++){
         bool isPresent = fCRaw.count(i);
         if( !isPresent ){
               std::string cname = Form("Run# %i: Raw_Canvas_%i",run_num,i);
               fCRaw[i] = new TCanvas(cname.c_str(),cname.c_str(),2000,1500);
               fCRaw[i]->ToggleEventStatus();
         }
         fCRaw[i]->cd();

         gPad->SetGrid();
         fhWFRaw[i]->Draw("hist");
         fCRaw[i]->Modified();
         fCRaw[i]->Draw();
         fCRaw[i]->Update();

         fCRawAll->cd();
         fCRawAll->cd(i+1);
         gPad->SetGrid();
         fhWFRaw.at(i)->Draw("hist");
      }
      fCRawAll->Modified();
      fCRawAll->Draw();
      fCRawAll->Update();

   }
   //Returns channel type as TColor (2==Bare == Red, 30 == BP == Green, 9 == LP == blue) based on DAQ Channel
   int GetChannelColor(int dChan)
   {
     int col = 9;//Long pass
     if(dChan < 32 && dChan > 27) col = 2;//Bare
     if(dChan < 4) col = 30;//Band pass
     return col;
   }

   void PersistencyPlot( const std::vector<double>* wf,int index)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      if(!(fFlags->fVerbose))
         {
            std::cout<<"wfmodule::PersistencyPlot "<<index<<" ";
         }
      int nsamples = (int)wf->size();
      if( !fhPersistency.count(index) )
         {
            std::string hname="hPersistencyCH"+std::to_string(index);
            std::string htitle="Persistency Plot for channel "+std::to_string(index)+";Time (ns);ADC";
            fhPersistency.at(index) = new TH1D(hname.c_str(),htitle.c_str(),nsamples,0.,(double)nsamples*fNanosecPerSample);
            fhPersistency.at(index)->SetStats(0);
         }
      else if( fhPersistency.at(index)->GetNbinsX() != nsamples )
         {
            std::string hname="hPersistencyCH"+std::to_string(index);
            std::string htitle="Persistency Plot for channel "+std::to_string(index)+";Time (ns);ADC";
            fhPersistency.at(index) = new TH1D(hname.c_str(),htitle.c_str(),nsamples,0.,(double)nsamples);
            fhPersistency.at(index)->SetStats(0);
         }
      for(int i=0; i<nsamples; ++i)
         {
             fhPersistency[index]->AddBinContent(i+1,wf->at(i));
         }
      if( index == fPersistencyChannel )
         {
            fCP->cd();
            fhPersistency.at(fPersistencyChannel)->Draw();
            fCP->Modified();
            fCP->Draw();
            fCP->Update();
         }
      if(fCounter==(fPersistencyEvents-1) && fFlags->fVerbose) std::cout<<"wfmodule::PersistencyPlot for channel:"<<index<<" Last WF."<<std::endl;
   }


   void ResetHisto()
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      if(fFlags->fVerbose) std::cout<<"Resetting Event: "<<fCounter<<std::endl;
      for(auto it=fhWFRaw.begin();it!=fhWFRaw.end();++it)
         if(it->second) it->second->Reset();
   }
};


class wfmoduleFactory: public TAFactory
{
public:
   WFFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      printf("wfmoduleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
         //fFlags.fVerbose = true;
   }

   void Finish()
   {
      printf("wfmoduleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("wfmoduleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new wfmodule(runinfo, &fFlags);
   }

};

static TARegister tar(new wfmoduleFactory);
