# Python Environment for MERCI
Author: David Gallacher


First we need conda, to install see below:
https://docs.conda.io/projects/conda/en/latest/user-guide/install/macos.html


## Setup and environment

Other than creating our test tree, and sample plotter we do not need ROOT or MERCI installed for this to work.


We will make a virtual environment first, using our custom environment file.

Make sure we are in the correct working directory


`cd /path/to/your/merci/python`

`conda env create -f environment.yml`

Select 'y' to proceed, then activate with

`conda activate merci`

(to remove an environment do  `conda env remove -n merci`)



## Testing and example macro

If you have ROOT installed you can run the "sampleTree.C" macro by:
root -l -q sampleTree.C

This will create a "testTree.root" file which contains a tree "T" with some random data.

You can then run "python samplePlot.py"

See [samplePlot](samplePlot.py) for examples on manipulating the output objects and plotting histograms.

Once in a python object, users are welcome to manipulate/plot data however they please.

The histograms from this example script will be saved to a file called "samplePlots.pkl", which can then be loaded by [loadSamplePlots](loadSamplePlots.py)

These examples highlight using pickling to save binary data of python objects and loading it later

The output of the sample plot macro should show two figures:

PE distribution + 2D Projections from ROOT TTree:

![Projections](/python/sample_projections.png)


1D Distribution of times summed over TTree:

![Time](/python/sample_time.png)

## UpROOT

UpROOT is an I/O python package for reading in ROOT files into python structures, either numpy arrays or pandas dataframes.
ROOT is not required to read or write ROOT files using upROOT.
upROOT: (https://uproot.readthedocs.io/en/latest/index.html)

The example script highlights some of the syntax for reading ROOT files with upROOT and manipulating their outputs in python


Included in the example is some histogramming with boost-histogram, and the hist libraries.
boost-histogram: (https://boost-histogram.readthedocs.io/en/latest/index.html)
hist: (https://hist.readthedocs.io/en/latest/index.html)


## Pickling

We can store our histograms and other python objects in pickle files. This makes it easy to save histograms to be reloaded later.

Pickled objects are stored as binary files and objects are serialized, so to retrieve a give object we need to access it in-order.

We can get around this order dependent requirement by using dictionaries. Instead of dumping the object itself, we save it to a dictionary with a name, and access it later by name. This is highlighted in loadSamplePlots.py

If we have millions of data events, we do NOT want to run the analysis macro everytime we need to adjust a plot. Save the histogrammed data to a pickle file and load it later for display formatting!
