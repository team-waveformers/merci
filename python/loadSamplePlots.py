#Example macro to read a histogram from a pickled file

import pickle
import boost_histogram as bh
import matplotlib.pyplot as plt
from hist import axis as haxis
from hist import Hist 


#Load up our pickled histograms and redraw them
#We dumped a dictionary of python objects, so we can access them here by name
with open("samplePlots.pkl", "rb") as f:
    histpe = pickle.load(f)['histpe']


fig, axes = plt.subplots(figsize=(10,4))
axes.bar(histpe.axes[0].centers,histpe.view(),width=histpe.axes[0].widths)
axes.set(ylabel="Counts",xlabel="PE")
axes.grid()
axes.set_title("PE Plot from ROOT TTree")

plt.show()