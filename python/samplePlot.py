##############
# Sample python script to inspect a flat TTree from ROOT using uproot and plot histograms using boost_histogram and matplotlib
# Author: David Gallacher
# To run: Run root sampleTree.C first, then activate the conda environment "merci" (See README.md)
# Then run 'python samplePlot.py'
##############


import uproot as up
import numpy as np
import pandas as pd
import boost_histogram as bh
import os
import matplotlib.pyplot as plt
import pickle
#Can't import all of hist and boost_histogram in one script, they collide names
from hist import axis as haxis
from hist import Hist


#First let's inspect to see what's in the file
#os.chdir('Documents/research/merci/python')
print(os.getcwd())
print("The file contains the following objects:")
with up.open("testTree.root") as file:
	print(file.classnames())

#We should see the names of the objects in our file, in this case just "{'T;1': 'TTree'}", since we have one object called "T" that has type "TTree"


#Now let's print the leaves as well.
#Get the tree
tree  = up.open("testTree.root:T")
print("TTree 'T' contains the following keys:")
tree.show()


#Lets put one of our branches ('pe') into a numpy array to work with

pe = tree['pe'].array(library="np") #Get the branch and cast it to a np array

# Now let's make a histogram of pe
nbins = 100
lowbin = 100
highbin = 300

#Make the histogram with our custom binning and ranges
histpe = bh.Histogram(bh.axis.Regular(nbins,lowbin,highbin))
#Fill the histogram from our np array
histpe.fill(pe)


#Display the plot, boost_histogram is powerful for making and storing histograms but doesn't include display
#so we use matplotlib to display as a bar chart

fig, axes = plt.subplots(1,4,figsize=(12,5))
axes[0].bar(histpe.axes[0].centers,histpe.view(),width=histpe.axes[0].widths)
axes[0].set(ylabel="Counts",xlabel="PE")
axes[0].grid()
axes[0].set_title("PE Plot from ROOT TTree")


#Next we will extract the x,y,z as a pandas DF and plot them in 2D projections
loc_data = tree.arrays(['x','y','z'],library="pd")
print(loc_data.head())#Print the start of the dataframe to confirm it was read in properly

#Format our 3D histogram
nbins_loc = 25
loc_low = 0
loc_high = 10
#3D data
hist3 = bh.Histogram(
	bh.axis.Regular(nbins_loc, loc_low, loc_high),
	bh.axis.Regular(nbins_loc,loc_low, loc_high),
	bh.axis.Regular(nbins_loc,loc_low, loc_high))

#Fill with our df columns
hist3.fill(loc_data['x'],loc_data['y'],loc_data['z'])

#Define a funtion to plot in 2D from boost_histogram
def plothist2d(h,ax):
    return ax.pcolormesh(*h.axes.edges.T, h.view().T)

#Project our 3D histogram in each dimension
plothist2d(hist3.project(0, 1),axes[1])
plothist2d(hist3.project(1, 2),axes[2])
plothist2d(hist3.project(0, 2),axes[3])

#Do some formatting
axes[1].set_title("X-Y Projection")
axes[2].set_title("Y-Z Projection")
axes[3].set_title("X-Z Projection")

axes[1].set(xlabel="X pos",ylabel="Y pos")
axes[2].set(xlabel="Y pos",ylabel="Z pos")
axes[3].set(xlabel="X pos",ylabel="Z pos")

plt.savefig('sample_projections.png')


#Last we will do some conditional cuts on our data before plotting it using the Hist module instead of bh

#load the data we want, we will apply cuts on pe and hit count then plot times for the results
hitdata = tree.arrays(['pe','hits','times'],library="pd")
#Slice hitdata based on pe and hits
cut_pe = 150 #Cut below 150 pe
cut_hits = 5 #Cut below 5 hits
#Use loc method to apply conditional cuts on pe and hit count, returning only a column of times for events that meet the criteria
cut_data = hitdata.loc[(hitdata['pe'] > cut_pe) & (hitdata['hits']>cut_hits)]
#Confirm by printing
print(cut_data.head())

#Plot the resulting time profile by making a 1d hist then drawing
timeaxis = haxis.Regular(100,0,2000,underflow=True,overflow=True,name="time")
hist_time = Hist(timeaxis)
times = cut_data['times'].to_numpy()
hist_time.fill(times)

figtime, axtime = plt.subplots(figsize=(10,4))
hist_time.project("time").plot(ax=axtime)
axtime.set(ylabel='Counts',xlabel='Time [ns]')
axtime.set_title("Time after cuts: pe > {}, hits > {}".format(cut_pe,cut_hits))
axtime.grid()


plt.savefig('sample_time.png')

#Display plots last
plt.show()


#Now let's save the plots as pickled files so we can get the histograms later without parsing the root file again
pdictionary = {'histpe':histpe,'hist_time':hist_time,'hist3':hist3} #Save them as a dictionary so we can access them by name
with open("samplePlots.pkl", "wb") as f:
    pickle.dump(pdictionary, f)
