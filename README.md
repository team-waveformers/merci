# MERCI

Modular onlinE multi-thReaded C++ based waveform toolkIt, aka MERCI, is an online/offline modular analysis toolkit for use with MIDAS based experiments, specializing in waveform analysis for SiPM and PMT based experiments, MERCI's modular structure allows for optional high through-put online analysis, and live data display through the JSROOT server.

We include decoders and unpacking modules for several CAEN Digitizer units, including:

* VX2740
* V1725
* V1740
* V1730


These modules can be incorporated into your projects workflow.

A simple example project is given in demo/ and can be used as a starting point for a project


# System Requirements

1. MIDAS v.XXX installed - Only required for online analysis
2. ROOT > v6.0 installed
3. CMake > 3.12 installed
4. C++ 11 required, so gcc >= 4.8.1


# Build Instructions

Ensure that $ROOTSYS is defined

```
mkdir build
cd build
cmake ..
cmake --build . --target install -- -j
```

# How MERCI Works

MERCI is a set of modules for the `manalyzer` utility, for analyzing MIDAS data.
"Modules" are bits of code that do a certain task. 
For example, there are modules for unpacking data, finding baselines, finding pulses, showing plots, etc.

## For Users

1. Turn modules on and off with the cmake options.

When you make the project (i.e. `cd build`, `cmake ..`), you can actually add a lot more options.
For MERCI, we've specified some extra options that are relevant to you, with defaults that make sense in case you don't provide values for these options. 
Some options will be specific to the experiment you're working on. It's best to get someone in your group to teach you, but you can also have a look in the CMakeLists.txt file in your experiment's subdirectory of MERCI.
Making MERCI with the options on or off will look something like `cmake -DCMAKE_BUILD_TYPE=Debug -DHITFIND=ON -DDSFILTER=ON -DTOTPULSEFIND=OFF -DFIT=OFF -DPLOT=OFF ..`.

2. Configure modules with the json file

After the project is built, you can still configure options like pulse-finder parameters, filter parameters, etc.
These are things that you might need to reconfigure many times, or do some trial-and-error to find good values for. 
You need to specify a configuration file when you run MERCI, using the `--conf` flag.
For example, I run MERCI like `./verana.exe --mt -D/home/maia/rootfiles/ /vera00/zdata1/backup/run0$1.mid.gz -- --conf  /home/maia/merci/config/verahitHPK1025.json`

If a required parameter is missing in the configuration file, you'll get console output that looks like this:
```
BaselineModuleFactory::NewRunObject, run 2254, file /zdata1/backup/run02254.mid.gz
Reading config file from path = /homelocal/vera-daq/maia/merci/phaar/config/vera.json
terminate called after throwing an instance of 'nlohmann::detail::type_error'
  what():  [json.exception.type_error.302] type must be string, but is null
Aborted (core dumped)
```
Starting from the last line and reading back, I can see that there was an error related to some code that uses `nlohmann`'s `json` library: there was supposed to be a string value, but instead there was nothing/null. 
Further, I can see that the module loaded immediately before this error was the `BaselineModule`, so I'm probably missing something in my configuration for the baseline module.
To figure out what's missing, I can compare my `.json` file to examples from others in my group, or read the source code at `src/baseline_module.cxx` to see what settings the `BaselineModule` constuctor looks for in the configuration file. 

## For Developers

MERCI analyzes MIDAS events. 
To understand MIDAS events, see the MIDAS Wiki.
"MIDAS Event Structure" is a particularly helpful page, https://daq00.triumf.ca/MidasWiki/index.php/Event_Structure 

MERCI is an implementation of the Midas Analyzer (manalyzer) tool.
To understand MERCI, you must first understand manalyzer. 
Review the manalyzer documentation and example programs at https://bitbucket.org/tmidas/manalyzer/src/master/ .
`manalyzer_example_root_graphics.cxx` is quite useful.
Also, make sure you read the entire README. 

Some important things to know about manalyzer and MIDAS events

- MIDAS clients (like frontends) can send events to the MIDAS buffer. Other MIDAS clients can read these events. For example, the Logger reads events and saves them to a file.
- manalyzer can be run as a MIDAS client, to get events from the buffer. This is "online". 
- manalyzer can also be told to read events from a file instead. This is "offline". 
- The only difference between the two, from your point of view as the MERCI developer, is that you might care about how fast your code is for online anlaysis.
- There is also a MIDAS ODB, which holds information about how the experiment is configured. One of the first events in a Run will be an ODB dump, so you can see all the settings.

Why manalyzer?

- Manalyzer takes care of some hard things, like multithreading, running ROOT graphics, and extracting MIDAS events from files or the buffer.
- you don't always have to use manalyzer to read midas files. For simpler tasks, see https://bitbucket.org/tmidas/midas/src/develop/python/examples/file_reader.py . But manalyzer is nice because multithreading means it's fast (if -mt flag is used).

manalyzer flow?

- Manalyzer passes events between modules.
- The modules happen in the order they were registered - added in the cmakelists.
- This can allow manalyzer to be fast and multithreaded.
- There is a flow event for every MIDAS event that is received. They won't all be relevant to you. The first step in your manalyzer/MERCI module is to filter the events and only continue on the ones that contain data you care about. This is why we have code like:
```
AdcEventFlow* adc_flow = flow->Find<AdcEventFlow>();
if (!adc_flow)
    return flow; // just pass the event to the next module
```

Why MERCI?

- We do a lot of similar things when analyzing our data, like unpacking the MIDAS banks, finding baselines, etc.
- It's nice to be able to re-use code, and mix-'n-match different modules that we've already written for other projects.

### directory structure

Some modules are shared between many experiments.
These are located in the `src` directory.
For example, unpacking and holding data from a certain device (`src/unpack*****_module`, `src/T*****RawData`), finding baselines (`src/baseline_module`), fitting pulses (`src/fitpulse_module`), plotting waveforms (`src/wfviewer_module`), etc..

Some modules are very specific to one experiment.
These are located in each experiment's directory, like `phaar`, `dsadc`, `lolx`, `ets_mcgill`. 
For example, `dsadc/filter_module`, `lolx/LoLX_treewriter_module`

Each experiment's directory also contains a `CMakeLists.txt` that builds a MERCI executable for that experiment's data.
It provides build options for the modules that are relevant to that experiment. 
See the CMakeLists for more detail. They are a great place to start in understanding how the files relate to each other. 

All modules can be further configured after the executable is made, using a `.json` configuration file.
For example, to set fit parameters that might change or not be known at the time of building the executable. 
Each experiment's directory also contains a `/config` folder, with example `.json` files. 

# Presentations and more Information

Include links to presentations for users to learn more?
