# Pulse Models

## SARF

The *Single Avalanche Response Function* is implemented in `src/sarf.hxx` and used in `src/fitpulse_module.cxx`.
$$ V(t) = A \times \Bigg\{ \frac{1-k}{2\tau_S} \Bigg[ \exp\Bigg(\frac{\sigma^2}{2\tau_S^2} - \frac{t-t_0}{\tau_S}\Bigg) \times  \mathrm{erfc}\Bigg(\frac{\sigma}{\sqrt{2}\tau_S} - \frac{t-t_0}{\sqrt{2}\sigma}\Bigg) \Bigg] + \frac{k}{2\tau_L} \Bigg[ \exp\Bigg(\frac{\sigma^2}{2\tau_L^2} - \frac{t-t_0}{\tau_L}\Bigg) \times  \mathrm{erfc}\Bigg(\frac{\sigma}{\sqrt{2}\tau_L} - \frac{t-t_0}{\sqrt{2}\sigma}\Bigg) \Bigg] \Bigg\}, $$
where $t_0$ is the pulse time, $\sigma$ is the square root of the variance of the Gaussian component, $\tau_S$ and $\tau_L$ are the two exponential decay constants and $0 < k < 1$ is the relative contribution of the two fall time components in the SiPM pulse shape. $A$ is the pulse area (proportional to the pulse charge).

## Average SPE

The average *Single PhotoElectron" function is implemented in `src/avgspe.hxx` and used in `dsadc/fitpduplus_module.cxx`.
$$ S(t) = A \Big( e^{\frac{t_0-t}{\tau_1}} - e^{\frac{t_0-t}{\tau_2}} \Big) \theta(t-t_0), $$ 
where $\theta(t)$ is the stepfunction and $\tau_1>\tau_2$ are the two exponential decay constants.