/******************************************************************
 * Flow Object(s) for LoLX analysis *
 *
 * D. Gallacher
 * October 2021
 *
******************************************************************/

#ifndef __LOLXFLOW__
#define __LOLXFLOW__

#include "manalyzer.h"

#include <map>
#include <vector>
#include <string>

//LoLX Headers
#include "LoLXDS.hxx"

class LoLXConfig {

  public:
    std::string fBoardType; //V1740, WAVE, VX2740
    int fNumberChannelPerModule;
    double fNanosecsPerSample;
    int fNSamples;
    int fResolution;

    LoLXConfig(){ init(); }
    LoLXConfig(std::string s)
    {
      fBoardType = s;
      fNSamples = 0;

       if(fBoardType=="VX2740")
          {
             fNumberChannelPerModule=64;
             fNanosecsPerSample=8.0; // 125 MS/s = 8 ns
             fResolution=0xffff; // 16 bits
          }
       else if(fBoardType=="V1740")
          {
             fNumberChannelPerModule=16;
             fNanosecsPerSample=16.0; // 62.5 MS/s = 16 ns
             fResolution=0xfff; // 12 bits
          }
      else if(fBoardType=="WAVE")
           {
              fNumberChannelPerModule=15;
              fNanosecsPerSample=1.0;
              fResolution=0xffff; // FIXME
           }
       else
          std::cerr<<"LolXConfig:: Unknown ADC type "<<s<<std::endl;
    }

    void print() const
    {
       std::cout<<fBoardType<<" # of channels: "<<fNumberChannelPerModule
                <<" Sampling period: "<<fNanosecsPerSample<<" ns"
                <<" Number of Samples: "<<fNSamples<<std::endl;
    }

    void init(){
      fBoardType="null";
      fNumberChannelPerModule = 0;
      fNanosecsPerSample = 0.0;
      fNSamples = 0;
      fResolution = 0;
    }

};


class LoLXProcessorFlow: public TAFlowEvent
{
public:
  LoLXDS *event;
  LoLXConfig *conf;

public:
  LoLXProcessorFlow(TAFlowEvent* flow):TAFlowEvent(flow)
  {
    event = new LoLXDS();
    #ifdef USE_WDAQ
      conf = new LoLXConfig("WAVE");
    #else
      conf = new LoLXConfig("V1740");
    #endif
  }

  ~LoLXProcessorFlow()
  {
    event->Clear();
    delete event;
    delete conf;
  }

  inline int GetNumberOfChannels() const { return (int) event->GetNHit(); }

};

#endif
