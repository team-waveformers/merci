# LoLX MERCI Analysis code

Authors: David Gallacher, Andrea Capra, Bernadette Rebeiro, Liang Xie, Soud Al Kharusi, Lisa Rudolph,

List of components

- **LoLX_treewriter_module.hxx/cxx** -- AnalyzeFlowEvent Module for writing out data from analysis

- **LoLXDS.hxx/cxx** - Custom DS for LoLX Data, inherits from TObject

- **LoLXChannel.hxx/cxx** - Part of the LoLXDS, Contains channel info

- **LoLXLinkDef.hxx** - For linking LoLXDS to ROOT

- **LoLXFlow.hxx** - FlowObject for containing the LoLXDS, used in pulsefinder

- **LoLX_pulsefinder_module.cxx** - David's Rough pulsefinder, fallback if fitting fails

- **LoLX_waveform_module.cxx** - Live waveform display Module

- **LoLX_histo_module.cxx** - Live analysis output histogram module


## Running instructions
- Confirm which config file you want to use, lolxana will look for files in $INSTALL_DIR/lolx/config/
  - To change from the default ("master.json") you can copy master.json and rename it for example: "davids_config.json"
  - To run with the modified config file use:
    - ./lolxana_offline.exe /path/to/data/run000XX_000.mid.lz4 -- --conf davids_config.json
    - ./lolxana_online.exe -i 172.16.10.1 -R8095 -- --conf davids_config.json
    - Arguments passed after '--' are passed along to all the modules, and can be used for custom flags

- To run in offline mode, you will use the executable "lolxana_offline.exe" that can be found in $MERCI_HOME/bin/
  - Example:  to run for 100 events do
    - ./lolxana_offline.exe path_to_raw_midas_file/run0000.mid.lz4 -e100
  - A default root output file is created in $MERCI_HOME/bin/root_output_files/ with default filename  "outputXXXX.root", where XXX is the run number

- To run in online mode, you must be on the LoLX DAQ machine, since this requires a MIDAS buffer to connect to.
  - instructions to connect to the LoLX DAQ can be found on the wiki
  - Example: to run : "./lolxana_online.exe -i 172.16.10.1 -R8095 &> /dev/null"
  - This connects the client to the localhost (172.16.10.1 for centos on the WaveDAQ), and binds the output to port 8095
  - To see the online display consult the LoLX Run instructions page on the wiki

- After processing a midas run you can inspect the output with $MERCI_HOME/scripts/lolx/inspect_waveforms.C
  - You can also use these scripts as examples for working with the LoLX data structure (LoLXDS)
  - To use the LoLXDS without compiling macros, you can copy the $MERCI_HOME/scripts/lolx/rootlogon.C file to your working directory and update the path to the MERCI LoLXDS library
  - when calling "root" from a directory with "rootlogon.C", it will call rootlogon.C first before your selected inputs, we use this to load our libraries into the ROOT CINT so that the LoLXDS is available without compiling.
  - To create your own compiled macro, include the path to lolxds.so in your LD_LIBRARY_PATH environment variable and pass to gcc/g++
  - See "scripts/lolx/compile_root_macro.sh" for an example to compile

## Module descriptions

### LoLX_treewriter_module

The Tree Writer module is responsible for writing the data to a TTree in the output ROOT file.
The TTree is composed of a LoLXDS as the top-layer, with a vector of channels contained within.

The LoLXDS contains all run-summary and event-level information, while the LoLXChannel
contains the channel-level summary information, including the pulse-level information.

### LoLX_pulsefinder_module

The PulseFinder module, written by David, uses a threshold technique to identify the leading edges
of pulses by looking for the threshold of the waveform in absolute units.

When pulse candidates are identified the "end" of the pulse is found by looking for
the time when the pulse returns back to the baseline, physical pulses are >100 ns in length.
Shorter pulses are most-likely noise triggers and are discarded.

Pulse peak times are identified by the zero-crossing of the derivative within the pulse Window,
and sub-pulses are flagged as well. Pulse "charge" is the raw integral of the pulse from the leading edge
to the pulse end. Pulse "height" is the value of the pulse peak, assuming a baseline of 0

The pulse-finding is handled in "LoLXPulseFinderModule::PulseFinder()" which populates a LoLXChannel, added to the LoLXPulseFlow which contains the LoLXDS object

Tunable parameters (can be found in the master.json file in MERCI_CONFIG) include:

- **Sigma** - Threshold for sub-peak pulse-finding, in number of derivative baseline RMS's

- **Amplitude** - Rejection threshold for discarding pulses, pulse height < Amplitude are thrown away

- **Width** - Rejection threshold for discarding pulses, pulse width < Width are thrown away

- **Pedestal** - Number of bins at the start of the trace to average for RMS calculations

- **EndThresh** - Threshold for finding the end of the pulse, in number of baseline RMS's

- **Threshold** - Threshold for pulsefinding in V/ADC, absolute threshold

- **ResetWindow** - Number of bins to reset when scanning for subpeaks, used in utils::FindMinimums()

- **PeakScan** - Number of bins to look forward when scanning for a subpeak, used in utils::FindMinimums()

- **DerivativeWindow** - Size of window [bin-window,bin+window] used for derivative of trace for subpeak scan,used in utils::FindMinimums()

- **NumMins** - Maximum number of subpeaks to scan for,used in utils::FindMinimums()

### Baseline Module

The baseline module is located in src/ and performs two stages of baseline correction

Baseline modes:
 * "simple" - Formerly mode 0, does a baseline subtraction using a pedestal at the start of the trace and flips the waveform to the opposite polarity
 * "lolx" - Formerly mode 1, does baseline restoration taking into account the pre-amplifier saturation to correct overshoot on large pulses.
 * "simple_neg" - Formerly mode 2, does the same subtraction as simple but doesn't flip waveforms
 * "oscillating" - Formerly mode 4, smoothes a specific channel to subtract off the slow oscillation from other channels

The first stage is a simple baseline subtraction, to bring the waveform down to 0.
This is done by averaging the first $Pedestal (From config) and shifting.

The second stage applies the src/utils.hxx BaselineRestoration() algorithm, which applies a recursive
shift to the baseline depending on the signal impulse. This is used to correct for capacitor saturation Leading
to baseline overshoot.

For the WaveDAQ data, an additional stage is introduced with "oscillating" mode, this takes a chosen channel (empty) and smooths the waveform to subtract from all other channels. This is to correct the slow oscillation from the wavedaq noise.

Tunable Parameters (master.json file in MERCI_CONFIG):

- **Mode** - Set to 0 to just do subtraction, set to 1 to do restoration as well

- **Pedestal** - Number of bins at the start of the trace to use for averaging

- **DecayTau** - Decay constant for correction to baseline, approximately equal to the decay constant of the baseline overshoot

- **Threshold** - Threshold for t0 finding, in V/ADC, Traces without pulse candidates don't get baseline restoration

- **NanoSecPerSample** - Nanoseconds per sample to fall back on if timing isn't available.

- **ScanSize** - Used formerly for derivative of baseline for t0 finding, not used currently

- **RestorationWindow** - Number of bins to scan over for end of trace baseline restoration window

- **BinJumpWindow** - Number of bins to jump backwards if there is a slope at the end of the trace (pulse at end of waveform)

- **BinJumpSlope** - Slope threshold for jumping backwards if there's a pulse at the end of the waveform


### LoLX_histo_module

To be updated

### LoLX_waveform_module

Live waveform module, displays raw and baseline corrected waveforms in MERCI Online.

- **Canvas** - Whether or not to draw waveforms on canvases
- **Persistency** - Whether or not to create a "Persistency" waveform that averages N events together live
- **PersistencyChannel** - What channel to create the Persistency waveform for
- **Persistency Events Limit** - Number of events to use for persistency
- **Save PDF** - Save waveforms to PDFs
- **Save Events Limit** - Number of events to save to PDFs
- **Num Channels** - Number of waveform channels
- **NanoSecPerSample** - Nano seconds per sample for live display, wavedaq waveforms have constant timing assumed for online display
- **Raw WF Minimum** - Minimum of time axis for waveform display
- **Raw WF Maximum** - Maximum of time axis for waveform display
