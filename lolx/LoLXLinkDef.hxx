//Linkdef file is required to build dictionaries for linking against ROOT
//Classes must appear in this list in order of apeparance, for instance, LoLXChannel is included in DS and needs to be linked first
// See https://root.cern.ch/root/htmldoc/guides/users-guide/AddingaClass.html for details

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;

#pragma link C++ class LoLXChannel+;
#pragma link C++ class LoLXDS+;

#pragma link C++ class std::vector<LoLXChannel*>;

#pragma link C++ class std::vector<LoLXChannel*>::iterator;

#pragma link C++ class std::vector<LoLXChannel*>::const_iterator;


#endif
