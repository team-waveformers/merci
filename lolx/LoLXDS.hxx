/******************************************************************
 *  LoLX Specific Event DS *
 * Copied from ETS DAQ DS, simple class that inherits from TObject for ROOT compatibility
 * Expand upon for our actual desired list of variables
 *
 * October 2021
 *
 ******************************************************************/

#ifndef ROOT_LoLXDS
#define ROOT_LoLXDS

//Custom headers
#include "LoLXChannel.hxx"

//ROOT headers
#include "TObject.h"
#include "TTimeStamp.h"
#include <iostream>
#include <algorithm>

class LoLXDS : public TObject {
private:

   //MIDAS Header information
   ///Run number        (midas run)
   int         run;
   ///Sub-Run number    (midas sub-run)
   int         subRun;
   ///Run type (RunID)
   int         runType; // Read from ODB
   ///sequence number   (midas subrun)
   int         seqNum;
   ///Trigger type      (midas trigger mask)
   uint64_t         trigID;
   ///Event ID          (midas subrun event index)
   int         evID;
   ///Event time        (midas subrun event time from 125MHz clock) in unix time (s)
   double      evTime;
   ///trigger timestamp from digitizer
   double      triggerTime;
  ///trigger timestamp (midas time stamp)
   TTimeStamp    timeStamp;
   ///Start of the run time
   TTimeStamp    fRunStartTime;
   ///Cut ID - event-level, can be used in the future to identify bad events from good
   uint        eventCutID;
   ///Number of hit Channels in this event
   int         nHit;
   /// Number of total channels
   int         nTot;

   /// Total event pe
  double totalpe;
  /// total pe in longpass filtered channels
  double totalpeLPfilt;
  /// total pe in bandpass filtered channels
  double totalpeBPfilt;
  /// total pe in bare sipm channels
  double totalpebare;

  /// fraction of total pe in the channel with the most pe
  double fmaxpe;
  /// id of the brightest channel
  Int_t fmaxpeid;
  /// fraction of total pe in the longpass channel with the most pe
  double fmaxpeLP;
  /// id of the brightest longpass channel
  int fmaxpeidLP;

  int fitterFlag; //Has the pulsefitter ran?

  int analysisFlag;// Has the analysis module ran?

  double event_t0; /// What is the t0 of this event, defined in LoLXAnalysisModule
  /// Channel summary data from each channel
  std::vector<LoLXChannel*> vChannel;

  /// Number of subevents in this event, calculated in LoLXAnalysisModule
  int sub_event_count;
  /// Time of the sub events
  std::vector<double> sub_event_times;

public:
   LoLXDS();
   ~LoLXDS();
   LoLXDS(const LoLXDS &rhs) : TObject() { Init(); CopyObj(rhs); }
   LoLXDS &operator=(const LoLXDS &rhs) { CopyObj(rhs); return *this; }

   void     Init();
   void     Clear(Option_t *option = "");
   void     CopyObj(const LoLXDS &rhs);

    // MIDAS header and event summary related information
   /// Set the runID for this event
   void     SetRun(int i) { run = i; }
   /// Set the subrunID for this event
   void     SetSubRun(int i) { subRun = i; }
   /// Set the run type for this event
   void     SetRunType(int i) { runType = i; }
   /// Set the sequence number for this event
   void     SetSeqNum(int i) { seqNum = i; }
   /// Set the trigger type for this event
   void     SetTrigID(uint64_t i) { trigID = i;  }
   /// Set the event ID for this event (midas subrun event index)
   void     SetEVID(int i) { evID = i; }
   /// Set the event time (midas subrun event time)
   void     SetEVTime(double t) { evTime  = t; }
   /// Set the trigger time stamp (From board)
   void     SetTriggerTime(double ts) { triggerTime  = ts; }
   /// Set the triggered time-stamp of this event
   int      SetTimeStamp(TTimeStamp *aTimeStamp);
   /// Set the triggered time-stamp of this event
   int      SetRunStartTime(TTimeStamp *_RunStartTime);
   /// Set the EventCutID for this event (low-level bad-cut idenification)
   void     SetEventCutID(uint i) {eventCutID = i;}

   ///Set the pulsfitter module flag
   void     SetFitterFlag(int flag){fitterFlag=flag;}
   ///Set the analysis module flag
   void     SetAnalysisFlag(int flag){analysisFlag=flag;}


   /// Get the run number for this event
   int    GetRun(){ return run; }
   /// Get the sub-run ID for this event
   int    GetSubRun(){ return subRun; }
   /// Get the run type for this run (Trigger configuration)
   int    GetRunType(){ return runType; }
   /// Get the sequence number for this event
   int    GetSeqNum() { return seqNum; }
   /// Get the global trigger ID for this event
   uint64_t    GetTrigID() { return trigID; }
   /// Get the event ID for this event (midas subrun event index)
   int    GetEVID(){ return evID; }
   /// Get the event time (Unix time in seconLoLXDS)(midas subrun event time)
   double GetEVTime(){ return evTime; }
   /// Get the triggered time-stamp of this event
   TTimeStamp   *GetTimeStamp(){return &timeStamp; }
   /// Get the triggered time-stamp of this event
   TTimeStamp   *GetRunStartTime(){return &fRunStartTime;}
   /// Set the EventCutID for this event (low-level bad-cut idenification)
   uint       GetEventCutID() {return eventCutID;}

   ///Set the pulsfitter module flag
   int     GetFitterFlag(){return fitterFlag;}
   ///Set the analysis module flag
   int     GetAnalysisFlag(){return analysisFlag;}


   /// Get the i-th channel object for this event
   LoLXChannel* GetChannel(int i);
   //Find the channel with a given id (SiPMID)
   LoLXChannel* FindChannel(int id);
   /// Check to see if the Channel object exists for this channel
   Bool_t    ExistChannel(int i);
   /// Add a channel to the list of channels
   void AddChannel(LoLXChannel *aChannel);
   /// Add a new channel to the list of channels
   LoLXChannel*    AddNewChannel();
   /// Empty the waveforms before saving to output file
   void      PruneWFs();
   /// Get the number of hit Channels in this event
   int     GetNHit();
   // Get the total number of channels
   int     GetNChannels(){return nTot;};


   /// Set the total pe for this event
  void SetTotalPE(double _totalpe){totalpe = _totalpe;}
  /// Get the total pe for this event
  double GetTotalPE(){return totalpe;}

  /// Set the total pe in the longpass channels for this event
  void SetTotalLPPE(double _totalpeLPfilt){totalpeLPfilt =_totalpeLPfilt;}
  /// Get the total pe in the longpass channels for this event
  double GetTotalLPPE(){return totalpeLPfilt;}

  /// Set the total pe in the bandpass channels for this event
  void SetTotalBPPE(double _totalpeBPfilt){totalpeBPfilt = _totalpeBPfilt;}
  /// Get the total pe in the bandpass channels for this event
  double GetTotalBPPE(){return totalpeBPfilt;}

  /// Set the total pe in the bare channels for this event
  void SetTotalBarePE(double _totalpebare){totalpebare = _totalpebare;}
  /// Get the total pe in the bare channels for this event
  double GetTotalBarePE(){return totalpebare;}

  /// Set the fraction of total pe in the brightest channel for this event
  void SetFMaxPE(double _fmaxpe){fmaxpe = _fmaxpe;}
  /// Get the fraction of total pe in the brightest channel for this event
  double GetFMaxPE(){return fmaxpe;}
  /// Get the pe of the brightest channel for this event
  double GetMaxPE(){return fmaxpe*totalpe;}

  /// Set the id of the brightest channel for this event
  void SetFMaxPEID(Int_t _fmaxpeid){fmaxpeid = _fmaxpeid;}
  /// Get the id of the brightest channel for this event
  Int_t GetFMaxPEID(){return fmaxpeid;}

  /// Set the fraction of total light in the brightest longpass channel for this event
  void SetFMaxPELP(double _fmaxpeLP){fmaxpeLP = _fmaxpeLP;}
  /// Get the fraction of total light in the brightest longpass channel for this event
  double GetFMaxPELP(){return fmaxpeLP;}
  /// Get the pe in the brightest longpass channel for this event
  double GetMaxPELP(){return fmaxpeLP*totalpeLPfilt;}

  /// Set the id of the brightest longpass channel for this event
  void SetFMaxPELPID(Int_t _fmaxpeidLP){fmaxpeidLP = _fmaxpeidLP;}
  /// Get the id of the brightest longpass channel for this event
  Int_t GetFMaxPELPID(){return fmaxpeidLP;}

  ///Set the t0 of this event
  void SetEventT0(double t){ event_t0 = t;}
  ///Get the t0 of this event
  double GetEventT0(){return event_t0;}

  ///Set the number of subevents  (1 == only 1 physics event, 2 == main triggered event + sub event)
  void SetSubEventCount(int is){sub_event_count = is;}
  /// Get the number of sub events
  int GetSubEventCount(){return sub_event_count;}
  ///Add a time to list of subevent times
  void AddSubEventTime(double t){sub_event_times.push_back(t);}
  ///Get the list of sub event times
  const std::vector<double>* GetSubEventTimes(){return &sub_event_times;}


   ClassDef(LoLXDS,4)  //Event structure
};

#endif
