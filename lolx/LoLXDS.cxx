#include "LoLXDS.hxx"
#include "LoLXChannel.hxx"

ClassImp(LoLXDS)

LoLXDS::LoLXDS()
{
  Init();
}

LoLXDS::~LoLXDS()
{
  Clear();
}

void LoLXDS::CopyObj(const LoLXDS &rhs)
{
  run = rhs.run;
  subRun = rhs.subRun;
  seqNum = rhs.seqNum;
  trigID = rhs.trigID;
  evID = rhs.evID;
  evTime = rhs.evTime;
  triggerTime = rhs.triggerTime;
  timeStamp = rhs.timeStamp;
  eventCutID = rhs.eventCutID;

  nHit = rhs.nHit;
  nTot = rhs.nTot;

  fitterFlag = rhs.fitterFlag;
  analysisFlag = rhs.analysisFlag;

  totalpe     = rhs.totalpe;
  totalpeLPfilt = rhs.totalpeLPfilt;
  totalpeBPfilt = rhs.totalpeBPfilt;
  totalpebare = rhs.totalpebare;

  fmaxpe = rhs.fmaxpe;
  fmaxpeid = rhs.fmaxpeid;
  fmaxpeLP = rhs.fmaxpeLP;
  fmaxpeidLP = rhs.fmaxpeidLP;

  event_t0 = rhs.event_t0;
  sub_event_count = rhs.sub_event_count;

  sub_event_times = rhs.sub_event_times;

  std::vector<LoLXChannel*>::iterator i;
  for (i=vChannel.begin(); i != vChannel.end(); i++)
  {
    delete (*i);
    vChannel.resize(0);
  }

  vChannel.resize((rhs.vChannel).size());
  for (unsigned i=0; i < (rhs.vChannel).size(); i++)
  {
    if ((rhs.vChannel)[i]) vChannel[i] = dynamic_cast<LoLXChannel*>((rhs.vChannel[i])->Clone()); // Use Clone() for proper polymorphism
    else vChannel[i] = 0;
  }


}

///Set timestamp for the event
int LoLXDS::SetTimeStamp(TTimeStamp *aTimeStamp)
{
  //Set this event's timestamp
  if ( aTimeStamp ){
    timeStamp= *aTimeStamp;
    return 0;
  }
  else return -1;
}

///Set timestamp for the event
int LoLXDS::SetRunStartTime(TTimeStamp *_RunStartTime)
{
  //Set this event's timestamp
  if ( _RunStartTime ){
    fRunStartTime= *_RunStartTime;
    return 0;
  }
  else return -1;
}

/// Return i-th waveform from the list of Channel's, Valid up to n-hit channels
LoLXChannel* LoLXDS::GetChannel(int i)
{
  if ( vChannel.empty() ) return 0;
  LoLXChannel *aChannel = NULL;
  if ( i < vChannel.size() ) aChannel = vChannel[i];
  return aChannel;
}
/// Return channel from the list of Channel's, given an input id
LoLXChannel* LoLXDS::FindChannel(int id)
{
  if ( vChannel.empty() ) return 0;
  LoLXChannel *aChannel = NULL;
  std::vector<LoLXChannel*>::iterator it;
  it = std::find_if(vChannel.begin(),vChannel.end(),[id](const LoLXChannel* ch){ return ch->GetChannelID() == id;} );
  if(it == vChannel.end()) return 0;
  aChannel = *it;
  return aChannel;
}

//Check to see if channel obj exists
Bool_t LoLXDS::ExistChannel(int i)
{
  if ( vChannel.empty() ) return 0;
  if ( GetChannel(i) ) return 1; //Check for null pointer
  return 0;
}

int LoLXDS::GetNHit()
{
  int nhit = 0;
  for(int iC =0;iC<nTot;iC++) GetChannel(iC)->GetT0_Chan() > 0 ? nhit++ : nhit;
  nHit = nhit; //Assign internal value
  return nhit;
}

/// Add a new Channel to this event.
void LoLXDS::AddChannel(LoLXChannel *aChannel)
{
  vChannel.push_back(aChannel);
  nTot++;
}

/// Add a Channel to this event and return the pointer
LoLXChannel* LoLXDS::AddNewChannel()
{
  LoLXChannel *aChannel = new LoLXChannel();
  vChannel.push_back(aChannel);
  nTot++;
  return aChannel;
}

void LoLXDS::PruneWFs()
{
  for(uint iChannel =0;iChannel<vChannel.size();iChannel++){
    vChannel[iChannel]->PruneWFs();
  }
}

/// Reset event
void LoLXDS::Clear(Option_t *option)
{
  Init();

  for(uint iChannel =0;iChannel<vChannel.size();iChannel++){
    delete (vChannel[iChannel]);
  }
  vChannel.clear();

  sub_event_times.clear();
}

///Initialize parameters
void LoLXDS::Init()
{
  run         = -1;
  subRun      = -1;
  seqNum      = 0;
  trigID      = 0;
  evID        = 0;
  evTime      = 0;
  triggerTime = 0;
  eventCutID  = 0;

  nHit        = 0;
  nTot        = 0;

  fitterFlag  = 1;// Change to 0 when fitter is implemented - DG
  analysisFlag = 0;

  totalpe       = -99.0;
  totalpeLPfilt = -99.0;
  totalpeBPfilt = -99.0;
  totalpebare   = -99.0;

  event_t0 = -9999;
  sub_event_count = -1;

  fmaxpe     = -99.0;
  fmaxpeid   = -99;
  fmaxpeLP   = -99.0;
  fmaxpeidLP = -99;

}
