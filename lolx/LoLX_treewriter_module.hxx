/******************************************************************
 *  LoLX Specific TTree Writer *
 *
 *
 * October 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include <map>


#include "TTree.h"
#include "TObjString.h"
#include "TError.h"

//LolX Specific headers
#include "LoLXDS.hxx"
#include "LoLXChannel.hxx"
#include "LoLXFlow.hxx"
#include "adcflow.hxx"

#include "json.hpp"
using json = nlohmann::json;

//Read preprocessor flag value as string literal
#define STRING(s) #s
#define STRSTRING(s) STRING(s)

class LoLXTreeWriterFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};

class LoLXTreeWriterModule : public TARunObject
{
public:
   LoLXTreeWriterFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors
   std::string config_path; // Config file path

private:
  int fNchan;
  TTree *Tree; //TTree to contain event information
  LoLXDS *lDS; //Custom DS for LoLX

  // per-run info
  int run_num;
  int subrun_num;
  int runType;
  UInt_t startTime;
  time_t start_time;
  TTimeStamp start_ts;


public:
   LoLXTreeWriterModule(TARunInfo* runinfo, LoLXTreeWriterFlags* f): TARunObject(runinfo),
							     fFlags(f),
							     fCounter(0), fError(0),
							     fNchan(0),Tree(0),lDS(0)
   {
      if(fFlags->fVerbose) std::cout<<"LoLXTreeWriterModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="LoLXTreeWriter";
#endif

#ifdef CONFIG_PATH
    config_path = STRSTRING(CONFIG_PATH) + fFlags->fConfigName;//Read config file path from CMAKE source dir (lolx)
#else
    config_path = fFlags->fConfigName;//If path isn't defined look in flags only
#endif
    std::cout << "Reading config file from path = "<< config_path << "\n";
    std::ifstream fin(config_path);
    json settings;
    fin>>settings;
    if(fFlags->fVerbose)
       std::cout<<"LoLXTreeWriterModule Json parsing success!"<<std::endl;
    fin.close();


   }

   //Required functions
   /// Begin run method
   void BeginRun(TARunInfo* runinfo);
   /// End run method
   void EndRun(TARunInfo* runinfo);
   /// Analyze Flow event method
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow);
   /// Get sub run number from file name
   int GetSubRun(std::string midasFilename);

};


class LoLXTreeWriterModuleFactory : public TAFactory
{
public:
   LoLXTreeWriterFlags fFlags;

public:
   void Usage()
   {
      printf("LoLXTreeWriterModuleFactory flags:\n");
      printf("--verbose\tprint status information\n");
   }

   void Init(const std::vector<std::string> &args)
   {
      printf("LoLXTreeWriterModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++){
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
             if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
	   }

   }

   void Finish()
   {
      printf("LoLXTreeWriterModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("LolXTreeWriterModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new LoLXTreeWriterModule(runinfo, &fFlags);
   }

};

static TARegister tar(new LoLXTreeWriterModuleFactory);
