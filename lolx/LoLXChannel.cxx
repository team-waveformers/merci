#include "LoLXChannel.hxx"

ClassImp(LoLXChannel)

LoLXChannel::LoLXChannel()
{
  Init();
}

LoLXChannel::~LoLXChannel()
{
  Clear();
}

///Initialize parameters
void LoLXChannel::Init()
{
  //init to silly values so people know they weren't set correctly
  type = -1;
  summed = -1;
  sipmID = -1;
  channelID = -1;
  boardID = -1;
  boardChannel = -1;
  sipmModel = -1;

  nPE = -9999.0;
  aPE = -9999.0;
  t0_chan = -999.0;

  baseline =-1.0;
  baseline_rms = -1.0;

  npulses = 0;

  sipmModelNames = {{0,"hamamatsu_vuv4"},{1,"fbk_hd3"}};

}


void LoLXChannel::CopyObj(const LoLXChannel &rhs)
{

  type = rhs.type;
  summed = rhs.summed;
  boardID = rhs.boardID;
  channelID = rhs.channelID;
  sipmID = rhs.sipmID;
  npulses = rhs.npulses;
  boardChannel = rhs.boardChannel;
  sipmModel = rhs.sipmModel;

  nPE = rhs.nPE;
  aPE = rhs.aPE;
  t0_chan = rhs.t0_chan;
  baseline = rhs.baseline;
  baseline_rms = rhs.baseline_rms;

  vWF_corr = rhs.vWF_corr;
  vWF_corrTimes = rhs.vWF_corrTimes;

  vPulseLeadingTime = rhs.vPulseLeadingTime;
  vPulsePeakTime = rhs.vPulsePeakTime;
  vPulseWidth = rhs.vPulseWidth;
  vPulseCharge = rhs.vPulseCharge;
  vPulseHeight = rhs.vPulseHeight;
  vPulsePreBaseline = rhs.vPulsePreBaseline;


}

/// Reset event, if additional DS are nested, need to call their destructors here
void LoLXChannel::Clear(Option_t *option)
{
  Init();

  vWF_corr.clear();

  vPulseLeadingTime.clear();
  vPulsePeakTime.clear();
  vPulseCharge.clear();
  vPulseHeight.clear();
  vPulseWidth.clear();
  vPulseType.clear();
  vPulsePreBaseline.clear();


}



void LoLXChannel::AddPulseLeadingTime(double _t0)
{
  vPulseLeadingTime.push_back(_t0);
  if(vPulseLeadingTime.size()==1) t0_chan =_t0;
  npulses++;
}

double LoLXChannel::GetPulseLeadingTime(int iPulse)
{
   double leading = -9999.0;
   if ( iPulse < vPulseLeadingTime.size() )  leading = vPulseLeadingTime[iPulse];
   else{ std::cout << "Pulse index does not exist, returning -9999"<<std::endl; }
   return leading;
}

void LoLXChannel::SetPulseWidth(int iPulse,double width)
{
  if ( iPulse < vPulseWidth.size() ) vPulseWidth[iPulse] = width;
  else{ std::cout << "Eror: Pulse index does not exist!!"<<std::endl;}
}

double LoLXChannel::GetPulseWidth(int iPulse)
{
   double width = -9999.0;
   if ( iPulse < vPulseWidth.size() )  width = vPulseWidth[iPulse];
   else{ std::cout << "Pulse index does not exist, returning -9999"<<std::endl; }
   return width;
}


double LoLXChannel::GetPulsePeakTime(int iPulse)
{
  double peak = -9999.0;
  if ( iPulse < vPulsePeakTime.size() ) peak = vPulsePeakTime[iPulse];
  else{ std::cout << "Pulse index does not exist, returning -9999"<<std::endl; }
  return peak;
}

double LoLXChannel::GetPulseCharge(int iPulse)
{
  double charge = -9999.0;
  if ( iPulse < vPulseCharge.size() ) charge = vPulseCharge[iPulse];
  else{ std::cout << "Pulse index does not exist, returning -9999"<<std::endl; }
  return charge;
}

double LoLXChannel::GetPulseHeight(int iPulse)
{
  double height = -9999.0;
  if ( iPulse < vPulseHeight.size() )  height = vPulseHeight[iPulse];
  else{ std::cout << "Pulse index does not exist, returning -9999"<<std::endl; }
  return height;
}

int LoLXChannel::GetPulseType(int iPulse)
{
  int type = -999;
  if ( iPulse < vPulseType.size() )  type = vPulseType[iPulse];
  else{ std::cout << "Pulse index does not exist, returning -9999"<<std::endl; }
  return type;
}

double LoLXChannel::GetPulsePreBaseline(int iPulse)
{
  double base = -999;
  if ( iPulse < vPulsePreBaseline.size() )  base = vPulsePreBaseline[iPulse];
  else{ std::cout << "Pulse index does not exist, returning -9999"<<std::endl; }
  return base;
}



//Remove the wf data
void LoLXChannel::PruneWFs(){
  vWF_corr.clear();
  vWF_corrTimes.clear();

  vPulseLeadingTime.clear();
  vPulsePeakTime.clear();
  vPulseCharge.clear();
  vPulseHeight.clear();
  vPulseWidth.clear();
  vPulseType.clear();
  vPulsePreBaseline.clear();

}
