/******************************************************************
 *  Simple LoLX Pulse Finder *
 *
 * D. Gallacher
 * October 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "baselineflow.hxx"
#include "adcflow.hxx"
#include "LoLXFlow.hxx"
#include "LoLXDS.hxx"
#include "LoLXChannel.hxx"
#include "utils.hxx"
#include "TWDRawData.hxx"

#include "TMath.h"
#include "TH1D.h"
#include "TH1.h"
#include "TGraph.h"
#include "TGraphSmooth.h"


#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <cassert>
#include <cstdlib>
#include <utility>


#include "json.hpp"
using json = nlohmann::json;

//Read preprocessor flag value as string literal
#define STRING(s) #s
#define STRSTRING(s) STRING(s)

class LoLXPulseFinderFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};


class LoLXPulseFinderModule: public TARunObject
{
public:
   LoLXPulseFinderFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors
   int nSecsPerSample;//Nano seconds per sample fallback

private:
   int fNtotPulses;

   Utils *util;
   bool fFixed;
   double fAmplitude; //Skip pulses with less than this charge
   double fWidth; //Skip pulses that are shorter than this value
   double fNsigma;  //Threshold for pulse finding in multiples of derivative baseline rms (for PulseFinder)
   double fThreshold;  //Threshold for pulse finding in pulse threshold value (V)
   double fEndThresh; //Threshold of baseline RMS's to be at "end of pulse"
   int fPedestal;  //Number of samples for derivative baseline rms
   int fDerWindow;// +/- derWindow  = size of window to calculate finite difference derivative
   int fPeakScanWidth;// How far ahead to look for the peak from leading edge (in bins)
   int fNmins;// Number of minimums to scan for
   int fResetWindow;// Number of bins to reset when scanning for minimums


public:
   LoLXPulseFinderModule(TARunInfo* runinfo, LoLXPulseFinderFlags* f): TARunObject(runinfo),
                                                               fFlags(f), fCounter(0), fError(0),
                                                               fNtotPulses(0)
   {
      if(fFlags->fVerbose) std::cout<<"LoLXPulseFinderModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="PulseFinder";
#endif
      std::string config_path;
#ifdef CONFIG_PATH
    config_path = STRSTRING(CONFIG_PATH) + fFlags->fConfigName;//Read config file path from CMAKE source dir (lolx)
#else
    config_path = fFlags->fConfigName;//If path isn't defined look in flags only
#endif
      std::cout << "Reading config file from path = "<< config_path << "\n";
      std::ifstream fin(config_path);
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"LoLXPulseFinderModule Json parsing success!"<<std::endl;
      fin.close();

      fFixed = settings["Pulse"]["Fixed"].get<bool>();
      fAmplitude = settings["Pulse"]["Amplitude"].get<double>();
      fNsigma = settings["Pulse"]["Sigma"].get<double>();
      fPedestal = settings["Pulse"]["Pedestal"].get<int>();
      fEndThresh = settings["Pulse"]["EndThresh"].get<double>();
      fWidth = settings["Pulse"]["Width"].get<double>();
      fThreshold = settings["Pulse"]["Threshold"].get<double>();
      fNmins = settings["Pulse"]["NumMins"].get<int>();
      fDerWindow = settings["Pulse"]["DerivativeWindow"].get<int>();
      fPeakScanWidth = settings["Pulse"]["PeakScan"].get<int>();
      fResetWindow = settings["Pulse"]["ResetWindow"].get<int>();


      util = new Utils();
      util->SetVerbosity(int(fFlags->fVerbose));
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"LoLXPulseFinderModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"LoLXPulseFinderModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;

   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"LoLXPulseFinderModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<" Total number of Pulses: "<<fNtotPulses<<std::endl;
   }


   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {


      BaselineEventFlow* corr_flow = flow->Find<BaselineEventFlow>();
      if( !corr_flow )
         {
            if(fFlags->fVerbose) std::cout << "Error Pulsefinder: Baseline flow not found" <<std::endl;
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      else if(!corr_flow->getNchannels()){
        if(fFlags->fVerbose) std::cout << "Error Pulsefinder: Baseline flow empty"<< std::endl;
        return flow;
      }

      AdcEventFlow* adc_flow = flow->Find<AdcEventFlow>();
      if( !adc_flow )
          {
            ++fError;
       #ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
       #endif
            return flow;
          }

      LoLXProcessorFlow* pulse_flow  = new LoLXProcessorFlow(flow);
      int nSamples = corr_flow->getNsamples(0);
      pulse_flow->conf->fNSamples = nSamples;
      nSecsPerSample = pulse_flow->conf->fNanosecsPerSample;

      if( fFlags->fVerbose )
         std::cout<<"LoLXPulseFinderModule::AnalyzeFlowEvent # of ch: "<<corr_flow->getNchannels()<<std::endl;



      for(int iChan=0; iChan < corr_flow->getNchannels(); iChan++){
          if(nSamples == 0 ){
            if(fFlags->fVerbose) std::cout << "No samples in WF# " << iChan << std::endl;
            continue;
          }

          const std::vector<double>* wf = corr_flow->getChannelVector(iChan);
          double rms = corr_flow->getBaselineRMS(iChan);

          //Header info
          int daq_chan = -1;
          std::map<int,unsigned int>::iterator it;
          it = corr_flow->fChannelMap.find(iChan);
          if(it != corr_flow->fChannelMap.end()) daq_chan = it->second;

          TH1D hCorr;
          #ifdef USE_WDAQ
            const std::vector<double>* wf_times = corr_flow->getChannelTimes(iChan);
            if(wf_times->size() != 0){
                util->ConvertRawWF_WDAQ(hCorr,wf,wf_times,daq_chan);
            }else{
              if(fFlags->fVerbose) std::cout << "Missing time vector, using constant timing from config"<<std::endl;
              util->ConvertRawWF(hCorr,wf,daq_chan,nSecsPerSample);
            }
          #else
            util->ConvertRawWF(hCorr,wf,daq_chan,nSecsPerSample);
          #endif


          LoLXChannel *lChan = PulseFinder(pulse_flow,hCorr,fThreshold);
          if(lChan == NULL ){
            if(fFlags->fVerbose) std::cout << "No pulses found in channel# "<< iChan <<std::endl;
            continue;
          }


          lChan->SetBaselineRMS(rms);
          lChan->SetBaseline(corr_flow->getBaseline(iChan));
          //Set the rest of the information in lolxtreewriter
          lChan->SetChannelID(daq_chan);
          #ifdef WRITE_WAVEFORMS
            lChan->SetWF_BaselineRemoved(corr_flow->getChannelVector(iChan));
          #if ( defined(USE_WDAQ) && defined(WRITE_WAVEFORMS))
            // Uncomment to write out non-baseline corrected waveforms
            // const TWDRawChannel *ch_wdaq = (const TWDRawChannel*) adc_flow->getChannel(iChan);
            // const std::vector<double> *v_times = ch_wdaq->GetTimes();
            // std::vector<double> vt = *v_times;
            // lChan->SetWFTimes_BaselineRemoved(vt);
            lChan->SetWFTimes_BaselineRemoved(corr_flow->getChannelTimes(iChan));
          #endif
          #endif
      }


      pulse_flow->event->GetNHit();//Ensure NHit is populated after pulse-finding
      flow = pulse_flow;
      ++fCounter;
      return flow;
   }

  //David's Pulsefinder, No derivatives, because the derivative is slow, thresh is in volts not sigmas
  LoLXChannel* PulseFinder(LoLXProcessorFlow *lFlow,TH1D &hCorr,double thresh)
 {

   int offset = 5;
   #ifdef USE_WDAQ
      offset = 5;
   #endif

   double baselineRMS = util->CalculateBaselineRMS(hCorr,offset,offset+fPedestal);
   double aBaseline = util->CalculateBaseline(hCorr,offset,offset+fPedestal);

   LoLXChannel *aChan = lFlow->event->AddNewChannel();
   const int width = int(300.0/hCorr.GetBinWidth(1));
   const int nBins = hCorr.GetNbinsX();
   //FindMinimum parameters
   std::vector<double> findMinPars = {double(fDerWindow),double(fResetWindow),double(fPeakScanWidth),double(fNmins),double(fPedestal),fNsigma,aBaseline};
   //pars[derivativeWindow,resetWindow,peakScanWindow,num_mins,baselineWindow,nSigmas,baseline]
   // list of bin numbers for t0's of pulse-candidates
   std::vector<double> pulseEdge;
   // List of bin numbers corresponding to the end of the pulse
   std::vector<double> pulseEnd;
   //Container for subpeak times and voltages <bin number,voltage>
   std::vector<std::vector<std::pair<int,double>>> subpeaks;
   //List of baselines before pulses
   std::vector<double> pulsePreBaselines;

   //Loop over the derivative to find candidate pulses
   for(int iBin = offset ; iBin < nBins ; iBin++){
     // Did we find the end of the pulse? If not pulse end = peakTime+300ns
     bool foundEnd  = false;

     //Scan over all bins in the derivative trace and determine which are below threshold
     if(hCorr.GetBinContent(iBin) < thresh && hCorr.GetBinContent(iBin+1) < thresh )
     {
       double pTime = hCorr.GetBinCenter(iBin);
       pulseEdge.push_back(pTime);

       //Find baseline
       double prePulseBaseline = util->CalculateBaseline(hCorr,iBin-offset-fPedestal,iBin-offset);
       findMinPars[6] = prePulseBaseline;
       pulsePreBaselines.push_back(prePulseBaseline);

       //Arbitrarily set for now, this will be reset to iBin+300ns if we fail the endBin scan
       int endBin = iBin+offset;
       //Loop over bins in the trace to find time corresponding to the pulse-end
       //If the bin is close to the
       for(int iPT = iBin+2; iPT < nBins; iPT++){
         //Are we close to the baseline?
         if((TMath::Abs(hCorr.GetBinContent(iPT)) + prePulseBaseline - baselineRMS*fEndThresh) < 0.0 ){
           pTime = hCorr.GetBinCenter(iPT);
           pulseEnd.push_back(pTime);
           endBin = iPT;
           foundEnd = true;
           goto next;
         }//End end of pulse check
       }//End loop for end of pulse
       next:

       //Edge-case handling
       if(endBin >= nBins) endBin = nBins;
       //FindMinimums returns a list of std::pairs<int,double> with the bin number of the peak and
       // the value at the peak - value at the leading edge of the pulse
       subpeaks.push_back(util->FindMinimums(hCorr,iBin-fPedestal,endBin,findMinPars));

       if(foundEnd){
         //Start looking after the end of the first pulse
         iBin = endBin + 1;
       }else {
         pulseEnd.push_back(hCorr.GetBinContent(iBin)+300);
         iBin+=width-1; // Jump ahead so we don't double count
       }

     }//End conditional for threshold
   }//End loop over all bins



   if(fFlags->fVerbose) std::cout << "Found " << pulseEdge.size()<<" pulses.."<<std::endl;

   //Loop over pulses found and add properties to DS
   for(unsigned int iPulse = 0; iPulse < pulseEdge.size(); iPulse++){

     double tStart = pulseEdge[iPulse];
     double tEnd = pulseEnd[iPulse];

     if(subpeaks[iPulse].empty()){
       if(fFlags->fVerbose) std::cout << "No peaks found above threshold"<<std::endl;
       continue;
     }

     double tPeak = hCorr.GetBinCenter(subpeaks[iPulse][0].first); //First entry is bin number of minimum
     double vPeak = fabs(subpeaks[iPulse][0].second); //Second is value at minimum - leading edge
     double preBase = pulsePreBaselines[iPulse];//Baseline calculated before the pulse
     double deltaT = tEnd-tStart;
     if(deltaT < 0) continue; //Skip non-physical pulses
     int startIntegral = hCorr.FindBin(pulseEdge[iPulse]);
     int endIntegral = hCorr.FindBin(pulseEnd[iPulse]);
     if(endIntegral < startIntegral) {
        endIntegral = hCorr.GetNbinsX();
     }     //This is not actually Q yet, needs ADC rescaling
     double totalQ = fabs(hCorr.Integral(startIntegral,endIntegral));
     //totalQ -= pulseBaselines[iPulse] * deltaT;

     // We can divide by the average amplitude or integral of a single PE pulse and estimate this way.
     // if(fFlags->fVerbose) std::cout << "Pulse properties: "<< Form(" tStart = %f, tEnd=%f, tPeak=%f, vPeak=%f, totalQ =%f",tStart,tEnd,tPeak,vPeak,totalQ)<<std::endl;

     if(fabs(vPeak) < fAmplitude) continue; //Skip non-physical pulses
     if(deltaT < fWidth) continue; //Skip non-physical pulses
     const int chanNum = lFlow->event->GetNChannels()-1;
     if(fFlags->fVerbose ) std::cout << "After cut Pulse properties: "<< Form("Channel #%i: tStart = %f, tEnd=%f, tPeak=%f, vPeak=%f, totalQ =%f",chanNum,tStart,tEnd,tPeak,vPeak,totalQ)<<std::endl;

     //Make the LoLXChannel and add to the flow
     aChan->AddPulseLeadingTime(tStart);
     aChan->AddPulsePeakTime(tPeak);
     aChan->AddPulseWidth(deltaT);
     aChan->AddPulseHeight(vPeak);
     aChan->AddPulseCharge(totalQ);
     aChan->AddPulseType(0);//0 for primary pulses
     aChan->AddPulsePreBaseline(preBase);

     //Set the subpeaks
     //Skip subpeak 0 (primary pulse)
     for(unsigned int iSP = 1; iSP<subpeaks[iPulse].size();iSP++){
       if(fabs(subpeaks[iPulse][iSP].second) < fAmplitude ) continue; // Skip unphysical subpeaks, probably noise triggers
       aChan->AddPulseLeadingTime(tStart);
       aChan->AddPulsePeakTime(hCorr.GetBinCenter(subpeaks[iPulse][iSP].first));
       aChan->AddPulseHeight(fabs(subpeaks[iPulse][iSP].second));
       aChan->AddPulseType(1);//1 for sub pulses
       //Add values for subpulses (width and charge not defined for subpeaks)
       aChan->AddPulseWidth(-1);
       aChan->AddPulseCharge(-1);
       aChan->AddPulsePreBaseline(preBase);
     }
     //Add to pulse counter
     fNtotPulses+=subpeaks[iPulse].size();
   }


   return aChan;
 }//End pulsefinder

};//End LoLXPulseFinderModule


class LoLXPulseFinderModuleFactory: public TAFactory
{
public:
   LoLXPulseFinderFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      printf("LoLXPulseFinderModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
   }

   void Finish()
   {
      printf("LoLXPulseFinderModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("LoLXPulseFinderModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new LoLXPulseFinderModule(runinfo, &fFlags);
   }

};

static TARegister tar(new LoLXPulseFinderModuleFactory);
