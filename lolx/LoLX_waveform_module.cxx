/******************************************************************
 * Waveforms *
 *
 * D. Gallacher
 * October 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "adcflow.hxx"
#include "baselineflow.hxx"
#include "LoLXFlow.hxx"
#include "LoLXDS.hxx"
#include "LoLXChannel.hxx"
#include "utils.hxx"

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <fstream>

#include "TCanvas.h"
#include "TH1D.h"
#include "TLine.h"
#include "TGraph.h"
#include "TGraphSmooth.h"

#include "json.hpp"
using json = nlohmann::json;

class LoLXWaveformFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};

//Read preprocessor flag value as string literal
#define STRING(s) #s
#define STRSTRING(s) STRING(s)


class LoLXWaveformModule: public TARunObject
{
public:
   LoLXWaveformFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors

   bool fEnabled; // enable canvases creation

   bool fSave; // save PDFs of canvases
   int fSaveEvents; // limit of events to save

   bool fPersistency; // enable summation of waveforms
   int fPersistencyChannel; // visualize a special channel
   int fPersistencyEvents; // terminate the summation at this event

   double fRawWFmin;
   double fRawWFmax;
   double fNanosecPerSample;//Number of nanoseconds per sample


   int fNumChannels; //Number of channels for the raw and corrected waveforms
   std::map<int,TH1D*> fhWFRaw; // raw waveforms
   std::map<int,TH1D*> fhWFCorr; // baseline subtracted waveforms
   std::map<int,TH1D*> fhPulses; // found pulse location and size
   std::map<int,TCanvas*> fCRaw; // the canvases for the raw waveforms
   std::map<int,TCanvas*> fCCorr; // the canvases for the corrected waveforms+pulses


   TDirectory* fPercyDir;
   TCanvas* fCP; // the canvas for the persistency plot for the selected channel
   TCanvas *fCRawAll;//All Raw waveforms on one canvas
   TCanvas *fCCorrAll;//All corr waveforms on one canvas
   std::map<int,TH1D*> fhPersistency; // the persistency histograms

   TH1D* fSmoothed;//Smoothed histogram from daq channel 29(no sipm connected)

   TDirectory* fRawDir;//Directory to hold raw plots
   TDirectory* fCorrDir;//Directory to hold corr plots

   Utils *utils;
   int fPedestal;//Bins for showing baselines


#ifdef EXPORT_ASCII
   std::ofstream fout; // this feature can be only be enabled from cmake
#endif




public:
   LoLXWaveformModule(TARunInfo* runinfo, LoLXWaveformFlags* fFlags): TARunObject(runinfo),fFlags(fFlags),
                                                 fCounter(0),fError(0),
                                                 fEnabled(false),fSave(false),
                                                 fPersistency(false)
   {
      if(fFlags->fVerbose) std::cout<<"LoLXWaveformModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Waveform";
#endif
      std::string config_path;
#ifdef CONFIG_PATH
    config_path = STRSTRING(CONFIG_PATH) + fFlags->fConfigName;//Read config file path from CMAKE source dir (lolx)
#else
    config_path = fFlags->fConfigName;//If path isn't defined look in flags only
#endif
      std::cout << "Reading config file from path = "<< config_path << "\n";
      //Change this to the new map-plots (single channels)
      fhWFRaw.clear();
      fhWFCorr.clear();
      fhPulses.clear();
      fhPersistency.clear();
      fCRaw.clear();
      fCCorr.clear();

      std::ifstream fin(config_path);
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"LoLXWaveformModule Json parsing success!"<<std::endl;
      fin.close();

      fEnabled = settings["Waveform"]["Canvas"].get<bool>();
      fSave = settings["Waveform"]["Save PDF"].get<bool>();
      fSaveEvents = settings["Waveform"]["Save Events Limit"].get<int>();
      fPersistency = settings["Waveform"]["Persistency"].get<bool>();
      fPersistencyChannel = settings["Waveform"]["Persistency Channel"].get<int>();
      fPersistencyEvents = settings["Waveform"]["Persistency Events Limit"].get<int>();
      fNumChannels = settings["Waveform"]["Num Channels"].get<int>();
      fRawWFmin = settings["Waveform"]["Raw WF Minimum"].get<double>();
      fRawWFmax = settings["Waveform"]["Raw WF Maximum"].get<double>();
      fNanosecPerSample = settings["Waveform"]["NanoSecPerSample"].get<double>();
      fPedestal = settings["Waveform"]["Pedestal"].get<int>();

      utils = new Utils();//Utility functions for analysis
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty())
         std::cout<<"LoLXWaveformModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"LoLXWaveformModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;

      if( fEnabled )
         {
           fCRawAll = new TCanvas("Raw_Waveforms","All Raw Waveforms",2000,1500);
           fCRawAll->Divide(8,4);
           fCRawAll->ToggleEventStatus();
           fCCorrAll = new TCanvas("Corr_Waveforms","All Corected Waveforms",2000,1500);
           fCCorrAll->Divide(8,4);
           fCCorrAll->ToggleEventStatus();

            int runn=runinfo->fRunNo;
            for(int iCh=0;iCh<fNumChannels;iCh++){
              std::string cname = Form("Run# %i: Raw_Canvas_%i",runn,iCh);
              if(fFlags->fVerbose&&0) std::cout<<"\tnew canvas: "<<cname<<std::endl;
              fCRaw[iCh] = new TCanvas(cname.c_str(),cname.c_str(),2000,1500);
              fCRaw.at(iCh)->ToggleEventStatus();
            }//end loop over channels

            fSmoothed = new TH1D("fSmoothed","",1024,fRawWFmin,fRawWFmax);

            if( runinfo->fFileName.empty() )
               {
                  gDirectory->cd("RootApp:/");
                  fRawDir = runinfo->fRoot->fgDir->mkdir("RawChannels");
                  fCorrDir = runinfo->fRoot->fgDir->mkdir("CorrChannels");
               }
            else
               {
                  runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
                  fRawDir = runinfo->fRoot->fOutputFile->mkdir("RawChannels");
                  fCorrDir = runinfo->fRoot->fOutputFile->mkdir("CorrChannels");
               }


         }

      if(fPersistency)
         {
            std::string cname="cPersistencyR"+std::to_string(runinfo->fRunNo)+"CH"+std::to_string(fPersistencyChannel);
            fCP = new TCanvas(cname.c_str(),cname.c_str(),900,700);
            if( runinfo->fFileName.empty() )
               {
                  gDirectory->cd("RootApp:/");
                  fPercyDir = new TDirectory("Persistency", "Persistency Histograms");
               }
            else
               {
                  runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
                  fPercyDir=runinfo->fRoot->fOutputFile->mkdir("Persistency");
               }
         }
   }

   void EndRun(TARunInfo* runinfo)
   {
      for(auto it=fhWFRaw.begin();it!=fhWFRaw.end();++it)
         if(it->second) delete it->second;
      fhWFRaw.clear();
      for(auto it=fhWFCorr.begin();it!=fhWFCorr.end();++it)
         if(it->second) delete it->second;
      fhWFCorr.clear();
      for(auto it=fhPulses.begin();it!=fhPulses.end();++it)
         if(it->second) delete it->second;
      fhPulses.clear();


      if( fEnabled )
         {

            for(auto it=fCRaw.begin(); it!=fCRaw.end(); ++it)
               if(it->second) delete it->second;
            fCRaw.clear();
            for(auto it=fCCorr.begin(); it!=fCCorr.end(); ++it)
               if(it->second) delete it->second;
            fCCorr.clear();
         }


      if(fPersistency)
         {
            fCP->SaveAs(".pdf");
            delete fCP;
            if( runinfo->fFileName.empty() )
               {
                  gDirectory->cd("RootApp:/");
                  if( fPercyDir ) delete fPercyDir;
               }
         }
      std::cout<<"LoLXWaveformModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }


   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      AdcEventFlow* adc_flow = flow->Find<AdcEventFlow>();
      if( !adc_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      BaselineEventFlow* corr_flow = flow->Find<BaselineEventFlow>();
      if( !corr_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }


      LoLXProcessorFlow* pulse_flow = flow->Find<LoLXProcessorFlow>();
         if( !pulse_flow )
            {
               ++fError;
   #ifdef HAVE_MANALYZER_PROFILER
               *flags|=TAFlag_SKIP_PROFILE;
   #endif
               return flow;
            }

      ResetHisto();

      //Get the actual number of channels
      fNumChannels = adc_flow->getNchannels();

      //Do some smoothing bullshit to test
      const std::vector<double> grawwf = pulse_flow->event->FindChannel(29)->GetWF_BaselineRemoved();
      utils->ConvertRawWF(*fSmoothed,&grawwf,29,fNanosecPerSample);
      TGraph gin(fSmoothed);
      TGraphSmooth gs("normal");
      TGraph *gout =  gs.SmoothKern(&gin,"normal",1024./4);//1/4 of the waveform as bandwidth
      for(int ip=1;ip<fSmoothed->GetNbinsX();ip++){ fSmoothed->SetBinContent(ip,gout->GetPointY(ip));}

      TDirectory* dir = runinfo->fRoot->fgDir;
      for(int ich=0; ich<fNumChannels; ++ich)
         {
            //const TV1740RawChannel* adc_raw = (TV1740RawChannel*) adc_flow->getChannel(ich);
            const std::vector<double> *raw_wf = adc_flow->getVector(ich);
            if(raw_wf->empty()) continue;//Skip empty waveforms
            //if( adc_raw->IsEmpty() ) continue;
            const std::vector<double>* corr_wf = corr_flow->getChannelVector(ich);
            LoLXChannel* lChannel = pulse_flow->event->GetChannel(ich);
            dir->cd();// RootApp:/manalyzer
            int daq_channel = lChannel->GetChannelID();
            //gDirectory->pwd();
            PlotChannel(raw_wf,corr_wf,daq_channel);
            if( fPersistency && (fCounter < fPersistencyEvents) )
               {
                  if( runinfo->fgFileList.size() )
                     runinfo->fRoot->fOutputFile->cd(); // <filename>:/
                  else
                     gDirectory->cd("RootApp:/");
                  fPercyDir->cd();
                  //gDirectory->pwd();
                  PersistencyPlot(corr_wf,daq_channel);
               }
         }

      if( fFlags->fVerbose ){
        //Print stuff here
      }
      double max_height=1.1;
      for( int iChan=0; iChan < fNumChannels; iChan++)
         {
            LoLXChannel* lChannel = pulse_flow->event->GetChannel(iChan);
            int daq_channel = lChannel->GetChannelID();
            PlotPulse(lChannel,daq_channel);
            if(lChannel->GetNPulses()) {
              max_height = lChannel->GetPulseHeight(0);
            } else{
              max_height = lChannel->GetBaselineRMS()*5;
            }
         }
      max_height*=2.0;


      // if this module is enabled, run until here
      // i.e., make waveforms available in JSROOT
      if( !fEnabled )
         {
            ++fCounter;
            return flow;
         }
      // if graphics is available on the host
      // run with -g and select a valid channel

#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif

      if( fFlags->fVerbose ){
        //Print stuff here
      }
      int runNum = runinfo->fRunNo;
      ShowPlots(max_height,runNum);
      ShowRaw(runNum);

      // read and plot here histos saved
      // in subdirectories
      if( runinfo->fgFileList.size() )
         runinfo->fRoot->fOutputFile->cd(); // <filename>:/
      else
         gDirectory->cd("RootApp:/");

      if( fSave && (fCounter < fSaveEvents) )
         {
            // TString sname(fC->GetName());
            // sname+="_Event"+std::to_string(fCounter)+".pdf";
            // fC->SaveAs(sname); fC->SaveAs(sname);
            // sname=fCA->GetName();
            // sname+="_Event"+std::to_string(fCounter)+".pdf";
            // fCA->SaveAs(sname); fCA->SaveAs(sname);
         }

      ++fCounter;
      return flow;
   }


   void PlotChannel(const std::vector<double> *raw_wf, const std::vector<double>* corr_wf,const int index)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      int Nbins = raw_wf->size();
      if( fFlags->fVerbose && 0 )
         std::cout<<"LoLXWaveformModule::PlotChannels   # of bins: "<<Nbins<<std::endl;
      std::string hname;
      std::string htitle;

      //Switch to the correct directory
      fRawDir->cd();

      if( !fhWFRaw.count(index) ){
        fhWFRaw[index] = new TH1D(Form("hRaw_%i",index),"Raw Waveform; Time (ns);Intensity (ADC/V)",Nbins,0,Nbins*fNanosecPerSample);
      }

      utils->ConvertRawWF(*fhWFRaw[index],raw_wf,index,fNanosecPerSample);
      fhWFRaw[index]->SetName(Form("hRaw_%i",index));
      fhWFRaw[index]->SetTitle("Raw Waveform; Time (ns);Intensity (ADC/V)");
      fhWFRaw[index]->SetLineColor(GetChannelColor(index));

      //Test baseline restoration with smoothed curve

      //Switch to the correct directory
      fCorrDir->cd();

      if( !fhWFCorr.count(index) ){
        fhWFCorr[index] = new TH1D(Form("hCorr_%i",index),"Corrected Waveform;Time (ns);Intensity (ADC/V)",Nbins,0,Nbins*fNanosecPerSample);
      }
      utils->ConvertRawWF(*fhWFCorr[index],corr_wf,index,fNanosecPerSample);
      fhWFCorr[index]->SetName(Form("hCorr_%i",index));
      fhWFCorr[index]->SetTitle("Corrected Waveform;Time (ns);Intensity (ADC/V)");
      fhWFCorr[index]->SetLineColor(GetChannelColor(index));

      if(index!=29) fhWFCorr[index]->Add(fSmoothed,-1);

      if( !fhPulses.count(index) ){
        fhPulses[index] = new TH1D(Form("hPulse_%i",index),"Pulse Waveform;Time (ns);Intensity (ADC/V)",Nbins,0,Nbins*fNanosecPerSample);
      }
      for(int iB = 1; iB < Nbins+2;iB++) fhPulses.at(index)->SetBinContent(iB,0.0);
      fhPulses.at(index)->SetLineColor(kRed+2);
      fhPulses.at(index)->SetLineWidth(2);
   }

   void PlotPulse(LoLXChannel* lChannel,int index)
   {
     #ifdef MODULE_MULTITHREAD
           std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
     #endif

     // Fill histogram with pulses, pulse width based on time and pulse height based on amplitude
     int np = lChannel->GetNPulses();
     if(!np) return;
     for(int iP =0; iP < np ;iP++){
       double pulseStart = lChannel->GetPulseLeadingTime(iP);
       double pulseEnd = pulseStart + lChannel->GetPulseWidth(iP);
       double pulseHeight = lChannel->GetPulseHeight(iP);
       double pulsePreBaseline = lChannel->GetPulsePreBaseline(iP);
       int pulsetype = lChannel->GetPulseType(iP);
       int startBin = fhPulses.at(index)->FindBin(pulseStart);
       int endBin = fhPulses.at(index)->FindBin(pulseEnd);
       //std::cout << Form("Pulse Properties:[start=%f,end=%f,height=%f,base=%f,startBin=%i,endbin=%i,type=%i] \n",pulseStart,pulseEnd,pulseHeight,pulsePreBaseline,startBin,endBin,pulsetype);
       //Loop over "pulse bins" and set them all to the pulse height
       //Shoulder at start of pulse representing the baseline
       for(int iB = startBin-fPedestal; iB < startBin; iB++){
         fhPulses.at(index)->SetBinContent(iB,pulsePreBaseline);
       }
       for(int iB = startBin; iB < endBin;iB++){
         fhPulses.at(index)->SetBinContent(iB,-pulseHeight+pulsePreBaseline);
       }//End loop over pulse bins
       //Little offset at end of pulse
       for(int iB = endBin; iB < endBin+fPedestal; iB++){
         fhPulses.at(index)->SetBinContent(iB,pulsePreBaseline);
       }

       if(pulsetype==1){
         double peakTime = lChannel->GetPulsePeakTime(iP);
         int peakBin = fhPulses.at(index)->FindBin(peakTime);
         fhPulses.at(index)->SetBinContent(peakBin,-pulseHeight+pulsePreBaseline);
         fhPulses.at(index)->SetBinContent(peakBin+1,-pulseHeight+pulsePreBaseline);
       }

     }//End loop over pulses

   }

   //Actually display the plots
   void ShowPlots( double& pulse_max, int& runn)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif

      if( fFlags->fVerbose ) std::cout<<"LoLXWaveformModule::ShowPlots "<<std::endl;
      for(int i=0;i<fNumChannels;i++){
        bool isPresent = fCCorr.count(i);
        if( !isPresent ){
              std::string cname = Form("Run #%i : Corr_Canvas_%i",runn,i);
              fCCorr[i] = new TCanvas(cname.c_str(),cname.c_str(),2000,1500);
              fCCorr.at(i)->ToggleEventStatus();
        }

        fCCorr.at(i)->cd();
        gPad->SetGrid();
        fhWFCorr.at(i)->Draw("hist");
        //fhWFCorr.at(i)->GetYaxis()->SetRangeUser(fhWFCorr.at(i)->GetMinimum()*1.2,pulse_max);
        fhPulses.at(i)->Draw("histsame");
        if(i==29){
          fSmoothed->SetLineColor(kBlack);
          fSmoothed->Draw("same");
        }

        fCCorr.at(i)->Modified();
        fCCorr.at(i)->Draw();
        fCCorr.at(i)->Update();

        fCCorrAll->cd();
        fCCorrAll->cd(i+1);
        gPad->SetGrid();
        fhWFCorr.at(i)->Draw("hist");
        if(i==29){
          fSmoothed->SetLineColor(kRed);
          fSmoothed->Draw("same");
        }
        //fhWFCorr.at(i)->GetYaxis()->SetRangeUser(fhWFCorr.at(i)->GetMinimum()*1.2,pulse_max);
        fhPulses.at(i)->Draw("same");

      }

      fCCorrAll->Modified();
      fCCorrAll->Draw();
      fCCorrAll->Update();

   }

   void ShowRaw(int &run_num)
   {
 #ifdef MODULE_MULTITHREAD
       std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
 #endif

       for(int i=0;i<fNumChannels;i++){
         bool isPresent = fCRaw.count(i);
         if( !isPresent ){
               std::string cname = Form("Run# %i: Raw_Canvas_%i",run_num,i);
               fCRaw[i] = new TCanvas(cname.c_str(),cname.c_str(),2000,1500);
               fCRaw[i]->ToggleEventStatus();
         }
         fCRaw[i]->cd();

         gPad->SetGrid();
         fhWFRaw[i]->Draw("hist");
         fCRaw[i]->Modified();
         fCRaw[i]->Draw();
         fCRaw[i]->Update();

         fCRawAll->cd();
         fCRawAll->cd(i+1);
         gPad->SetGrid();
         fhWFRaw.at(i)->Draw("hist");
      }
      fCRawAll->Modified();
      fCRawAll->Draw();
      fCRawAll->Update();

   }
   //Returns channel type as TColor (2==Bare == Red, 30 == BP == Green, 9 == LP == blue) based on DAQ Channel
   int GetChannelColor(int dChan)
   {
     int col = 9;//Long pass
     if(dChan < 32 && dChan > 27) col = 2;//Bare
     if(dChan < 4) col = 30;//Band pass
     return col;
   }

   void PersistencyPlot( const std::vector<double>* corr_wf,int index)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      if(!(fFlags->fVerbose))
         {
            std::cout<<"LoLXWaveformModule::PersistencyPlot "<<index<<" ";
         }
      int nsamples = (int)corr_wf->size();
      if( !fhPersistency.count(index) )
         {
            std::string hname="hPersistencyCH"+std::to_string(index);
            std::string htitle="Persistency Plot for channel "+std::to_string(index)+";Time (ns);ADC";
            fhPersistency.at(index) = new TH1D(hname.c_str(),htitle.c_str(),nsamples,0.,(double)nsamples*fNanosecPerSample);
            fhPersistency.at(index)->SetStats(0);
         }
      else if( fhPersistency.at(index)->GetNbinsX() != nsamples )
         {
            std::string hname="hPersistencyCH"+std::to_string(index);
            std::string htitle="Persistency Plot for channel "+std::to_string(index)+";Time (ns);ADC";
            fhPersistency.at(index) = new TH1D(hname.c_str(),htitle.c_str(),nsamples,0.,(double)nsamples);
            fhPersistency.at(index)->SetStats(0);
         }
      for(int i=0; i<nsamples; ++i)
         {
            double binContent = fhPersistency.at(index)->GetBinContent(i+1);
            fhPersistency.at(index)->SetBinContent(i+1,binContent+corr_wf->at(i));
         }
      if( index == fPersistencyChannel )
         {
            fCP->cd();
            fhPersistency.at(fPersistencyChannel)->Draw();
            fCP->Modified();
            fCP->Draw();
            fCP->Update();
         }
      if(fCounter==(fPersistencyEvents-1) && fFlags->fVerbose) std::cout<<"LoLXWaveformModule::PersistencyPlot for channel:"<<index<<" Last WF."<<std::endl;
   }


   void ResetHisto()
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      if(fFlags->fVerbose) std::cout<<"Resetting Event: "<<fCounter<<std::endl;
      for(auto it=fhWFRaw.begin();it!=fhWFRaw.end();++it)
         if(it->second) it->second->Reset();
      for(auto it=fhWFCorr.begin();it!=fhWFCorr.end();++it)
         if(it->second) it->second->Reset();
      for(auto it=fhPulses.begin();it!=fhPulses.end();++it)
         if(it->second) it->second->Reset();

      fSmoothed->Reset();
   }
};


class LoLXWaveformModuleFactory: public TAFactory
{
public:
   LoLXWaveformFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      printf("LoLXWaveformModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
         //fFlags.fVerbose = true;
   }

   void Finish()
   {
      printf("LoLXWaveformModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("LoLXWaveformModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new LoLXWaveformModule(runinfo, &fFlags);
   }

};

static TARegister tar(new LoLXWaveformModuleFactory);
