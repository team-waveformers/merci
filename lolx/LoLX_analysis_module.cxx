/******************************************************************
 * LoLXAnalysis Module
 * Author: David Gallacher, Bernadette Rebeiro
 * January 2022
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "LoLXChannel.hxx"
#include "LoLXDS.hxx"
#include "LoLXFlow.hxx"
#include "baselineflow.hxx"
#include "adcflow.hxx"

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <sstream>
#include <utility>
#include <map>

#include "json.hpp"
using json = nlohmann::json;

//Read preprocessor flag value as string literal
#define STRING(s) #s
#define STRSTRING(s) STRING(s)

class LoLXAnalysisModuleFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};

class LoLXAnalysisModule : public TARunObject
{
public:
   LoLXAnalysisModuleFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors

   //Add public data members here

   double fNanosecsPerSample;

   int fNChan; //Channels in SPE calibration table
   std::string fSPETable;//Table of SPE calibrations
   //Map of SPE Calibration with DAQ channel as key with pair <charge,amplitude>
   std::map<int,std::pair<double,double>> mSPECalibration;

   std::string fSiPMInfoFile;// name of SiPM info file, read from config
   std::map<int,std::vector<std::string>> fSiPMInfo;//Contains SiPM Info (Preamp board,channel,fabricator,id etc)

   double fT0_low; /// Window low for T0 of event search, pulses must be within [t0_low,t0_high] to be considered
   double fT0_high;/// Window high for T0 of event search

   double fSubEventThresh; ///Height threshold for a subevent

private:

public:
   LoLXAnalysisModule(TARunInfo* runinfo, LoLXAnalysisModuleFlags* f): TARunObject(runinfo),
							     fFlags(f),
							     fCounter(0), fError(0)
   {
    if(fFlags->fVerbose) std::cout<<"LoLXAnalysisModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
    fModuleName="LoLXAnalysisModule";
#endif
    std::string config_path;
#ifdef CONFIG_PATH
    config_path = STRSTRING(CONFIG_PATH) + fFlags->fConfigName;//Read config file path from CMAKE source dir (lolx)
#else
    config_path = fFlags->fConfigName;//If path isn't defined look in flags only
#endif
    std::cout << "Reading config file from path = "<< config_path << "\n";
    std::ifstream fin(config_path);
    json settings;
    fin>>settings;
    if(fFlags->fVerbose)
       std::cout<<"LoLXAnalysisModule Json parsing success!"<<std::endl;
    fin.close();
    //Read the data from config.json
    fNChan = settings["Analysis"]["NumChannels"].get<int>();
    fSPETable = settings["Analysis"]["SPECalibrationFile"].get<std::string>();
    fSiPMInfoFile = settings["Analysis"]["SiPMInfoFile"].get<std::string>();
    fT0_low = settings["Analysis"]["EventT0Low"].get<double>();
    fT0_high = settings["Analysis"]["EventT0High"].get<double>();
    fSubEventThresh = settings["Analysis"]["SubEventThreshold"].get<double>();

    //Parse the JSON file for SPE Calibration
    std::string fname = fSPETable;
    std::cout << "Fname for SPE table "<<fname<<std::endl;
    std::ifstream fin_spe(fname.c_str());
    json settings_spe;
    fin_spe>>settings_spe;
    if(fFlags->fVerbose)
       std::cout<<"LoLXAnalysisModule SPE Json parsing success!"<<std::endl;
    fin_spe.close();


    //Read the data from SPE Table into map of [daq_chan][calibrations]
    for(int iChan =0; iChan < fNChan;iChan++){
      double charge = settings_spe["SPECharge"][Form("Ch_%i",iChan)].get<double>();
      double amp = settings_spe["SPEAmplitude"][Form("Ch_%i",iChan)].get<double>();
      if(fFlags->fVerbose) std::cout << "SPE Calibration for channel "<< iChan << " = [" << charge << ","<<amp<<"]"<<std::endl;
      //Insert into map
      mSPECalibration.insert(std::make_pair(iChan,std::make_pair(charge,amp)));
    }

    FillMap(fSiPMInfoFile);//Fill the map of sipm info from file

   }

   //Required functions
   /// Begin run method
   void BeginRun(TARunInfo* runinfo)
   {
     if(runinfo->fFileName.empty() )
        std::cout<<"LoLXAnalysisrModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
     else
        std::cout<<"LoLXAnalysisModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   /// End run method
   void EndRun(TARunInfo* runinfo)
   {
     std::cout<<"LoLXAnalysisModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

   /// Analyze flow event method
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow){

     BaselineEventFlow* corr_flow = flow->Find<BaselineEventFlow>();
     if( !corr_flow )
        {
           if(fFlags->fVerbose) std::cout << "Baseline flow not found" <<std::endl;
           ++fError;
  #ifdef HAVE_MANALYZER_PROFILER
           *flags|=TAFlag_SKIP_PROFILE;
  #endif
           return flow;
        }


      AdcEventFlow* raw_flow = flow->Find<AdcEventFlow>();
         if( !raw_flow )
          {
               ++fError;
      #ifdef HAVE_MANALYZER_PROFILER
               *flags|=TAFlag_SKIP_PROFILE;
      #endif
               return flow;
          }

     LoLXProcessorFlow* pulse_flow = flow->Find<LoLXProcessorFlow>();
        if( !pulse_flow )
         {
              ++fError;
     #ifdef HAVE_MANALYZER_PROFILER
              *flags|=TAFlag_SKIP_PROFILE;
     #endif
              return flow;
         } else if( !pulse_flow->event->GetFitterFlag() ){//Check if we have channels with pulses..
           ++fError;
           #ifdef HAVE_MANALYZER_PROFILER
              *flags|=TAFlag_SKIP_PROFILE;
          #endif
              return flow;
         }
      fCounter++;


      fNanosecsPerSample = pulse_flow->conf->fNanosecsPerSample;//Get Nanosecs per sample from config
      ParseMap(pulse_flow->event);//Fill DS with sipm info
      CalculateEventT0(pulse_flow->event);//Get the T0
      CalibratePulseFinderPE(pulse_flow->event);//Needs to happen before ComputeFMaxPE because it needs PE variables
      ComputeFMaxPE(pulse_flow->event);
      FindSubEvents(pulse_flow->event,corr_flow);//Find SubEvents

      int nsub = pulse_flow->event->GetSubEventCount();
      //if (nsub>1) std::cout << "more than one sub event for event "<< fCounter <<"\n";
      pulse_flow->event->SetAnalysisFlag(1);//Done with analysis
      if(fFlags->fVerbose) std::cout << "LoLXAnalysisModule: Finished \n";
      return flow;

    }

    //Find T0 of the event by looking for the earliest pulse in the t0 window
    void CalculateEventT0(LoLXDS *ev)
    {
      if(fFlags->fVerbose) std::cout << "LoLXAnalysisModule: CalculateEventT0 \n";
      double t0_ev = 1e10;
      LoLXChannel *chan = NULL;
  		for(int ic = 0 ; ic < ev->GetNHit() ; ic++){
  			chan = ev->GetChannel(ic);
        double t0_chan = 1e10;
        for(int ip = 0; ip < chan->GetNPulses() ; ip++){
          double t0_pulse = chan->GetPulseLeadingTime(ip);
          // std::cout << "t0 pulse = "<< t0_pulse << std::endl;
          if( t0_pulse < fT0_low || t0_pulse > fT0_high ) continue; //Far away from trigger window
    			if( t0_pulse < t0_chan ) t0_chan = t0_pulse;
        }
  			if( t0_chan < t0_ev ) t0_ev = t0_chan;
  		}
      // std::cout << "t0 event = "<< t0_ev<<"\n";
      if(t0_ev > 1e5){
        t0_ev = -9999.0;
      }
      ev->SetEventT0(t0_ev);
    }

    //This method calculates the PE from the pulsefinder module
    void CalibratePulseFinderPE(LoLXDS *ev)
    {
      if(fFlags->fVerbose) std::cout << "LoLXAnalysisModule: CalibratePulseFinderPE \n";

      LoLXChannel *chan = NULL;
      const int nHit = ev->GetNChannels();

      double npe = 0.0;
      double npe_lp = 0.0;
      double npe_bp = 0.0;
      double npe_bare = 0.0;

      double ape = 0.0;
      double ape_lp = 0.0;
      double ape_bp = 0.0;
      double ape_bare = 0.0;

      //Loop over channels and calibrate charge/amplitude and sum to PE
      for(int iC=0;iC < nHit; iC++)
      {
        chan = ev->GetChannel(iC);
        int daqchan = chan->GetChannelID();
        int type = chan->GetType();
        double npe_chan = 0.0;
        double ape_chan = 0.0;
        //Get the SPE Calibrations
        double spe_charge = mSPECalibration.at(daqchan).first; // Read SPE Charge from map
        double spe_amp = mSPECalibration.at(daqchan).second; // Read SPE Amplitude from map
        const int nPulses = chan->GetNPulses();
        for(int iP=0;iP < nPulses; iP++){
          if(chan->GetPulseType(iP)==1) continue; //Only count "main" pulses
          double pulse_npe = chan->GetPulseCharge(iP)/spe_charge;
          double pulse_ape = chan->GetPulseHeight(iP)/spe_amp;
          npe_chan+=pulse_npe;
          ape_chan+=pulse_ape;
        }//End of loop over pulses

        //Set summary variables
        chan->SetNPE(npe_chan);
        chan->SetAPE(ape_chan);
        npe+=npe_chan;
        ape+=ape_chan;
        if(type==0) // For Bare SiPMs, type==0
        {
          npe_bare +=npe_chan;
          ape_bare +=ape_chan;
        }
        else if(type==1) //For Bandpass filtered SiPMs, type==1
        {
          npe_bp += npe_chan;
          ape_bp += ape_chan;
        }
        else //For Longpass filtered SiPMs, type==2
        {
          npe_lp += npe_chan;
          ape_lp += ape_chan;
        }
      }//End of loop over channels

      ev->SetTotalPE(npe);
      ev->SetTotalBPPE(npe_bp);
      ev->SetTotalBarePE(npe_bare);
      ev->SetTotalLPPE(npe_lp);
    }//End of CalibratePE

    // Calculate the fmaxpe variables for the event
    //FMaxpe is the fraction of total light in the brightest channel
    void ComputeFMaxPE(LoLXDS* ev)
    {
      if(fFlags->fVerbose) std::cout << "LoLXAnalysisModule: ComputeFMaxPE \n";
      const double tot_pe = ev->GetTotalPE();
      double pe_max = -10000.0;
      double pe_lp_max = -10000.0;
      int id_max = -1;
      int id_lp_max = -1;
      int type;

      LoLXChannel *chan = NULL;

      for(int iCh = 0; iCh < ev->GetNChannels();iCh++)
      {
        chan = ev->GetChannel(iCh);
        type = chan->GetType();
        double pe_chan = chan->GetNPE();
        int id_chan = chan->GetChannelID();//DAQ Chan
        if(pe_chan < 0)
           continue;//Skip empty channels
        if(pe_chan > pe_max)
        {
          pe_max = pe_chan;
          id_max = id_chan;
        }
        if ((type==2)&&(pe_chan > pe_lp_max))
        {
          pe_lp_max = pe_chan;
          id_lp_max = id_chan;
        }
       }//End loop over channels

       double fmaxpe = pe_max/tot_pe;
       double fmaxpe_lp = pe_lp_max/tot_pe;

       ev->SetFMaxPE(fmaxpe);        // maximum PE in all channels
       ev->SetFMaxPEID(id_max);      // DAQ channel ID where this maximum PE was recorded
       ev->SetFMaxPELP(fmaxpe_lp);   // maximum PE in Longpass filtered SiPMs
       ev->SetFMaxPELPID(id_lp_max); // Longpass filtered DAQ channel ID
    }//End ComputeFMaxPE


    //Read from file to populate map of sipm summary info
    void FillMap(std::string infoFileName)
    {
        if(fFlags->fVerbose) std::cout << "LoLXAnalysisModule: FillMap \n";
        std::ifstream infile;
         infile.open(infoFileName.c_str());
         if (!infile.is_open()){
           std::cerr << "Error in LoLXAnalysisModule: SiPM Info File " << infoFileName  << " not found. Dieing now" <<std::endl;
           exit(1);
         }
         std::string line;
         int nline = 0;// line number
         while (!infile.eof()){
               nline++;
               std::getline(infile,line);
               if(nline<2) continue;// Skip header lines
               if(infile.eof()) continue; // Really skip if its the end of file
               std::vector<std::string> elems;
               std::stringstream ss;
               ss.str(line);
               std::string item;
               while (std::getline(ss, item, ',')){
                     elems.push_back(item);
               }
               // Fill vectors
               fSiPMInfo.insert(std::make_pair(atoi(elems[0].c_str()),std::vector<std::string>(elems.begin()+1,elems.end())));
        }//End while loop

    }

    void ParseMap(LoLXDS *ds)
    {
      if(fFlags->fVerbose) std::cout << "LoLXAnalysisModule: ParseMap \n";
      LoLXChannel *chan = NULL;
      //Fill the Channels from the map
      for(int iC = 0; iC< ds->GetNChannels(); iC++)
      {
        chan = ds->GetChannel(iC);
        int daqchan = chan->GetChannelID();//Daq channel is index of map
        // Add method to parse map string
        std::vector<std::string> mapstr = fSiPMInfo[daqchan];
        //Do some stuff to intrpret mapstr and get out the relevant data
        if(fFlags->fVerbose){
          std::cout <<" Daq channel = "<< daqchan << " read in : ";
          for(unsigned int i = 0;i<mapstr.size();i++) std::cout << mapstr[i]<< ":";
          std::cout << std::endl;
        }

        int summed = 0;
        int board_num = 0;
        int board_chan = 0;
        int sipm_id = 0;
        int sipm_model = 0;
        int sipm_type =0;

        if (mapstr.size()<8)
        {
          std::cout << "Error LoLXAnalysisModule: Unexpected size of input sipm info string" <<std::endl;
          exit(1);
        }

        //File format
        //DAQ Channel (0-31)[key],Wavelength Filter,Preamp (Nanopi),Nanopi Input (1-16),Nanopi output (1-16),DAQ Module,V1740 DAQ Channel (0-31),WDAQ Channel (0-15),SiPM ID
        summed  = mapstr[7].find("-") < mapstr[7].size() ? 1 : 0;
        board_num = atoi(mapstr[1].c_str());
        board_chan = atoi(mapstr[3].c_str());
        sipm_id = atoi(mapstr[7].c_str());
        sipm_model =0; //Not in table for now, only have ham
        if( mapstr[0] == "Long Pass")
        {
          sipm_type = 2;
        }
        else if(mapstr[0] == "Bare")
        {
          sipm_type = 0;
        }
        else if(mapstr[0] == "Band Pass")
        {
          sipm_type = 1;
        }
        else
        {
          sipm_type = -1; //Empty channel
        }


        //std::cout << "Parsed Properties = " << Form("summed = %i, board_num = %i, board_chan = %i, sipm_id = %i, sipm_model = %i, sipm_type = %i",
        //summed,board_num,board_chan,sipm_id,sipm_model,sipm_type)<<std::endl;

        chan->SetBoardID(board_num);
        chan->SetBoardChannel(board_chan);//Add method in LoLXChannel.hxx
        chan->SetSiPMID(sipm_id);
        chan->SetType(sipm_type);
        chan->SetSummed(summed);
        chan->SetSiPMModel(sipm_model);//Add to lolXChannel.hxx

      }
    //
    }

    /// Using the summed waveform, look for a substantial second peak indicating a second physics event
    void FindSubEvents(LoLXDS *ev, BaselineEventFlow *c_flow)
    {
      if(fFlags->fVerbose) std::cout << "LoLXAnalysisModule: FindSubEvents \n";
      int nsamp = (c_flow->getChannelVector(0))->size();
      std::vector<double> vSummed(nsamp,0); //Summed waveform
      int nSubEvents = 0;
      //Loop over baseline corrected waveforms and create a summed waveform
      for(int iChan=0; iChan < c_flow->getNchannels(); iChan++){
          //Get waveform
          const std::vector<double>* wf = c_flow->getChannelVector(iChan);
          //Add waveform to summed waveform, element wise
          std::transform(vSummed.begin( ), vSummed.end( ), wf->begin( ), vSummed.begin( ), std::plus<double>{});
      }//End loop over channels

      //Find if there are more than a single peak over the threshold
      std::vector<double>::iterator itr = vSummed.begin();
      double thresh = fSubEventThresh;//Cant capture global variables
      while (true){
        itr = std::find_if(itr,vSummed.end(),
        [thresh](const double& val){return val < thresh;});
        //If we're at the end of the waveform, break and return
        if(itr == vSummed.end()){
          if(fFlags->fVerbose) std::cout << "No more peaks over fSubEventThresh"<<std::endl;
          break;
        }else{
          //Add sub event
          nSubEvents++;
          double time = std::distance(vSummed.begin(),itr);
          time *= fNanosecsPerSample;
          ev->AddSubEventTime(time);
          //std::cout <<"Found subevent at t = "<< time <<"\n";
        }
        //Move ahead until we're below threshold/10
        itr = std::find_if_not(itr,vSummed.end(),
        [thresh](const double& val){return val < thresh/10.0;});
      }


      ev->SetSubEventCount(nSubEvents);


    }

};


class LoLXAnalysisModuleFactory : public TAFactory
{
public:
   LoLXAnalysisModuleFlags fFlags;

public:

   void Init(const std::vector<std::string> &args)
   {
      printf("LoLXAnalysisModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++){
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
             if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
	   }
   }

   void Finish()
   {
      printf("LoLXAnalysisModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("LoLXAnalysisModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new LoLXAnalysisModule(runinfo, &fFlags);
   }

};

static TARegister tar(new LoLXAnalysisModuleFactory);
