/******************************************************************
 *  LoLX Specific channel DS *
 * Expand upon for our actual desired list of variables
 *
 * October 2021
 * David Gallacher,
 *
 ******************************************************************/

#ifndef ROOT_LoLXChannel
#define ROOT_LoLXChannel

//Custom headers

//ROOT headers
#include "TObject.h"
#include "TTimeStamp.h"
#include <iostream>
#include <map>

class LoLXChannel : public TObject
{
private:

   //Header info for channels
   int         type;                    ///Type of Channel, (long,band,bare)
   int         summed;                  ///Summed or non-summed channel
   int         sipmID;                  ///Unique SiPM IDs
   int         channelID;               ///DAQ channel for this channel obj
   int         boardID;                 ///Which prem-amp board corresponds to this channel
   int         npulses;                 ///Total number of pulses found
   int         boardChannel;            ///Preamplifier board output channel for this DAQ channel
   int         sipmModel;                /// SiPM Model (Ham, FBK.. etc)


   //Channel Summary info
   double      nPE;                     /// Total charge estimated PE in this Channel
   double      aPE;                     /// Total amplitude estimated PE in this Channel
   double      t0_chan;                 /// T0 of first pulse in this Channel
   double      baseline;                /// Average baseline
   double      baseline_rms;            /// RMS of the baseline

   //Add more variables here..

   //The baseline corrected waveform for this channel
   std::vector<double> vWF_corr;          /// The baseline corrected waveform data
   std::vector<double> vWF_corrTimes;     /// The baseline corrected waveform times


   // a bunch of pulse-level summary info
   std::vector<double> vPulseLeadingTime;   /// Leading edge time (t0) of pulses (ns)
   std::vector<double> vPulsePeakTime;      /// Peak time of pulses (ns)
   std::vector<double> vPulseWidth;         /// Width of pulses (ns)
   std::vector<double> vPulseCharge;        /// Charge of Pulses (ADC*ns)
   std::vector<double> vPulseHeight;        /// Amplitude of pulses (ADC)
   std::vector<int> vPulseType;          /// pulse type, 0==main pulse, 1== subpulse
   std::vector<double> vPulsePreBaseline;   /// Baseline previous to the pulse




public:
   LoLXChannel();
   ~LoLXChannel();
   LoLXChannel(const LoLXChannel &rhs) : TObject() { Init(); CopyObj(rhs); }
   LoLXChannel &operator=(const LoLXChannel &rhs) { CopyObj(rhs); return *this; }
   // bool operator==(const LoLXChannel &c1, const LoLXChannel &c2){return c1.GetSiPMID() == c2.GetSiPMID();}
   // bool operator==(const LoLXChannel *c1 , const LoLXChannel *c2){return c1->GetSiPMID() == c2->GetSiPMID();}

   void     Init();
   void     Clear(Option_t *option = "");
   void     CopyObj(const LoLXChannel &rhs);

   //Get header info

  /// Set the type of Channel (0 == bare, 1 == bandpass, 2==longpass )
  void       SetType(int i){ type = i; }
  int      GetType() const { return type; }

  /// Set whether or not this channel is summed
  void       SetSummed(int _summed){ summed = _summed; }
  int      GetSummed() const { return summed; }

  /// Set the SiPM ID (unique ID for each SiPM)
  void       SetSiPMID(int i){ sipmID = i; }
  int      GetSiPMID() const { return sipmID; }

  /// Set the Channel ID (digitizer channel)
  void       SetChannelID(int i){ channelID = i; }
  int      GetChannelID() const { return channelID; }

  /// Set the board ID (TRIUMF preamplifier board number)
  void       SetBoardID(int i){ boardID = i; }
  int      GetBoardID() const { return boardID; }

  /// Set the board channel (TRIUMF preamplifier board channel number)
  void       SetBoardChannel(int i){ boardChannel = i; }
  int      GetBoardChannel() const { return boardChannel; }

  /// Set the SiPM model type
  void       SetSiPMModel(int i){ sipmModel = i; }
  int      GetModel() const { return sipmModel; }
  std::string      GetModelName() { return sipmModelNames[sipmModel]; }


  //Get summary info

  /// Set the total integrated charge for this channel from all pulses in the event window
  void     SetNPE(double _nPE){ nPE = _nPE; }
  double   GetNPE(){ return nPE; }

  /// Set the total amplitude based PE for this channel from all pulses in the event window
  void     SetAPE(double _aPE){ aPE = _aPE; }
  double   GetAPE(){ return aPE; }


  /// Get the t0 of this channel
  double   GetT0_Chan(){ return t0_chan; }

  /// Get the baseline for this channel, from LoLX_baseline_module
  void       SetBaseline(double _baseline){ baseline = _baseline; }
  double   GetBaseline(){ return baseline; }

  /// Get the RMS of the baseline for this channel, from LoLX_baseline_module
  void       SetBaselineRMS(double _baseline_rms){ baseline_rms = _baseline_rms; }
  double   GetBaselineRMS(){ return baseline_rms; }


  /// Get the baseline corrected waveform
  std::vector<double>   GetWF_BaselineRemoved(){return vWF_corr;}
  void       SetWF_BaselineRemoved(std::vector<double> _vWF){vWF_corr = _vWF;}
  void       SetWF_BaselineRemoved(const std::vector<double>* _vWF){vWF_corr = *_vWF;}

  /// Get the baseline corrected waveform
  std::vector<double>   GetWFTimes_BaselineRemoved(){return vWF_corrTimes;}
  void       SetWFTimes_BaselineRemoved(std::vector<double> _vWFTimes){vWF_corrTimes = _vWFTimes;}
  void       SetWFTimes_BaselineRemoved(const std::vector<double>* _vWFTimes){vWF_corrTimes = *_vWFTimes;}


  /// Get rid of the WFs for this channel, call this before filling TTree and writing to output file
  void       PruneWFs();


  //Add/get from vectors
  /// Add pulse leading edge time
  void       AddPulseLeadingTime(double _t0);
  double   GetPulseLeadingTime(int iPulse);
  /// Add pulse peaking time
  void       AddPulsePeakTime(double _tPeak){ vPulsePeakTime.push_back(_tPeak); }
  double   GetPulsePeakTime(int iPulse);

  /// Add pulse width [ns]
  void       AddPulseWidth(double _width){ vPulseWidth.push_back(_width); }
  double   GetPulseWidth(int iPulse);
  void     SetPulseWidth(int iPulse,double width);

  /// Add pulse charge
  void       AddPulseCharge(double _adc){ vPulseCharge.push_back(_adc); }
  double   GetPulseCharge(int iPulse);

  /// Add pulse height
  void       AddPulseHeight(double _adc){ vPulseHeight.push_back(_adc); }
  double   GetPulseHeight(int iPulse);

  /// Add pulsetype
  void       AddPulseType(int _type){ vPulseType.push_back(_type); }
  int      GetPulseType(int iPulse);

  /// Add pulse baseline
  void       AddPulsePreBaseline(double _base){ vPulsePreBaseline.push_back(_base); }
  double      GetPulsePreBaseline(int iPulse);


  /// Get number of pulses
  int   GetNPulses(){ return npulses; }

  //Map of sipm models to numbers
  std::map<int, std::string> sipmModelNames;


   ClassDef(LoLXChannel,3)  //Event structure
};

#endif
