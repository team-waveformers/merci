#include "LoLX_treewriter_module.hxx"

void LoLXTreeWriterModule::BeginRun(TARunInfo* runinfo)
{
  if(runinfo->fFileName.empty() )
     std::cout<<"LoLXTTreeWriterModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
  else
     std::cout<<"LoLXTTreeWriterModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;

  lDS = new LoLXDS(); //New event structure
  runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory

  Tree = new TTree("T","LoLX Event DS");
  Tree->Branch("event",&lDS,32000,0);

  //Parse the JSON File
  std::ifstream fin(config_path);
  json settings;
  fin>>settings;
  if(fFlags->fVerbose)
     std::cout<<"LoLXTreeWriterModule Json parsing success!"<<std::endl;
  fin.close();

  std::cout<<"LoLXTreeWriterModule::BeginRun Saving settings to rootfile... ";
  runinfo->fRoot->fOutputFile->cd(); // select correct ROOT directory
  int error_level_save = gErrorIgnoreLevel;
  gErrorIgnoreLevel = kFatal;
  TObjString settings_obj(settings.dump().c_str());
  int bytes_written = gDirectory->WriteTObject(&settings_obj,"config");
  if( bytes_written > 0 )
     std::cout<<" DONE ("<<bytes_written<<")"<<std::endl;
  else
     std::cout<<" FAILED"<<std::endl;
  gErrorIgnoreLevel = error_level_save;

  // Get per-run info
  run_num = runinfo->fRunNo;
  // std::string filename = runinfo->fFileName;
  //subrun_num = GetSubRun(filename);
  //std::cout << "Sub run number = "<< subrun_num <<std::endl;
  runType = 0;
  runinfo->fOdb->RI("/Runinfo/runID/",&runType,0,0);
  startTime = 0;
  runinfo->fOdb->RU32("/Runinfo/Start time binary/",&startTime,0,0);
  start_time = (time_t) startTime;   //Cast to time_t
  start_ts=TTimeStamp(start_time,0); //ROOT TTimeStamp start run time
}

void LoLXTreeWriterModule::EndRun(TARunInfo* runinfo)
{
    std::cout<<"LoLXTreeWriterModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;

}

TAFlowEvent* LoLXTreeWriterModule::AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
{

  if(fFlags->fVerbose) std::cout<<"LoLXTreeWriterModule::AnalyzeFlowEvent" <<std::endl;

  AdcEventFlow* raw_flow = flow->Find<AdcEventFlow>();
     if( !raw_flow )
      {
           ++fError;
  #ifdef HAVE_MANALYZER_PROFILER
           *flags|=TAFlag_SKIP_PROFILE;
  #endif
           return flow;
      }

  LoLXProcessorFlow* pulse_flow = flow->Find<LoLXProcessorFlow>();
     if( !pulse_flow )
      {
           ++fError;
  #ifdef HAVE_MANALYZER_PROFILER
           *flags|=TAFlag_SKIP_PROFILE;
  #endif
           return flow;
      }
      //Check if LoLX_analysis_module has ran
      if(!pulse_flow->event->GetAnalysisFlag())
      {
          ++fError;
        #ifdef HAVE_MANALYZER_PROFILER
          *flags|=TAFlag_SKIP_PROFILE;
        #endif
          return flow;
      }


    //Get MIDAS Header info
    uint64_t tmask = raw_flow->getTriggerMask();
    int event_num = raw_flow->getEventNumber();
    double tstamp = raw_flow->fmidas_ts;
    time_t ev_time_stamp = (time_t) tstamp;//Cast to time_t
    int nsec = (int)((tstamp - (int)tstamp) * 1e9 + 0.5);
    TTimeStamp ev_ts(ev_time_stamp,nsec); //ROOT TTimeStamp event time

    //Get the channel data from the pulse-finder
    lDS = pulse_flow->event;
    if(fFlags->fVerbose) std::cout << "Filling event"<<std::endl;

    //Fill MIDAS Header info
    lDS->SetRun(run_num);
    lDS->SetSubRun(subrun_num);
    lDS->SetRunType(runType);
    lDS->SetSeqNum(event_num);
    lDS->SetTrigID(tmask);
    lDS->SetEVID(event_num);
    lDS->SetEVTime(tstamp);
    lDS->SetTimeStamp(&ev_ts);
    lDS->SetRunStartTime(&start_ts);

    lDS->GetNHit();

    Tree->Fill();
    ++fCounter;
    return flow;
}

//Parse string for sub run number based one pattern: "/path/to/file/runXXXX_YYY.mid.lz4"
//With YYY = subrun number
int LoLXTreeWriterModule::GetSubRun(std::string midasFilename)
{
   size_t last = 0,next = 0;
   while ((next = midasFilename.find("/", last)) != std::string::npos) {
     last = next + 1;
   }
   std::string infile = midasFilename.substr(last);
   int start = infile.find("_")+1;
   int end = infile.find(".");
   std::string fname = infile.substr(start,end-start);
   //Check if there are subruns
   int sbrn = 0;
   int srn = 0;
   if(fname.find("_")!=std::string::npos) sbrn=1;
   if(sbrn){
     size_t l = 0, n = 0;
     while ((n = fname.find("_", l)) != std::string::npos) {
       l = n + 1;
     }
     srn = atoi(fname.substr(l).c_str());
     fname = fname.substr(0,fname.find("_"));
   }

   return srn;
}
