/******************************************************************
 * Histogram module *
 *
 * S. Al Kharusi, D. Gallacher
 * February 2022
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "adcflow.hxx"
#include "baselineflow.hxx"
#include "LoLXFlow.hxx"
#include "LoLXDS.hxx"
#include "LoLXChannel.hxx"
#include "utils.hxx"

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <cfloat>

#include "TCanvas.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TLine.h"
#include "TROOT.h"
#include "TStyle.h"

#include "json.hpp"
using json = nlohmann::json;

class LoLXHistogramFlags
{
public:
   bool fVerbose = true;
   std::string fConfigName="master.json";
};

//Read preprocessor flag value as string literal
#define STRING(s) #s
#define STRSTRING(s) STRING(s)


class LoLXHistogramModule: public TARunObject
{
public:
   LoLXHistogramFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors

   bool fEnabled; // enable canvases creation

   bool fSave; // save PDFs of canvases
   int fSaveEvents; // limit of events to save
   double fSPEQ_cutoff;//Maximum charge for 2d historgram of spe
   double fSPEA_cutoff;//Maximum charge for 2d historgram of spe
   double fSPE_Bare_cutoff;//Maximum value to display on axis of Bare pass charge
   double fSPE_Band_cutoff;//Maximum value to display on axis of Band pass charge
   double fSPE_Long_cutoff;//Maximum value to display on axis of Long pass charge

   double fRawWFmax; //Length of an event window in ns
   double fNanosecPerSample; //How long is one bin in time?
   int fNumBinsSPEA;//Number of SPE Amplitude bins


   int fNumChannels; //Number of channels for the raw and corrected waveforms
   std::map<int,TH1D*> fhSPEQ; // Charge SPE distribution to channel number (key)
   std::map<int,TH1D*> fhSPEA; // Ampltiude SPE distribution to channel number
   std::map<int,TH1D*> fhTime2NextPulse; // Histogram of time-to-next-pulse (single channel)
   std::map<int,TH1D*> fhTimeSinceTrigger; // Histogram of time-since-trigger (single channel)
   std::map<int,TH2D*> fhTimeSinceTrigger_V_Q; // Histogram of time-since-trigger vs charge (Giacomo plot) (single channel)

   //The pulse rate for each channel can be a line graph of time since run start
   //std::map<int,TH1D*> fhPulseRate; // Histogram of pulse rates -- actually, maybe this can be a 30 bin histogram w/ bin heights corresponding to channel rates?

   //General plots combining info from multiple channels
   TH1D *fhPulseRate; //Histogram of pulse rate from all channels
   TH2D *fhSPEQ_V_A; /// 2d distribution of charge vs amplitude for all channels
   TH2D *fhBare_V_Band; /// 2d distribution of event charge for Bare channels vs Band channels
   TH2D *fhBare_V_Long; /// 2d distribution of event charge for Bare channels vs Long pass channels
   TH2D *fhBand_V_Long; /// 2d distribution of event charge for Long pass channels vs Band pass channels

   TH1D *fhBareLeadingTime; /// 1d distribution of leading pulse time in Bare channels minus trigger time
   TH1D *fhBPLeadingTime; /// 1d distribution of leading pulse time in Band channels minus trigger time
   TH1D *fhLPLeadingTime; /// 1d distribution of leading pulse time in Long channels minus trigger time

   TH1D *fhPulseTimesBare; /// 1d distribution of pulse times for Bare channels minus trigger time
   TH1D *fhPulseTimesBP; /// 1d distribution of pulse times for Band pass channels minus trigger time
   TH1D *fhPulseTimesLP; /// 1d distribution of pulse times for Long pass channels minus trigger time

   TH2D *fhPulseTimes_V_A; /// 2d distribution of pulse arrival time vs amplitude for all channels
   TH2D *fhPulseTimes_V_Q; /// 2d distribution of pulse arrival time vs amplitude for all channels

   //PE plots
   TH1D *fhBarePE;
   TH1D *fhBPPE;
   TH1D *fhLPPE;

   //SPE Graph
   TGraph *fgSPEh;
   TGraph *fgSPEq;


   //Canvases
   std::map<int,TCanvas*> fCSPEQ; //
   std::map<int,TCanvas*> fCSPEA; //
   std::map<int,TCanvas*> fCTime2NextPulse; //
   std::map<int,TCanvas*> fCTimeSinceTrigger; //
   std::map<int,TCanvas*> fCTimeSinceTrigger_V_Q; //

   //Canvases to show all channels at once
   TCanvas *fCSPEQAll;//
   TCanvas *fCSPEAAll;//
   TCanvas *fCTime2NextPulseAll;//
   TCanvas *fCTimeSinceTriggerAll;//
   TCanvas *fCTimeSinceTrigger_V_QAll;//

   //Directory where these plots will go
   TDirectory* fAnalysisDir;
   TDirectory* fAnalysisCanvasDir;

   TDirectory* fChannelDir;//Subdir for channel plots
   //TDirectory* fChannelCanvasDir;//Subdir for channel plots

   Utils *utils;

#ifdef EXPORT_ASCII
   std::ofstream fout; // this feature can be only be enabled from cmake
#endif


public:
   LoLXHistogramModule(TARunInfo* runinfo, LoLXHistogramFlags* fFlags): TARunObject(runinfo),fFlags(fFlags),
                                                 fCounter(0),fError(0),
                                                 fEnabled(false),fSave(false)
   {
      if(fFlags->fVerbose) std::cout<<"LoLXHistogramModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Histo";
#endif
      std::string config_path;
#ifdef CONFIG_PATH
      config_path = STRSTRING(CONFIG_PATH) + fFlags->fConfigName;//Read config file path from CMAKE source dir (lolx)
#else
      config_path = fFlags->fConfigName;//If path isn't defined look in flags only
#endif
      std::cout << "Reading config file from path = "<< config_path << "\n";

      //Clearing histogram maps
      fhSPEQ.clear(); //
      fhSPEA.clear(); //
      fhTime2NextPulse.clear(); //
      fhTimeSinceTrigger.clear(); //
      fhTimeSinceTrigger_V_Q.clear(); //

      //Clearing canvas maps
      fCSPEQ.clear(); //
      fCSPEA.clear(); //
      fCTime2NextPulse.clear(); //
      fCTimeSinceTrigger.clear(); //
      fCTimeSinceTrigger_V_Q.clear(); //

      std::ifstream fin(config_path);
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"LoLXHistogramModule Json parsing success!"<<std::endl;
      fin.close();

      fEnabled = settings["Histo"]["Canvas"].get<bool>();
      fSave = settings["Histo"]["Save PDF"].get<bool>();
      fSaveEvents = settings["Histo"]["Save Events Limit"].get<int>();
      fNumChannels = settings["Waveform"]["Num Channels"].get<int>();
      fRawWFmax = settings["Waveform"]["Raw WF Maximum"].get<double>();
      fNanosecPerSample = settings["Waveform"]["NanoSecPerSample"].get<double>();
      fSPEQ_cutoff = settings["Histo"]["Max SPE Charge"].get<double>();
      fSPEA_cutoff = settings["Histo"]["Max SPE Height"].get<double>();
      fNumBinsSPEA = settings["Histo"]["Num SPEA Bins"].get<int>();

      //Set all 2D hist to draw with colz by default -- does it work?? :O No.
      gStyle->SetHistFillStyle(1001);//colz is
      gROOT->ForceStyle();

      utils = new Utils();//Utility functions for analysis
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty())
         std::cout<<"LoLXHistogramModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"LoLXHistogramModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;

      if( fEnabled ){
         fCSPEQAll= new TCanvas("SPEQ","All SPEQ Histograms",2000,1500);//
         fCSPEQAll->Divide(8,4);
         fCSPEQAll->ToggleEventStatus();


         fCSPEAAll= new TCanvas("SPEA","All SPEA Histograms",2000,1500);//
         fCSPEAAll->Divide(8,4);
         fCSPEAAll->ToggleEventStatus();

         fCTime2NextPulseAll= new TCanvas("T2NP","All T2NP Histograms",2000,1500);//
         fCTime2NextPulseAll->Divide(8,4);
         fCTime2NextPulseAll->ToggleEventStatus();

         fCTimeSinceTriggerAll= new TCanvas("TST","All TST Histograms",2000,1500);//
         fCTimeSinceTriggerAll->Divide(8,4);
         fCTimeSinceTriggerAll->ToggleEventStatus();

         fCTimeSinceTrigger_V_QAll= new TCanvas("TST_V_Q","All TSTvQ Histograms",2000,1500);//
         fCTimeSinceTrigger_V_QAll->Divide(8,4);
         fCTimeSinceTrigger_V_QAll->ToggleEventStatus();

         int runn = runinfo->fRunNo;

         gDirectory->cd("RootApp:/");
         //fAnalysisDir = new TDirectory("AnalysisHistograms", "Analysis Histograms");
         fAnalysisDir = runinfo->fRoot->fgDir->mkdir("Analysis");

         fChannelDir = fAnalysisDir->mkdir("Channels");


         //Move this to config file
         //-----------------------//
         int nbinQ = 50;
         double minQ = 0.1;
         int maxQ = 50;
         int nbinA = 50;
         double minA = 0.005;
         double maxA = 0.7;

         int nbinBare = nbinQ;
         double minBare = minQ;
         double maxBare = maxQ;

         int nbinBand = nbinQ;
         double minBand = minQ;
         double maxBand = maxQ;

         int nbinLong = nbinQ;
         double minLong = minQ;
         double maxLong = maxQ;

         int nbinsLead = 100;
         double minLead = 1.0;
         double maxLead = 1000.0;

         int nbinsPT= 100;
         double minPT = 1.0;
         double maxPT = 1000.0;
         //-----------------------//


         //cd to the right directory
         fAnalysisDir->cd();

         //Update me -- should the filter channels be normalized by SiPM?
         fhSPEQ_V_A = new TH2D("SPEQ_V_A","Charge vs Amplitude (all channels); Charge (V*ns); Amplitude (V)",nbinQ,minQ,maxQ,nbinA,minA,maxA);
         fhBare_V_Band = new TH2D("Bare_V_Band","Total Event Charge; Bare SiPM Charge (V*ns); Band Pass Charge (V*ns)",nbinBare,minBare,maxBare,nbinBand,minBand,maxBand);
         fhBare_V_Long = new TH2D("Bare_V_Long","Total Event Charge; Bare SiPM Charge (V*ns); Long Pass Charge (V*ns)",nbinBare,minBare,maxBare,nbinLong,minLong,maxLong);
         fhBand_V_Long = new TH2D("Band_V_Long","Total Event Charge; Band Pass Charge (V*ns); Long Pass Charge (V*ns)",nbinBand,minBand,maxBand,nbinLong,minLong,maxLong);

         fhBareLeadingTime = new TH1D("BareLeadingTime", "Leading Pulse Time of Bare Channels; Leading Pulse Time (ns)", nbinsLead, minLead, maxLead);
         fhBPLeadingTime = new TH1D("BPLeadingTime", "Leading Pulse Time of Band Pass Channels; Leading Pulse Time (ns)", nbinsLead, minLead, maxLead);
         fhLPLeadingTime = new TH1D("LPLeadingTime", "Leading Pulse Time of Long Pass Channels; Leading Pulse Time (ns)", nbinsLead, minLead, maxLead);


         fhPulseTimesBare = new TH1D("PulseTimesBare", "Pulse Arrival Times in Bare Channels; Pulse Time (ns)", nbinsPT, minPT, maxPT);
         fhPulseTimesBP = new TH1D("PulseTimesBP", "Pulse Arrival Times in Band Channels; Pulse Time (ns)", nbinsPT, minPT, maxPT);
         fhPulseTimesLP = new TH1D("PulseTimesLP", "Pulse Arrival Times in Long Channels; Pulse Time (ns)", nbinsPT, minPT, maxPT);

         fhPulseTimes_V_A = new TH2D("PulseTimes_V_A","Pulse Times vs Amplitude; Pulse Arrival Time (ns); Pulse Amplitude (V)",nbinsPT,minPT,maxPT,nbinA,minA,maxA);
         fhPulseTimes_V_Q = new TH2D("PulseTimes_V_Q","Pulse Times vs Charge; Pulse Arrival Time (ns); Pulse Charge (V*ns)",nbinsPT,minPT,maxPT,nbinQ,minQ,maxQ);

         fhBand_V_Long = new TH2D("Band_V_Long","Total Event Charge; Band Pass Charge (V*ns); Long Pass Charge (V*ns)",nbinBand,minBand,maxBand,nbinLong,minLong,maxLong);

         fhBarePE = new TH1D("BarePE","Summed bare channel PE;PE;Count",100,0,300);
         fhBPPE = new TH1D("BPPE","Summed BP channel PE;PE;Count",100,0,300);
         fhLPPE = new TH1D("LPPE","Summed LP channel PE;PE;Count",100,0,300);


         fgSPEh = new TGraph(fNumChannels);
         fgSPEh->SetTitle("SPE Heights; Channel Number; SPE Height");
         fgSPEh->SetName("SPE_Heights");
         fgSPEq = new TGraph(fNumChannels);
         fgSPEq->SetTitle("SPE Charges; Channel Number; SPE Charge");
         fgSPEq->SetName("SPE_Charges");

         fAnalysisDir->Append(fgSPEh);
         fAnalysisDir->Append(fgSPEq);

         }
    }//End BeginRun
   void EndRun(TARunInfo* runinfo)
   {

      if( fEnabled )
         {
            for(auto it=fCSPEQ.begin(); it!=fCSPEQ.end(); ++it)
               if(it->second) delete it->second;
            fCSPEQ.clear();

            for(auto it=fCSPEA.begin(); it!=fCSPEA.end(); ++it)
               if(it->second) delete it->second;
            fCSPEA.clear();

            for(auto it=fCTime2NextPulse.begin(); it!=fCTime2NextPulse.end(); ++it)
               if(it->second) delete it->second;
            fhTime2NextPulse.clear();

            for(auto it=fCTimeSinceTrigger.begin(); it!=fCTimeSinceTrigger.end(); ++it)
               if(it->second) delete it->second;
            fhTimeSinceTrigger.clear();

            for(auto it=fCTimeSinceTrigger_V_Q.begin(); it!=fCTimeSinceTrigger_V_Q.end(); ++it)
               if(it->second) delete it->second;
            fCTimeSinceTrigger_V_Q.clear();

            delete fhSPEQ_V_A;
            delete fhBare_V_Band;
            delete fhBare_V_Long;
            delete fhBand_V_Long;
            delete fhBareLeadingTime;
            delete fhBPLeadingTime;
            delete fhLPLeadingTime;
            delete fhPulseTimesBare;
            delete fhPulseTimesBP;
            delete fhPulseTimesLP;

            delete fhPulseTimes_V_A;
            delete fhPulseTimes_V_Q;

            delete fhBarePE;
            delete fhBPPE;
            delete fhLPPE;

            delete fgSPEh;
            delete fgSPEq;
         }


      std::cout<<"LoLXHistogramModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }


   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* runinfo, TAFlags* flags, TAFlowEvent* flow)
   {
      AdcEventFlow* adc_flow = flow->Find<AdcEventFlow>();
      if( !adc_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }
      BaselineEventFlow* corr_flow = flow->Find<BaselineEventFlow>();
      if( !corr_flow )
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      LoLXProcessorFlow* pulse_flow = flow->Find<LoLXProcessorFlow>();
         if( !pulse_flow )
            {
               ++fError;
   #ifdef HAVE_MANALYZER_PROFILER
               *flags|=TAFlag_SKIP_PROFILE;
   #endif
               return flow;
            }
         if(!pulse_flow->event->GetAnalysisFlag())//Analysis module hasn't ran yet
         {
             ++fError;
  #ifdef HAVE_MANALYZER_PROFILER
             *flags|=TAFlag_SKIP_PROFILE;
  #endif
             return flow;
         }


      //ResetHisto();

      //Making plots here (for all channel data)
      UpdateGeneralPlots(pulse_flow);

      //Get the actual number of channels
      fNumChannels = adc_flow->getNchannels();

      for(int ich=0; ich<fNumChannels; ++ich)
         {
            LoLXChannel* lChannel = pulse_flow->event->GetChannel(ich);
            int daq_channel = lChannel->GetChannelID();
            UpdateChannelPlots(pulse_flow, daq_channel);
         }

      if( fFlags->fVerbose ){
        //printf("Ran until here \n");//Print stuff here
      }

      // if this module is enabled, run until here
      // i.e., make waveforms available in JSROOT
      if( !fEnabled )
         {
            ++fCounter;
            return flow;
         }
      // if graphics is available on the host
      // run with -g and select a valid channel

#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif

      if( fFlags->fVerbose ){
        //Print stuff here
      }
      int runNum = runinfo->fRunNo;
      //ShowPlots(runNum);

      // read and plot here histos saved
      // in subdirectories
      if( runinfo->fgFileList.size() )
         runinfo->fRoot->fOutputFile->cd(); // <filename>:/
      else
         gDirectory->cd("RootApp:/");

      if( fSave && (fCounter < fSaveEvents) )
         {
            // TString sname(fC->GetName());
            // sname+="_Event"+std::to_string(fCounter)+".pdf";
            // fC->SaveAs(sname); fC->SaveAs(sname);
            // sname=fCA->GetName();
            // sname+="_Event"+std::to_string(fCounter)+".pdf";
            // fCA->SaveAs(sname); fCA->SaveAs(sname);
         }

      ++fCounter;
      return flow;
   }//End AnalyzeFlowEvent

   //Update the plots for the all-channel data here
   void UpdateGeneralPlots(LoLXProcessorFlow *pFlow)
   {
#ifdef MODULE_MULTITHREAD
        std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif

    if( fFlags->fVerbose ){} //std::cout<<"LoLXHistogramModule::UpdateGeneralPlots"<<std::endl;

    LoLXDS *ev = pFlow->event;

    //Loop over channels
    LoLXChannel *chan = NULL;
    for(int iCh = 0; iCh < ev->GetNChannels(); iCh++){
      chan = ev->GetChannel(iCh);
      int chanType = chan->GetType(); // Bare == 0; Band==1; Long==2

      int np = chan->GetNPulses();
      if(!np){
         if( fFlags->fVerbose ) //std::cout<<"LoLXHistogramModule::UpdateGeneralPlots No Pulses Found"<<std::endl;
         continue; //if no pulses in channel, skip
      }

      if( fFlags->fVerbose ) std::cout<<Form("LoLXHistogramModule::UpdateGeneralPlots %d Pulses Found",np) <<std::endl;
      //Initialize firstPulseTime variables, may need to also place cut and only display the pulses to be AFTER trigger time?
      double firstPulseTimeBare = DBL_MAX;
      double firstPulseTimeBP = DBL_MAX;
      double firstPulseTimeLP = DBL_MAX;

      for(int iP=0; iP < np; iP++){ // Loop over pulses in this channel
         double pulse_a = chan->GetPulseHeight(iP);
         double pulse_q = chan->GetPulseCharge(iP);
         int pulseType = chan->GetPulseType(iP); // 0 for main pulse, 1 for subpulses
         double pulseTime = chan->GetPulseLeadingTime(iP);
         double pulsePeakTime = chan->GetPulsePeakTime(iP);

         if(pulseType ==0){
            fhPulseTimes_V_A->Fill(pulseTime, pulse_a);
            fhPulseTimes_V_Q->Fill(pulseTime, pulse_q);
         }


         if(pulseType==0){ //only care about main pulses here
            if(chanType==0){//Bare==0
               fhPulseTimesBare->Fill(pulseTime);
               if(firstPulseTimeBare > pulseTime) fhBareLeadingTime->Fill(pulseTime); //since pulseType==0 are chronological, this will only trigger once per channel
            }else if(chanType==1){//Bandpass==1
               fhPulseTimesBP->Fill(pulseTime);
               if(firstPulseTimeBP > pulseTime) fhBPLeadingTime->Fill(pulseTime);
            }else {//Longpass==2
               fhPulseTimesLP->Fill(pulseTime);
               if(firstPulseTimeLP > pulseTime) fhLPLeadingTime->Fill(pulseTime);
            }
         }else if(pulseType==1){ //only care about sub pulses here
            if(chanType==0){//Bare==0
               fhPulseTimesBare->Fill(pulsePeakTime);
            }else if(chanType==1){//Bandpass==1
               fhPulseTimesBP->Fill(pulsePeakTime);
            }else {//Longpass==2
               fhPulseTimesLP->Fill(pulsePeakTime);
            }
         }

         //Fill histograms
         int fMaxPulses = 1000; //fix this

         if(np<fMaxPulses){
            fhSPEQ_V_A->Fill(pulse_q,pulse_a);
         }
      }//End loop over pulses

    }//End loop over channels

   //Hockey stick plots
   double tpe = ev->GetTotalPE();
   double barepe = ev->GetTotalBarePE();
   double bppe = ev->GetTotalBPPE();
   double lppe = ev->GetTotalLPPE();

   fhBare_V_Band->Fill(barepe, bppe);
   fhBare_V_Long->Fill(barepe, lppe);
   fhBand_V_Long->Fill(bppe, lppe);

   fhBarePE->Fill(barepe);
   fhBPPE->Fill(bppe);
   fhLPPE->Fill(lppe);
   if(!fhSPEA.empty()){
      for(int i = 0; i<fNumChannels; i++){
         double SPEh = fhSPEA.at(i)->GetBinCenter(fhSPEA.at(i)->GetMaximumBin());
         double SPEq = fhSPEQ.at(i)->GetBinCenter(fhSPEQ.at(i)->GetMaximumBin());
         fgSPEh->SetPoint(i, i, SPEh);
         fgSPEq->SetPoint(i, i, SPEq);
      }
   }

  }//End UpdateGeneralPlots


   void UpdateChannelPlots(LoLXProcessorFlow *pFlow, const int index)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      if( fFlags->fVerbose)
         std::cout<<"LoLXHistogramModule::UpdateChannelPlots"<<std::endl;
      std::string hname;
      std::string htitle;

      //cd to the right directory
      fChannelDir->cd();

      if( !fhSPEQ.count(index) ){
        fhSPEQ[index] = new TH1D(Form("hSPEQ_%02i",index),"Charge Histogram;Charge (ADC*ns);Counts",10*fSPEQ_cutoff,0,fSPEQ_cutoff);
      }
      if( !fhSPEA.count(index) ){
        fhSPEA[index] = new TH1D(Form("hSPEA_%i",index),"Amplitude Histogram;Pulse Height (V);Counts",fNumBinsSPEA,0,fSPEA_cutoff);
      }
      if( !fhTime2NextPulse.count(index) ){
        fhTime2NextPulse[index] = new TH1D(Form("hTime2NextPulse_%i",index),"Time to Next Pulse;Time (ns);Counts",fRawWFmax,0,fRawWFmax*fNanosecPerSample);
      }
      if( !fhTimeSinceTrigger.count(index) ){
        fhTimeSinceTrigger[index] = new TH1D(Form("hTimeSinceTrigger_%i",index),"Time Since Trigger;Time (ns);Counts",fRawWFmax,0,fRawWFmax*fNanosecPerSample);
      }
      if( !fhTimeSinceTrigger_V_Q.count(index) ){
        fhTimeSinceTrigger_V_Q[index] = new TH2D(Form("hTimeSinceTrigger_V_Q_%i",index),"Time Since Trigger vs Charge;Time Since Trigger (ns);Charge (V*ns)",fRawWFmax,0,fRawWFmax*fNanosecPerSample, 10*fSPEQ_cutoff, 0, fSPEQ_cutoff);
      }

      LoLXDS *ev = pFlow->event;
      LoLXChannel *chan = NULL;

      chan = ev->FindChannel(index);

      int np = chan->GetNPulses();
      if(!np) return;

      double TriggerTime = 300.0; //Temporary for test

      for(int iP=0; iP < np; iP++){
         fhSPEA.at(index)->Fill(chan->GetPulseHeight(iP));
         fhSPEA[index]->SetName(Form("hSPEA_%i",index));
         fhSPEA[index]->SetTitle("Pulse Height; Height (V); Counts");

         fhSPEQ.at(index)->Fill(chan->GetPulseCharge(iP));
         if(iP+1 < np) fhTime2NextPulse.at(index)->Fill(chan->GetPulsePeakTime(iP+1) - chan->GetPulsePeakTime(iP));
         fhTimeSinceTrigger.at(index)->Fill(chan->GetPulsePeakTime(iP) - TriggerTime);
         fhTimeSinceTrigger_V_Q.at(index)->Fill(chan->GetPulsePeakTime(iP) - TriggerTime, chan->GetPulseCharge(iP));
      }

   }


   void ShowPlots(int& runn)
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      // fChannelCanvasDir->cd();
      if( fFlags->fVerbose ) std::cout<<"LoLXHistogramModule::Show Plots "<<std::endl;
      for(int i=0;i<fNumChannels;i++){
         // SPEQ histograms first
         if( !fCSPEQ.count(i)){
              std::string cname = Form("Run #%i : SPEQ_Canvas_%i",runn,i);
              fCSPEQ[i] = new TCanvas(cname.c_str(),cname.c_str(),2000,1500);
              fCSPEQ.at(i)->ToggleEventStatus();
         }
         fCSPEQ.at(i)->cd();
         gPad->SetGrid();
         fhSPEQ.at(i)->Draw("hist");

         fCSPEQ.at(i)->Modified();
         fCSPEQ.at(i)->Draw();
         fCSPEQ.at(i)->Update();

         fCSPEQAll->cd();
         gPad->SetGrid();
         fhSPEQ.at(i)->Draw("hist");

         fCSPEQAll->Modified();
         fCSPEQAll->Draw();
         fCSPEQAll->Update();

         // SPEA Histograms
         if( !fCSPEA.count(i)){
               std::string cname = Form("Run #%i : SPEA_Canvas_%i",runn,i);
               fCSPEA[i] = new TCanvas(cname.c_str(),cname.c_str(),2000,1500);
               fCSPEA.at(i)->ToggleEventStatus();
         }

         fCSPEA.at(i)->cd();
         gPad->SetGrid();
         fhSPEA.at(i)->Draw("hist");
         //fhWFCorr.at(i)->GetYaxis()->SetRangeUser(fhWFCorr.at(i)->GetMinimum()*1.2,pulse_max);
         //fhPulses.at(i)->Draw("histsame"); //do we ever want to plot onto the same hist? Maybe

         fCSPEA.at(i)->Modified();
         fCSPEA.at(i)->Draw();
         fCSPEA.at(i)->Update();

         fCSPEAAll->cd();
         gPad->SetGrid();
         fhSPEA.at(i)->Draw("hist");

         fCSPEAAll->Modified();
         fCSPEAAll->Draw();
         fCSPEAAll->Update();



         if( !fCTime2NextPulse.count(i)){
               std::string cname = Form("Run #%i : Time2NextPulse_Canvas_%i",runn,i);
               fCTime2NextPulse[i] = new TCanvas(cname.c_str(),cname.c_str(),2000,1500);
               fCTime2NextPulse.at(i)->ToggleEventStatus();
         }

         fCTime2NextPulse.at(i)->cd();
         gPad->SetGrid();
         fhTime2NextPulse.at(i)->Draw("hist");

         fCTime2NextPulse.at(i)->Modified();
         fCTime2NextPulse.at(i)->Draw();
         fCTime2NextPulse.at(i)->Update();

         fCTime2NextPulseAll->cd();
         fCTime2NextPulseAll->cd(i+1);
         gPad->SetGrid();
         fhTime2NextPulse.at(i)->Draw("hist");

         fCTime2NextPulseAll->Modified();
         fCTime2NextPulseAll->Draw();
         fCTime2NextPulseAll->Update();



         if( !fCTimeSinceTrigger.count(i)){
               std::string cname = Form("Run #%i : TimeSinceTrigger_Canvas_%i",runn,i);
               fCTimeSinceTrigger[i] = new TCanvas(cname.c_str(),cname.c_str(),2000,1500);
               fCTimeSinceTrigger.at(i)->ToggleEventStatus();
         }

         fCTimeSinceTrigger.at(i)->cd();
         gPad->SetGrid();
         fhTimeSinceTrigger.at(i)->Draw("hist");

         fCTimeSinceTrigger.at(i)->Modified();
         fCTimeSinceTrigger.at(i)->Draw();
         fCTimeSinceTrigger.at(i)->Update();

         fCTimeSinceTriggerAll->cd();
         fCTimeSinceTriggerAll->cd(i+1);
         gPad->SetGrid();
         fhTimeSinceTrigger.at(i)->Draw("hist");

         fCTimeSinceTriggerAll->Modified();
         fCTimeSinceTriggerAll->Draw();
         fCTimeSinceTriggerAll->Update();

         //std::cout << "Here 6"<<std::endl;


         if( !fCTimeSinceTrigger_V_Q.count(i)){
               std::string cname = Form("Run #%i : TimeSinceTrigger_V_Q_Canvas_%i",runn,i);
               fCTimeSinceTrigger_V_Q[i] = new TCanvas(cname.c_str(),cname.c_str(),2000,1500);
               fCTimeSinceTrigger_V_Q.at(i)->ToggleEventStatus();
         }

         fCTimeSinceTrigger_V_Q.at(i)->cd();
         gPad->SetGrid();
         fhTimeSinceTrigger_V_Q.at(i)->Draw("colz");

         fCTimeSinceTrigger_V_Q.at(i)->Modified();
         fCTimeSinceTrigger_V_Q.at(i)->Draw();
         fCTimeSinceTrigger_V_Q.at(i)->Update();

         fCTimeSinceTrigger_V_QAll->cd();
         fCTimeSinceTrigger_V_QAll->cd(i+1);
         gPad->SetGrid();
         fhTimeSinceTrigger_V_Q.at(i)->Draw("colz");

         fCTimeSinceTrigger_V_QAll->Modified();
         fCTimeSinceTrigger_V_QAll->Draw();
         fCTimeSinceTrigger_V_QAll->Update();


      }



      //fhPulseRate->Draw("hist"); // need to add this in

      fhSPEQ_V_A->Draw("colz");
      fhBare_V_Band->Draw("colz");
      fhBare_V_Long->Draw("colz");
      fhBand_V_Long->Draw("colz");

      fhBareLeadingTime->Draw("hist");
      fhBPLeadingTime->Draw("hist");
      fhLPLeadingTime->Draw("hist");

      fhPulseTimesBare->Draw("hist");
      fhPulseTimesBP->Draw("hist");
      fhPulseTimesLP->Draw("hist");

      fhPulseTimes_V_A->Draw("colz");
      fhPulseTimes_V_Q->Draw("colz");



   }


   void ResetHisto()
   {
#ifdef MODULE_MULTITHREAD
      std::lock_guard<std::mutex> lock(TAMultithreadHelper::gfLock);
#endif
      if(fFlags->fVerbose) std::cout<<"Resetting Event: "<<fCounter<<std::endl;
      for(auto it=fhSPEQ.begin();it!=fhSPEQ.end();++it)
         if(it->second) it->second->Reset();
      for(auto it=fhSPEA.begin();it!=fhSPEA.end();++it)
         if(it->second) it->second->Reset();
      for(auto it=fhTime2NextPulse.begin();it!=fhTime2NextPulse.end();++it)
         if(it->second) it->second->Reset();
      for(auto it=fhTimeSinceTrigger.begin();it!=fhTimeSinceTrigger.end();++it)
         if(it->second) it->second->Reset();
      for(auto it=fhTimeSinceTrigger_V_Q.begin();it!=fhTimeSinceTrigger_V_Q.end();++it)
         if(it->second) it->second->Reset();
   }
};


class LoLXHistogramModuleFactory: public TAFactory
{
public:
   LoLXHistogramFlags fFlags;

public:
   void Init(const std::vector<std::string> &args)
   {
      printf("LoLXHistogramModuleFactory::Init!\n");

      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
   }

   void Finish()
   {
      printf("LoLXHistogramModuleFactory::Finish!\n");
   }

   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("LoLXHistogramModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new LoLXHistogramModule(runinfo, &fFlags);
   }

};

static TARegister tar(new LoLXHistogramModuleFactory);
